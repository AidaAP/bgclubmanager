package com.bgclubmanager.table

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.reserve.ReservedTable
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class TableServiceSpec extends Specification {

    def tableService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "table cannot be created without logged-in user"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = tableService.create(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.create.error.notAllowed'
        !Table.findAllByClub(table.club).any { it == table }

        when: "logged-in user"
        table.clearErrors()
        springSecurityService.reauthenticate(table.club.owner.username)
        result = tableService.create(table)

        then:
        !result.hasErrors()
        Table.findAllByClub(table.club).any { it == table }
    }

    void "table is mandatory to create table"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "create table without table"
        def result = tableService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.create.error.noTable'
        !Table.findAllByClub(table.club).any { it == table }

        when: "create table with table"
        result = tableService.create(table)

        then:
        !result.hasErrors()
        Table.findAllByClub(table.club).any { it == table }
    }

    void "table must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def table = new Table(name: 'testTableName', description: 'testTableDescription', maxCapacity: 10, latitude:
                10, longitude: 10)

        when: "create table without club"
        def result = tableService.create(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.create.error.noClub'
        !Table.findAllByClub(club).any { it == table }

        when: "create table with club"
        table.club = club
        table.clearErrors()
        result = tableService.create(table)

        then:
        !result.hasErrors()
        Table.findAllByClub(table.club).any { it == table }
    }

    void "table can only be created by a club admin"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)

        when: "create table by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(table.club, Member.Type.BASIC).user.username)
        def result = tableService.create(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.create.error.notAllowed'
        !Table.findAllByClub(table.club).any { it == table }

        when: "create table by club admin"
        springSecurityService.reauthenticate(table.club.owner.username)
        table.clearErrors()
        result = tableService.create(table)

        then:
        !result.hasErrors()
        Table.findAllByClub(table.club).any { it == table }
    }

    void "tables are created as specified"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "create table"
        def result = tableService.create(table)

        then:
        !result.hasErrors()
        result.id
        result.club == table.club
        result.name == table.name
        result.description == table.description
        result.maxCapacity == table.maxCapacity
        result.latitude == table.latitude
        result.longitude == table.longitude
        Table.findAllByClub(table.club).any { it == table }
    }

    void "table cannot be updated without logged-in user"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        table.name = "${table.name}Modif"
        table.description = "${table.description}Modif"
        table.maxCapacity = table.maxCapacity + 1
        table.latitude = table.latitude + 1
        table.longitude = table.longitude + 1

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = tableService.update(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.update.error.notAllowed'

        when: "logged-in user"
        table.clearErrors()
        springSecurityService.reauthenticate(table.club.owner.username)
        result = tableService.update(table)

        then:
        !result.hasErrors()
        Table.findByName(table.name)
    }

    void "table is mandatory to update table"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        table.name = "${table.name}Modif"
        table.description = "${table.description}Modif"
        table.maxCapacity = table.maxCapacity + 1
        table.latitude = table.latitude + 1
        table.longitude = table.longitude + 1
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "update table without table"
        def result = tableService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.update.error.noTable'

        when: "update table with table"
        result = tableService.update(table)

        then:
        !result.hasErrors()
        Table.findByName(table.name)
    }

    void "table must exist to be updated"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)

        when: "update non-existent table"
        def result = tableService.update(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.update.error.notFoundTable'

        when: "update existent table"
        table.save()
        table.name = "${table.name}Modif"
        table.description = "${table.description}Modif"
        table.maxCapacity = table.maxCapacity + 1
        table.latitude = table.latitude + 1
        table.longitude = table.longitude + 1
        table.clearErrors()
        result = tableService.update(table)

        then:
        !result.hasErrors()
        Table.findByName(table.name)
    }

    void "table can only be updated by a club admin"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        table.name = "${table.name}Modif"
        table.description = "${table.description}Modif"
        table.maxCapacity = table.maxCapacity + 1
        table.latitude = table.latitude + 1
        table.longitude = table.longitude + 1

        when: "update table by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(table.club, Member.Type.BASIC).user.username)
        def result = tableService.update(table)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.update.error.notAllowed'

        when: "update table by club admin"
        springSecurityService.reauthenticate(table.club.owner.username)
        table.clearErrors()
        result = tableService.update(table)

        then:
        !result.hasErrors()
        Table.findByName(table.name)
    }

    void "tables are updated as specified"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        table.name = "${table.name}Modif"
        table.description = "${table.description}Modif"
        table.maxCapacity = table.maxCapacity + 1
        table.latitude = table.latitude + 1
        table.longitude = table.longitude + 1
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "update table"
        def result = tableService.update(table)

        then:
        !result.hasErrors()
        result.id
        result.club == table.club
        result.name == table.name
        result.description == table.description
        result.maxCapacity == table.maxCapacity
        result.latitude == table.latitude
        result.longitude == table.longitude
        Table.findByName(table.name)
    }

    void "table must exist to be deleted"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "delete non-existent table"
        def result = tableService.delete('non-existent-table-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent table"
        result = tableService.delete(table.id)

        then:
        !result.hasErrors()
        !Table.exists(table.id)
    }

    void "a table can only be deleted by a club admin"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = tableService.delete(table.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.delete.error.notAllowed'

        when: "logged-in no club member"
        table.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = tableService.delete(table.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.delete.error.notAllowed'

        when: "logged-in club member"
        table.clearErrors()
        (new Member(club: table.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = tableService.delete(table.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'table.delete.error.notAllowed'

        when: "logged-in club admin"
        table.clearErrors()
        springSecurityService.reauthenticate(table.club.owner.username)
        result = tableService.delete(table.id)

        then:
        !result.hasErrors()
        !Table.exists(table.id)
    }

    void "table deletion deletes all data related to the table"() {

        given:
        def table = new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity:
                10, latitude: 10, longitude: 10)
        table.save()
        // Table reserves
        def reserve1 = new Reserve(user: club.owner, club: club, title: 'testReserve1', startDate: new Date() + 1,
                endDate: new Date() + 2)
        reserve1.reservedGames = []
        reserve1.addToReservedTables(new ReservedTable(table: table))
        reserve1.save(flush: true)
        def reserve2 = new Reserve(user: club.owner, club: club, title: 'testReserve2', startDate: new Date() + 3,
                endDate: new Date() + 4)
        reserve2.reservedGames = []
        reserve2.addToReservedTables(new ReservedTable(table: (new Table(club: club, name: 'testOtherTableName',
                description: 'testOtherTableDescription', maxCapacity: 10, latitude: 10, longitude: 10)).save()))
        reserve2.save(flush: true)
        springSecurityService.reauthenticate(table.club.owner.username)

        when: "delete table"
        def result = tableService.delete(table.id)

        then:
        !result.hasErrors()
        !Table.exists(table.id)
        !Reserve.withTable(table).list()
        !Reserve.exists(reserve1.id)
        Reserve.exists(reserve2.id)
    }
}
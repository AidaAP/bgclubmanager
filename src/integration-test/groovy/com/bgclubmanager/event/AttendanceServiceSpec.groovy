package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class AttendanceServiceSpec extends Specification {

    def attendanceService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club1
    def club2
    def event

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club1 = (new Club(owner: adminUser, name: 'testClubName1')).save(flush: true)
        (new Member(club: club1, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()

        club2 = (new Club(owner: basicUser, name: 'testClubName2')).save(flush: true)
        (new Member(club: club2, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()

        event = new Event(club: club1, name: 'testName', description: 'testDescription', startDate: new Date() + 30,
                endDate: new Date() + 31, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date() - 15, endDateEnrollment: new Date() + 20, selfEnrollment: false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
    }

    def cleanup() {
        Organization.findAllByEvent(event)*.delete()
        event.delete()
        Member.findAllByClub(club2)*.delete()
        club2.delete()
        Member.findAllByClub(club1)*.delete()
        club1.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "attendance cannot be requested without logged-in user"() {

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = attendanceService.requestAttendance(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.notAllowed'

        when: "logged-in user"
        springSecurityService.reauthenticate(event.club.owner.username)
        def dateBefore = new Date()
        result = attendanceService.requestAttendance(event)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        (event.selfEnrollment && Attendance.findByEventAndUser(event, event.club.owner)) ||
                (!event.selfEnrollment && AttendanceRequest.findByEventAndUserAndDateBetween(event, event.club.owner, dateBefore, dateAfter))
    }

    void "event is mandatory to request attendance"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request attendance for no event"
        def result = attendanceService.requestAttendance(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.noEvent'

        when: "request attendance for event"
        def dateBefore = new Date()
        result = attendanceService.requestAttendance(event)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        (event.selfEnrollment && Attendance.findByEventAndUser(event, event.club.owner)) ||
                (!event.selfEnrollment && AttendanceRequest.findByEventAndUserAndDateBetween(event, event.club.owner, dateBefore, dateAfter))
    }

    void "user must be a member of an event organizer or the event must be public to request attendance"() {

        when: "request attendance with no member and no public event"
        springSecurityService.reauthenticate(club2.owner.username)
        event.isPublic = false
        event.save(flush: true)
        def result = attendanceService.requestAttendance(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.notAllowed'

        when: "request attendance with member and no public event"
        springSecurityService.reauthenticate(event.club.owner.username)
        def dateBefore = new Date()
        result = attendanceService.requestAttendance(event)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        (event.selfEnrollment && Attendance.findByEventAndUser(event, event.club.owner)) ||
                (!event.selfEnrollment && AttendanceRequest.findByEventAndUserAndDateBetween(event, event.club.owner, dateBefore, dateAfter))

        when: "request attendance with no member and public event"
        springSecurityService.reauthenticate(club2.owner.username)
        event.isPublic = true
        event.save(flush: true)
        dateBefore = new Date()
        result = attendanceService.requestAttendance(event)
        dateAfter = new Date()

        then:
        !result.hasErrors()
        (event.selfEnrollment && Attendance.findByEventAndUser(event, club2.owner)) ||
                (!event.selfEnrollment && AttendanceRequest.findByEventAndUserAndDateBetween(event, club2.owner, dateBefore, dateAfter))
    }

    void "attendance cannot be requested out of date"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request attendance out of date"
        event.startDateEnrollment = (new Date()) + 1
        event.endDateEnrollment = (new Date()) + 2
        event.save(flush: true)
        def result = attendanceService.requestAttendance(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.notEnrollable'
        AttendanceRequest.countByEventAndUser(event, event.club.owner) == 0
        Attendance.countByEventAndUser(event, event.club.owner) == 0

        when: "request attendance in date"
        event.startDateEnrollment = (new Date()) - 1
        event.endDateEnrollment = (new Date()) + 1
        event.save(flush: true)
        def dateBefore = new Date()
        result = attendanceService.requestAttendance(event)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        (event.selfEnrollment && Attendance.findByEventAndUser(event, event.club.owner)) ||
                (!event.selfEnrollment && AttendanceRequest.findByEventAndUserAndDateBetween(event, event.club.owner, dateBefore, dateAfter))
    }

    void "attendance cannot be requested with a current attendance request"() {

        given:
        // Non-self-enrollment event
        event.selfEnrollment = false
        event.save(flush: true)
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request attendance"
        def dateBefore = new Date()
        def result = attendanceService.requestAttendance(event)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        AttendanceRequest.findByEventAndUserAndDateBetween(event, event.club.owner, dateBefore, dateAfter)

        when: "request attendance with current attendance request"
        result = attendanceService.requestAttendance(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.alreadyRequested'
        AttendanceRequest.countByEventAndUser(event, event.club.owner) == 1
    }

    void "attendance cannot be requested with for an event attendee"() {

        given:
        // Self-enrollment event
        event.selfEnrollment = true
        event.save(flush: true)
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request attendance"
        def result = attendanceService.requestAttendance(event)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(event, event.club.owner)
        Attendance.findByEventAndUser(event, event.club.owner)

        when: "request attendance for an event attendee"
        result = attendanceService.requestAttendance(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.request.error.alreadyAttendee'
        !AttendanceRequest.findByEventAndUser(event, event.club.owner)
        Attendance.countByEventAndUser(event, event.club.owner) == 1
    }

    void "when event has reached max attendees, attendance is not accepted automatically"() {

        given:
        // Self-enrollment event with max attendees reached
        event.selfEnrollment = true
        event.maxAttendees = 1
        event.save(flush: true)
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()

        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request first attendance"
        def result = attendanceService.requestAttendance(event)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(event, event.club.owner)
        Attendance.findByEventAndUser(event, event.club.owner)

        when: "request second attendance"
        springSecurityService.reauthenticate(user.username)
        result = attendanceService.requestAttendance(event)

        then:
        !result.hasErrors()
        AttendanceRequest.findByEventAndUser(event, user)
        !Attendance.findByEventAndUser(event, user)
    }

    void "attendance request cannot be accepted without logged-in user"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = attendanceService.acceptAttendanceRequest(attendanceRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.acceptRequest.error.notAllowed'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)
        result = attendanceService.acceptAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "attendance request is mandatory to accept attendance request"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)

        when: "accept attendance request with no attendance request"
        def result = attendanceService.acceptAttendanceRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.acceptRequest.error.noRequest'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "accept attendance request with attendance request"
        result.clearErrors()
        result = attendanceService.acceptAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "users must be admins of the event club to accept attendance requests"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()

        when: "accept attendance request as non-admin of event club"
        def organization = new Organization(event: event, club: club2)
        organization.save()
        springSecurityService.reauthenticate(organization.club.owner.username)
        def result = attendanceService.acceptAttendanceRequest(attendanceRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.acceptRequest.error.notAllowed'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "accept attendance request as admin of event club"
        result.clearErrors()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)
        result = attendanceService.acceptAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "cannot accept attendance requests when max attendees is reached"() {

        given:
        event.maxAttendees = 1
        event.save()
        def user1 = new User(username: 'testUser1', password: 'testUser1', email: 'testUser1@bgclubmanager.es',
                language: language)
        user1.save()
        (new UserRole(user: user1, role: basicRole)).save()
        (new Member(club: event.club, user: user1, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest1 = new AttendanceRequest(event: event, user: user1, date: new Date())
        attendanceRequest1.save()
        def user2 = new User(username: 'testUser2', password: 'testUser2', email: 'testUser2@bgclubmanager.es',
                language: language)
        user2.save()
        (new UserRole(user: user2, role: basicRole)).save()
        (new Member(club: event.club, user: user2, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest2 = new AttendanceRequest(event: event, user: user2, date: new Date())
        attendanceRequest2.save()
        springSecurityService.reauthenticate(attendanceRequest1.event.club.owner.username)

        when: "accept attendance request when max attendees is not reached"
        def result = attendanceService.acceptAttendanceRequest(attendanceRequest1)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest1.event, attendanceRequest1.user)
        Attendance.findByEventAndUser(attendanceRequest1.event, attendanceRequest1.user)

        when: "accept attendance request when max attendees is reached"
        result = attendanceService.acceptAttendanceRequest(attendanceRequest2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.acceptRequest.error.maxAttendees'
        AttendanceRequest.findByEventAndUser(attendanceRequest2.event, attendanceRequest2.user)
        !Attendance.findByEventAndUser(attendanceRequest2.event, attendanceRequest2.user)
    }

    void "when accepting multiple attendance requests, requests are treated separately"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def validRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        validRequest.save()
        def invalidRequest = new AttendanceRequest(event: event, user: null, date: new Date())
        invalidRequest.save()
        springSecurityService.reauthenticate(validRequest.event.club.owner.username)
        AttendanceRequest[] requests = [validRequest, invalidRequest]

        when: "accept multiple attendance requests"
        def result = attendanceService.acceptAttendanceRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "attendance request cannot be rejected without logged-in user"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = attendanceService.rejectAttendanceRequest(attendanceRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.rejectRequest.error.notAllowed'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)
        result = attendanceService.rejectAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "attendance request is mandatory to reject attendance request"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)

        when: "reject attendance request with no attendance request"
        def result = attendanceService.rejectAttendanceRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.rejectRequest.error.noRequest'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "reject attendance request with attendance request"
        result.clearErrors()
        result = attendanceService.rejectAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "users must be admins of the event club to reject attendance requests"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        attendanceRequest.save()

        when: "reject attendance request as non-admin of event club"
        def organization = new Organization(event: event, club: club2)
        organization.save()
        springSecurityService.reauthenticate(organization.club.owner.username)
        def result = attendanceService.rejectAttendanceRequest(attendanceRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.rejectRequest.error.notAllowed'
        AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)

        when: "reject attendance request as admin of event club"
        result.clearErrors()
        springSecurityService.reauthenticate(attendanceRequest.event.club.owner.username)
        result = attendanceService.rejectAttendanceRequest(attendanceRequest)

        then:
        !result.hasErrors()
        !AttendanceRequest.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
        !Attendance.findByEventAndUser(attendanceRequest.event, attendanceRequest.user)
    }

    void "when rejecting multiple attendance requests, requests are treated separately"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def validRequest = new AttendanceRequest(event: event, user: user, date: new Date())
        validRequest.save()
        def invalidRequest = new AttendanceRequest(event: event, user: null, date: new Date())
        invalidRequest.save()
        springSecurityService.reauthenticate(validRequest.event.club.owner.username)
        AttendanceRequest[] requests = [validRequest, invalidRequest]

        when: "reject multiple attendance requests"
        def result = attendanceService.rejectAttendanceRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "attendance cannot be cancelled without logged-in user"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user)
        attendance.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = attendanceService.cancelAttendance(attendance)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.cancel.error.notAllowed'
        Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "logged-in user"
        springSecurityService.reauthenticate(attendance.user.username)
        result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)
    }

    void "attendance is mandatory to cancel attendance"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user)
        attendance.save()
        springSecurityService.reauthenticate(attendance.user.username)

        when: "cancel no attendance"
        def result = attendanceService.cancelAttendance(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.cancel.error.noAttendance'
        Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "cancel attendance"
        result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)
    }

    void "user must be admin of the event club or the attendee to cancel attendance"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user)
        attendance.save()

        when: "cancel attendance as attendee"
        springSecurityService.reauthenticate(attendance.user.username)
        def result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "cancel attendance as admin"
        attendance = new Attendance(event: event, user: user)
        attendance.save()
        springSecurityService.reauthenticate(attendance.event.club.owner.username)
        result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "cancel attendance as non-attendee and non-admin"
        attendance = new Attendance(event: event, user: user)
        attendance.save()
        def user2 = new User(username: 'testUser2', password: 'testUser2', email: 'testUser2@bgclubmanager.es',
                language: language)
        user2.save()
        (new UserRole(user: user2, role: basicRole)).save()
        springSecurityService.reauthenticate(user2.username)
        result = attendanceService.cancelAttendance(attendance)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.cancel.error.notAllowed'
        Attendance.findByEventAndUser(attendance.event, attendance.user)
    }

    void "event must be enrollable to cancel attendance"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user)
        attendance.save()
        springSecurityService.reauthenticate(attendance.user.username)

        when: "non-enrollable event"
        attendance.event.startDateEnrollment = new Date() - 10
        attendance.event.endDateEnrollment = new Date() - 5
        attendance.event.save()
        def result = attendanceService.cancelAttendance(attendance)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.cancel.error.notEnrollable'
        Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "enrollable event"
        attendance.event.startDateEnrollment = new Date() - 10
        attendance.event.endDateEnrollment = new Date() + 10
        attendance.event.save()
        attendance.clearErrors()
        result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)
    }

    void "user must be an event attendee to cancel attendance"() {

        given:
        def user = new User(username: 'testUser', password: 'testUser', email: 'testUser@bgclubmanager.es', language:
                language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user)
        springSecurityService.reauthenticate(attendance.user.username)

        when: "non-attendee user"
        def result = attendanceService.cancelAttendance(attendance)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'attendance.cancel.error.notAttendee'
        !Attendance.findByEventAndUser(attendance.event, attendance.user)

        when: "attendee user"
        attendance.clearErrors()
        attendance.save()
        result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)
    }

    void "pending requests are accepted on attendance cancellation"() {

        given:
        event.maxAttendees = 1
        event.selfEnrollment = true
        event.save()
        def user1 = new User(username: 'testUser1', password: 'testUser1', email: 'testUser1@bgclubmanager.es',
                language: language)
        user1.save()
        (new UserRole(user: user1, role: basicRole)).save()
        (new Member(club: event.club, user: user1, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendance = new Attendance(event: event, user: user1)
        attendance.save()
        def user2 = new User(username: 'testUser2', password: 'testUser2', email: 'testUser2@bgclubmanager.es',
                language: language)
        user2.save()
        (new UserRole(user: user2, role: basicRole)).save()
        (new Member(club: event.club, user: user2, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest1 = new AttendanceRequest(event: event, user: user2, date: new Date() - 5)
        attendanceRequest1.save()
        def user3 = new User(username: 'testUser3', password: 'testUser3', email: 'testUser3@bgclubmanager.es',
                language: language)
        user3.save()
        (new UserRole(user: user3, role: basicRole)).save()
        (new Member(club: event.club, user: user3, type: Member.Type.BASIC, startDate: new Date())).save()
        def attendanceRequest2 = new AttendanceRequest(event: event, user: user3, date: new Date())
        attendanceRequest2.save()
        springSecurityService.reauthenticate(attendance.user.username)

        when: "cancel attendance"
        def result = attendanceService.cancelAttendance(attendance)

        then:
        !result.hasErrors()
        !Attendance.findByEventAndUser(attendance.event, attendance.user)
        Attendance.findByEventAndUser(attendanceRequest1.event, attendanceRequest1.user)
        !Attendance.findByEventAndUser(attendanceRequest2.event, attendanceRequest2.user)
    }
}
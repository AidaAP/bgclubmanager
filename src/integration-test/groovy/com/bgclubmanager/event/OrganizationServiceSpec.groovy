package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class OrganizationServiceSpec extends Specification {

    def organizationService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club1
    def club2
    def event

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club1 = (new Club(owner: adminUser, name: 'testClubName1')).save(flush: true)
        (new Member(club: club1, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()

        club2 = (new Club(owner: basicUser, name: 'testClubName2')).save(flush: true)
        (new Member(club: club2, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()

        event = new Event(club: club1, name: 'testName', description: 'testDescription', startDate: new Date() + 30,
                endDate: new Date() + 31, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date() + 15, endDateEnrollment: new Date() + 20, selfEnrollment: false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
    }

    def cleanup() {
        Organization.findAllByEvent(event)*.delete()
        event.delete()
        Member.findAllByClub(club2)*.delete()
        club2.delete()
        Member.findAllByClub(club1)*.delete()
        club1.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "event organizers cannot be set without logged-in user"() {

        given:
        def club3 = (new Club(owner: basicUser, name: 'testClubName3')).save()
        (new Member(club: club3, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Organization(event: event, club: club3)).save()
        Club[] organizers = [club1, club2]

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = organizationService.setEventOrganizers(event, organizers)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.setEventOrganizers.error.notAllowed'

        when: "logged-in user"
        event.clearErrors()
        springSecurityService.reauthenticate(event.club.owner.username)
        result = organizationService.setEventOrganizers(event, organizers)

        then:
        !result.hasErrors()
        organizers.every { org ->
            event.getOrganizers().any { it.id == org.id } || OrganizationRequest.findByEventAndClub(event, org)
        }
    }

    void "event is mandatory to set event organizers"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)
        def club3 = (new Club(owner: basicUser, name: 'testClubName3')).save()
        (new Member(club: club3, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Organization(event: event, club: club3)).save()
        Club[] organizers = [club1, club2]

        when: "set event organizers for no event"
        def result = organizationService.setEventOrganizers(null, organizers)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.setEventOrganizers.error.noEvent'

        when: "set organizers for event"
        result = organizationService.setEventOrganizers(event, organizers)

        then:
        !result.hasErrors()
        organizers.every { org ->
            event.getOrganizers().any { it.id == org.id } || OrganizationRequest.findByEventAndClub(event, org)
        }
    }

    void "only admins of the owner club can set event organizers"() {

        given:
        def club3 = (new Club(owner: basicUser, name: 'testClubName3')).save()
        (new Member(club: club3, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Organization(event: event, club: club3)).save()
        Club[] organizers = [club1, club2]

        when: "set event organizers with no admin"
        springSecurityService.reauthenticate(basicUser.username)
        def result = organizationService.setEventOrganizers(event, organizers)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.setEventOrganizers.error.notAllowed'

        when: "set event organizers with admin"
        springSecurityService.reauthenticate(event.club.owner.username)
        event.clearErrors()
        result = organizationService.setEventOrganizers(event, organizers)

        then:
        !result.hasErrors()
        organizers.every { org ->
            event.getOrganizers().any { it.id == org.id } || OrganizationRequest.findByEventAndClub(event, org)
        }
    }

    void "event owner must be an event organizer"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)
        def club3 = (new Club(owner: basicUser, name: 'testClubName3')).save()
        (new Member(club: club3, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Organization(event: event, club: club3)).save()
        Club[] organizers = [club2]

        when: "set event organizers without owner"
        def result = organizationService.setEventOrganizers(event, organizers)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.setEventOrganizers.error.noOwner'

        when: "set event organizers with owner"
        event.clearErrors()
        organizers = [club1, club2]
        result = organizationService.setEventOrganizers(event, organizers)

        then:
        !result.hasErrors()
        organizers.every { org ->
            event.getOrganizers().any { it.id == org.id } || OrganizationRequest.findByEventAndClub(event, org)
        }
    }

    void "organization cannot be requested without logged-in user"() {

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = organizationService.requestOrganization(event, club2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.request.error.notAllowed'
        !OrganizationRequest.findByEventAndClub(event, club2)

        when: "logged-in user"
        springSecurityService.reauthenticate(event.club.owner.username)
        def dateBefore = new Date()
        result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)
    }

    void "event is mandatory to request organization"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request organization for no event"
        def result = organizationService.requestOrganization(null, club2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.request.error.noEvent'
        !OrganizationRequest.findByEventAndClub(null, club2)
        !OrganizationRequest.findByEventAndClub(event, club2)

        when: "request organization for event"
        def dateBefore = new Date()
        result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)
    }

    void "club is mandatory to request organization"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request organization for no club"
        def result = organizationService.requestOrganization(event, null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.request.error.noClub'
        !OrganizationRequest.findByEventAndClub(event, null)
        !OrganizationRequest.findByEventAndClub(event, club2)

        when: "request organization for club"
        def dateBefore = new Date()
        result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)
    }

    void "event organization cannot be requested more than once"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request organization for event"
        def dateBefore = new Date()
        def result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)

        when: "request organization again"
        result = organizationService.requestOrganization(event, club2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.request.error.alreadyRequested'
        OrganizationRequest.countByEventAndClub(event, club2) == 1
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)
    }

    void "event organization cannot be requested for organizers"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request organization for organizer"
        def result = organizationService.requestOrganization(event, event.club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.request.error.alreadyOrganizer'
        OrganizationRequest.countByEventAndClub(event, event.club) == 0

        when: "request organization for non-organizer"
        def dateBefore = new Date()
        result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        OrganizationRequest.countByEventAndClub(event, club2) == 1
        OrganizationRequest.findByEventAndClubAndDateBetween(event, club2, dateBefore, dateAfter)
    }

    void "organization requests are created correctly"() {

        given:
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "request organization for event"
        def dateBefore = new Date()
        def result = organizationService.requestOrganization(event, club2)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        result.id != null
        result.event.id == event.id
        result.club.id == club2.id
        result.date != null
        result.date >= dateBefore
        result.date <= dateAfter
    }

    void "organization request cannot be accepted without logged-in user"() {

        given:
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = organizationService.acceptOrganizationRequest(organizationRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.acceptRequest.error.notAllowed'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)
        result = organizationService.acceptOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        Organization.findByEventAndClub(event, club2)
    }

    void "organization request is mandatory to accept organization request"() {

        given:
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)

        when: "accept organization request with no organization request"
        def result = organizationService.acceptOrganizationRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.acceptRequest.error.noRequest'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "accept organization request with organization request"
        result.clearErrors()
        result = organizationService.acceptOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        Organization.findByEventAndClub(event, club2)
    }

    void "users must be admins of the club to accept organization requests"() {

        given:
        def noAdminUser = new User(username: 'noAdminUser', password: 'noAdminUser', email:
                'noAdminUser@bgclubmanager.es', language: language)
        noAdminUser.save()
        (new UserRole(user: noAdminUser, role: adminRole)).save()
        (new Member(club: club2, user: noAdminUser, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        springSecurityService.reauthenticate(noAdminUser.username)
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()

        when: "accept organization request for non-admin club"
        def result = organizationService.acceptOrganizationRequest(organizationRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.acceptRequest.error.notAllowed'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "accept organization request for admin club"
        result.clearErrors()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)
        result = organizationService.acceptOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        Organization.findByEventAndClub(event, club2)
    }

    void "when accepting multiple organization requests, requests are treated separately"() {

        given:
        def validRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        validRequest.save()
        def invalidRequest = new OrganizationRequest(event: event, club: null, date: new Date())
        invalidRequest.save()
        springSecurityService.reauthenticate(validRequest.club.owner.username)
        OrganizationRequest[] requests = [validRequest, invalidRequest]

        when: "accept multiple organization requests"
        def result = organizationService.acceptOrganizationRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "organization request cannot be rejected without logged-in user"() {

        given:
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = organizationService.rejectOrganizationRequest(organizationRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.rejectRequest.error.notAllowed'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)
        result = organizationService.rejectOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)
    }

    void "organization request is mandatory to reject organization request"() {

        given:
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)

        when: "reject organization request with no organization request"
        def result = organizationService.rejectOrganizationRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.rejectRequest.error.noRequest'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "reject organization request with organization request"
        result.clearErrors()
        result = organizationService.rejectOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)
    }

    void "users must be admins of the club to reject organization requests"() {

        given:
        def noAdminUser = new User(username: 'noAdminUser', password: 'noAdminUser', email:
                'noAdminUser@bgclubmanager.es', language: language)
        noAdminUser.save()
        (new UserRole(user: noAdminUser, role: adminRole)).save()
        (new Member(club: club2, user: noAdminUser, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        springSecurityService.reauthenticate(noAdminUser.username)
        def organizationRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        organizationRequest.save()

        when: "reject organization request for non-admin club"
        def result = organizationService.rejectOrganizationRequest(organizationRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.rejectRequest.error.notAllowed'
        OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)

        when: "reject organization request for admin club"
        result.clearErrors()
        springSecurityService.reauthenticate(organizationRequest.club.owner.username)
        result = organizationService.rejectOrganizationRequest(organizationRequest)

        then:
        !result.hasErrors()
        !OrganizationRequest.findByEventAndClub(event, club2)
        !Organization.findByEventAndClub(event, club2)
    }

    void "when rejecting multiple organization requests, requests are treated separately"() {

        given:
        def validRequest = new OrganizationRequest(event: event, club: club2, date: new Date())
        validRequest.save()
        def invalidRequest = new OrganizationRequest(event: event, club: null, date: new Date())
        invalidRequest.save()
        springSecurityService.reauthenticate(validRequest.club.owner.username)
        OrganizationRequest[] requests = [validRequest, invalidRequest]

        when: "reject multiple organization requests"
        def result = organizationService.rejectOrganizationRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "organization cannot be left without logged-in user"() {

        given:
        def organization = new Organization(event: event, club: club2)
        organization.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.leave.error.notAllowed'
        Organization.findByEventAndClub(organization.event, organization.club)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(organization.club.owner.username)
        result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        !result.hasErrors()
        !Organization.findByEventAndClub(organization.event, organization.club)
    }

    void "event is mandatory to leave organization"() {

        given:
        def organization = new Organization(event: event, club: club2)
        organization.save()
        springSecurityService.reauthenticate(organization.club.owner.username)

        when: "leave organization with no event"
        def result = organizationService.leaveOrganization(null, [organization.club] as Club[])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.leave.error.noEvent'
        Organization.findByEventAndClub(organization.event, organization.club)

        when: "leave organization with event"
        result.clearErrors()
        result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        !result.hasErrors()
        !Organization.findByEventAndClub(organization.event, organization.club)
    }

    void "user must be admin of every club leaving organization"() {

        given:
        def organization = new Organization(event: event, club: club2)
        organization.save()

        when: "leave organization with no admin"
        springSecurityService.reauthenticate(User.list().find { !it.isMemberOf(organization.club) }.username)
        def result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.leave.error.notAllowed'
        Organization.findByEventAndClub(organization.event, organization.club)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(organization.club.owner.username)
        result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        !result.hasErrors()
        !Organization.findByEventAndClub(organization.event, organization.club)
    }

    void "event owner cannot leave organization"() {

        given:
        def organization = new Organization(event: event, club: club2)
        organization.save()

        when: "leave organization for owner"
        springSecurityService.reauthenticate(organization.event.club.owner.username)
        def result = organizationService.leaveOrganization(organization.event, [organization.event.club] as Club[])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'organization.leave.error.noOwner'
        Organization.findByEventAndClub(organization.event, organization.event.club)

        when: "leave organization for no owner"
        result.clearErrors()
        springSecurityService.reauthenticate(organization.club.owner.username)
        result = organizationService.leaveOrganization(organization.event, [organization.club] as Club[])

        then:
        !result.hasErrors()
        !Organization.findByEventAndClub(organization.event, organization.club)
    }
}
package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class EventServiceSpec extends Specification {

    def eventService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = (new Club(owner: adminUser, name: 'testClubName')).save(flush: true)
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "event cannot be created without logged-in user"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = eventService.create(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.create.error.notAllowed'
        !Event.findAllByClub(event.club).any { it == event }

        when: "logged-in user"
        event.clearErrors()
        springSecurityService.reauthenticate(event.club.owner.username)
        result = eventService.create(event)

        then:
        !result.hasErrors()
        Event.findAllByClub(event.club).any { it == event }
        Organization.findByEventAndClub(event, event.club)
    }

    void "event is mandatory to create event"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "create event without event"
        def result = eventService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.create.error.noEvent'
        !Event.findAllByClub(event.club).any { it == event }

        when: "create event with event"
        result = eventService.create(event)

        then:
        !result.hasErrors()
        Event.findAllByClub(event.club).any { it == event }
        Organization.findByEventAndClub(event, event.club)
    }

    void "event must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def event = new Event(name: 'testName', description: 'testDescription', startDate: new Date() + 15, endDate:
                new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)

        when: "create event without club"
        def result = eventService.create(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.create.error.noClub'
        !Event.findAllByClub(event.club).any { it == event }

        when: "create event with club"
        event.club = club
        event.clearErrors()
        result = eventService.create(event)

        then:
        !result.hasErrors()
        Event.findAllByClub(event.club).any { it == event }
        Organization.findByEventAndClub(event, event.club)
    }

    void "event can only be created by a club admin"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)

        when: "create event by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(event.club, Member.Type.BASIC).user.username)
        def result = eventService.create(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.create.error.notAllowed'
        !Event.findAllByClub(event.club).any { it == event }

        when: "create event by club admin"
        springSecurityService.reauthenticate(event.club.owner.username)
        event.clearErrors()
        result = eventService.create(event)

        then:
        !result.hasErrors()
        Event.findAllByClub(event.club).any { it == event }
        Organization.findByEventAndClub(event, event.club)
    }

    void "events are created as specified"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "create event"
        def result = eventService.create(event)

        then:
        !result.hasErrors()
        result.id
        result.club == event.club
        result.name == event.name
        result.description == event.description
        result.startDate == event.startDate
        result.endDate == event.endDate
        result.latitude == event.latitude
        result.longitude == event.longitude
        result.isPublic == event.isPublic
        result.maxAttendees == event.maxAttendees
        result.startDateEnrollment == event.startDateEnrollment
        result.endDateEnrollment == event.endDateEnrollment
        result.selfEnrollment == event.selfEnrollment
        Event.findAllByClub(event.club).any { it == event }
        Organization.findByEventAndClub(event, event.club)
    }

    void "event cannot be updated without logged-in user"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
        event.name = "${event.name}Modif"
        event.description = "${event.description}Modif"
        event.startDate = event.startDate + 1
        event.endDate = event.endDate + 1
        event.latitude = event.latitude + 1
        event.longitude = event.longitude + 1
        event.isPublic = !event.isPublic
        event.maxAttendees = event.maxAttendees + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.selfEnrollment = !event.selfEnrollment

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = eventService.update(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.update.error.notAllowed'

        when: "logged-in user"
        event.clearErrors()
        springSecurityService.reauthenticate(event.club.owner.username)
        result = eventService.update(event)

        then:
        !result.hasErrors()
        Event.findByName(event.name)
    }

    void "event is mandatory to update event"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
        event.name = "${event.name}Modif"
        event.description = "${event.description}Modif"
        event.startDate = event.startDate + 1
        event.endDate = event.endDate + 1
        event.latitude = event.latitude + 1
        event.longitude = event.longitude + 1
        event.isPublic = !event.isPublic
        event.maxAttendees = event.maxAttendees + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.selfEnrollment = !event.selfEnrollment
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "update event without event"
        def result = eventService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.update.error.noEvent'

        when: "update event with event"
        result = eventService.update(event)

        then:
        !result.hasErrors()
        Event.findByName(event.name)
    }

    void "event must exist to be updated"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)

        when: "update non-existent event"
        def result = eventService.update(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.update.error.notFoundEvent'

        when: "update existent event"
        event.save()
        (new Organization(event: event, club: event.club)).save()
        event.name = "${event.name}Modif"
        event.description = "${event.description}Modif"
        event.startDate = event.startDate + 1
        event.endDate = event.endDate + 1
        event.latitude = event.latitude + 1
        event.longitude = event.longitude + 1
        event.isPublic = !event.isPublic
        event.maxAttendees = event.maxAttendees + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.selfEnrollment = !event.selfEnrollment
        event.clearErrors()
        result = eventService.update(event)

        then:
        !result.hasErrors()
        Event.findByName(event.name)
    }

    void "event can only be updated by a club admin"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
        event.name = "${event.name}Modif"
        event.description = "${event.description}Modif"
        event.startDate = event.startDate + 1
        event.endDate = event.endDate + 1
        event.latitude = event.latitude + 1
        event.longitude = event.longitude + 1
        event.isPublic = !event.isPublic
        event.maxAttendees = event.maxAttendees + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.selfEnrollment = !event.selfEnrollment

        when: "update event by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(event.club, Member.Type.BASIC).user.username)
        def result = eventService.update(event)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.update.error.notAllowed'

        when: "update event by club admin"
        springSecurityService.reauthenticate(event.club.owner.username)
        event.clearErrors()
        result = eventService.update(event)

        then:
        !result.hasErrors()
        Event.findByName(event.name)
    }

    void "events are updated as specified"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        (new Organization(event: event, club: event.club)).save()
        event.name = "${event.name}Modif"
        event.description = "${event.description}Modif"
        event.startDate = event.startDate + 1
        event.endDate = event.endDate + 1
        event.latitude = event.latitude + 1
        event.longitude = event.longitude + 1
        event.isPublic = !event.isPublic
        event.maxAttendees = event.maxAttendees + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.startDateEnrollment = event.startDateEnrollment + 1
        event.selfEnrollment = !event.selfEnrollment
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "update event"
        def result = eventService.update(event)

        then:
        !result.hasErrors()
        result.id
        result.club == event.club
        result.name == event.name
        result.description == event.description
        result.startDate == event.startDate
        result.endDate == event.endDate
        result.latitude == event.latitude
        result.longitude == event.longitude
        result.isPublic == event.isPublic
        result.maxAttendees == event.maxAttendees
        result.startDateEnrollment == event.startDateEnrollment
        result.endDateEnrollment == event.endDateEnrollment
        result.selfEnrollment == event.selfEnrollment
        Event.findByName(event.name)
    }

    void "event must exist to be deleted"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "delete non-existent event"
        def result = eventService.delete('non-existent-event-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent event"
        result = eventService.delete(event.id)

        then:
        !result.hasErrors()
        !Event.exists(event.id)
    }

    void "an event can only be deleted by a club admin"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = eventService.delete(event.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.delete.error.notAllowed'

        when: "logged-in no club member"
        event.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = eventService.delete(event.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.delete.error.notAllowed'

        when: "logged-in club member"
        event.clearErrors()
        (new Member(club: event.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = eventService.delete(event.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'event.delete.error.notAllowed'

        when: "logged-in club admin"
        event.clearErrors()
        springSecurityService.reauthenticate(event.club.owner.username)
        result = eventService.delete(event.id)

        then:
        !result.hasErrors()
        !Event.exists(event.id)
    }

    void "event deletion deletes all data related to the event"() {

        given:
        def event = new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() +
                15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event.save()
        // Organization requests
        def club2 = (new Club(owner: basicUser, name: 'testClubName2')).save()
        (new OrganizationRequest(event: event, club: club2, date: new Date())).save()
        // Organizations
        def club3 = (new Club(owner: basicUser, name: 'testClubName3')).save()
        (new Organization(event: event, club: club3)).save()
        // Attendance requests
        (new AttendanceRequest(event: event, user: club3.owner, date: new Date())).save()
        // Attendances
        (new Attendance(event: event, user: event.club.owner)).save()
        springSecurityService.reauthenticate(event.club.owner.username)

        when: "delete event"
        def result = eventService.delete(event.id)

        then:
        !result.hasErrors()
        !Event.exists(event.id)
        !OrganizationRequest.findByEvent(event)
        !Organization.findByEvent(event)
        !AttendanceRequest.findByEvent(event)
        !Attendance.findByEvent(event)
    }
}
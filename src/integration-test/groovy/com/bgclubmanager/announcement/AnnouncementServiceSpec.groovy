package com.bgclubmanager.announcement

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class AnnouncementServiceSpec extends Specification {

    def announcementService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "announcement cannot be created without logged-in user"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = announcementService.create(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.create.error.notAllowed'

        when: "logged-in user"
        announcement.clearErrors()
        springSecurityService.reauthenticate(announcement.club.owner.username)
        result = announcementService.create(announcement)

        then:
        !result.hasErrors()
        Announcement.findAllByClub(announcement.club).any { it == announcement }
    }

    void "announcement is mandatory to create announcement"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "create announcement without announcement"
        def result = announcementService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.create.error.noAnnouncement'
        !Announcement.findAllByClub(announcement.club).any { it == announcement }

        when: "create announcement with announcement"
        result = announcementService.create(announcement)

        then:
        !result.hasErrors()
        Announcement.findAllByClub(announcement.club).any { it == announcement }
    }

    void "announcement must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def announcement = new Announcement(title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)

        when: "create announcement without club"
        def result = announcementService.create(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.create.error.noClub'
        !Announcement.findAllByClub(club).any { it == announcement }

        when: "create announcement with club"
        announcement.club = club
        announcement.clearErrors()
        result = announcementService.create(announcement)

        then:
        !result.hasErrors()
        Announcement.findAllByClub(announcement.club).any { it == announcement }
    }

    void "announcement can only be created by a club admin"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)

        when: "create announcement by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(announcement.club, Member.Type.BASIC).user.username)
        def result = announcementService.create(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.create.error.notAllowed'
        !Announcement.findAllByClub(announcement.club).any { it == announcement }

        when: "create announcement by club admin"
        springSecurityService.reauthenticate(announcement.club.owner.username)
        announcement.clearErrors()
        result = announcementService.create(announcement)

        then:
        !result.hasErrors()
        Announcement.findAllByClub(announcement.club).any { it == announcement }
    }

    void "announcements are created as specified"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "create announcement"
        def result = announcementService.create(announcement)

        then:
        !result.hasErrors()
        result.id
        result.club == announcement.club
        result.title == announcement.title
        result.content == announcement.content
        result.published == announcement.published
        result.startDatePublish == announcement.startDatePublish
        result.endDatePublish == announcement.endDatePublish
        Announcement.findAllByClub(announcement.club).any { it == announcement }
    }

    void "announcement cannot be read without logged-in user"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = announcementService.read(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.read.error.notAllowed'
        !Reading.findByUserAndAnnouncement(springSecurityService.currentUser, announcement)

        when: "logged-in user"
        announcement.clearErrors()
        springSecurityService.reauthenticate(announcement.club.owner.username)
        def dateBefore = new Date()
        result = announcementService.read(announcement)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Reading.findByUserAndAnnouncementAndDateBetween(springSecurityService.currentUser, announcement, dateBefore,
                dateAfter)
    }

    void "announcement is mandatory to read announcement"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save()
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "read announcement without announcement"
        def result = announcementService.read(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.read.error.noAnnouncement'
        !Reading.findByUserAndAnnouncement(springSecurityService.currentUser, null)

        when: "read announcement with announcement"
        def dateBefore = new Date()
        result = announcementService.read(announcement)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Reading.findByUserAndAnnouncementAndDateBetween(springSecurityService.currentUser, announcement, dateBefore,
                dateAfter)
    }

    void "user cannot read announcements from clubs he is not a member of (unless he is an admin)"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "read announcement from club he is a member of"
        springSecurityService.reauthenticate(announcement.club.owner.username)
        def dateBefore = new Date()
        def result = announcementService.read(announcement)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Reading.findByUserAndAnnouncementAndDateBetween(springSecurityService.currentUser, announcement, dateBefore,
                dateAfter)

        when: "read announcement from club he is not a member of"
        springSecurityService.reauthenticate(user.username)
        result = announcementService.read(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.read.error.notAllowed'
        !Reading.findByUserAndAnnouncement(springSecurityService.currentUser, announcement)

        when: "read announcement from club he is not a member of (but he is an admin)"
        announcement.clearErrors()
        def adm = new User(username: 'adm', password: 'adm', email: 'adm@bgclubmanager.es', language: language)
        adm.save()
        (new UserRole(user: adm, role: adminRole)).save()
        springSecurityService.reauthenticate(adm.username)
        dateBefore = new Date()
        result = announcementService.read(announcement)
        dateAfter = new Date()

        then:
        !result.hasErrors()
        Reading.findByUserAndAnnouncementAndDateBetween(springSecurityService.currentUser, announcement, dateBefore,
                dateAfter)
    }

    void "announcement cannot be updated without logged-in user"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: false,
                startDatePublish: null, endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        announcement.title = "${announcement.title}Modif"
        announcement.content = "${announcement.content}Modif"
        announcement.published = !announcement.published
        announcement.startDatePublish = new Date()
        announcement.endDatePublish = null

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = announcementService.update(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.notAllowed'

        when: "logged-in user"
        announcement.clearErrors()
        springSecurityService.reauthenticate(announcement.club.owner.username)
        result = announcementService.update(announcement)

        then:
        !result.hasErrors()
    }

    void "announcement is mandatory to update announcement"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: false,
                startDatePublish: null, endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        announcement.title = "${announcement.title}Modif"
        announcement.content = "${announcement.content}Modif"
        announcement.published = !announcement.published
        announcement.startDatePublish = new Date()
        announcement.endDatePublish = null
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "update announcement without announcement"
        def result = announcementService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.noAnnouncement'

        when: "update announcement with announcement"
        result = announcementService.update(announcement)

        then:
        !result.hasErrors()
    }

    void "announcement must exist to be updated"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: false,
                startDatePublish: null, endDatePublish: null)
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "update non-existent announcement"
        def result = announcementService.update(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.notFoundAnnouncement'

        when: "update existent announcement"
        announcement.clearErrors()
        announcement.save(failOnError: true, flush: true, validate: true)
        announcement.title = "${announcement.title}Modif"
        announcement.content = "${announcement.content}Modif"
        announcement.published = !announcement.published
        announcement.startDatePublish = new Date()
        announcement.endDatePublish = null
        result = announcementService.update(announcement)

        then:
        !result.hasErrors()
    }

    void "announcement can only be updated by a club admin"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: false,
                startDatePublish: null, endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        announcement.title = "${announcement.title}Modif"
        announcement.content = "${announcement.content}Modif"
        announcement.published = !announcement.published
        announcement.startDatePublish = new Date()
        announcement.endDatePublish = null
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = announcementService.update(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.notAllowed'

        when: "logged-in no club member"
        announcement.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = announcementService.update(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.notAllowed'

        when: "logged-in club member"
        announcement.clearErrors()
        (new Member(club: club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = announcementService.update(announcement)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.update.error.notAllowed'

        when: "logged-in club admin"
        announcement.clearErrors()
        springSecurityService.reauthenticate(announcement.club.owner.username)
        result = announcementService.update(announcement)

        then:
        !result.hasErrors()
    }

    void "announcement must exist to be deleted"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        (new Reading(user: announcement.club.owner, announcement: announcement, date: new Date())).save(failOnError:
                true, flush: true, validate: true)
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "delete non-existent announcement"
        def result = announcementService.delete('non-existent-announcement-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent announcement"
        result = announcementService.delete(announcement.id)

        then:
        !result.hasErrors()
        !Announcement.exists(announcement.id)
    }

    void "an announcement can only be deleted by a club admin"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        (new Reading(user: announcement.club.owner, announcement: announcement, date: new Date())).save(failOnError:
                true, flush: true, validate: true)
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = announcementService.delete(announcement.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.delete.error.notAllowed'

        when: "logged-in no club member"
        announcement.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = announcementService.delete(announcement.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.delete.error.notAllowed'

        when: "logged-in club member"
        announcement.clearErrors()
        (new Member(club: club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = announcementService.delete(announcement.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'announcement.delete.error.notAllowed'

        when: "logged-in club admin"
        announcement.clearErrors()
        springSecurityService.reauthenticate(announcement.club.owner.username)
        result = announcementService.delete(announcement.id)

        then:
        !result.hasErrors()
        !Announcement.exists(announcement.id)
    }

    void "announcement deletion deletes all data related to the announcement"() {

        given:
        def announcement = new Announcement(club: club, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)
        announcement.save(failOnError: true, flush: true, validate: true)
        // Announcement readings
        (new Reading(user: announcement.club.owner, announcement: announcement, date: new Date())).save(failOnError:
                true, flush: true, validate: true)
        springSecurityService.reauthenticate(announcement.club.owner.username)

        when: "delete announcement"
        def result = announcementService.delete(announcement.id)

        then:
        !result.hasErrors()
        !Announcement.exists(announcement.id)
        !Reading.findByAnnouncement(announcement)
    }
}
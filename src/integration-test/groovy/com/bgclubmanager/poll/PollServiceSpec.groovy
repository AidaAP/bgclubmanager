package com.bgclubmanager.poll

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class PollServiceSpec extends Specification {

    def pollService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "poll cannot be created without logged-in user"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null, options: [[id: null, text: 'testText1'], [id: null, text: 'testText2']])

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = pollService.create(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.create.error.notAllowed'

        when: "logged-in user"
        springSecurityService.reauthenticate(pollCommand.club.owner.username)
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "poll is mandatory to create poll"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null, options: [[id: null, text: 'testText1'], [id: null, text: 'testText2']])
        springSecurityService.reauthenticate(pollCommand.club.owner.username)

        when: "create poll without poll"
        def result = pollService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.create.error.noPoll'
        !Poll.findAllByClub(pollCommand.club).any { it == result }

        when: "create poll with poll"
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "poll must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def pollCommand = new PollCommand(title: 'testTitle', description: 'testDescription', startDateVoting: new
                Date(), endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null,
                options: [[id: null, text: 'testText1'], [id: null, text: 'testText2']])

        when: "create poll without club"
        def result = pollService.create(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.create.error.noClub'
        !Poll.findAllByClub(club).any { it == result }

        when: "create poll with club"
        pollCommand.club = club
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "poll can only be created by a club admin"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null, options: [[id: null, text: 'testText1'], [id: null, text: 'testText2']])

        when: "create poll by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(pollCommand.club, Member.Type.BASIC).user.username)
        def result = pollService.create(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.create.error.notAllowed'
        !Poll.findAllByClub(pollCommand.club).any { it == result }

        when: "create poll by club admin"
        springSecurityService.reauthenticate(pollCommand.club.owner.username)
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "polls must have, at least, 2 options"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null)
        springSecurityService.reauthenticate(pollCommand.club.owner.username)

        when: "create poll with less than 2 options"
        pollCommand.options = [[id: null, text: 'testText1']]
        def result = pollService.create(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].codes.contains('poll.options.minSize.notmet')
        !Poll.findAllByClub(pollCommand.club).any { it == result }

        when: "create poll with 2 options"
        pollCommand.options = [[id: null, text: 'testText1'], [id: null, text: 'testText2']]
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }

        when: "create poll with more than 2 options"
        pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null, options: [[id: null, text: 'testText1'], [id: null, text: 'testText2'],
                                                [id: null, text: 'testText3']])
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "polls options cannot have same text"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null)
        springSecurityService.reauthenticate(pollCommand.club.owner.username)

        when: "poll options have same text"
        pollCommand.options = [[id: null, text: 'testText1'], [id: null, text: 'testText1']]
        def result = pollService.create(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.error.options.sameText'
        !Poll.findAllByClub(pollCommand.club).any { it == result }

        when: "poll options do not have same text"
        pollCommand.options = [[id: null, text: 'testText1'], [id: null, text: 'testText2']]
        result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "polls are created as specified"() {

        given:
        def pollCommand = new PollCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null, options: [[id: null, text: 'testText1'], [id: null, text: 'testText2']])
        springSecurityService.reauthenticate(pollCommand.club.owner.username)

        when: "create poll"
        def result = pollService.create(pollCommand)

        then:
        !result.hasErrors()
        result.id
        result.club?.id == pollCommand.club?.id
        result.title == pollCommand.title
        result.description == pollCommand.description
        result.startDateVoting == pollCommand.startDateVoting
        result.endDateVoting == pollCommand.endDateVoting
        result.published == pollCommand.published
        result.startDatePublish == pollCommand.startDatePublish
        result.endDatePublish == pollCommand.endDatePublish
        result.options.size() == pollCommand.options.size()
        result.options.each { resOption ->
            pollCommand.options.any { it.text == resOption.text }
        }
        Poll.findAllByClub(pollCommand.club).any { it == result }
    }

    void "poll cannot be updated without logged-in user"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date(), options: [
                [id: poll.options[0].id, text: poll.options[0].text],
                [id: poll.options[1].id, text: poll.options[1].text]])

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.notAllowed'

        when: "logged-in user"
        springSecurityService.reauthenticate(poll.club.owner.username)
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "poll is mandatory to update poll"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date(), options: [
                [id: poll.options[0].id, text: poll.options[0].text],
                [id: poll.options[1].id, text: poll.options[1].text]])
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "update poll without poll"
        def result = pollService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.noPoll'

        when: "update poll with poll"
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "poll must exist to be updated"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date(), options: [
                [id: poll.options[0].id, text: poll.options[0].text],
                [id: poll.options[1].id, text: poll.options[1].text]])
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "update non-existent poll"
        pollCommand.id = 'non_existent_poll_id'
        def result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.notFoundPoll'

        when: "update existent poll"
        pollCommand.id = poll.id
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "poll can only be updated by a club admin"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date(), options: [
                [id: poll.options[0].id, text: poll.options[0].text],
                [id: poll.options[1].id, text: poll.options[1].text]])
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.notAllowed'

        when: "logged-in no club member"
        poll.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.notAllowed'

        when: "logged-in club member"
        poll.clearErrors()
        (new Member(club: club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.notAllowed'

        when: "logged-in club admin"
        springSecurityService.reauthenticate(poll.club.owner.username)
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "to update a poll, polls must have, at least, 2 options"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date())
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "update poll with less than 2 options"
        pollCommand.options = [[id: poll.options[0].id, text: poll.options[0].text]]
        def result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].codes.contains('poll.options.minSize.notmet')

        when: "update poll with 2 options"
        pollCommand.options = [[id: poll.options[0].id, text: poll.options[0].text],
                               [id: null, text: "${poll.options[0].text}Modif"]]
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()

        when: "update poll with more than 2 options"
        pollCommand.options = [[id: poll.options[0].id, text: poll.options[0].text],
                               [id: null, text: "${poll.options[0].text}Modif"],
                               [id: null, text: "${poll.options[0].text}Modif2"]]
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "to update a poll, polls options cannot have same text"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date())
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "poll options have same text"
        pollCommand.options = [[id: poll.options[0].id, text: 'testText1'],
                               [id: poll.options[1].id, text: 'testText1']]
        def result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.error.options.sameText'

        when: "poll options do not have same text"
        pollCommand.options = [[id: poll.options[0].id, text: 'testText1'],
                               [id: poll.options[1].id, text: 'testText2']]
        result = pollService.update(pollCommand)

        then:
        !result.hasErrors()
    }

    void "polls with votes cannot be updated"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def pollCommand = new PollCommand(id: poll.id, club: poll.club, title: "${poll.title}Modif", description:
                "${poll.description}Modif", startDateVoting: new Date(), endDateVoting: new Date(), published:
                !poll.published, startDatePublish: new Date(), endDatePublish: new Date(), options: [
                [id: poll.options[0].id, text: poll.options[0].text],
                [id: poll.options[1].id, text: poll.options[1].text]])
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "poll has no votes"
        def result = pollService.update(pollCommand)

        then:
        !result.hasErrors()

        when: "poll has votes"
        (new Vote(user: poll.club.owner, pollOption: poll.options[0], date: new Date())).save()
        result = pollService.update(pollCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.update.error.hasVotes'
    }

    void "poll cannot be voted without logged-in user"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.notAllowed'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, poll.options[0])

        when: "logged-in user"
        springSecurityService.reauthenticate(poll.club.owner.username)
        def dateBefore = new Date()
        result = pollService.vote(poll.options[0])
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Vote.findByUserAndPollOptionAndDateBetween(springSecurityService.currentUser, poll.options[0], dateBefore,
                dateAfter)
    }

    void "pollOption is mandatory to vote poll"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "vote poll without pollOption"
        def result = pollService.vote(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.noPollOption'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, null)

        when: "vote poll with pollOption"
        def dateBefore = new Date()
        result = pollService.vote(poll.options[0])
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Vote.findByUserAndPollOptionAndDateBetween(springSecurityService.currentUser, poll.options[0], dateBefore,
                dateAfter)
    }

    void "user cannot vote polls from clubs he is not a member of"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "vote poll from club he is a member of"
        springSecurityService.reauthenticate(poll.club.owner.username)
        def dateBefore = new Date()
        def result = pollService.vote(poll.options[0])
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Vote.findByUserAndPollOptionAndDateBetween(springSecurityService.currentUser, poll.options[0], dateBefore,
                dateAfter)

        when: "vote from club he is not a member of"
        springSecurityService.reauthenticate(user.username)
        result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.notAllowed'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, poll.options[0])
    }

    void "poll must be visible, votable and not already voted by user"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "not published poll"
        poll.published = false
        poll.save()
        def result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.notVisible'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, poll.options[0])

        when: "not visible poll"
        poll.published = true
        poll.startDatePublish = (new Date()) - 10
        poll.endDatePublish = (new Date()) - 5
        poll.save()
        result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.notVisible'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, poll.options[0])

        when: "not votable poll"
        poll.startDatePublish = (new Date()) - 10
        poll.endDatePublish = (new Date()) + 5
        poll.startDateVoting = (new Date()) - 9
        poll.endDateVoting = (new Date()) - 7
        poll.save()
        result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.notVotable'
        !Vote.findByUserAndPollOption(springSecurityService.currentUser, poll.options[0])

        when: "visible, votable and not already voted"
        poll.startDateVoting = (new Date()) - 9
        poll.endDateVoting = (new Date()) + 4
        poll.save()
        def dateBefore = new Date()
        result = pollService.vote(poll.options[0])
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        Vote.findByUserAndPollOptionAndDateBetween(springSecurityService.currentUser, poll.options[0], dateBefore,
                dateAfter)

        when: "already voted"
        result = pollService.vote(poll.options[0])

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.vote.error.alreadyVoted'
        Vote.findAllByUserAndPollOption(springSecurityService.currentUser, poll.options[0]).size() == 1
    }

    void "poll must exist to be deleted"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        (new Vote(user: poll.club.owner, pollOption: poll.options[0], date: new Date())).save()
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "delete non-existent poll"
        def result = pollService.delete('non-existent-poll-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent poll"
        result = pollService.delete(poll.id)

        then:
        !result.hasErrors()
        !Poll.exists(poll.id)
    }

    void "a poll can only be deleted by a club admin"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        (new Vote(user: poll.club.owner, pollOption: poll.options[0], date: new Date())).save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = pollService.delete(poll.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.delete.error.notAllowed'

        when: "logged-in no club member"
        poll.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = pollService.delete(poll.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.delete.error.notAllowed'

        when: "logged-in club member"
        poll.clearErrors()
        (new Member(club: club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = pollService.delete(poll.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'poll.delete.error.notAllowed'

        when: "logged-in club admin"
        poll.clearErrors()
        springSecurityService.reauthenticate(poll.club.owner.username)
        result = pollService.delete(poll.id)

        then:
        !result.hasErrors()
        !Poll.exists(poll.id)
    }

    void "poll deletion deletes all data related to the poll"() {

        given:
        def poll = new Poll(club: club, title: 'testTitle', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        // Poll options
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save()
        // Poll votes
        (new Vote(user: poll.club.owner, pollOption: poll.options[0], date: new Date())).save()
        springSecurityService.reauthenticate(poll.club.owner.username)

        when: "delete poll"
        def result = pollService.delete(poll.id)

        then:
        !result.hasErrors()
        !Poll.exists(poll.id)
        !PollOption.findByPoll(poll)
        !Vote.fromPoll(poll).list()
    }
}
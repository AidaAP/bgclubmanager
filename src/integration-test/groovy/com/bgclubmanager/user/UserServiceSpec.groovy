package com.bgclubmanager.user

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.event.Event
import com.bgclubmanager.game.Game
import com.bgclubmanager.mail.MailUtils.MailType
import com.bgclubmanager.poll.Poll
import com.bgclubmanager.poll.PollOption
import com.bgclubmanager.poll.Vote
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.reserve.ReservedGame
import com.bgclubmanager.reserve.ReservedTable
import com.bgclubmanager.table.Table
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class UserServiceSpec extends Specification {

    def grailsApplication
    def passwordEncoder
    def springSecurityService
    def userService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()
    }

    def cleanup() {
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "logged-in users cannot register users"() {

        given:
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)

        when: "logged-in basic user"
        springSecurityService.reauthenticate(basicUser.username)
        def result = userService.register(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.register.error.notAllowed'
        !User.findByUsername(user.username)

        when: "logged-in admin user"
        user.clearErrors()
        springSecurityService.reauthenticate(adminUser.username)
        result = userService.register(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.register.error.notAllowed'
        !User.findByUsername(user.username)

        when: "not logged-in user"
        user.clearErrors()
        SecurityContextHolder.clearContext()
        result = userService.register(user)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "register user creates a basic user"() {

        given:
        SecurityContextHolder.clearContext()
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)

        when: "register user"
        def result = userService.register(user)

        then:
        !result.hasErrors()
        result.id
        result.username == user.username
        result.password == user.password
        !result.name
        !result.surname
        result.email == user.email
        !result.urlImage
        result.enabled
        !result.accountExpired
        !result.accountLocked
        !result.passwordExpired
        result.language == user.language
        User.findByUsername(user.username)
        UserRole.countByUser(result) == 1
        UserRole.exists(result.id, basicRole.id)
    }

    void "only admin users can create admin users"() {

        given:
        def user = new User(username: 'user', email: 'user@bgclubmanager.es', language: language)

        when: "not logged-in user"
        SecurityContextHolder.clearContext()
        def result = userService.createAdmin(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.createAdmin.error.notAllowed'
        !User.findByUsername(user.username)

        when: "logged-in basic user"
        user.clearErrors()
        springSecurityService.reauthenticate(basicUser.username)
        result = userService.createAdmin(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.createAdmin.error.notAllowed'
        !User.findByUsername(user.username)

        when: "logged-in admin user"
        user.clearErrors()
        springSecurityService.reauthenticate(adminUser.username)
        result = userService.createAdmin(user)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "password cannot be specified when creating admin users"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', email: 'user@bgclubmanager.es', language: language)

        when: "password specified"
        user.password = 'password'
        def result = userService.createAdmin(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.createAdmin.error.password'
        !User.findByUsername(user.username)

        when: "password not specified"
        user.password = null
        user.clearErrors()
        result = userService.createAdmin(user)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "createAdmin creates an admin user with password"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', email: 'user@bgclubmanager.es', language: language)

        when: "create admin user"
        def result = userService.createAdmin(user)

        then:
        !result.hasErrors()
        result.id
        result.username == user.username
        result.password
        !result.name
        !result.surname
        result.email == user.email
        !result.urlImage
        result.enabled
        !result.accountExpired
        !result.accountLocked
        !result.passwordExpired
        result.language == user.language
        User.findByUsername(user.username)
        UserRole.countByUser(result) == 1
        UserRole.exists(result.id, adminRole.id)
    }

    void "user is mandatory to create user"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)
        Role[] roles = [basicRole]
        MailType[] mailTypes = [MailType.REGISTER_USER]

        when: "create user without user"
        def result = userService.create(null, roles, mailTypes)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.create.error.noUser'
        !User.findByUsername(user.username)

        when: "create user with user"
        user.clearErrors()
        result = userService.create(user, roles, mailTypes)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "roles are mandatory to create user"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)
        Role[] roles = [basicRole]
        MailType[] mailTypes = [MailType.REGISTER_USER]

        when: "create user without roles"
        def result = userService.create(user, null, mailTypes)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.create.error.noRoles'
        !User.findByUsername(user.username)

        when: "create user with roles"
        user.clearErrors()
        result = userService.create(user, roles, mailTypes)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "basic users cannot create users"() {

        given:
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)
        Role[] roles = [basicRole]
        MailType[] mailTypes = [MailType.REGISTER_USER]

        when: "logged-in basic user"
        springSecurityService.reauthenticate(basicUser.username)
        def result = userService.create(user, roles, mailTypes)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.create.error.notAllowed'
        !User.findByUsername(user.username)

        when: "logged-in admin user"
        user.clearErrors()
        springSecurityService.reauthenticate(adminUser.username)
        result = userService.create(user, roles, mailTypes)

        then:
        !result.hasErrors()
        User.findByUsername(user.username)
    }

    void "users are created as specified"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', password: 'password', name: 'name', surname: 'surname', email:
                'user@bgclubmanager.es', urlImage: '/test/images/img.png', enabled: false, accountExpired: true,
                accountLocked: true, passwordExpired: true, language: language)
        Role[] roles = [basicRole, adminRole]
        MailType[] mailTypes = [MailType.REGISTER_USER]

        when: "create user"
        def result = userService.create(user, roles, mailTypes)

        then:
        !result.hasErrors()
        result.id
        result.username == user.username
        result.password == user.password
        result.name == user.name
        result.surname == user.surname
        result.email == user.email
        result.urlImage == user.urlImage
        result.enabled == user.enabled
        result.accountExpired == user.accountExpired
        result.accountLocked == user.accountLocked
        result.passwordExpired == user.passwordExpired
        result.language == user.language
        User.findByUsername(user.username)
        UserRole.countByUser(result) == roles.size()
        roles.each {
            UserRole.exists(result.id, it.id)
        }
    }

    void "command is mandatory to change password"() {

        given:
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def user = new User(username: 'user', password: oldPassword, email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(user.username)
        def command = new ChangePasswordCommand(springSecurityService: springSecurityService, oldPassword: oldPassword,
                newPassword: newPassword, confirmNewPassword: newPassword)

        when: "change password without command"
        def result = userService.changePassword(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.changePassword.error.noCommand'

        when: "change password with command"
        result = userService.changePassword(command)

        then:
        !result.hasErrors()
        passwordEncoder.isPasswordValid(User.read(user.id).password, newPassword, null)
    }

    void "password cannot be changed without logged-in user"() {

        given:
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def user = new User(username: 'user', password: oldPassword, email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        def command = new ChangePasswordCommand(springSecurityService: springSecurityService, oldPassword:
                oldPassword, newPassword: newPassword, confirmNewPassword: newPassword)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.changePassword.error.notLoggedIn'

        when: "logged-in user"
        command.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = userService.changePassword(command)

        then:
        !result.hasErrors()
        passwordEncoder.isPasswordValid(User.read(user.id).password, newPassword, null)
    }

    void "oldPassword must be correct to change password"() {

        given:
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def user = new User(username: 'user', password: oldPassword, email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(user.username)
        def command = new ChangePasswordCommand(springSecurityService: springSecurityService, newPassword: newPassword,
                confirmNewPassword: newPassword)

        when: "without oldPassword"
        def result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.null.message'

        when: "with incorrect oldPassword"
        command.oldPassword = oldPassword + 'incorrect'
        command.clearErrors()
        result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.changePassword.error.oldPassword'

        when: "with correct oldPassword"
        command.oldPassword = oldPassword
        command.clearErrors()
        result = userService.changePassword(command)

        then:
        !result.hasErrors()
        passwordEncoder.isPasswordValid(User.read(user.id).password, newPassword, null)
    }

    void "newPassword must be correct to change password"() {

        given:
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def user = new User(username: 'user', password: oldPassword, email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(user.username)
        def command = new ChangePasswordCommand(springSecurityService: springSecurityService, oldPassword: oldPassword,
                confirmNewPassword: newPassword)

        when: "without newPassword"
        def result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 2
        result.errors.allErrors.any { (it.field == 'newPassword') && (it.code == 'default.null.message') }
        result.errors.allErrors.any {
            (it.field == 'confirmNewPassword') && (it.code == 'user.changePassword.error' +
                    '.confirmNewPassword')
        }

        when: "with newPassword"
        command.newPassword = newPassword
        command.clearErrors()
        result = userService.changePassword(command)

        then:
        !result.hasErrors()
        passwordEncoder.isPasswordValid(User.read(user.id).password, newPassword, null)
    }

    void "confirmNewPassword must be correct to change password"() {

        given:
        def oldPassword = 'oldPassword'
        def newPassword = 'newPassword'
        def user = new User(username: 'user', password: oldPassword, email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(user.username)
        def command = new ChangePasswordCommand(springSecurityService: springSecurityService, oldPassword: oldPassword,
                newPassword: newPassword)

        when: "without confirmNewPassword"
        def result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 2
        result.errors.allErrors[0].code == 'default.null.message'

        when: "with incorrect confirmNewPassword"
        command.confirmNewPassword = newPassword + 'incorrect'
        command.clearErrors()
        result = userService.changePassword(command)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.changePassword.error.confirmNewPassword'

        when: "with correct confirmNewPassword"
        command.confirmNewPassword = newPassword
        command.clearErrors()
        result = userService.changePassword(command)

        then:
        !result.hasErrors()
        passwordEncoder.isPasswordValid(User.read(user.id).password, newPassword, null)
    }

    void "user is mandatory to update user"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', password: 'password', name: 'name', surname: 'surname', email:
                'user@bgclubmanager.es', urlImage: 'urlImage', language: language)
        user.save(failOnError: true, flush: true, validate: true)
        user.name = "${user.name}Modif"
        user.surname = "${user.surname}Modif"
        user.email = "modif${user.email}"
        user.urlImage = "${user.urlImage}Modif"

        when: "update user without user"
        def result = userService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.update.error.noUser'

        when: "update user with user"
        result = userService.update(user)

        then:
        !result.hasErrors()
    }

    void "user must exist to update user"() {

        given:
        springSecurityService.reauthenticate(adminUser.username)
        def user = new User(username: 'user', password: 'password', name: 'name', surname: 'surname', email:
                'user@bgclubmanager.es', urlImage: 'urlImage', language: language)

        when: "user does not exist"
        def result = userService.update(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.update.error.notFoundUser'

        when: "user exists"
        user.clearErrors()
        user.save(failOnError: true, flush: true, validate: true)
        user.name = "${user.name}Modif"
        user.surname = "${user.surname}Modif"
        user.email = "modif${user.email}"
        user.urlImage = "${user.urlImage}Modif"
        result = userService.update(user)

        then:
        !result.hasErrors()
    }

    void "user can only be updated by admin users or by the user itself"() {

        given:
        def user = new User(username: 'user', password: 'password', name: 'name', surname: 'surname', email:
                'user@bgclubmanager.es', urlImage: 'urlImage', language: language)
        user.save(failOnError: true, flush: true, validate: true)
        user.name = "${user.name}Modif"
        user.surname = "${user.surname}Modif"
        user.email = "modif${user.email}"
        user.urlImage = "${user.urlImage}Modif"

        when: "basic user updates other user"
        springSecurityService.reauthenticate(basicUser.username)
        def result = userService.update(user)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.update.error.notAllowed'

        when: "admin user updates other user"
        user.clearErrors()
        springSecurityService.reauthenticate(adminUser.username)
        result = userService.update(user)

        then:
        !result.hasErrors()

        when: "basic user updates itself"
        springSecurityService.reauthenticate(user.username)
        result = userService.update(user)

        then:
        !result.hasErrors()
    }

    void "a user can only be deleted by an admin or itself"() {

        given:
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = userService.delete(user.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.delete.error.notAllowed'
        User.exists(user.id)

        when: "logged-in basic user"
        springSecurityService.reauthenticate(basicUser.username)
        result = userService.delete(user.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'user.delete.error.notAllowed'
        User.exists(user.id)

        when: "logged-in admin user"
        springSecurityService.reauthenticate(adminUser.username)
        result = userService.delete(user.id)

        then:
        !result.hasErrors()
        !User.exists(user.id)

        when: "logged-in user to delete"
        user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language: language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(user.username)
        result = userService.delete(user.id)

        then:
        !result.hasErrors()
        !User.exists(user.id)
    }

    void "user must exist to be deleted"() {

        given:
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        springSecurityService.reauthenticate(adminUser.username)

        when: "delete non-existent user"
        def result = userService.delete('non-existent-user-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent user"
        result = userService.delete(user.id)

        then:
        !result.hasErrors()
        !User.exists(user.id)
    }

    void "user deletion deletes all data related to the user"() {

        given:
        def user = new User(username: 'user', password: 'password', email: 'user@bgclubmanager.es', language:
                language)
        user.save(flush: true)
        // Role
        (new UserRole(user: user, role: basicRole)).save(flush: true)
        // Token
        springSecurityService.reauthenticate(user.username)
        userService.generateToken().id
        // Club memberships
        def basicMemberClub = new Club(owner: adminUser, name: 'testBasicMemberClub')
        basicMemberClub.save(flush: true)
        (new Member(club: basicMemberClub, user: user, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        def adminMemberClub = new Club(owner: adminUser, name: 'testAdminMemberClub')
        adminMemberClub.save(flush: true)
        (new Member(club: adminMemberClub, user: user, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        // Club ownerships
        def ownedClub = new Club(owner: user, name: 'testOwnedClub')
        ownedClub.save(flush: true)
        // User readings
        (new Announcement(club: basicMemberClub, title: 'testTile', content: 'testContent', published: true,
                startDatePublish: new Date(), endDatePublish: null)).save(flush: true)
        (new Reading(user: user, announcement: Announcement.findByClub(basicMemberClub), date: new Date())).save(flush: true)
        // User votes
        def poll = new Poll(club: basicMemberClub, title: 'testTile', description: 'testDescription',
                startDateVoting: new Date(), endDateVoting: null, published: true, startDatePublish: new Date(),
                endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save(flush: true)
        (new Vote(user: user, pollOption: poll.options[0], date: new Date())).save(flush: true)
        // User attendances
        def event1 = new Event(club: basicMemberClub, name: 'testName1', description: 'testDescription1', startDate:
                new Date() + 15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees:
                100, startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event1.save(flush: true)
        def event2 = new Event(club: basicMemberClub, name: 'testName2', description: 'testDescription2', startDate: new
                Date() + 15, endDate: new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees:
                100, startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)
        event2.save(flush: true)
        (new AttendanceRequest(user: user, event: event1, date: new Date())).save(flush: true)
        (new Attendance(user: user, event: event2)).save(flush: true)
        // Club game
        (new Game(club: basicMemberClub, name: 'testGame', copies: 1)).save(flush: true)
        // Club table
        (new Table(club: basicMemberClub, name: 'testTable')).save(flush: true)
        // User reserves
        def reserve1 = new Reserve(user: user, club: basicMemberClub, title: 'testReserve1', startDate: new Date() + 1,
                endDate: new Date() + 2)
        reserve1.addToReservedGames(new ReservedGame(game: Game.findByClub(basicMemberClub)))
        reserve1.reservedTables = []
        reserve1.save(flush: true)
        def reserve2 = new Reserve(user: user, club: basicMemberClub, title: 'testReserve2', startDate: new Date() + 3,
                endDate: new Date() + 4)
        reserve2.reservedGames = []
        reserve2.addToReservedTables(new ReservedTable(table: Table.findByClub(basicMemberClub)))
        reserve2.save(flush: true)
        def reserve3 = new Reserve(user: user, club: basicMemberClub, title: 'testReserve3', startDate: new Date() + 5,
                endDate: new Date() + 6)
        reserve3.addToReservedGames(new ReservedGame(game: Game.findByClub(basicMemberClub)))
        reserve3.addToReservedTables(new ReservedTable(table: Table.findByClub(basicMemberClub)))
        reserve3.save(flush: true)
        springSecurityService.reauthenticate(adminUser.username)

        when: "delete user"
        def result = userService.delete(user.id)

        then:
        !result.hasErrors()
        !User.exists(user.id)
        !UserRole.findByUser(user)
        !UserToken.findByUser(user)
        !Member.findByUser(user)
        !Club.findByOwner(user)
        ownedClub.owner.username == grailsApplication.config.default.user.username
        User.findByUsername(grailsApplication.config.default.user.username).isAdminOf(ownedClub)
        !Reading.findByUser(user)
        !Vote.findByUser(user)
        !Reserve.findByUser(user)
    }

    void "token cannot be generated without logged-in user"() {

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = userService.generateToken()

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'userToken.generate.error.notLoggedIn'

        when: "logged-in user"
        springSecurityService.reauthenticate(basicUser.username)
        result = userService.generateToken()

        then:
        !result.hasErrors()
        result.id != null
        result.user == springSecurityService.currentUser
    }

    void "user can oly have one token"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)
        assert UserToken.countByUser(basicUser) == 0
        def oldToken = null

        when: "generate first token"
        def result = userService.generateToken()

        then:
        !result.hasErrors()
        result.id != null
        result.user == springSecurityService.currentUser
        result.id != oldToken
        UserToken.countByUser(springSecurityService.currentUser) == 1

        when: "generate second token"
        oldToken = result.id
        result = userService.generateToken()

        then:
        !result.hasErrors()
        result.id != null
        result.user == springSecurityService.currentUser
        result.id != oldToken
        UserToken.countByUser(springSecurityService.currentUser) == 1
    }
}
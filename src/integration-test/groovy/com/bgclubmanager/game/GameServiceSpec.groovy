package com.bgclubmanager.game

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.reserve.ReservedGame
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class GameServiceSpec extends Specification {

    def gameService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "game cannot be created without logged-in user"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = gameService.create(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.create.error.notAllowed'

        when: "logged-in user"
        game.clearErrors()
        springSecurityService.reauthenticate(game.club.owner.username)
        result = gameService.create(game)

        then:
        !result.hasErrors()
        Game.findAllByClub(game.club).any { it == game }
    }

    void "game is mandatory to create game"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "create game without game"
        def result = gameService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.create.error.noGame'
        !Game.findAllByClub(game.club).any { it == game }

        when: "create game with game"
        result = gameService.create(game)

        then:
        !result.hasErrors()
        Game.findAllByClub(game.club).any { it == game }
    }

    void "game must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def game = new Game(name: 'testGameName', description: 'testGameDescription', urlImage: 'testGameUrlImage',
                copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')

        when: "create game without club"
        def result = gameService.create(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.create.error.noClub'
        !Game.findAllByClub(club).any { it == game }

        when: "create game with club"
        game.club = club
        game.clearErrors()
        result = gameService.create(game)

        then:
        !result.hasErrors()
        Game.findAllByClub(game.club).any { it == game }
    }

    void "game can only be created by a club admin"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')

        when: "create game by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(game.club, Member.Type.BASIC).user.username)
        def result = gameService.create(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.create.error.notAllowed'
        !Game.findAllByClub(game.club).any { it == game }

        when: "create game by club admin"
        springSecurityService.reauthenticate(game.club.owner.username)
        game.clearErrors()
        result = gameService.create(game)

        then:
        !result.hasErrors()
        Game.findAllByClub(game.club).any { it == game }
    }

    void "games are created as specified"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "create game"
        def result = gameService.create(game)

        then:
        !result.hasErrors()
        result.id
        result.club == game.club
        result.name == game.name
        result.description == game.description
        result.urlImage == game.urlImage
        result.copies == game.copies
        result.urlBGG == game.urlBGG
        Game.findAllByClub(game.club).any { it == game }
    }

    void "game cannot be updated without logged-in user"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        game.name = "${game.name}Modif"
        game.description = "${game.description}Modif"
        game.urlImage = "${game.urlImage}Modif"
        game.copies = game.copies + 1
        game.urlBGG = "${game.urlBGG}Modif"

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = gameService.update(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.update.error.notAllowed'

        when: "logged-in user"
        game.clearErrors()
        springSecurityService.reauthenticate(game.club.owner.username)
        result = gameService.update(game)

        then:
        !result.hasErrors()
        Game.findByName(game.name)
    }

    void "game is mandatory to update game"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        game.name = "${game.name}Modif"
        game.description = "${game.description}Modif"
        game.urlImage = "${game.urlImage}Modif"
        game.copies = game.copies + 1
        game.urlBGG = "${game.urlBGG}Modif"
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "update game without game"
        def result = gameService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.update.error.noGame'

        when: "update game with game"
        result = gameService.update(game)

        then:
        !result.hasErrors()
        Game.findByName(game.name)
    }

    void "game must exist to be updated"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')

        when: "update non-existent game"
        def result = gameService.update(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.update.error.notFoundGame'

        when: "update existent game"
        game.save()
        game.name = "${game.name}Modif"
        game.description = "${game.description}Modif"
        game.urlImage = "${game.urlImage}Modif"
        game.copies = game.copies + 1
        game.urlBGG = "${game.urlBGG}Modif"
        game.clearErrors()
        result = gameService.update(game)

        then:
        !result.hasErrors()
        Game.findByName(game.name)
    }

    void "game can only be updated by a club admin"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        game.name = "${game.name}Modif"
        game.description = "${game.description}Modif"
        game.urlImage = "${game.urlImage}Modif"
        game.copies = game.copies + 1
        game.urlBGG = "${game.urlBGG}Modif"

        when: "update game by no club admin"
        springSecurityService.reauthenticate(Member.findByClubAndType(game.club, Member.Type.BASIC).user.username)
        def result = gameService.update(game)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.update.error.notAllowed'

        when: "update game by club admin"
        springSecurityService.reauthenticate(game.club.owner.username)
        game.clearErrors()
        result = gameService.update(game)

        then:
        !result.hasErrors()
        Game.findByName(game.name)
    }

    void "games are updated as specified"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        game.name = "${game.name}Modif"
        game.description = "${game.description}Modif"
        game.urlImage = "${game.urlImage}Modif"
        game.copies = game.copies + 1
        game.urlBGG = "${game.urlBGG}Modif"
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "update game"
        def result = gameService.update(game)

        then:
        !result.hasErrors()
        result.id
        result.club == game.club
        result.name == game.name
        result.description == game.description
        result.urlImage == game.urlImage
        result.copies == game.copies
        result.urlBGG == game.urlBGG
        Game.findByName(game.name)
    }

    void "game must exist to be deleted"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "delete non-existent game"
        def result = gameService.delete('non-existent-game-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent game"
        result = gameService.delete(game.id)

        then:
        !result.hasErrors()
        !Game.exists(game.id)
    }

    void "a game can only be deleted by a club admin"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        def user = new User(username: 'user', password: 'user', email: 'user@bgclubmanager.es', language: language)
        user.save()
        (new UserRole(user: user, role: basicRole)).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = gameService.delete(game.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.delete.error.notAllowed'

        when: "logged-in no club member"
        game.clearErrors()
        springSecurityService.reauthenticate(user.username)
        result = gameService.delete(game.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.delete.error.notAllowed'

        when: "logged-in club member"
        game.clearErrors()
        (new Member(club: game.club, user: user, type: Member.Type.BASIC, startDate: new Date())).save()
        result = gameService.delete(game.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'game.delete.error.notAllowed'

        when: "logged-in club admin"
        game.clearErrors()
        springSecurityService.reauthenticate(game.club.owner.username)
        result = gameService.delete(game.id)

        then:
        !result.hasErrors()
        !Game.exists(game.id)
    }

    void "game deletion deletes all data related to the game"() {

        given:
        def game = new Game(club: club, name: 'testGameName', description: 'testGameDescription', urlImage:
                'testGameUrlImage', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')
        game.save()
        // Game reserves
        def reserve1 = new Reserve(user: club.owner, club: club, title: 'testReserve1', startDate: new Date() + 1,
                endDate: new Date() + 2)
        reserve1.addToReservedGames(new ReservedGame(game: game))
        reserve1.reservedTables = []
        reserve1.save(flush: true)
        def reserve2 = new Reserve(user: club.owner, club: club, title: 'testReserve2', startDate: new Date() + 3,
                endDate: new Date() + 4)
        reserve2.addToReservedGames(new ReservedGame(game: (new Game(club: club, name: 'testOtherGameName',
                description: 'testOtherGameDescription', urlImage: 'testOtherGameUrlImage', copies: 10, urlBGG:
                'https://www.boardgamegeek.com/boardgame/2136/pachisi')).save()))
        reserve2.reservedTables = []
        reserve2.save(flush: true)
        springSecurityService.reauthenticate(game.club.owner.username)

        when: "delete game"
        def result = gameService.delete(game.id)

        then:
        !result.hasErrors()
        !Game.exists(game.id)
        !Reserve.withGame(game).list()
        !Reserve.exists(reserve1.id)
        Reserve.exists(reserve2.id)
    }
}
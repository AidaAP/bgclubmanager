package com.bgclubmanager.club

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.core.Language
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.event.Event
import com.bgclubmanager.event.Organization
import com.bgclubmanager.event.OrganizationRequest
import com.bgclubmanager.game.Game
import com.bgclubmanager.poll.Poll
import com.bgclubmanager.poll.PollOption
import com.bgclubmanager.poll.Vote
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.reserve.ReservedGame
import com.bgclubmanager.reserve.ReservedTable
import com.bgclubmanager.table.Table
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class ClubServiceSpec extends Specification {

    def clubService
    def grailsApplication
    def messageSource
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()
    }

    def cleanup() {
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "club cannot be created without logged-in user"() {

        given:
        def club = new Club(name: 'testClubName', description: 'testClubDescription', email: 'testClub@bgclubmanager' +
                '.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage: '/test/images/img.png')

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = clubService.create(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.create.error.notAllowed'

        when: "logged-in user"
        springSecurityService.reauthenticate(basicUser.username)
        result = clubService.create(club)

        then:
        !result.hasErrors()
        result.id != null
        result.owner == basicUser
        Club.findByName(club.name)
    }

    void "club is mandatory to create club"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)
        def club = new Club(name: 'testClubName', description: 'testClubDescription', email: 'testClub@bgclubmanager' +
                '.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage: '/test/images/img.png')

        when: "create club without club"
        def result = clubService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.create.error.noClub'
        !Club.findByName(club.name)

        when: "create club with club"
        club.clearErrors()
        result = clubService.create(club)

        then:
        !result.hasErrors()
        Club.findByName(club.name)
    }

    void "clubs are created as specified"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)
        def club = new Club(name: 'testClubName', description: 'testClubDescription', email: 'testClub@bgclubmanager' +
                '.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage: '/test/images/img.png')

        when: "create club"
        def result = clubService.create(club)

        then:
        !result.hasErrors()
        result.id
        result.owner == basicUser
        result.name == club.name
        result.description == club.description
        result.email == club.email
        result.phone == club.phone
        result.latitude == club.latitude
        result.longitude == club.longitude
        result.urlImage == club.urlImage
        Club.findByName(club.name)
    }

    void "club is mandatory to update club"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)
        def club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(failOnError: true, flush: true, validate: true)
        (new Member(club: club, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        club.name = "${club.name}Modif"
        club.description = "${club.description}Modif"
        club.email = "modif${club.email}"
        club.phone = "${club.phone}1"
        club.latitude = 20
        club.longitude = 20
        club.urlImage = "${club.urlImage}Modif"

        when: "update club without club"
        def result = clubService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.update.error.noClub'

        when: "update club with club"
        result = clubService.update(club)

        then:
        !result.hasErrors()
    }

    void "club must exist to update club"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)
        def club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')

        when: "club does not exist"
        def result = clubService.update(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.update.error.notFoundClub'

        when: "club exists"
        club.clearErrors()
        club.save(failOnError: true, flush: true, validate: true)
        (new Member(club: club, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        club.name = "${club.name}Modif"
        club.description = "${club.description}Modif"
        club.email = "modif${club.email}"
        club.phone = "${club.phone}1"
        club.latitude = 20
        club.longitude = 20
        club.urlImage = "${club.urlImage}Modif"
        result = clubService.update(club)

        then:
        !result.hasErrors()
    }

    void "club can only be updated by admin users or by its admins"() {

        given:
        def club = new Club(owner: adminUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(failOnError: true, flush: true, validate: true)
        club.name = "${club.name}Modif"
        club.description = "${club.description}Modif"
        club.email = "modif${club.email}"
        club.phone = "${club.phone}1"
        club.latitude = 20
        club.longitude = 20
        club.urlImage = "${club.urlImage}Modif"

        when: "basic user updates a club he does not admin"
        springSecurityService.reauthenticate(basicUser.username)
        def result = clubService.update(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.update.error.notAllowed'

        when: "basic user updates a club he admins"
        (new Member(club: club, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        result = clubService.update(club)

        then:
        !result.hasErrors()

        when: "admin user updates a club he does not admin"
        springSecurityService.reauthenticate(adminUser.username)
        club.name = "${club.name}Modif"
        club.description = "${club.description}Modif"
        club.email = "modif${club.email}"
        club.phone = "${club.phone}1"
        club.latitude = 20
        club.longitude = 20
        club.urlImage = "${club.urlImage}Modif"
        result = clubService.update(club)

        then:
        !result.hasErrors()

        when: "admin user updates a club he admins"
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        result = clubService.update(club)

        then:
        !result.hasErrors()
    }

    void "user must be logged-in to find clubs between coordinates"() {

        given:
        def random = new Random()
        def latitude1 = random.nextInt(181) - 90
        def longitude1 = random.nextInt(361) - 180
        def latitude2 = random.nextInt(181) - 90
        def longitude2 = random.nextInt(361) - 180
        new Club(owner: basicUser, name: 'testClubName1', description: 'testClubDescription1', email:
                'testClub1@bgclubmanager.es', latitude: (latitude1+latitude2 ?: 1)/2, longitude:
                (longitude1+longitude2 ?: 1)/2).save()
        new Club(owner: basicUser, name: 'testClubName2', description: 'testClubDescription2', email:
                'testClub2@bgclubmanager.es', latitude: 90, longitude: 180).save()
        new Club(owner: basicUser, name: 'testClubName3', description: 'testClubDescription3', email:
                'testClub3@bgclubmanager.es', latitude: -90, longitude: -180).save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = clubService.findBetweenCoordinates(latitude1, longitude1, latitude2, longitude2)

        then:
        result
        result.error
        result.error == messageSource.getMessage('club.findBetweenCoordinates.error.notAllowed', null,
                LocaleContextHolder.locale)

        when: "logged-in user"
        springSecurityService.reauthenticate(basicUser.username)
        result = clubService.findBetweenCoordinates(latitude1, longitude1, latitude2, longitude2)

        then:
        result
        !result.error
    }

    void "every parameter is mandatory to find clubs between coordinates"() {

        given:
        def random = new Random()
        def latitude1 = random.nextInt(181) - 90
        def longitude1 = random.nextInt(361) - 180
        def latitude2 = random.nextInt(181) - 90
        def longitude2 = random.nextInt(361) - 180
        new Club(owner: basicUser, name: 'testClubName1', description: 'testClubDescription1', email:
                'testClub1@bgclubmanager.es', latitude: (latitude1+latitude2 ?: 1)/2, longitude:
                (longitude1+longitude2 ?: 1)/2).save()
        new Club(owner: basicUser, name: 'testClubName2', description: 'testClubDescription2', email:
                'testClub2@bgclubmanager.es', latitude: 90, longitude: 180).save()
        new Club(owner: basicUser, name: 'testClubName3', description: 'testClubDescription3', email:
                'testClub3@bgclubmanager.es', latitude: -90, longitude: -180).save()
        springSecurityService.reauthenticate(basicUser.username)

        when: "missing latitude1"
        def result = clubService.findBetweenCoordinates(null, longitude1, latitude2, longitude2)

        then:
        result
        result.error
        result.error == messageSource.getMessage('club.findBetweenCoordinates.error.missingParams', null,
                LocaleContextHolder.locale)

        when: "missing longitude1"
        result = clubService.findBetweenCoordinates(latitude1, null, latitude2, longitude2)

        then:
        result
        result.error
        result.error == messageSource.getMessage('club.findBetweenCoordinates.error.missingParams', null,
                LocaleContextHolder.locale)

        when: "missing latitude2"
        result = clubService.findBetweenCoordinates(latitude1, longitude1, null, longitude2)

        then:
        result
        result.error
        result.error == messageSource.getMessage('club.findBetweenCoordinates.error.missingParams', null,
                LocaleContextHolder.locale)

        when: "missing longitude2"
        result = clubService.findBetweenCoordinates(latitude1, longitude1, latitude2, null)

        then:
        result
        result.error
        result.error == messageSource.getMessage('club.findBetweenCoordinates.error.missingParams', null,
                LocaleContextHolder.locale)

        when: "every parameter is specified"
        result = clubService.findBetweenCoordinates(latitude1, longitude1, latitude2, longitude2)

        then:
        result
        !result.error
    }

    void "find clubs between coordinates finds correct clubs"() {

        given:
        def random = new Random()
        def latitude1 = random.nextInt(181) - 90
        def longitude1 = random.nextInt(361) - 180
        def latitude2 = random.nextInt(181) - 90
        def longitude2 = random.nextInt(361) - 180
        new Club(owner: basicUser, name: 'testClubName1', description: 'testClubDescription1', email:
                'testClub1@bgclubmanager.es', latitude: (latitude1+latitude2 ?: 1) / 2, longitude:
                (longitude1+longitude2 ?: 1) / 2).save()
        new Club(owner: basicUser, name: 'testClubName2', description: 'testClubDescription2', email:
                'testClub2@bgclubmanager.es', latitude: 90, longitude: 180).save()
        new Club(owner: basicUser, name: 'testClubName3', description: 'testClubDescription3', email:
                'testClub3@bgclubmanager.es', latitude: -90, longitude: -180).save()
        springSecurityService.reauthenticate(basicUser.username)

        when: "search clubs between coordinates"
        def result = clubService.findBetweenCoordinates(latitude1, longitude1, latitude2, longitude2)

        then:
        result
        !result.error
        result.clubs.every {
            it.latitude <= Math.max(latitude1, latitude2) &&
                    it.latitude >= Math.min(latitude1, latitude2) &&
                    it.longitude <= Math.max(longitude1, longitude2) &&
                    it.longitude >= Math.min(longitude1, longitude2)
        }
        Club.findAllByLatitudeBetweenAndLongitudeBetween(Math.min(latitude1, latitude2), Math.max(latitude1,
                latitude2), Math.min(longitude1, longitude2), Math.max(longitude1, longitude2)).every {
            result.clubs.contains(it)
        }
    }

    void "club must exist to be deleted"() {

        given:
        def club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(flush: true)
        springSecurityService.reauthenticate(club.owner.username)

        when: "delete non-existent club"
        def result = clubService.delete('non-existent-club-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent club"
        result = clubService.delete(club.id)

        then:
        !result.hasErrors()
        !Club.exists(club.id)
    }

    void "a club can only be deleted by an admin or its owner"() {

        given:
        def club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(flush: true)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = clubService.delete(club.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.delete.error.notAllowed'
        Club.exists(club.id)

        when: "logged-in non-admin non-owner user"
        def basicUser2 = new User(username: 'basicUser2', password: 'basicUser2', email: 'basicUser2@bgclubmanager.es',
                language: language)
        basicUser2.save()
        (new UserRole(user: basicUser2, role: basicRole)).save()
        springSecurityService.reauthenticate(basicUser2.username)
        result = clubService.delete(club.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'club.delete.error.notAllowed'
        Club.exists(club.id)

        when: "logged-in admin user"
        springSecurityService.reauthenticate(adminUser.username)
        result = clubService.delete(club.id)

        then:
        !result.hasErrors()
        !Club.exists(club.id)

        when: "logged-in owner"
        club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(flush: true)
        springSecurityService.reauthenticate(club.owner.username)
        result = clubService.delete(club.id)

        then:
        !result.hasErrors()
        !Club.exists(club.id)
    }

    void "club deletion deletes all data related to the club"() {

        given:
        def club = new Club(owner: basicUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save(flush: true)
        def club2 = new Club(owner: adminUser, name: 'testClubName2', description: 'testClubDescription2', email:
                'testClub2@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club2.save(flush: true)
        // Club memberships
        (new Member(club: club, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
        (new Member(club: club, user: adminUser, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        // Club announcements
        (new Announcement(club: club, title: 'testTile', content: 'testContent', published: true, startDatePublish:
                new Date(), endDatePublish: null)).save(flush: true)
        (new Reading(user: basicUser, announcement: Announcement.findByClub(club), date: new Date())).save(flush: true)
        // Club polls
        def poll = new Poll(club: club, title: 'testTile', description: 'testDescription', startDateVoting: new Date(),
                endDateVoting: null, published: true, startDatePublish: new Date(), endDatePublish: null)
        poll.addToOptions(new PollOption(text: 'testText1'))
        poll.addToOptions(new PollOption(text: 'testText2'))
        poll.save(flush: true)
        (new Vote(user: basicUser, pollOption: poll.options[0], date: new Date())).save(flush: true)
        // Club events
        (new Event(club: club, name: 'testName', description: 'testDescription', startDate: new Date() + 15, endDate:
                new Date() + 16, latitude: 30, longitude: 30, isPublic: false, maxAttendees: 100,
                startDateEnrollment: new Date(), endDateEnrollment: new Date() + 7, selfEnrollment:false)).save(flush: true)
        (new Organization(club: club, event: Event.findByClub(club))).save(flush: true)
        (new OrganizationRequest(club: club2, event: Event.findByClub(club), date: new Date())).save(flush: true)
        (new Attendance(user: adminUser, event: Event.findByClub(club))).save(flush: true)
        (new AttendanceRequest(user: basicUser, event: Event.findByClub(club), date: new Date())).save(flush: true)
        // Club games
        (new Game(club: club, name: 'testGame1', copies: 1)).save(flush: true)
        (new Game(club: club, name: 'testGame2', copies: 2)).save(flush: true)
        (new Game(club: club, name: 'testGame3', copies: 3)).save(flush: true)
        // Club tables
        (new Table(club: club, name: 'testTable1')).save(flush: true)
        (new Table(club: club, name: 'testTable2')).save(flush: true)
        (new Table(club: club, name: 'testTable3')).save(flush: true)
        // Club reserves
        def reserve1 = new Reserve(user: club.owner, club: club, title: 'testReserve1', startDate: new Date() + 1,
                endDate: new Date() + 2)
        reserve1.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve1.reservedTables = []
        reserve1.save(flush: true)
        def reserve2 = new Reserve(user: club.owner, club: club, title: 'testReserve2', startDate: new Date() + 3,
                endDate: new Date() + 4)
        reserve2.reservedGames = []
        reserve2.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve2.save(flush: true)
        def reserve3 = new Reserve(user: club.owner, club: club, title: 'testReserve3', startDate: new Date() + 5,
                endDate: new Date() + 6)
        reserve3.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve3.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve3.save(flush: true)
        springSecurityService.reauthenticate(adminUser.username)

        when: "delete club"
        def result = clubService.delete(club.id)

        then:
        !result.hasErrors()
        !Club.exists(club.id)
        !Member.findByClub(club)
        !Announcement.findByClub(club)
        !Poll.findByClub(club)
        !Reserve.findByClub(club)
        !Game.findByClub(club)
        !Table.findByClub(club)
    }
}
package com.bgclubmanager.club

import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class MembershipServiceSpec extends Specification {

    def grailsApplication
    def membershipService
    def springSecurityService

    def adminRole
    def adminUser
    def basicRole
    def basicUser
    def club
    def language

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName', description: 'testClubDescription', email:
                'testClub@bgclubmanager.es', phone: '123456789', latitude: 43.37125, longitude: -8.41880, urlImage:
                '/test/images/img.png')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save(flush: true)
    }

    def cleanup() {
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "membership cannot be requested without logged-in user"() {

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = membershipService.requestMembership(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.request.error.notAllowed'
        !MembershipRequest.findByClubAndUser(club, basicUser)

        when: "logged-in user"
        springSecurityService.reauthenticate(basicUser.username)
        def dateBefore = new Date()
        result = membershipService.requestMembership(club)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        MembershipRequest.findByClubAndUserAndDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "club is mandatory to request membership"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)

        when: "request membership for no club"
        def result = membershipService.requestMembership(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.request.error.noClub'
        !MembershipRequest.findByClubAndUser(null, basicUser)
        !MembershipRequest.findByClubAndUser(club, basicUser)

        when: "request membership for club"
        def dateBefore = new Date()
        result = membershipService.requestMembership(club)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        MembershipRequest.findByClubAndUserAndDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "club membership cannot be requested more than once"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)

        when: "request membership for club"
        def dateBefore = new Date()
        def result = membershipService.requestMembership(club)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        MembershipRequest.findByClubAndUserAndDateBetween(club, basicUser, dateBefore, dateAfter)

        when: "request membership again"
        result = membershipService.requestMembership(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.request.error.alreadyRequested'
        MembershipRequest.countByClubAndUser(club, basicUser) == 1
        MembershipRequest.findByClubAndUserAndDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "club membership cannot be requested by members"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)

        when: "request membership for club"
        def dateBefore = new Date()
        def result = membershipService.requestMembership(club)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        MembershipRequest.countByClubAndUser(club, basicUser) == 1
        MembershipRequest.findByClubAndUserAndDateBetween(club, basicUser, dateBefore, dateAfter)

        when: "request membership for member"
        springSecurityService.reauthenticate(club.owner.username)
        result = membershipService.requestMembership(club)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.request.error.alreadyMember'
        MembershipRequest.countByClubAndUser(club, club.owner) == 0
    }

    void "membership requests are created correctly"() {

        given:
        springSecurityService.reauthenticate(basicUser.username)

        when: "request membership for club"
        def dateBefore = new Date()
        def result = membershipService.requestMembership(club)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        result.id != null
        result.club.id == club.id
        result.user == springSecurityService.currentUser
        result.date != null
        result.date >= dateBefore
        result.date <= dateAfter
    }

    void "membership request cannot be accepted without logged-in user"() {

        given:
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = membershipService.acceptMembershipRequest(membershipRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.acceptRequest.error.notAllowed'
        MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUser(club, basicUser)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(club.owner.username)
        def dateBefore = new Date()
        result = membershipService.acceptMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "membership request is mandatory to accept membership request"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "accept membership request with no membership request"
        def result = membershipService.acceptMembershipRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.acceptRequest.error.noRequest'

        when: "accept membership request with membership request"
        result.clearErrors()
        def dateBefore = new Date()
        result = membershipService.acceptMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "users must be admins of the club to accept membership requests"() {

        given:
        def noAdminUser = new User(username: 'noAdminUser', password: 'noAdminUser', email:
                'noAdminUser@bgclubmanager.es', language: language)
        noAdminUser.save()
        (new UserRole(user: noAdminUser, role: adminRole)).save()
        (new Member(club: club, user: noAdminUser, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        springSecurityService.reauthenticate(noAdminUser.username)
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "accept membership request for non-admin club"
        def result = membershipService.acceptMembershipRequest(membershipRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.acceptRequest.error.notAllowed'
        MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUser(club, basicUser)

        when: "accept membership request for admin club"
        result.clearErrors()
        springSecurityService.reauthenticate(club.owner.username)
        def dateBefore = new Date()
        result = membershipService.acceptMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "when accepting multiple membership requests, requests are treated separately"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def validRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        validRequest.save()
        def invalidRequest = new MembershipRequest(club: club, user: club.owner, date: new Date())
        invalidRequest.save()
        MembershipRequest[] requests = [validRequest, invalidRequest]

        when: "accept multiple membership requests"
        def result = membershipService.acceptMembershipRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "membership request cannot be rejected without logged-in user"() {

        given:
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = membershipService.rejectMembershipRequest(membershipRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.rejectRequest.error.notAllowed'
        MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUser(club, basicUser)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(club.owner.username)
        def dateBefore = new Date()
        result = membershipService.rejectMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "membership request is mandatory to reject membership request"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "reject membership request with no membership request"
        def result = membershipService.rejectMembershipRequest(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.rejectRequest.error.noRequest'

        when: "reject membership request with membership request"
        result.clearErrors()
        def dateBefore = new Date()
        result = membershipService.rejectMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "users must be admins of the club to reject membership requests"() {

        given:
        def noAdminUser = new User(username: 'noAdminUser', password: 'noAdminUser', email:
                'noAdminUser@bgclubmanager.es', language: language)
        noAdminUser.save()
        (new UserRole(user: noAdminUser, role: adminRole)).save()
        (new Member(club: club, user: noAdminUser, type: Member.Type.BASIC, startDate: new Date())).save(flush: true)
        springSecurityService.reauthenticate(noAdminUser.username)
        def membershipRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        membershipRequest.save()

        when: "reject membership request for non-admin club"
        def result = membershipService.rejectMembershipRequest(membershipRequest)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.rejectRequest.error.notAllowed'
        MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUser(club, basicUser)

        when: "reject membership request for admin club"
        result.clearErrors()
        springSecurityService.reauthenticate(club.owner.username)
        def dateBefore = new Date()
        result = membershipService.rejectMembershipRequest(membershipRequest)
        def dateAfter = new Date()

        then:
        !result.hasErrors()
        !MembershipRequest.findByClubAndUser(club, basicUser)
        !Member.findByClubAndUserAndStartDateBetween(club, basicUser, dateBefore, dateAfter)
    }

    void "when rejecting multiple membership requests, requests are treated separately"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def validRequest = new MembershipRequest(club: club, user: basicUser, date: new Date())
        validRequest.save()
        def invalidRequest = null
        MembershipRequest[] requests = [validRequest, invalidRequest]

        when: "reject multiple membership requests"
        def result = membershipService.rejectMembershipRequests(requests)

        then:
        result
        result.size() == requests.size()
        result.any { it.hasErrors() }
        result.any { !it.hasErrors() }
    }

    void "membership cannot be deleted without logged-in user"() {

        given:
        def membership = new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())
        membership.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = membershipService.delete(membership.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.delete.error.notAllowed'
        Member.findByClubAndUser(membership.club, membership.user)

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(membership.user.username)
        result = membershipService.delete(membership.id)

        then:
        !result.hasErrors()
        !Member.findByClubAndUser(membership.club, membership.user)
    }

    void "membership must exist to be deleted"() {

        given:
        def membership = new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())
        membership.save()
        springSecurityService.reauthenticate(membership.user.username)

        when: "delete null membership"
        def result = membershipService.delete(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete non-existent membership"
        result.clearErrors()
        result = membershipService.delete('non-existent-membership-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "delete existent membership"
        result.clearErrors()
        result = membershipService.delete(membership.id)

        then:
        !result.hasErrors()
        !Member.findByClubAndUser(membership.club, membership.user)
    }

    void "memberships can only be deleted by a club admin or the user itself"() {

        given:
        def otherUser = new User(username: 'otherUser', password: 'otherUser', email: 'otherUser@bgclubmanager.es',
                language: language)
        otherUser.save()
        (new UserRole(user: otherUser, role: basicRole)).save()
        def membership1 = new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())
        membership1.save()
        def membership2 = new Member(club: club, user: otherUser, type: Member.Type.BASIC, startDate: new Date())
        membership2.save()

        when: "no club admin or user itself"
        springSecurityService.reauthenticate(membership1.user.username)
        def result = membershipService.delete(membership2.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.delete.error.notAllowed'

        when: "user itself"
        result.clearErrors()
        springSecurityService.reauthenticate(membership1.user.username)
        result = membershipService.delete(membership1.id)

        then:
        !result.hasErrors()
        !Member.findByClubAndUser(membership1.club, membership1.user)

        when: "club admin"
        result.clearErrors()
        springSecurityService.reauthenticate(membership2.club.owner.username)
        result = membershipService.delete(membership2.id)

        then:
        !result.hasErrors()
        !Member.findByClubAndUser(membership2.club, membership2.user)
    }

    void "cannot delete owner membership"() {

        given:
        def membership = new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())
        membership.save()
        springSecurityService.reauthenticate(club.owner.username)

        when: "delete no owner membership"
        def result = membershipService.delete(membership.id)

        then:
        !result.hasErrors()
        !Member.findByClubAndUser(membership.club, membership.user)

        when: "delete owner membership"
        result.clearErrors()
        def ownerMembership = Member.findByClubAndUser(club, club.owner)
        result = membershipService.delete(ownerMembership.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.delete.error.notAllowed'
    }

    void "membership type cannot be changed without logged-in user"() {

        given:
        def type1 = Member.Type.BASIC
        def type2 = Member.Type.ADMIN
        def membership = new Member(club: club, user: basicUser, type: type1, startDate: new Date())
        membership.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = membershipService.changeMembershipType(membership.id, type2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.changeType.error.notAllowed'
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type1

        when: "logged-in user"
        result.clearErrors()
        springSecurityService.reauthenticate(membership.club.owner.username)
        result = membershipService.changeMembershipType(membership.id, type2)

        then:
        !result.hasErrors()
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type2
    }

    void "membership must exist to have its type changed"() {

        given:
        def type1 = Member.Type.BASIC
        def type2 = Member.Type.ADMIN
        def membership = new Member(club: club, user: basicUser, type: type1, startDate: new Date())
        membership.save()
        springSecurityService.reauthenticate(membership.club.owner.username)

        when: "change null membership type"
        def result = membershipService.changeMembershipType(null, type2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "change non-existent membership type"
        result.clearErrors()
        result = membershipService.changeMembershipType('non-existent-membership-id', type2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "change existent membership type"
        result.clearErrors()
        result = membershipService.changeMembershipType(membership.id, type2)

        then:
        !result.hasErrors()
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type2
    }

    void "membership type can only be changed by club admins"() {

        given:
        def type1 = Member.Type.BASIC
        def type2 = Member.Type.ADMIN
        def membership = new Member(club: club, user: basicUser, type: type1, startDate: new Date())
        membership.save()

        when: "no member"
        def otherUser = new User(username: 'otherUser', password: 'otherUser', email: 'otherUser@bgclubmanager.es',
                language: language)
        otherUser.save()
        (new UserRole(user: otherUser, role: adminRole)).save()
        springSecurityService.reauthenticate(otherUser.username)
        def result = membershipService.changeMembershipType(membership.id, type2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.changeType.error.notAllowed'
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type1

        when: "club basic member"
        result.clearErrors()
        (new Member(club: club, user: otherUser, type: Member.Type.BASIC, startDate: new Date())).save()
        result = membershipService.changeMembershipType(membership.id, type2)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.changeType.error.notAllowed'
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type1

        when: "club admin"
        result.clearErrors()
        springSecurityService.reauthenticate(Member.admins(club).list().first().username)
        result = membershipService.changeMembershipType(membership.id, type2)

        then:
        !result.hasErrors()
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == type2
    }

    void "membership type can only be downgraded by club owner"() {

        given:
        def otherUser = new User(username: 'otherUser', password: 'otherUser', email: 'otherUser@bgclubmanager.es',
                language: language)
        otherUser.save()
        (new UserRole(user: otherUser, role: adminRole)).save()
        def membershipAdmin1 = new Member(club: club, user: basicUser, type: Member.Type.ADMIN, startDate: new Date())
        membershipAdmin1.save()
        def membershipAdmin2 = new Member(club: club, user: otherUser, type: Member.Type.ADMIN, startDate: new Date())
        membershipAdmin2.save()

        when: "no owner"
        springSecurityService.reauthenticate(basicUser.username)
        def result = membershipService.changeMembershipType(membershipAdmin2.id, Member.Type.BASIC)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.changeType.error.notAllowedDowngrade'
        Member.countByClubAndUser(membershipAdmin2.club, membershipAdmin2.user) == 1
        Member.findByClubAndUser(membershipAdmin2.club, membershipAdmin2.user).type == Member.Type.ADMIN

        when: "club owner"
        result.clearErrors()
        springSecurityService.reauthenticate(membershipAdmin2.club.owner.username)
        result = membershipService.changeMembershipType(membershipAdmin2.id, Member.Type.BASIC)

        then:
        !result.hasErrors()
        Member.countByClubAndUser(membershipAdmin2.club, membershipAdmin2.user) == 1
        Member.findByClubAndUser(membershipAdmin2.club, membershipAdmin2.user).type == Member.Type.BASIC
    }

    void "owner membership cannot be downgraded"() {

        given:
        def membership = Member.findByClubAndUser(club, club.owner)
        springSecurityService.reauthenticate(club.owner.username)

        when: "downgrade owner membership"
        def result = membershipService.changeMembershipType(membership.id, Member.Type.BASIC)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'membership.changeType.error.notAllowedDowngrade'
        Member.countByClubAndUser(membership.club, membership.user) == 1
        Member.findByClubAndUser(membership.club, membership.user).type == Member.Type.ADMIN
    }
}
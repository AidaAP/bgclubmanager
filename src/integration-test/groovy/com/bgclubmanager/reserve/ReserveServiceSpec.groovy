package com.bgclubmanager.reserve

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.core.Language
import com.bgclubmanager.game.Game
import com.bgclubmanager.table.Table
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole
import grails.test.mixin.integration.Integration
import grails.transaction.Rollback
import groovy.time.TimeCategory
import org.springframework.security.core.context.SecurityContextHolder
import spock.lang.Specification

@Integration
@Rollback
class ReserveServiceSpec extends Specification {

    def reserveService
    def grailsApplication
    def springSecurityService

    def basicRole
    def adminRole
    def basicUser
    def adminUser
    def language
    def club

    def setup() {

        language = Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)

        adminRole = Role.findByAuthority(grailsApplication.config.default.role.admin)
        basicRole = Role.findByAuthority(grailsApplication.config.default.role.user)

        adminUser = new User(username: 'adminUser', password: 'adminUser', email: 'adminUser@bgclubmanager.es',
                language: language)
        adminUser.save()
        (new UserRole(user: adminUser, role: adminRole)).save()

        basicUser = new User(username: 'basicUser', password: 'basicUser', email: 'basicUser@bgclubmanager.es',
                language: language)
        basicUser.save()
        (new UserRole(user: basicUser, role: basicRole)).save()

        club = new Club(owner: adminUser, name: 'testClubName')
        club.save()
        (new Member(club: club, user: adminUser, type: Member.Type.ADMIN, startDate: new Date())).save()
        (new Member(club: club, user: basicUser, type: Member.Type.BASIC, startDate: new Date())).save()

        (new Game(club: club, name: 'testGameName1', description: 'testGameDescription1', urlImage:
                'testGameUrlImage1', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')).save()
        (new Game(club: club, name: 'testGameName2', description: 'testGameDescription2', urlImage:
                'testGameUrlImage2', copies: 1, urlBGG: 'https://www.boardgamegeek.com/boardgame/2136/pachisi')).save()

        (new Table(club: club, name: 'testTableName', description: 'testTableDescription', maxCapacity: 10, latitude:
                null, longitude:null)).save()
    }

    def cleanup() {
        Table.findAllByClub(club)*.delete()
        Game.findAllByClub(club)*.delete()
        Member.findAllByClub(club)*.delete()
        club.delete()
        UserRole.findAllByUser(basicUser)*.delete()
        basicUser.delete()
        UserRole.findAllByUser(adminUser)*.delete()
        adminUser.delete()
    }

    void "reserve is mandatory to create reserve"() {

        given:
        def games = [Game.findByClub(club)]
        def tables = []
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)

        when: "create reserve without reserve"
        def result = reserveService.create(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.create.error.noReserve'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }

        when: "create reserve with reserve"
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }
    }

    void "reserve must pertain to a club"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def games = [Game.findByClub(club)]
        def tables = []
        def reserveCommand = new ReserveCommand(title: 'testTitle', description: 'testDescription', startDate: new
                Date() + 1, endDate: new Date() + 2, games: games, tables: tables)

        when: "create reserve without club"
        def result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.create.error.noClub'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }

        when: "create reserve with club"
        reserveCommand.club = club
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }
    }

    void "reserve cannot be created without logged-in user"() {

        given:
        def games = [Game.findByClub(club)]
        def tables = []
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.create.error.notAllowed'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }

        when: "logged-in user"
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }
    }

    void "reserve can only be created by a club member"() {

        given:
        def games = [Game.findByClub(club)]
        def tables = []
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)
        def noMemberUser = new User(username: 'noMemberUser', password: 'noMemberUser', email:
                'noMemberUser@bgclubmanager.es', language: language)
        noMemberUser.save()
        (new UserRole(user: noMemberUser, role: adminRole)).save()

        when: "create poll by no club member"
        springSecurityService.reauthenticate(noMemberUser.username)
        def result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.create.error.notAllowed'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }

        when: "create reserve by club member"
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }
    }

    void "reserves must include, at least, one game or table"() {

        given:
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2)
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)

        when: "reserve without games and tables/rooms"
        reserveCommand.games = []
        reserveCommand.tables = []
        def result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 2
        result.errors.allErrors.every { it.code == 'reserve.error.games.tables.empty' }
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }

        when: "reserve games"
        reserveCommand.games = [Game.findByClub(reserveCommand.club)]
        reserveCommand.tables = []
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        reserveCommand.games.every { ReservedGame.findByReserveAndGame(result, it) }
        reserveCommand.tables.every { ReservedTable.findByReserveAndTable(result, it) }

        when: "reserve tables"
        reserveCommand.games = []
        reserveCommand.tables = [Table.findByClub(reserveCommand.club)]
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        reserveCommand.games.every { ReservedGame.findByReserveAndGame(result, it) }
        reserveCommand.tables.every { ReservedTable.findByReserveAndTable(result, it) }

        when: "reserve games and tables"
        reserveCommand.games = [Game.findByClub(reserveCommand.club)]
        reserveCommand.tables = [Table.findByClub(reserveCommand.club)]
        reserveCommand.startDate = new Date() + 3
        reserveCommand.endDate = new Date() + 4
        result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        reserveCommand.games.every { ReservedGame.findByReserveAndGame(result, it) }
        reserveCommand.tables.every { ReservedTable.findByReserveAndTable(result, it) }
    }

    void "all reserved games must be available"() {

        given:
        def games = [Game.findByClub(club)]
        def tables = []
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)

        when: "reserve available games"
        def result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }

        when: "reserve unavailable games"
        result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.error.games.notAvailable'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
    }

    void "all reserved tables must be available"() {

        given:
        def games = []
        def tables = [Table.findByClub(club)]
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)

        when: "reserve available tables"
        def result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        games.every { ReservedGame.findByReserveAndGame(result, it) }
        tables.every { ReservedTable.findByReserveAndTable(result, it) }

        when: "reserve unavailable tables"
        result = reserveService.create(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.error.tables.notAvailable'
        !Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
    }

    void "reserves are created as specified"() {

        given:
        def games = [Game.findByClub(club)]
        def tables = [Table.findByClub(club)]
        def reserveCommand = new ReserveCommand(club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2, games: games, tables: tables)
        springSecurityService.reauthenticate(reserveCommand.club.owner.username)

        when: "create reserve"
        def result = reserveService.create(reserveCommand)

        then:
        !result.hasErrors()
        result.id
        result.user?.id == springSecurityService.currentUser.id
        result.club?.id == reserveCommand.club?.id
        result.title == reserveCommand.title
        result.description == reserveCommand.description
        result.startDate == reserveCommand.startDate
        result.endDate == reserveCommand.endDate
        result.reservedGames.size() == reserveCommand.games.size()
        result.reservedGames.every { reserveCommand.games.contains(it.game) }
        result.reservedTables.size() == reserveCommand.tables.size()
        result.reservedTables.every { reserveCommand.tables.contains(it.table) }
        Reserve.findAllByUser(springSecurityService.currentUser).any { it == result }
        Reserve.findAllByClub(reserveCommand.club).any { it == result }
    }

    void "reserve is mandatory to update reserve"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate + 1, endDate: reserve
                .endDate + 2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "update reserve without reserve"
        def result = reserveService.update(null)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.update.error.noReserve'

        when: "update reserve with reserve"
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()
    }

    void "reserve must exist to be updated"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate + 1, endDate: reserve
                .endDate + 2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "update non-existent reserve"
        reserveCommand.id = 'non_existent_reserve_id'
        def result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.update.error.notFoundReserve'

        when: "update existent reserve"
        reserveCommand.id = reserve.id
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()
    }

    void "reserve cannot be updated without logged-in user"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate + 1, endDate: reserve
                .endDate + 2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.update.error.notAllowed'

        when: "logged-in user"
        springSecurityService.reauthenticate(reserve.user.username)
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()
    }

    void "reserve can only be updated by the user that made the reserve"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate + 1, endDate: reserve
                .endDate + 2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "other logged-in user"
        springSecurityService.reauthenticate(basicUser.username)
        def result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.update.error.notAllowed'

        when: "logged-in reserve user"
        springSecurityService.reauthenticate(reserve.user.username)
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()
    }

    void "a started reserve cannot be updated"() {

        given:
        def startDate
        use(TimeCategory) {
            startDate = new Date() + 3.seconds
        }
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: startDate, endDate: startDate + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate, endDate: reserve.endDate +
                2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "update non-started reserve"
        def result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()

        when: "update started reserve"
        sleep(4000)
        result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.update.error.startedReserve'
    }

    void "updated reserves must include, at least, one game or table"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def reserveCommand = new ReserveCommand(id: reserve.id, club: reserve.club, title: "${reserve.title}Modif",
                description: "${reserve.description}Modif", startDate: reserve.startDate + 1, endDate: reserve
                .endDate + 2, games: reserve.reservedGames.collect { it.game }, tables: [])

        when: "reserve without games and tables/rooms"
        reserveCommand.games = []
        reserveCommand.tables = []
        def result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 2
        result.errors.allErrors.every { it.code == 'reserve.error.games.tables.empty' }

        when: "reserve games"
        reserveCommand.games = [Game.findByClub(reserveCommand.club)]
        reserveCommand.tables = []
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()

        when: "reserve tables"
        reserveCommand.games = []
        reserveCommand.tables = [Table.findByClub(reserveCommand.club)]
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()

        when: "reserve games and tables"
        reserveCommand.games = [Game.findByClub(reserveCommand.club)]
        reserveCommand.tables = [Table.findByClub(reserveCommand.club)]
        reserveCommand.startDate = new Date() + 3
        reserveCommand.endDate = new Date() + 4
        result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()
    }

    void "all updated reserve games must be available"() {

        given:
        def games = Game.findAllByClub(club)
        springSecurityService.reauthenticate(club.owner.username)
        def reserve1 = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle1',
                description: 'testDescription1', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve1.addToReservedGames(new ReservedGame(game: games[0]))
        reserve1.reservedTables = []
        reserve1.save()
        def reserve2 = new Reserve(user: reserve1.user, club: reserve1.club, title: 'testTitle2', description:
                'testDescription2', startDate: reserve1.startDate, endDate: reserve1.endDate)
        reserve2.addToReservedGames(new ReservedGame(game: games[1]))
        reserve2.reservedTables = []
        reserve2.save()
        def reserveCommand = new ReserveCommand(id: reserve1.id, club: reserve1.club, title: "${reserve1.title}Modif",
                description: "${reserve1.description}Modif", startDate: reserve1.startDate, endDate: reserve1
                .endDate, games: reserve1.reservedGames.collect { it.game }, tables: [])

        when: "update reserve with available games"
        def result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()

        when: "reserve unavailable games"
        reserveCommand.games = reserve2.reservedGames.collect { it.game }
        result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.error.games.notAvailable'
    }

    void "all updated reserve tables must be available"() {

        given:
        def table = Table.findByClub(club)
        springSecurityService.reauthenticate(club.owner.username)
        def reserve1 = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle1',
                description: 'testDescription1', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve1.reservedGames = []
        reserve1.addToReservedTables(new ReservedTable(table: table))
        reserve1.save()
        def reserve2 = new Reserve(user: reserve1.user, club: reserve1.club, title: 'testTitle2', description:
                'testDescription2', startDate: reserve1.startDate + 5, endDate: reserve1.endDate + 5)
        reserve2.reservedGames = []
        reserve2.addToReservedTables(new ReservedTable(table: table))
        reserve2.save()
        def reserveCommand = new ReserveCommand(id: reserve1.id, club: reserve1.club, title: "${reserve1.title}Modif",
                description: "${reserve1.description}Modif", startDate: reserve1.startDate, endDate: reserve1
                .endDate, games: [], tables: reserve1.reservedTables.collect { it.table })

        when: "update reserve with available tables"
        def result = reserveService.update(reserveCommand)

        then:
        !result.hasErrors()

        when: "reserve unavailable tables"
        reserveCommand.startDate = reserve2.startDate
        reserveCommand.endDate = reserve2.endDate
        result = reserveService.update(reserveCommand)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.error.tables.notAvailable'
    }

    void "reserve must exist to be cancelled"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()

        when: "cancel non-existent reserve"
        def result = reserveService.cancel('non-existent-reserve-id')

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'default.not.found.message'

        when: "cancel existent reserve"
        result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)
    }

    void "a reserve can only be cancelled by a club admin or the user that made the reserve"() {

        given:
        def reserve = new Reserve(user: basicUser, club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()

        when: "no logged-in user"
        SecurityContextHolder.clearContext()
        def result = reserveService.cancel(reserve.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.cancel.error.notAllowed'

        when: "logged-in basic user that did not make the reserve"
        reserve.clearErrors()
        def otherBasicUser = (new User(username: 'otherBasicUser', password: 'otherBasicUser', email:
                'otherBasicUser@bgclubmanager.es', language: language)).save()
        (new UserRole(user: otherBasicUser, role: basicRole)).save()
        springSecurityService.reauthenticate(otherBasicUser.username)
        result = reserveService.cancel(reserve.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.cancel.error.notAllowed'

        when: "logged-in user that made the reserve"
        reserve.clearErrors()
        springSecurityService.reauthenticate(reserve.user.username)
        result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)

        when: "logged-in club admin user"
        reserve = new Reserve(user: basicUser, club: club, title: 'testTitle', description: 'testDescription',
                startDate: new Date() + 3, endDate: new Date() + 4)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        springSecurityService.reauthenticate(club.owner.username)
        result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)
    }

    void "finished reserves cannot be cancelled"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)

        when: "cancel non-started reserve"
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription')
        use(TimeCategory) {
            reserve.startDate = new Date() + 10.seconds
            reserve.endDate = reserve.startDate + 10.seconds
        }
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        def result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)

        when: "cancel non-finished started reserve"
        reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription')
        use(TimeCategory) {
            reserve.startDate = new Date() + 10.seconds
            reserve.endDate = reserve.startDate + 10.seconds
        }
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        sleep(11000)
        result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)

        when: "cancel finished reserve"
        reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription')
        use(TimeCategory) {
            reserve.startDate = new Date() + 10.seconds
            reserve.endDate = reserve.startDate + 10.seconds
        }
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()
        sleep(21000)
        result = reserveService.cancel(reserve.id)

        then:
        result.hasErrors()
        result.errors.allErrors.size() == 1
        result.errors.allErrors[0].code == 'reserve.cancel.error.alreadyFinished'
    }

    void "reserve cancellation deletes all data related to the reserve"() {

        given:
        springSecurityService.reauthenticate(club.owner.username)
        def reserve = new Reserve(user: springSecurityService.currentUser, club: club, title: 'testTitle', description:
                'testDescription', startDate: new Date() + 1, endDate: new Date() + 2)
        reserve.addToReservedGames(new ReservedGame(game: Game.findByClub(club)))
        reserve.addToReservedTables(new ReservedTable(table: Table.findByClub(club)))
        reserve.save()

        when: "cancel reserve"
        def result = reserveService.cancel(reserve.id)

        then:
        !result.hasErrors()
        !ReservedGame.findByReserve(reserve)
        !ReservedTable.findByReserve(reserve)
        !Reserve.exists(reserve.id)
    }
}
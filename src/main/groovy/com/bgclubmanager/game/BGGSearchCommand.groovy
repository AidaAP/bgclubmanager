package com.bgclubmanager.game

import grails.converters.JSON
import grails.validation.Validateable

class BGGSearchCommand implements Validateable {

    String name
    List<Game> games = []

    void setStrGames(final String strGames) {

        List<Game> bggGames = []

        def jsonGames = JSON.parse(strGames)
        jsonGames.each { jsonGame ->
            bggGames += new Game(jsonGame)
        }

        this.games = bggGames
    }
}
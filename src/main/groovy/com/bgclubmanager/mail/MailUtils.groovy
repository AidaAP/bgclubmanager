package com.bgclubmanager.mail

import groovy.util.logging.Log4j

@Log4j
class MailUtils {

    def mailService
    def messageSource

    enum MailType {
        REGISTER_USER, CREATE_ADMIN, ACCEPT_MEMBERSHIP_REQUEST, REJECT_MEMBERSHIP_REQUEST, REVOKE_MEMBERSHIP,
        CHANGE_MEMBERSHIP_TYPE, ACCEPT_ATTENDANCE_REQUEST, REJECT_ATTENDANCE_REQUEST, CANCEL_ATTENDANCE, CANCEL_RESERVE
    }

    boolean sendMail(final MailType type, final Map model) {

        def mailTo = model.user.email
        def mailSubject = messageSource.getMessage("mail.subject.${type}", null, model.user.locale)
        def templatePath = "/_mail/${type}"

        return sendMail(mailTo, mailSubject, templatePath, model)
    }

    boolean sendMail(final String mailTo, final String mailSubject, final String templatePath, final Map model) {

        def result = true

        try {
            def resultado = mailService.sendMail {
                multipart true
                to mailTo
                subject mailSubject
                text(view: "$templatePath/_text", model: model)
                html(view: "$templatePath/_html", model: model)
            }
        } catch (Exception e) {
            result = false
            log.error e
        }

        return result
    }
}
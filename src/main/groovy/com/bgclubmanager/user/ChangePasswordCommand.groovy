package com.bgclubmanager.user

import grails.validation.Validateable

class ChangePasswordCommand implements Validateable {

    def springSecurityService

    String oldPassword
    String newPassword
    String confirmNewPassword

    static constraints = {
        oldPassword nullable: true, validator: { val, obj, errors ->
            if (!val?.trim()) {
                errors.rejectValue('oldPassword', 'default.null.message')
            } else {
                def currentPassword = obj.springSecurityService.currentUser.password
                if (!obj.springSecurityService.passwordEncoder.isPasswordValid(currentPassword, val, null)) {
                    errors.rejectValue('oldPassword', 'user.changePassword.error.oldPassword')
                }
            }
        }
        newPassword nullable: true, validator: { val, obj, errors ->
            if (!val?.trim()) {
                errors.rejectValue('newPassword', 'default.null.message')
            }
        }
        confirmNewPassword nullable: true, validator: { val, obj, errors ->
            if (!val?.trim()) {
                errors.rejectValue('confirmNewPassword', 'default.null.message')
            }
            if (val != obj.newPassword) {
                errors.rejectValue('confirmNewPassword', 'user.changePassword.error.confirmNewPassword')
            }
        }
    }
}
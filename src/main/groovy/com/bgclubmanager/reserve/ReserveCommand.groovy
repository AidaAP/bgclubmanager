package com.bgclubmanager.reserve

import com.bgclubmanager.club.Club
import com.bgclubmanager.game.Game
import com.bgclubmanager.table.Table
import grails.validation.Validateable

class ReserveCommand implements Validateable {

    String id
    Club club
    String title
    String description
    Date startDate
    Date endDate
    List<Game> games = new ArrayList<>()
    List<Table> tables = new ArrayList<>()

    Reserve getReserveToCreate() {
        return new Reserve(club: club, title: this.title, description: this.description, startDate: this.startDate,
                endDate: this.endDate, reservedGames: [], reservedTables: [])
    }

    void setAttributesToUpdate(final Reserve reserve) {
        reserve.club = this.club
        reserve.title = this.title
        reserve.description = this.description
        reserve.startDate = this.startDate
        reserve.endDate = this.endDate
    }

    static ReserveCommand fromReserve(final Reserve reserve) {

        def result = null

        if (reserve) {

            result = new ReserveCommand()

            result.id = reserve.id
            result.club = reserve.club
            result.title = reserve.title
            result.description = reserve.description
            result.startDate = reserve.startDate
            result.endDate = reserve.endDate
            result.games = reserve.reservedGames.collect { it.game }
            result.tables = reserve.reservedTables.collect { it.table }
        }

        return result
    }
}
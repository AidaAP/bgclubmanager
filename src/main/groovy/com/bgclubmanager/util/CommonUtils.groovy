package com.bgclubmanager.util

class CommonUtils {

    static String getItemColor(final String itemId) {

        def revId = itemId.reverse()

        def hash = 0
        (0..revId.length() - 1).each {
            hash = revId.bytes[it] + ((hash << 5) - hash)
        }

        def hex = Integer.toHexString(hash & 0x00FFFFFF)

        return "#${hex.take(6).padLeft(6, '0')}"
    }
}
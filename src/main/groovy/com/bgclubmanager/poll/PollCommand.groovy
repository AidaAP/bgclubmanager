package com.bgclubmanager.poll

import com.bgclubmanager.club.Club
import grails.validation.Validateable

class PollCommand implements Validateable {

    String id
    Club club
    String title
    String description
    Date startDateVoting
    Date endDateVoting
    boolean published
    Date startDatePublish
    Date endDatePublish
    List<Map> options = new ArrayList<>()

    static PollCommand fromParams(Map params) {

        def result = new PollCommand()

        result.id = params.id ?: null
        result.club = Club.read(params.clubId)
        result.title = params.title
        result.description = params.description

        result.startDateVoting = params.startDateVoting ? Date.parse(params.dateFormat, params.startDateVoting) : null
        result.endDateVoting = params.endDateVoting ? Date.parse(params.dateFormat, params.endDateVoting) : null

        result.published = params.published

        result.startDatePublish = params.startDatePublish ? Date.parse(params.dateFormat, params.startDatePublish) :
                null
        result.endDatePublish = params.endDatePublish ? Date.parse(params.dateFormat, params.endDatePublish) : null

        result.options = params.options

        return result
    }

    Poll getPollToCreate() {
        return new Poll(club: this.club, title: this.title, description: this.description, startDateVoting:
                this.startDateVoting, endDateVoting: this.endDateVoting, published: this.published,
                startDatePublish: this.startDatePublish, endDatePublish: this.endDatePublish)
    }

    void setAttributesToUpdate(final Poll poll) {
        poll.title = this.title
        poll.description = this.description
        poll.startDateVoting = this.startDateVoting
        poll.endDateVoting = this.endDateVoting
        poll.published = this.published
        poll.startDatePublish = this.startDatePublish
        poll.endDatePublish = this.endDatePublish
    }
}
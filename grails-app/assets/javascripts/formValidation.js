formValidation = function () {}

formValidation.showErrors = function(errors) {
    $.each(errors, function(idx, error) {
        var field = error.field;
        var $formGroup = $('[id="formGroup_' + field + '"]');
        $formGroup.addClass('has-error');
        $formGroup.find('span.help-block').text(error.message);
    });
};

formValidation.clearErrors = function() {
    var $previousErrors = $('[id^="formGroup_"].has-error');
    $previousErrors.find('span.help-block').text('');
    $previousErrors.removeClass('has-error');
};
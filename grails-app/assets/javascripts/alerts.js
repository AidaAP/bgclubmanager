alerts = function () {}

alerts.show = function(message, type, time) {

    var alert = '<div class="alert alert-' + type + ' alert-dismissible" role="alert">';
    alert += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
    alert += '<span aria-hidden="true">&times;</span>';
    alert += '</button>';
    alert += message;
    alert += '</div>';

    $('#alertContainer').append(alert);

    setTimeout(function () {
        $(".alert").alert('close');
    }, time);
}
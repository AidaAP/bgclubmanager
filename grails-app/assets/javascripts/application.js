// This is a manifest file that'll be compiled into application.js.
//
// Any JavaScript file within this directory can be referenced here using a relative path.
//
// You're free to add application-wide JavaScript to this file, but it's generally better
// to create separate JavaScript files as needed.
//
//= require jquery-2.2.0.min
//= require bootstrap
//= require dropzone.min
//= require bootstrapselect/bootstrap-select.min
//= require bootstrapselect/i18n/defaults-en_US.min
//= require bootstrapselect/i18n/defaults-es_ES.min
//= require formValidation
//= require alerts
//= require loader
//= require_self

if (typeof jQuery !== 'undefined') {
    (function($) {
        $(document).ajaxStart(function() {
            $('#spinner').fadeIn();
        }).ajaxStop(function() {
            $('#spinner').fadeOut();
        });
    })(jQuery);
}

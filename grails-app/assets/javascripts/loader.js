loader = function () {}

loader.show = function() {
    var loader = '<i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>';
    $('#loaderContainer .loader').append(loader);
    $('#loaderContainer').removeClass('hidden');
}

loader.hide = function() {
    $('#loaderContainer').addClass('hidden');
    $('#loaderContainer .loader').empty();
}
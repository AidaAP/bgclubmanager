package com.bgclubmanager.reserve

import com.bgclubmanager.club.Club
import com.bgclubmanager.mail.MailUtils
import grails.transaction.Transactional
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class ReserveService {

    def mailUtils
    def messageSource
    def springSecurityService

    def create(final ReserveCommand reserveCommand) {

        def result = reserveCommand?.getReserveToCreate() ?: new Reserve()

        // We must specify the reserve to create
        if (!reserveCommand) {
            result.errors.reject('reserve.create.error.noReserve')
            return result
        }

        // Reserve must pertain to a club
        if (!reserveCommand.club) {
            result.errors.reject('reserve.create.error.noClub')
            return result
        }

        def user = springSecurityService.currentUser

        // Users must be logged-in to create a reserve
        if (!user) {
            result.errors.reject('reserve.create.error.notAllowed')
            return result
        }

        // Users must be club members to create a reserve
        if (!user.isMemberOf(reserveCommand.club)) {
            result.errors.reject('reserve.create.error.notAllowed')
            return result
        }

        result.user = user

        return createUpdate(reserveCommand, result)
    }

    def update(final ReserveCommand reserveCommand) {

        def result = Reserve.get(reserveCommand?.id) ?: new Reserve()

        // We must specify the reserve to update
        if (!reserveCommand) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('reserve.update.error.noReserve')
            return result
        }

        // Reserve must exist
        if (!Reserve.exists(result.id)) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('reserve.update.error.notFoundReserve')
            return result
        }

        def user = springSecurityService.currentUser

        // Users must be logged-in to update a reserve
        if (!user) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('reserve.update.error.notAllowed')
            return result
        }

        // Reserve can only be updated by the user that made the reserve
        if (user != result.user) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('reserve.update.error.notAllowed')
            return result
        }

        // A started reserve cannot be updated
        if (result.isStarted()) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('reserve.update.error.startedReserve')
            return result
        }

        reserveCommand?.setAttributesToUpdate(result)

        return createUpdate(reserveCommand, result)
    }

    def createUpdate(final ReserveCommand reserveCommand, final Reserve reserve) {

        // Reserves must include, at least, one game or table
        if (!reserveCommand.games && !reserveCommand.tables) {
            transactionStatus.setRollbackOnly()
            reserve.errors.rejectValue('reservedGames', 'reserve.error.games.tables.empty')
            reserve.errors.rejectValue('reservedTables', 'reserve.error.games.tables.empty')
            return reserve
        }

        // All games must be available in the dates
        if (reserveCommand.games.any { !it.isAvailable(reserveCommand.startDate, reserveCommand.endDate, reserve.id) }) {
            transactionStatus.setRollbackOnly()
            reserve.errors.rejectValue('reservedGames', 'reserve.error.games.notAvailable')
            return reserve
        }

        // All tables must be available in the dates
        if (reserveCommand.tables.any { !it.isAvailable(reserveCommand.startDate, reserveCommand.endDate, reserve.id) }) {
            transactionStatus.setRollbackOnly()
            reserve.errors.rejectValue('reservedTables', 'reserve.error.tables.notAvailable')
            return reserve
        }

        // Add games
        reserve.reservedGames.clear()
        reserveCommand.games.each {
            reserve.addToReservedGames(new ReservedGame(game: it))
        }

        // Add tables
        reserve.reservedTables.clear()
        reserveCommand.tables.each {
            reserve.addToReservedTables(new ReservedTable(table: it))
        }

        reserve.save()

        return reserve
    }

    @Transactional(readOnly = true)
    def findReserves(final Club club, final String[] games, final String[] tables, final Date fromDate, final Date
            toDate) {

        def result = [:]

        def user = springSecurityService.currentUser

        // Users must be logged-in to find reserves
        if (!user) {
            result.error = messageSource.getMessage('reserve.find.error.notAllowed', null, LocaleContextHolder.locale)
            return result
        }

        def reserves = Reserve.createCriteria().list {
            if (club) {
                // Find all club reserves
                eq('club', club)
            } else {
                // Find all user reserves
                eq('user', user)
            }
            if (fromDate) {
                gt('endDate', fromDate)
            }
            if (toDate) {
                lt('startDate', toDate)
            }
        }

        if (games) {
            reserves = ReservedGame.createCriteria().list {
                projections { distinct('reserve') }
                'in'('reserve', reserves)
                game {
                    'in'('id', games)
                }
            }
        }

        if (tables) {
            reserves = ReservedTable.createCriteria().list {
                projections { distinct('reserve') }
                'in'('reserve', reserves)
                table {
                    'in'('id', tables)
                }
            }
        }

        result.reserves = reserves

        return result
    }

    def cancel(final String reserveId) {

        def result = new Reserve()

        // Reserve must exist
        def reserveDB = Reserve.get(reserveId)
        if (!reserveDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('reserve.default.label',
                    null, LocaleContextHolder.locale), reserveId] as Object[], null)
            return result
        }

        def user = springSecurityService.currentUser

        // A reserve can only be cancelled by a club admin or the user that made the reserve
        if (!user?.isAdminOf(reserveDB.club) && (user != reserveDB.user)) {
            result.errors.reject('reserve.cancel.error.notAllowed')
            return result
        }

        // Finished reserves cannot be cancelled
        if (reserveDB.isFinished()) {
            result.errors.reject('reserve.cancel.error.alreadyFinished')
            return result
        }

        reserveDB.delete()

        // Send mail
        if (!reserveDB.hasErrors() && (user != reserveDB.user)) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.CANCEL_RESERVE, [user: reserveDB.user, reserve:
                    reserveDB])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set attendance request errors
                result.errors.reject('reserve.cancel.error')
            }
        }

        return reserveDB
    }
}

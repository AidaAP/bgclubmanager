package com.bgclubmanager.game

import com.bgclubmanager.club.Club
import com.bgclubmanager.reserve.Reserve
import grails.transaction.Transactional
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class GameService {

    def fileService
    def messageSource
    def springSecurityService

    def create(final Game game) {

        def result = game ?: new Game()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create a game
        if (!user) {
            result.errors.reject('game.create.error.notAllowed')
            return result
        }

        // We must specify the game to create
        if (!game) {
            result.errors.reject('game.create.error.noGame')
            return result
        }

        // Game must pertain to a club
        if (!game.club) {
            result.errors.reject('game.create.error.noClub')
            return result
        }

        // A game can only be created by a club admin
        if (!user.isAdminOf(game.club)) {
            result.errors.reject('game.create.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    def update(final Game game) {

        def result = game ?: new Game()

        def user = springSecurityService.currentUser

        // Users must be logged-in to update a game
        if (!user) {
            result.errors.reject('game.update.error.notAllowed')
            return result
        }

        // We must specify the game to update
        if (!game) {
            result.errors.reject('game.update.error.noGame')
            return result
        }

        // Game must exist
        if (!Game.exists(result.id)) {
            result.errors.reject('game.update.error.notFoundGame')
            return result
        }

        // A game can only be updated by a club admin
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('game.update.error.notAllowed')
            return result
        }

        def oldUrlImage = Game.withNewSession { return Game.read(result.id)?.urlImage }

        result.save()

        if (!result.hasErrors()) {
            // Delete old image if needed
            if (oldUrlImage && (oldUrlImage != result.urlImage)) {
                fileService.deleteFile(oldUrlImage)
            }
        }

        return result
    }

    @Transactional(readOnly = true)
    def getGames(final Club club, final Integer offset, final Integer max) {

        def games = Game.createCriteria().list(offset: offset ?: 0, max: max ?: 0) {
            if (club) {
                eq('club', club)
            }
            order('name', 'asc')
        }

        return [ list: games, totalCount: games.totalCount ]
    }

    def delete(final String gameId) {

        def result = new Game()

        // Game must exist
        def gameDB = Game.get(gameId)
        if (!gameDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('game.default.label', null,
                    LocaleContextHolder.locale), gameId] as Object[], null)
            return result
        }

        // A game can only be deleted by a club admin
        if (!springSecurityService.currentUser?.isAdminOf(gameDB.club)) {
            result.errors.reject('game.delete.error.notAllowed')
            return result
        }

        // Delete reserves for this table
        Reserve.withGame(gameDB).list()?.each { it.delete() }

        gameDB.delete()

        if (!gameDB.hasErrors() && gameDB.urlImage) {
            // Delete game image
            fileService.deleteFile(gameDB.urlImage)
        }

        return gameDB
    }
}

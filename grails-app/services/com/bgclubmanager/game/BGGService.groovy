package com.bgclubmanager.game

import grails.transaction.Transactional
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.springframework.beans.factory.annotation.Value

@Transactional
class BGGService {

    @Value('${bgg.api.url}')
    final String BASE_URL

    @Value('${bgg.api.uriPath.search}')
    final String URI_SEARCH

    @Value('${bgg.api.uriPath.detail}')
    final String URI_DETAIL

    @Value('${bgg.gameUrlTemplate}')
    final String TEMPLATE_GAME_URL

    def messageSource

    @Transactional(readOnly = true)
    def search(final String gameName) {

        Game[] result = []

        if (gameName) {

            def http = new HTTPBuilder(BASE_URL)

            // Obtain game ids
            def idNameList = []
            http.request(Method.GET, ContentType.XML) { req ->

                uri.path = URI_SEARCH
                uri.query = [query: gameName]

                response.success = { resp, reader ->
                    reader.item.each {
                        def id = it.@id.text()
                        if (!idNameList.any { item -> item.id == id }) {
                            idNameList << [id: id, name: it.name.@value.text()]
                        }
                    }
                }

                response.failure = { resp, reader ->
                    log.error "Error searching games by name (${resp.status}): ${reader}"
                }
            }

            if (idNameList) {

                // Obtain games
                http.request(Method.GET, ContentType.XML) { req ->

                    uri.path = URI_DETAIL
                    uri.query = [id: idNameList*.id.join(',')]

                    response.success = { resp, reader ->
                        result = reader.item.collect { item ->
                            new Game(name: idNameList.find { it.id == item.@id.text() }.name,
                                    description: item.description.text(),
                                    urlImage: item.thumbnail?.text() ?: item.image?.text(),
                                    urlBGG: TEMPLATE_GAME_URL.replace('GAME_TYPE', item.@type.text()).replace('GAME_ID', item.@id.text()))
                        }
                    }

                    response.failure = { resp, reader ->
                        log.error "Error retrieving items by ids (${resp.status}): ${reader}"
                    }
                }
            }
        }

        return result
    }
}

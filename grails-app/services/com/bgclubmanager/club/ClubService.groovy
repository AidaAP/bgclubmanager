package com.bgclubmanager.club

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.event.Event
import com.bgclubmanager.event.Organization
import com.bgclubmanager.event.OrganizationRequest
import com.bgclubmanager.game.Game
import com.bgclubmanager.poll.Poll
import com.bgclubmanager.poll.Vote
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.table.Table
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class ClubService {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def fileService
    def messageSource
    def springSecurityService

    @Transactional(readOnly = true)
    def listAll(final Integer offsetParam, final Integer maxParam, final String sortParam, final String orderParam) {

        def list = Club.createCriteria().list {
            projections {
                property('id', 'id')
                property('name', 'name')
                property('urlImage', 'urlImage')
            }
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            firstResult(offsetParam ?: 0)
            maxResults(maxParam ?: 0)
            order(sortParam ?: 'name', orderParam ?: 'asc')
        }
        def totalCount = Club.count

        return [ list: list, totalCount: totalCount ]
    }

    def create(final Club club) {

        def result = club ?: new Club()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create a club
        if (!user) {
            result.errors.reject('club.create.error.notAllowed')
            return result
        }

        // We must specify the club to create
        if (!club) {
            result.errors.reject('club.create.error.noClub')
            return result
        }

        // Assign owner
        result.owner = user

        result.save()

        // Assign first admin (owner)
        if (!result.hasErrors()) {

            def adminMember = new Member(club: result, user: user, type: Member.Type.ADMIN, startDate: new Date())
            adminMember.save()

            if (adminMember.hasErrors()) {
                // Revert created club
                transactionStatus.setRollbackOnly()
                // Set club errors
                result.errors.reject('club.create.error')
            }
        }

        return result
    }

    def update(final Club club) {

        // We must specify the club to update
        if (!club) {
            def result = new Club()
            result.errors.reject('club.update.error.noClub')
            return result
        }

        // Club must exist
        if (!Club.exists(club.id)) {
            club.errors.reject('club.update.error.notFoundClub')
            return club
        }

        // Clubs can only be updated by admin users or its admins
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && (!Member.admins(club).list().contains
                (springSecurityService.currentUser))) {
            club.errors.reject('club.update.error.notAllowed')
            return club
        }

        def oldUrlImage = Club.withNewSession { return Club.read(club.id)?.urlImage }

        club.save()

        if (!club.hasErrors()) {
            // Delete old image if needed
            if (oldUrlImage && (oldUrlImage != club.urlImage)) {
                fileService.deleteFile(oldUrlImage)
            }
        }

        return club
    }

    @Transactional(readOnly = true)
    Map findBetweenCoordinates(final Float latitude1, final Float longitude1, final Float latitude2, final Float
            longitude2) {

        def result = [:]

        // Users must be logged-in to find clubs between coordinates
        if (!springSecurityService.isLoggedIn()) {
            result.error = messageSource.getMessage('club.findBetweenCoordinates.error.notAllowed', null,
                    LocaleContextHolder.locale)
            return result
        }

        // We must specify all parameters
        if (!(latitude1 && longitude1 && latitude2 && longitude2)) {
            result.error = messageSource.getMessage('club.findBetweenCoordinates.error.missingParams', null,
                    LocaleContextHolder.locale)
            return result
        }

        def minLatitude = Math.min(latitude1, latitude2)
        def maxLatitude = Math.max(latitude1, latitude2)
        def minLongitude = Math.min(longitude1, longitude2)
        def maxLongitude = Math.max(longitude1, longitude2)

        result.clubs = Club.findAllByLatitudeBetweenAndLongitudeBetween(minLatitude, maxLatitude, minLongitude, maxLongitude)

        return result
    }

    def delete(final String clubId) {

        def result = new Club()

        // Club must exist
        def clubDB = Club.get(clubId)
        if (!clubDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('club.default.label',
                    null, LocaleContextHolder.locale), clubId] as Object[], null)
            return result
        }

        // A club can only be deleted by an admin or its owner
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && (clubDB.owner.id != springSecurityService.currentUserId)) {
            result.errors.reject('club.delete.error.notAllowed')
            return result
        }

        // Delete club membership requests
        MembershipRequest.findAllByClub(clubDB).each { it.delete() }

        // Delete club memberships
        Member.findAllByClub(clubDB).each { it.delete() }

        // Delete club announcements
        Announcement.findAllByClub(clubDB).each { announcement ->
            Reading.findAllByAnnouncement(announcement).each { it.delete() }
            announcement.delete()
        }

        // Delete club polls
        Poll.findAllByClub(clubDB).each { poll ->
            Vote.fromPoll(poll).list().each { it.delete() }
            poll.delete()
        }

        // Delete club events
        Event.findAllByClub(clubDB).each { event ->
            OrganizationRequest.findAllByEvent(event).each { it.delete() }
            Organization.findAllByEvent(event).each { it.delete() }
            AttendanceRequest.findAllByEvent(event).each { it.delete() }
            Attendance.findAllByEvent(event).each { it.delete() }
            event.delete()
        }

        // Delete club organization requests
        OrganizationRequest.findAllByClub(clubDB).each { it.delete() }

        // Delete club organizations
        Organization.findAllByClub(clubDB).each { it.delete() }

        // Delete club reserves
        Reserve.findAllByClub(clubDB).each { it.delete() }

        // Delete club games
        Game.findAllByClub(clubDB).each {
            it.delete()
            if (!it.hasErrors() && it.urlImage) {
                // Delete game image
                fileService.deleteFile(it.urlImage)
            }
        }

        // Delete club tables
        Table.findAllByClub(clubDB).each { it.delete() }

        clubDB.delete()

        if (!clubDB.hasErrors() && clubDB.urlImage) {
            // Delete club image
            fileService.deleteFile(clubDB.urlImage)
        }

        return clubDB
    }
}
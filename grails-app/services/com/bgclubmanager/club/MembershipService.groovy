package com.bgclubmanager.club

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.mail.MailUtils
import com.bgclubmanager.reserve.Reserve
import grails.transaction.Transactional
import org.hibernate.criterion.CriteriaSpecification
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class MembershipService {

    def mailUtils
    def messageSource
    def springSecurityService

    def requestMembership(final Club club) {

        def result = new MembershipRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to request club membership
        if (!user) {
            result.errors.reject('membership.request.error.notAllowed')
            return result
        }

        result.user = user

        // We must specify the club to which request membership
        if (!club) {
            result.errors.reject('membership.request.error.noClub')
            return result
        }

        result.club = club

        // User must not have a current request
        if (MembershipRequest.findByClubAndUser(club, user)) {
            result.errors.reject('membership.request.error.alreadyRequested')
            return result
        }

        // User must not be a club member
        if (user.isMemberOf(club)) {
            result.errors.reject('membership.request.error.alreadyMember')
            return result
        }

        result.date = new Date()

        result.save()

        return result
    }

    @Transactional(readOnly = true)
    def getMembershipRequests(final Club club, final Integer offset, final Integer max) {

        def user = springSecurityService.currentUser
        def clubFilter = club ? [club] : Member.adminClubs(user).list()

        def paramOffset = offset ?: 0
        def paramMax = max ?: 0

        return MembershipRequest.createCriteria().list(offset: paramOffset, max: paramMax) {
            if (clubFilter) {
                inList('club', clubFilter)
            }
            order('date', 'desc')
        }
    }

    def acceptMembershipRequests(final MembershipRequest[] membershipRequests) {

        def result = []

        membershipRequests.each {
            result << acceptMembershipRequest(it)
        }

        return result
    }

    def acceptMembershipRequest(final MembershipRequest membershipRequest) {

        def result = new MembershipRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to accept membership requests
        if (!user) {
            result.errors.reject('membership.acceptRequest.error.notAllowed')
            return result
        }

        // We must specify the membership request to accept
        if (!membershipRequest) {
            result.errors.reject('membership.acceptRequest.error.noRequest')
            return result
        }

        result = membershipRequest

        // Users must be admins of the club to accept membership requests
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('membership.acceptRequest.error.notAllowed')
            return result
        }

        // Remove membership request
        result.delete()

        // Create membership
        if (!result.hasErrors()) {

            def member = new Member(club: result.club, user: result.user, type: Member.Type.BASIC, startDate: new Date())
            member.save()

            if (member.hasErrors()) {
                // Revert removed membership request
                transactionStatus.setRollbackOnly()
                // Set membership request errors
                result.errors.reject('membership.acceptRequest.error')
            }
        }

        // Send mail
        if (!result.hasErrors()) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.ACCEPT_MEMBERSHIP_REQUEST,
                    [user: membershipRequest.user, club: membershipRequest.club])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set membership request errors
                result.errors.reject('membership.acceptRequest.error')
            }
        }

        // Mark visible news as read
        if (!result.hasErrors()) {
            def announcements = Announcement.visible.list() {
                eq('club', membershipRequest.club)
            }
            announcements?.each {
                (new Reading(user: membershipRequest.user, announcement: it, date: new Date())).save(flush: true)
            }
        }

        return result
    }

    def rejectMembershipRequests(final MembershipRequest[] membershipRequests) {

        def result = []

        membershipRequests.each {
            result << rejectMembershipRequest(it)
        }

        return result
    }

    def rejectMembershipRequest(final MembershipRequest membershipRequest) {

        def result = new MembershipRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to reject membership requests
        if (!user) {
            result.errors.reject('membership.rejectRequest.error.notAllowed')
            return result
        }

        // We must specify the membership request to reject
        if (!membershipRequest) {
            result.errors.reject('membership.rejectRequest.error.noRequest')
            return result
        }

        result = membershipRequest

        // Users must be admins of the club to reject membership requests
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('membership.rejectRequest.error.notAllowed')
            return result
        }

        // Remove membership request
        result.delete()

        // Send mail
        if (!result.hasErrors()) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.REJECT_MEMBERSHIP_REQUEST,
                    [user: membershipRequest.user, club: membershipRequest.club])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set membership request errors
                result.errors.reject('membership.acceptRequest.error')
            }
        }

        return result
    }

    @Transactional(readOnly = true)
    def getMembers(final Club club, final Integer offset, final Integer max) {

        return Member.createCriteria().list(offset: offset ?: 0, max: max ?: 0) {
            createAlias('user', 'u')
            projections {
                property('u.id', 'id')
                property('u.username', 'username')
                property('u.name', 'name')
                property('u.surname', 'surname')
                property('u.urlImage', 'urlImage')
            }
            eq('club', club)
            resultTransformer(CriteriaSpecification.ALIAS_TO_ENTITY_MAP)
            order('u.username', 'asc')
        }
    }

    def delete(final String memberId, boolean sendMail = false) {

        def result = new Member()

        def currentUser = springSecurityService.currentUser

        // Users must be logged-in to delete membership
        if (!currentUser) {
            result.errors.reject('membership.delete.error.notAllowed')
            return result
        }

        // Membership must exist
        def memberDB = Member.get(memberId)
        if (!memberDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('membership.default.label',
                    null, LocaleContextHolder.locale), memberId] as Object[], null)
            return result
        }

        result = memberDB

        // A membership can only be deleted by a club admin or the user itself
        if ((result.user.id != currentUser.id) && (!currentUser.isAdminOf(result.club))) {
            result.errors.reject('membership.delete.error.notAllowed')
            return result
        }

        // Cannot delete owner membership
        if (result.club.owner.id == result.user.id) {
            result.errors.reject('membership.delete.error.notAllowed')
            return result
        }

        // Delete future user reserves in club
        Reserve.findAllByUserAndClubAndStartDateGreaterThanEquals(result.user, result.club, new Date()).each { it.delete() }

        result.delete()

        // Send mail
        if (sendMail && !result.hasErrors()) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.REVOKE_MEMBERSHIP, [user: result.user, club: result.club])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set membership errors
                result.errors.reject('membership.delete.error')
            }
        }

        return result
    }

    def deleteMultiple(final String[] memberIds) {

        def result = []

        memberIds.each {
            result << delete(it, true)
        }

        return result
    }

    def changeMultipleMembershipTypes(final String[] memberIds, final Member.Type newType) {

        def result = []

        memberIds.each {
            result << changeMembershipType(it, newType)
        }

        return result
    }

    def changeMembershipType(final String memberId, final Member.Type newType) {

        def result = new Member()

        def currentUser = springSecurityService.currentUser

        // Users must be logged-in to change membership type
        if (!currentUser) {
            result.errors.reject('membership.changeType.error.notAllowed')
            return result
        }

        // Membership must exist
        def memberDB = Member.get(memberId)
        if (!memberDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('membership.default.label',
                    null, LocaleContextHolder.locale), memberId] as Object[], null)
            return result
        }

        result = memberDB

        // Membership type can only be changed by club admins
        if (!currentUser.isAdminOf(memberDB.club)) {
            result.errors.reject('membership.changeType.error.notAllowed')
            return result
        }

        // Membership type can only be downgraded by club owner
        if (Member.Type.BASIC.equals(newType) && (memberDB.club.owner.id != currentUser.id)) {
            result.errors.reject('membership.changeType.error.notAllowedDowngrade')
            return result
        }

        // Cannot downgrade owner membership
        if (Member.Type.BASIC.equals(newType) && (memberDB.club.owner.id == memberDB.user.id)) {
            result.errors.reject('membership.changeType.error.notAllowedDowngrade')
            return result
        }

        if (result.type != newType) {

            def type1 = "membership.type.${result.type}"

            result.type = newType
            result.save()

            // Send mail
            if (!result.hasErrors()) {

                def type2 = "membership.type.${result.type}"

                def mailOk = mailUtils.sendMail(MailUtils.MailType.CHANGE_MEMBERSHIP_TYPE, [user: result.user,
                                                                                            club: result.club,
                                                                                            type1: type1, type2: type2])

                if (!mailOk) {
                    // Revert previous actions
                    transactionStatus.setRollbackOnly()
                    // Set membership errors
                    result.errors.reject('membership.changeType.error')
                }
            }
        }

        return result
    }
}
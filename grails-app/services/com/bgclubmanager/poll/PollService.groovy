package com.bgclubmanager.poll

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import grails.transaction.Transactional
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class PollService {

    def messageSource
    def springSecurityService

    def create(final PollCommand pollCommand) {

        def result = pollCommand?.getPollToCreate() ?: new Poll()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create a poll
        if (!user) {
            result.errors.reject('poll.create.error.notAllowed')
            return result
        }

        // We must specify the poll to create
        if (!pollCommand) {
            result.errors.reject('poll.create.error.noPoll')
            return result
        }

        // Poll must pertain to a club
        if (!result.club) {
            result.errors.reject('poll.create.error.noClub')
            return result
        }

        // A poll can only be created by a club admin
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('poll.create.error.notAllowed')
            return result
        }

        // Add options
        pollCommand.options.each {
            result.addToOptions(new PollOption(text: it.text))
        }

        result.save()

        return result
    }

    def update(final PollCommand pollCommand) {

        def result = Poll.get(pollCommand?.id) ?: new Poll()
        pollCommand?.setAttributesToUpdate(result)

        def user = springSecurityService.currentUser

        // Users must be logged-in to update a poll
        if (!user) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('poll.update.error.notAllowed')
            return result
        }

        // We must specify the poll to update
        if (!pollCommand) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('poll.update.error.noPoll')
            return result
        }

        // Poll must exist
        if (!Poll.exists(result.id)) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('poll.update.error.notFoundPoll')
            return result
        }

        // A poll can only be updated by a club admin
        if (!user.isAdminOf(result.club)) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('poll.update.error.notAllowed')
            return result
        }

        // A poll cannot be updated if it is already voted
        if (result.hasVotes()) {
            transactionStatus.setRollbackOnly()
            result.errors.reject('poll.update.error.hasVotes')
            return result
        }

        // Add options
        def removeOptionIds = result.options.collect { it.id }
        pollCommand.options.each {
            if (it.id) {
                result.options.find { option -> option.id == it.id }.text = it.text
                removeOptionIds.remove(it.id)
            } else {
                result.addToOptions(new PollOption(text: it.text))
            }
        }
        removeOptionIds.each {
            result.removeFromOptions(PollOption.read(it))
        }

        result.save()

        if (result.hasErrors()) {
            transactionStatus.setRollbackOnly()
        }

        return result
    }

    @Transactional(readOnly = true)
    def getPolls(final Club club, final Integer offset, final Integer max) {

        def list = []
        def totalCount = 0

        def user = springSecurityService.currentUser
        def clubFilter = club ? [club] : Member.memberships(user).list()

        if (clubFilter) {

            def isAdmin = false
            if (club) {
                isAdmin = user.isAdminOf(club)
            }

            def pollCriteria = isAdmin ? Poll.createCriteria() : Poll.visible

            def polls = pollCriteria.list(offset: offset ?: 0, max: max ?: 0) {
                inList('club', clubFilter)
                order('published', 'asc')
                order('startDatePublish', 'desc')
            }

            list = polls.collect { [ poll: it, votable: it.isVotable() && !it.isVotedBy(user) ] }
            totalCount = polls.totalCount
        }

        return [ list: list, totalCount: totalCount ]
    }

    @Transactional(readOnly = true)
    def getUnvotedPolls(final Integer offset, final Integer max) {

        def list = []
        def totalCount = 0

        def user = springSecurityService.currentUser
        def clubFilter = Member.memberships(user).list()

        if (clubFilter) {

            def votedPollIds = Vote.findAllByUser(user)?.collect { it.pollOption.poll.id }

            def polls = Poll.visibleAndVotable.list(offset: offset ?: 0, max: max ?: 0) {
                inList('club', clubFilter)
                if (votedPollIds) {
                    not {
                        inList('id', votedPollIds)
                    }
                }
                order('published', 'asc')
                order('startDatePublish', 'desc')
            }

            list = polls
            totalCount = polls.totalCount
        }

        return [ list: list, totalCount: totalCount ]
    }

    def vote(final PollOption pollOption) {

        def user = springSecurityService.currentUser

        def result = new Vote(user: user, pollOption: pollOption, date: new Date())

        // Users must be logged-in to vote a poll
        if (!user) {
            result.errors.reject('poll.vote.error.notAllowed')
            return result
        }

        // We must specify the poll option to vote
        if (!pollOption) {
            result.errors.reject('poll.vote.error.noPollOption')
            return result
        }

        // User must be a member of the club
        if (!user.isMemberOf(pollOption.poll?.club)) {
            result.errors.reject('poll.vote.error.notAllowed')
            return result
        }

        // Poll must be visible
        if (!pollOption.poll?.isVisible()) {
            result.errors.reject('poll.vote.error.notVisible')
            return result
        }

        // Poll must be votable
        if (!pollOption.poll?.isVotable()) {
            result.errors.reject('poll.vote.error.notVotable')
            return result
        }

        // Users can only vote a poll once
        if (pollOption.poll?.isVotedBy(user)) {
            result.errors.reject('poll.vote.error.alreadyVoted')
            return result
        }

        // Vote
        result.save()

        return result
    }

    def delete(final String pollId) {

        def result = new Poll()

        // Poll must exist
        def pollDB = Poll.get(pollId)
        if (!pollDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('poll.default.label',
                    null, LocaleContextHolder.locale), pollId] as Object[], null)
            return result
        }

        // A poll can only be deleted by a club admin
        if (!springSecurityService.currentUser?.isAdminOf(pollDB.club)) {
            result.errors.reject('poll.delete.error.notAllowed')
            return result
        }

        // Delete poll votes
        Vote.fromPoll(pollDB).list().each { it.delete() }

        pollDB.delete()

        return pollDB
    }
}
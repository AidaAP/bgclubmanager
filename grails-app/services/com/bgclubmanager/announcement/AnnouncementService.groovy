package com.bgclubmanager.announcement

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class AnnouncementService {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def messageSource
    def springSecurityService

    def create(final Announcement announcement) {

        def result = announcement ?: new Announcement()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create an announcement
        if (!user) {
            result.errors.reject('announcement.create.error.notAllowed')
            return result
        }

        // We must specify the announcement to create
        if (!announcement) {
            result.errors.reject('announcement.create.error.noAnnouncement')
            return result
        }

        // Announcement must pertain to a club
        if (!announcement.club) {
            result.errors.reject('announcement.create.error.noClub')
            return result
        }

        // An announcement can only be created by a club admin
        if (!user.isAdminOf(announcement.club)) {
            result.errors.reject('announcement.create.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    def update(final Announcement announcement) {

        def result = announcement ?: new Announcement()

        def user = springSecurityService.currentUser

        // Users must be logged-in to update an announcement
        if (!user) {
            result.errors.reject('announcement.update.error.notAllowed')
            return result
        }

        // We must specify the announcement to update
        if (!announcement) {
            result.errors.reject('announcement.update.error.noAnnouncement')
            return result
        }

        // Announcement must exist
        if (!Announcement.exists(result.id)) {
            result.errors.reject('announcement.update.error.notFoundAnnouncement')
            return result
        }

        // An announcement can only be updated by a club admin
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('announcement.update.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    @Transactional(readOnly = true)
    def getAnnouncements(final Club club, final Integer offset, final Integer max) {

        def list = []
        def totalCount = 0

        def user = springSecurityService.currentUser
        def clubFilter = club ? [club] : Member.memberships(user).list()

        if (clubFilter) {

            def isAdmin = false
            if (club) {
                isAdmin = user.isAdminOf(club)
            }

            def announcementCriteria = isAdmin ? Announcement.createCriteria() : Announcement.visible

            def announcements = announcementCriteria.list(offset: offset ?: 0, max: max ?: 0) {
                inList('club', clubFilter)
                order('published', 'asc')
                order('startDatePublish', 'desc')
            }

            list = announcements.collect { [ announcement: it, read: it.readBy(user) ] }
            totalCount = announcements.totalCount
        }

        return [ list: list, totalCount: totalCount ]
    }

    @Transactional(readOnly = true)
    def getUnreadAnnouncements(final Integer offset, final Integer max) {

        def list = []
        def totalCount = 0

        def user = springSecurityService.currentUser
        def clubFilter = Member.memberships(user).list()

        if (clubFilter) {

            def readAnnouncementIds = Reading.findAllByUser(user)?.collect { it.announcement.id }

            def announcements = Announcement.visible.list(offset: offset ?: 0, max: max ?: 0) {
                inList('club', clubFilter)
                if (readAnnouncementIds) {
                    not {
                        inList('id', readAnnouncementIds)
                    }
                }
                order('published', 'asc')
                order('startDatePublish', 'desc')
            }

            list = announcements
            totalCount = announcements.totalCount
        }

        return [ list: list, totalCount: totalCount ]
    }

    def read(final Announcement announcement) {

        def result = announcement ?: new Announcement()

        def user = springSecurityService.currentUser

        // Users must be logged-in to read an announcement
        if (!user) {
            result.errors.reject('announcement.read.error.notAllowed')
            return result
        }

        // We must specify the announcement to read
        if (!announcement) {
            result.errors.reject('announcement.read.error.noAnnouncement')
            return result
        }

        // User must be and admin or a member of the club
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !user.isMemberOf(announcement.club)) {
            result.errors.reject('announcement.read.error.notAllowed')
            return result
        }

        // Mark announcement as read for the first time
        if (!Reading.findByUserAndAnnouncement(user, announcement)) {
            def reading = new Reading(user: user, announcement: announcement, date: new Date())
            reading.save()
        }

        return result
    }

    def delete(final String announcementId) {

        def result = new Announcement()

        // Announcement must exist
        def announcementDB = Announcement.get(announcementId)
        if (!announcementDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('announcement.default.label',
                    null, LocaleContextHolder.locale), announcementId] as Object[], null)
            return result
        }

        // An announcement can only be deleted by a club admin
        if (!springSecurityService.currentUser?.isAdminOf(announcementDB.club)) {
            result.errors.reject('announcement.delete.error.notAllowed')
            return result
        }

        // Delete announcement readings
        Reading.findAllByAnnouncement(announcementDB).each { it.delete() }

        announcementDB.delete()

        return announcementDB
    }
}
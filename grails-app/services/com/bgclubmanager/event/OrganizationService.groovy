package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import grails.transaction.Transactional

@Transactional
class OrganizationService {

    def springSecurityService

    def requestOrganization(final Event event, final Club club) {

        def result = new OrganizationRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to request event organization
        if (!user) {
            result.errors.reject('organization.request.error.notAllowed')
            return result
        }

        // We must specify the event to which request organization
        if (!event) {
            result.errors.reject('organization.request.error.noEvent')
            return result
        }

        result.event = event

        // Event organization can only be requested by an owner admin
        if (!user.isAdminOf(event?.club)) {
            result.errors.reject('organization.request.error.notAllowed')
            return result
        }

        // We must specify the club to which request organization
        if (!club) {
            result.errors.reject('organization.request.error.noClub')
            return result
        }

        result.club = club

        // Club must not have a current organization request for the event
        if (OrganizationRequest.findByEventAndClub(event, club)) {
            result.errors.reject('organization.request.error.alreadyRequested')
            return result
        }

        // Club must not be an event organizer
        if (Organization.findByEventAndClub(event, club)) {
            result.errors.reject('organization.request.error.alreadyOrganizer')
            return result
        }

        result.date = new Date()

        result.save()

        return result
    }

    def setEventOrganizers(final Event event, final Club[] clubs) {

        def result = event ?: new Event()

        def user = springSecurityService.currentUser

        // Users must be logged-in to set event organizers
        if (!user) {
            result.errors.reject('organization.setEventOrganizers.error.notAllowed')
            return result
        }

        // We must specify the event to which set organizers
        if (!event) {
            result.errors.reject('organization.setEventOrganizers.error.noEvent')
            return result
        }

        // Event organizers can only be set by a owner admin
        if (!user.isAdminOf(event.club)) {
            result.errors.reject('organization.setEventOrganizers.error.notAllowed')
            return result
        }

        // Event owner must be an organizer
        if (!clubs.contains(event.club)) {
            result.errors.reject('organization.setEventOrganizers.error.noOwner')
            return result
        }

        def oldOrganizers = event.getOrganizers()

        // Add organizers
        def organizersToAdd = clubs.findAll { club ->
            return (!oldOrganizers.any { it.id == club.id }) && (!OrganizationRequest.findByEventAndClub(event, club))
        }
        organizersToAdd?.each {
            def organizationRequest = requestOrganization(event, it)
            if (organizationRequest.hasErrors()) {
                // Revert
                transactionStatus.setRollbackOnly()
                // Set errors
                result.errors.reject('organization.setEventOrganizers.error')
            }
        }

        // Remove organizers
        def organizersToRemove = oldOrganizers.findAll { oldOrg -> return !clubs.any { it.id == oldOrg.id } }
        if (organizersToRemove) {
            Organization.findAllByEventAndClubInList(event, organizersToRemove).each {
                it.delete()
                if (it.hasErrors()) {
                    // Revert
                    transactionStatus.setRollbackOnly()
                    // Set errors
                    result.errors.reject('organization.setEventOrganizers.error')
                }
            }
        }

        return result
    }

    def acceptOrganizationRequests(final OrganizationRequest[] organizationRequests) {

        def result = []

        organizationRequests.each {
            result << acceptOrganizationRequest(it)
        }

        return result
    }

    def acceptOrganizationRequest(final OrganizationRequest organizationRequest) {

        def result = new OrganizationRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to accept organization requests
        if (!user) {
            result.errors.reject('organization.acceptRequest.error.notAllowed')
            return result
        }

        // We must specify the organization request to accept
        if (!organizationRequest) {
            result.errors.reject('organization.acceptRequest.error.noRequest')
            return result
        }

        result = organizationRequest

        // Users must be admins of the club to accept organization requests
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('organization.acceptRequest.error.notAllowed')
            return result
        }

        // Remove organization request
        result.delete()

        // Create organization
        if (!result.hasErrors()) {

            def organization = new Organization(event: result.event, club: result.club)
            organization.save()

            if (organization.hasErrors()) {
                // Revert removed organization request
                transactionStatus.setRollbackOnly()
                // Set organization request errors
                result.errors.reject('organization.acceptRequest.error')
            }
        }

        return result
    }

    def rejectOrganizationRequests(final OrganizationRequest[] organizationRequests) {

        def result = []

        organizationRequests.each {
            result << rejectOrganizationRequest(it)
        }

        return result
    }

    def rejectOrganizationRequest(final OrganizationRequest organizationRequest) {

        def result = new OrganizationRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to reject organization requests
        if (!user) {
            result.errors.reject('organization.rejectRequest.error.notAllowed')
            return result
        }

        // We must specify the organization request to reject
        if (!organizationRequest) {
            result.errors.reject('organization.rejectRequest.error.noRequest')
            return result
        }

        result = organizationRequest

        // Users must be admins of the club to reject organization requests
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('organization.rejectRequest.error.notAllowed')
            return result
        }

        // Remove organization request
        result.delete()

        return result
    }

    def leaveOrganization(final Event event, final Club[] clubs) {

        def result = event ?: new Event()

        def user = springSecurityService.currentUser

        // Users must be logged-in to leave organization
        if (!user) {
            result.errors.reject('organization.leave.error.notAllowed')
            return result
        }

        // We must specify the event to which leave organization
        if (!event) {
            result.errors.reject('organization.leave.error.noEvent')
            return result
        }

        // Organization can only be left by club admin
        if (clubs.any { !user.isAdminOf(it) }) {
            result.errors.reject('organization.leave.error.notAllowed')
            return result
        }

        // Event owner must be an organizer
        if (clubs.contains(event.club)) {
            result.errors.reject('organization.leave.error.noOwner')
            return result
        }

        // Remove organizers
        def organizersToRemove = event.getOrganizers().findAll { org -> return clubs.any { it.id == org.id } }
        if (organizersToRemove) {
            Organization.findAllByEventAndClubInList(event, organizersToRemove).each {
                it.delete()
                if (it.hasErrors()) {
                    // Revert
                    transactionStatus.setRollbackOnly()
                    // Set errors
                    result.errors.reject('organization.leave.error')
                }
            }
        }

        return result
    }
}

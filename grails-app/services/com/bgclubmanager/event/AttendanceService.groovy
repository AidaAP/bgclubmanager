package com.bgclubmanager.event

import com.bgclubmanager.mail.MailUtils
import grails.transaction.Transactional

@Transactional
class AttendanceService {

    def mailUtils
    def springSecurityService

    def requestAttendance(final Event event) {

        def result = new AttendanceRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to request event attendance
        if (!user) {
            result.errors.reject('attendance.request.error.notAllowed')
            return result
        }

        result.user = user

        // We must specify the event to which request attendance
        if (!event) {
            result.errors.reject('attendance.request.error.noEvent')
            return result
        }

        result.event = event

        // User must be a member of event organizer (unless the event is public)
        if (!event.isPublic && !event.getOrganizers().any { user.isMemberOf(it) }) {
            result.errors.reject('attendance.request.error.notAllowed')
            return result
        }

        // Event must be enrollable (within enrollment dates)
        if (!event.isEnrollable()) {
            result.errors.reject('attendance.request.error.notEnrollable')
            return result
        }

        // User must not have a current attendance request for the event
        if (AttendanceRequest.findByEventAndUser(event, user)) {
            result.errors.reject('attendance.request.error.alreadyRequested')
            return result
        }

        // User must not be an event attendee
        if (Attendance.findByEventAndUser(event, user)) {
            result.errors.reject('attendance.request.error.alreadyAttendee')
            return result
        }

        result.date = new Date()

        result.save()

        // Automatically accept request if event has self-enrollment and maxAttendees is not reached
        if (!result.hasErrors() && event.selfEnrollment && !event.isMaxAttendeesReached()) {

            def attendance = new Attendance(event: event, user: user)
            attendance.save()

            if (attendance.hasErrors()) {
                // Revert request
                transactionStatus.setRollbackOnly()
                // Set attendance request errors
                result.errors.reject('attendance.request.error')
            } else {

                // Delete request
                result.delete()

                if (result.hasErrors()) {
                    // Revert request and attendance
                    transactionStatus.setRollbackOnly()
                    // Set attendance request errors
                    result.errors.reject('attendance.request.error')
                }
            }
        }

        return result
    }

    def acceptAttendanceRequests(final AttendanceRequest[] attendanceRequests) {

        def result = []

        attendanceRequests.each {
            result << acceptAttendanceRequest(it)
        }

        return result
    }

    def acceptAttendanceRequest(final AttendanceRequest attendanceRequest) {

        def result = new AttendanceRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to accept attendance requests
        if (!user) {
            result.errors.reject('attendance.acceptRequest.error.notAllowed')
            return result
        }

        // We must specify the attendance request to accept
        if (!attendanceRequest) {
            result.errors.reject('attendance.acceptRequest.error.noRequest')
            return result
        }

        result = attendanceRequest

        // Users must be admins of the event club to accept attendance requests
        if (!user.isAdminOf(result.event.club)) {
            result.errors.reject('attendance.acceptRequest.error.notAllowed')
            return result
        }

        // Max attendees must no be reached
        if (result.event.isMaxAttendeesReached()) {
            result.errors.reject('attendance.acceptRequest.error.maxAttendees')
            return result
        }

        // Remove attendance request
        result.delete()

        // Create attendance
        if (!result.hasErrors()) {

            def attendance = new Attendance(event: result.event, user: result.user)
            attendance.save()

            if (attendance.hasErrors()) {
                // Revert removed attendance request
                transactionStatus.setRollbackOnly()
                // Set attendance request errors
                result.errors.reject('attendance.acceptRequest.error')
            }
        }

        // Send mail
        if (!result.hasErrors()) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.ACCEPT_ATTENDANCE_REQUEST,
                    [user: attendanceRequest.user, event: attendanceRequest.event])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set attendance request errors
                result.errors.reject('attendance.acceptRequest.error')
            }
        }

        return result
    }

    def rejectAttendanceRequests(final AttendanceRequest[] attendanceRequests) {

        def result = []

        attendanceRequests.each {
            result << rejectAttendanceRequest(it)
        }

        return result
    }

    def rejectAttendanceRequest(final AttendanceRequest attendanceRequest) {

        def result = new AttendanceRequest()

        def user = springSecurityService.currentUser

        // Users must be logged-in to reject attendance requests
        if (!user) {
            result.errors.reject('attendance.rejectRequest.error.notAllowed')
            return result
        }

        // We must specify the attendance request to reject
        if (!attendanceRequest) {
            result.errors.reject('attendance.rejectRequest.error.noRequest')
            return result
        }

        result = attendanceRequest

        // Users must be admins of the event club to reject attendance requests
        if (!user.isAdminOf(result.event.club)) {
            result.errors.reject('attendance.rejectRequest.error.notAllowed')
            return result
        }

        // Remove organization request
        result.delete()

        // Send mail
        if (!result.hasErrors()) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.REJECT_ATTENDANCE_REQUEST,
                    [user: attendanceRequest.user, event: attendanceRequest.event])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set attendance request errors
                result.errors.reject('attendance.rejectRequest.error')
            }
        }

        return result
    }

    def cancelAttendances(final Attendance[] attendances) {

        def result = []

        attendances.each {
            result << cancelAttendance(it)
        }

        return result
    }

    def cancelAttendance(final Attendance attendance) {

        def result = new Attendance()

        def user = springSecurityService.currentUser

        // Users must be logged-in to cancel attendances
        if (!user) {
            result.errors.reject('attendance.cancel.error.notAllowed')
            return result
        }

        // We must specify the attendance to cancel
        if (!attendance) {
            result.errors.reject('attendance.cancel.error.noAttendance')
            return result
        }

        result = attendance

        // Users must be admins of the event club or the attendee to cancel attendances
        if (!((user == result.user) || user.isAdminOf(result.event.club))) {
            result.errors.reject('attendance.cancel.error.notAllowed')
            return result
        }

        // Event must be enrollable (within enrollment dates)
        if (!result.event?.isEnrollable()) {
            result.errors.reject('attendance.cancel.error.notEnrollable')
            return result
        }

        // User must be an event attendee
        if (!Attendance.findByEventAndUser(attendance.event, attendance.user)) {
            result.errors.reject('attendance.cancel.error.notAttendee')
            return result
        }

        // Remove attendance
        result.delete()

        // Send mail if the attendance is not cancelled by the attendee
        if (!result.hasErrors() && (user != attendance.user)) {

            def mailOk = mailUtils.sendMail(MailUtils.MailType.CANCEL_ATTENDANCE, [user: attendance.user, event:
                    attendance.event])

            if (!mailOk) {
                // Revert previous actions
                transactionStatus.setRollbackOnly()
                // Set attendance errors
                result.errors.reject('attendance.cancel.error')
            }
        }

        // Accept pending requests if event is self-enrollment and there are free places
        if (!result.hasErrors() && attendance.event.selfEnrollment && !attendance.event.isMaxAttendeesReached()) {

            Integer numFreePlaces = attendance.event.maxAttendees ? attendance.event.maxAttendees - Attendance
                    .countByEvent(attendance.event) : null
            if (numFreePlaces) {

                AttendanceRequest[] requests = AttendanceRequest.createCriteria().list {
                    eq('event', attendance.event)
                    maxResults(numFreePlaces)
                    order('date', 'asc')
                }

                if (!user.isAdminOf(attendance.event.club)) {
                    springSecurityService.reauthenticate attendance.event.club.owner.username
                }

                acceptAttendanceRequests(requests)

                if (!user.isAdminOf(attendance.event.club)) {
                    springSecurityService.reauthenticate user.username
                }
            }
        }

        return result
    }
}

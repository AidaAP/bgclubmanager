package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class EventService {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def messageSource
    def springSecurityService

    def create(final Event event) {

        def result = event ?: new Event()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create an event
        if (!user) {
            result.errors.reject('event.create.error.notAllowed')
            return result
        }

        // We must specify the event to create
        if (!event) {
            result.errors.reject('event.create.error.noEvent')
            return result
        }

        // Event must pertain to a club
        if (!event.club) {
            result.errors.reject('event.create.error.noClub')
            return result
        }

        // An event can only be created by a club admin
        if (!user.isAdminOf(event.club)) {
            result.errors.reject('event.create.error.notAllowed')
            return result
        }

        result.save()

        // Assign first organizer (owner)
        if (!result.hasErrors()) {

            def organizer = new Organization(event: result, club: result.club)
            organizer.save()

            if (organizer.hasErrors()) {
                // Revert created event
                transactionStatus.setRollbackOnly()
                // Set event errors
                result.errors.reject('event.create.error')
            }
        }

        return result
    }

    def update(final Event event) {

        def result = event ?: new Event()

        def user = springSecurityService.currentUser

        // Users must be logged-in to update an event
        if (!user) {
            result.errors.reject('event.update.error.notAllowed')
            return result
        }

        // We must specify the event to update
        if (!event) {
            result.errors.reject('event.update.error.noEvent')
            return result
        }

        // Event must exist
        if (!Event.exists(result.id)) {
            result.errors.reject('event.update.error.notFoundEvent')
            return result
        }

        // An event can only be updated by a club admin
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('event.update.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    @Transactional(readOnly = true)
    def getClubEvents(final Club club, final Integer offset, final Integer max) {

        def list = []
        def totalCount = 0

        if (club) {

            def user = springSecurityService.currentUser

            def events = Organization.createCriteria().list(offset: offset ?: 0, max: max ?: 0) {
                projections { groupProperty('event') }
                eq('club', club)
                event {
                    if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !user.isMemberOf(club)) {
                        eq('isPublic', true)
                    }
                    order('startDate', 'desc')
                }
            }

            list = events
            totalCount = events.totalCount
        }

        return [ list: list, totalCount: totalCount ]
    }

    @Transactional(readOnly = true)
    def getUnfinishedEvents() {

        def user = springSecurityService.currentUser

        return Attendance.createCriteria().list {
            projections { property('event') }
            eq('user', user)
            event {
                gt('endDate', new Date())
            }
        }
    }

    def delete(final String eventId) {

        def result = new Event()

        // Event must exist
        def eventDB = Event.get(eventId)
        if (!eventDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('event.default.label', null,
                    LocaleContextHolder.locale), eventId] as Object[], null)
            return result
        }

        // An event can only be deleted by a club admin
        if (!springSecurityService.currentUser?.isAdminOf(eventDB.club)) {
            result.errors.reject('event.delete.error.notAllowed')
            return result
        }

        // Delete event attendance requests
        AttendanceRequest.findAllByEvent(eventDB).each { it.delete() }

        // Delete event attendances
        Attendance.findAllByEvent(eventDB).each { it.delete() }

        // Delete organization requests
        OrganizationRequest.findAllByEvent(eventDB).each { it.delete() }

        // Delete organizations
        Organization.findAllByEvent(eventDB).each { it.delete() }

        eventDB.delete()

        return eventDB
    }

    @Transactional(readOnly = true)
    def findEvents(final Date fromDate, final Date toDate, final Float latitude1, final Float longitude1, final Float
            latitude2, final Float longitude2) {

        def result = [:]

        def user = springSecurityService.currentUser

        // Users must be logged-in to find events
        if (!user) {
            result.error = messageSource.getMessage('event.find.error.notAllowed', null, LocaleContextHolder.locale)
            return result
        }

        def minLatitude = null
        def maxLatitude = null
        if (latitude1 && latitude2) {
            minLatitude = Math.min(latitude1, latitude2)
            maxLatitude = Math.max(latitude1, latitude2)
        }

        def minLongitude = null
        def maxLongitude
        if (longitude1 && longitude2) {
            minLongitude = Math.min(longitude1, longitude2)
            maxLongitude = Math.max(longitude1, longitude2)
        }

        Club[] userClubs = Member.memberships(user).list()

        def events = Organization.createCriteria().listDistinct {
            projections { property('event') }
            or {
                event {
                    eq('isPublic', true)
                }
                inList('club', userClubs)
            }
            event {
                if (fromDate) {
                    gt('endDate', fromDate)
                }
                if (toDate) {
                    lt('startDate', toDate)
                }
                if (minLatitude) {
                    isNotNull('latitude')
                    ge('latitude', minLatitude)
                }
                if (maxLatitude) {
                    isNotNull('latitude')
                    le('latitude', maxLatitude)
                }
                if (minLongitude) {
                    isNotNull('longitude')
                    ge('longitude', minLongitude)
                }
                if (maxLongitude) {
                    isNotNull('longitude')
                    le('longitude', maxLongitude)
                }
            }
        }

        result.events = events

        return result
    }
}
package com.bgclubmanager.user

import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.club.MembershipRequest
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.mail.MailUtils.MailType
import com.bgclubmanager.poll.Vote
import com.bgclubmanager.reserve.Reserve
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.transaction.Transactional
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class UserService {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    @Value('${default.role.user}')
    final String ROLE_USER

    @Value('${default.user.username}')
    final String BASIC_ADMIN_USERNAME

    def fileService
    def mailUtils
    def messageSource
    def springSecurityService

    def register(final User user) {

        // Users can only be registered by non-logged users
        if (springSecurityService.isLoggedIn()) {
            user.errors.reject('user.register.error.notAllowed')
            return user
        }

        // Users are registered as basic users
        Role[] roles = [Role.findByAuthority(ROLE_USER)]

        // Once the user is created, we send a welcome mail
        MailType[] mailTypes = [MailType.REGISTER_USER]

        return create(user, roles, mailTypes)
    }

    def createAdmin(final User user) {

        // Only admin users can create admin users
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN)) {
            user.errors.reject('user.createAdmin.error.notAllowed')
            return user
        }

        // Admin password is generated automatically
        if (user.password) {
            user.errors.reject('user.createAdmin.error.password')
            return user
        }

        // Random password
        user.password = UUID.randomUUID().toString()

        // Users are registered as admin users
        Role[] roles = [Role.findByAuthority(ROLE_ADMIN)]

        // Once the user is created, we send a welcome mail
        MailType[] mailTypes = [MailType.CREATE_ADMIN]

        return create(user, roles, mailTypes)
    }

    def create(final User user, final Role[] roles, final MailType[] mailTypes) {

        // We must specify the user to create
        if (!user) {
            def result = new User()
            result.errors.reject('user.create.error.noUser')
            return result
        }

        // We must specify roles
        if (!roles) {
            user.errors.reject('user.create.error.noRoles')
            return user
        }

        // Users can only be created by non-registered users (when registering themselves) or by admins
        if (springSecurityService.isLoggedIn() && SpringSecurityUtils.ifNotGranted(ROLE_ADMIN)) {
            user.errors.reject('user.create.error.notAllowed')
            return user
        }

        user.save()

        // Assign roles
        if (!user.hasErrors()) {

            roles.each {

                def userRole = new UserRole(user: user, role: it)
                userRole.save()

                if (userRole.hasErrors()) {
                    // Revert created user
                    transactionStatus.setRollbackOnly()
                    // Set user errors
                    user.errors.reject('user.create.error')
                }
            }
        }

        // Send mails
        if (!user.hasErrors()) {

            mailTypes.each {

                def mailOk = mailUtils.sendMail(it, [ user: user])

                if (!mailOk) {
                    // Revert created user
                    transactionStatus.setRollbackOnly()
                    // Set user errors
                    user.errors.reject('user.create.error')
                }
            }
        }

        return user
    }

    def changePassword(final ChangePasswordCommand command) {

        // We must specify the command
        if (!command) {
            def result = new ChangePasswordCommand()
            result.errors.reject('user.changePassword.error.noCommand')
            return result
        }

        def user = springSecurityService.currentUser

        // Users must be logged-in to change their password
        if (!user) {
            command.errors.reject('user.changePassword.error.notLoggedIn')
            return command
        }

        command.validate()
        if (!command.hasErrors()) {

            user.password = command.newPassword
            user.save()

            if (user.hasErrors()) {
                user.errors.allErrors.each {
                    command.errors.reject(it.code)
                }
            }
        }

        return command
    }

    def update(final User user) {

        // We must specify the user to update
        if (!user) {
            def result = new User()
            result.errors.reject('user.update.error.noUser')
            return result
        }

        // User must exist
        if (!User.exists(user.id)) {
            user.errors.reject('user.update.error.notFoundUser')
            return user
        }

        // Users can only be updated by admin users or by the user itself
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && (user.id != springSecurityService.currentUserId)) {
            user.errors.reject('user.update.error.notAllowed')
            return user
        }

        def oldUrlImage = User.withNewSession { return User.read(user.id)?.urlImage }

        user.save()

        if (!user.hasErrors()) {
            // Delete old image if needed
            if (oldUrlImage && (oldUrlImage != user.urlImage)) {
                fileService.deleteFile(oldUrlImage)
            }
        }

        return user
    }

    def delete(final String userId) {

        def result = new User()

        // A user can only be deleted by an admin or itself
        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && (userId != springSecurityService.currentUserId)) {
            result.errors.reject('user.delete.error.notAllowed')
            return result
        }

        // User must exist
        def userDB = User.get(userId)
        if (!userDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('user.default.label',
                    null, LocaleContextHolder.locale), userId] as Object[], null)
            return result
        }

        // Delete user roles
        UserRole.findAllByUser(userDB).each { it.delete() }

        // Delete user token
        UserToken.findByUser(userDB)?.delete()

        // Delete user memberships and membership requests
        Member.findAllByUser(userDB).each { it.delete() }
        MembershipRequest.findAllByUser(userDB).each{ it.delete() }

        // Delete user readings
        Reading.findAllByUser(userDB).each { it.delete() }

        // Delete user votes
        Vote.findAllByUser(userDB).each { it.delete() }

        // Delete attendances and attendance requests
        AttendanceRequest.findAllByUser(userDB).each { it.delete() }
        Attendance.findAllByUser(userDB).each { it.delete() }

        // Delete user reserves
        Reserve.findAllByUser(userDB).each { it.delete() }

        // If the user is the owner of any clubs, those clubs are reasigned to the basic admin and he will manage
        // their deletion or reasignation
        def basicAdmin = User.findByUsername(BASIC_ADMIN_USERNAME)
        Club.findAllByOwner(userDB).each {

            // Owner
            it.owner = basicAdmin
            it.save()

            // Admin
            def adminMember = new Member(club: it, user: basicAdmin, type: Member.Type.ADMIN, startDate: new Date())
            adminMember.save()
        }

        userDB.delete()

        return userDB
    }

    def generateToken() {

        def result

        def user = springSecurityService.currentUser

        // Users must be logged-in to generate their token
        if (!user) {
            result = new UserToken()
            result.errors.reject('userToken.generate.error.notLoggedIn')
            return result
        }

        result = UserToken.findByUser(user)
        if (result) {
            // Delete old token
            result.delete(flush: true)
        }

        // Generate token
        result = new UserToken(user: user)
        result.save()

        if (result.hasErrors()) {
            // Revert delete old token
            transactionStatus.setRollbackOnly()
        }

        return result
    }
}
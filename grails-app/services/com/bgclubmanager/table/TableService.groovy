package com.bgclubmanager.table

import com.bgclubmanager.club.Club
import com.bgclubmanager.reserve.Reserve
import grails.transaction.Transactional
import org.springframework.context.i18n.LocaleContextHolder

@Transactional
class TableService {

    def messageSource
    def springSecurityService

    def create(final Table table) {

        def result = table ?: new Table()

        def user = springSecurityService.currentUser

        // Users must be logged-in to create a table
        if (!user) {
            result.errors.reject('table.create.error.notAllowed')
            return result
        }

        // We must specify the table to create
        if (!table) {
            result.errors.reject('table.create.error.noTable')
            return result
        }

        // Table must pertain to a club
        if (!table.club) {
            result.errors.reject('table.create.error.noClub')
            return result
        }

        // A table can only be created by a club admin
        if (!user.isAdminOf(table.club)) {
            result.errors.reject('table.create.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    def update(final Table table) {

        def result = table ?: new Table()

        def user = springSecurityService.currentUser

        // Users must be logged-in to update a table
        if (!user) {
            result.errors.reject('table.update.error.notAllowed')
            return result
        }

        // We must specify the table to update
        if (!table) {
            result.errors.reject('table.update.error.noTable')
            return result
        }

        // Table must exist
        if (!Table.exists(result.id)) {
            result.errors.reject('table.update.error.notFoundTable')
            return result
        }

        // A table can only be updated by a club admin
        if (!user.isAdminOf(result.club)) {
            result.errors.reject('table.update.error.notAllowed')
            return result
        }

        result.save()

        return result
    }

    @Transactional(readOnly = true)
    def getTables(final Club club, final Integer offset, final Integer max) {

        def tables = Table.createCriteria().list(offset: offset ?: 0, max: max ?: 0) {
            if (club) {
                eq('club', club)
            }
            order('name', 'asc')
        }

        return [ list: tables, totalCount: tables.totalCount ]
    }

    def delete(final String tableId) {

        def result = new Table()

        // Table must exist
        def tableDB = Table.get(tableId)
        if (!tableDB) {
            result.errors.reject('default.not.found.message', [messageSource.getMessage('table.default.label', null,
                    LocaleContextHolder.locale), tableId] as Object[], null)
            return result
        }

        // An table can only be deleted by a club admin
        if (!springSecurityService.currentUser?.isAdminOf(tableDB.club)) {
            result.errors.reject('table.delete.error.notAllowed')
            return result
        }

        // Delete reserves for this table
        Reserve.withTable(tableDB).list()?.each { it.delete() }

        tableDB.delete()

        return tableDB
    }
}

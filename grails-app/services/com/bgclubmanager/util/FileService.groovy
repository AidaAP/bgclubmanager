package com.bgclubmanager.util

import grails.transaction.Transactional
import org.apache.commons.lang.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.web.multipart.MultipartFile
import org.apache.commons.io.FilenameUtils

@Transactional
class FileService {

    @Value('${default.file.baseDir}')
    final String BASE_DIR

    @Value('${default.file.dataDir}')
    final String DATA_DIR

    @Value('${default.file.userImgDir}')
    final String USER_IMAGE_DIR

    @Value('${default.file.clubImgDir}')
    final String CLUB_IMAGE_DIR

    @Value('${default.file.gameImgDir}')
    final String GAME_IMAGE_DIR

    def saveUserImage(final MultipartFile file) {

        def path = "${BASE_DIR}${DATA_DIR}${USER_IMAGE_DIR}"

        return saveFile(file, path)
    }

    def saveClubImage(final MultipartFile file) {

        def path = "${BASE_DIR}${DATA_DIR}${CLUB_IMAGE_DIR}"

        return saveFile(file, path)
    }

    def saveClubImage(final String base64File) {

        def path = "${BASE_DIR}${DATA_DIR}${CLUB_IMAGE_DIR}"

        return saveFile(base64File, path)
    }

    def saveGameImage(final MultipartFile file) {

        def path = "${BASE_DIR}${DATA_DIR}${GAME_IMAGE_DIR}"

        return saveFile(file, path)
    }

    def saveGameImage(final String base64File) {

        def path = "${BASE_DIR}${DATA_DIR}${GAME_IMAGE_DIR}"

        return saveFile(base64File, path)
    }

    def saveExternalGameImage(final String externalImageUrl) {

        def path = "${BASE_DIR}${DATA_DIR}${GAME_IMAGE_DIR}"

        return saveExternalFile(externalImageUrl, path)
    }

    def saveFile(final MultipartFile file, final String path) {

        def result = null

        if (file && path) {

            def dir = new File(path)
            if (!dir.exists()) {
                dir.mkdirs()
            }

            if (dir.exists()) {

                def fileName = "${UUID.randomUUID().toString()}_${System.currentTimeMillis()}"
                def fileExtension = FilenameUtils.getExtension(file.originalFilename)
                def filePath = "${path}${fileName}.${fileExtension}"

                def newFile = new File(filePath)
                file.transferTo(newFile)

                if (newFile.exists()) {
                    result = filePath
                }
            }
        }

        return result
    }

    def saveFile(final String base64File, final String path) {

        def result = null

        if (base64File && base64File.startsWith('data:image/') && path) {

            def dir = new File(path)
            if (!dir.exists()) {
                dir.mkdirs()
            }

            if (dir.exists()) {

                def fileName = "${UUID.randomUUID().toString()}_${System.currentTimeMillis()}"
                def fileExtension = StringUtils.substringBetween(base64File, 'data:image/', ';base64')
                def filePath = "${path}${fileName}.${fileExtension}"

                def fileString = base64File.tokenize(",")[1]
                def decodedFile = fileString.decodeBase64()

                def newFile = new File(filePath)
                newFile.withOutputStream {
                    it.write(decodedFile)
                }

                if (newFile.exists()) {
                    result = filePath
                }
            }
        }

        return result
    }

    def saveExternalFile(final String externalFileUrl, final String path) {

        def result = null

        if (externalFileUrl && path) {

            def dir = new File(path)
            if (!dir.exists()) {
                dir.mkdirs()
            }

            if (dir.exists()) {

                def fileName = "${UUID.randomUUID().toString()}_${System.currentTimeMillis()}"
                def fileExtension = externalFileUrl.substring(externalFileUrl.lastIndexOf('.') + 1)
                def filePath = "${path}${fileName}.${fileExtension}"

                def newFile = new File(filePath)
                def os = newFile.newOutputStream()
                os << new URL(externalFileUrl).openStream()
                os.close()

                if (newFile.exists()) {
                    result = filePath
                }
            }
        }

        return result
    }

    def deleteFile(final String path) {

        def result = true

        def file = new File(path)
        if (file.exists()) {
            result = file.delete()
        }

        return result
    }
}
databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-user") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user'")
        }
        createTable(tableName: "user") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "username", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "password", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "name", type: "VARCHAR(255)")
            column(name: "surname", type: "VARCHAR(255)")
            column(name: "email", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "url_image", type: "VARCHAR(255)")
            column(name: "enabled", type: "BIT(1)") {
                constraints(nullable: "false")
            }
            column(name: "account_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }
            column(name: "account_locked", type: "BIT(1)") {
                constraints(nullable: "false")
            }
            column(name: "password_expired", type: "BIT(1)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addPrimaryKey-user") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user' AND CONSTRAINT_TYPE='PRIMARY KEY'")
        }
        addPrimaryKey(constraintName: "PK-user-id", tableName: "user", columnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-user-username") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-user-username", tableName: "user", columnNames: "username")
    }

    changeSet(author: "aida.aljan", id: "createIndexes-user") {
        createIndex(indexName: "IDX-user-username", tableName: "user", unique: true) {
            column(name: "username")
        }
    }

    changeSet(author: "aida.aljan", id: "createTable-role") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='role'")
        }
        createTable(tableName: "role") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "authority", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addPrimaryKey-role") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='role' AND CONSTRAINT_TYPE='PRIMARY KEY'")
        }
        addPrimaryKey(constraintName: "PK-role-id", tableName: "role", columnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-role-authority") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='role' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-role-authority", tableName: "role", columnNames: "authority")
    }

    changeSet(author: "aida.aljan", id: "createIndexes-role") {
        createIndex(indexName: "IDX-role-auhority", tableName: "role", unique: true) {
            column(name: "authority")
        }
    }

    changeSet(author: "aida.aljan", id: "createTable-user_role") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_role'")
        }
        createTable(tableName: "user_role") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
            column(name: "role_id", type: "VARCHAR(255)") {
                constraints(nullable: "false")
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addPrimaryKey-user_role") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_role' AND CONSTRAINT_TYPE='PRIMARY KEY'")
        }
        addPrimaryKey(constraintName: "PK-user_role-id", tableName: "user_role", columnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraints-user_role-user") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_role' AND CONSTRAINT_TYPE='FOREIGN KEY'")
        }
        addForeignKeyConstraint(constraintName: "FK-user_role-user_id", baseTableName: "user_role", baseColumnNames:
                "user_id", referencedTableName: "user", referencedColumnNames: "id")
        addForeignKeyConstraint(constraintName: "FK-user_role-role_id", baseTableName: "user_role", baseColumnNames:
                "role_id", referencedTableName: "role", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-user_role-user_id_role_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_role' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-user_role-user_id_role_id", tableName: "user_role", columnNames:
                "user_id,role_id")
    }

    changeSet(author: "aida.aljan", id: "createIndexes-user_role") {
        createIndex(indexName: "IDX-user_role-user_id", tableName: "user_role", unique: false) {
            column(name: "user_id")
        }
        createIndex(indexName: "IDX-user_role-role_id", tableName: "user_role", unique: false) {
            column(name: "role_id")
        }
        createIndex(indexName: "IDX-user_role-user_id_role_id", tableName: "user_role", unique: true) {
            column(name: "user_id")
            column(name: "role_id")
        }
    }
}
databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-user_token") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_token'")
        }
        createTable(tableName: "user_token") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-user_token-id")
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false, unique: true, uniqueConstraintName: "UQ-user_token-user_id")
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-user_token") {
        createIndex(indexName: "IDX-user_token-user_id", tableName: "user_token", unique: true) {
            column(name: "user_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-user_token-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user_token' AND " +
                    "CONSTRAINT_NAME='FK-user_token-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-user_token-user_id", baseTableName: "user_token", baseColumnNames:
                "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }
}
databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-announcement") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='announcement'")
        }
        createTable(tableName: "announcement") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-announcement-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "content", type: "TEXT") {
                constraints(nullable: false)
            }
            column(name: "published", type: "BIT(1)") {
                constraints(nullable: false)
            }
            column(name: "start_date_publish", type: "DATETIME") {
                constraints(nullable: true)
            }
            column(name: "end_date_publish", type: "DATETIME") {
                constraints(nullable: true)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-announcement") {
        createIndex(indexName: "IDX-announcement-published_start_date_publish_end_date_publish", tableName:
                "announcement", unique: false) {
            column(name: "published")
            column(name: "start_date_publish")
            column(name: "end_date_publish")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-announcement-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='announcement' AND " +
                    "CONSTRAINT_NAME='FK-announcement-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-announcement-club_id", baseTableName: "announcement",
                baseColumnNames: "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-reading") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reading'")
        }
        createTable(tableName: "reading") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-reading-id")
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "announcement_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-reading") {
        createIndex(indexName: "IDX-reading-user_id", tableName: "reading", unique: false) {
            column(name: "user_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reading-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reading' AND CONSTRAINT_NAME='FK-reading-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reading-user_id", baseTableName: "reading", baseColumnNames:
                "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reading-announcement_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reading' AND " +
                    "CONSTRAINT_NAME='FK-reading-announcement_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reading-announcement_id", baseTableName: "reading", baseColumnNames:
                "announcement_id", referencedTableName: "announcement", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-reading-user_id_announcement_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reading' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-reading-user_id_announcement_id", tableName: "reading", columnNames:
                "user_id, announcement_id")
    }
}
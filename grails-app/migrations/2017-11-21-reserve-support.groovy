databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-reserve") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserve'")
        }
        createTable(tableName: "reserve") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-reserve-id")
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT") {
                constraints(nullable: true)
            }
            column(name: "start_date", type: "DATETIME") {
                constraints(nullable: false)
            }
            column(name: "end_date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-reserve") {
        createIndex(indexName: "IDX-reserve-user_id", tableName: "reserve", unique: false) {
            column(name: "user_id")
        }
        createIndex(indexName: "IDX-reserve-club_id", tableName: "reserve", unique: false) {
            column(name: "club_id")
        }
        createIndex(indexName: "IDX-reserve-start_date_end_date", tableName: "reserve", unique: false) {
            column(name: "start_date")
            column(name: "end_date")
        }
        createIndex(indexName: "IDX-reserve-user_id_start_date_end_date", tableName: "reserve", unique: false) {
            column(name: "user_id")
            column(name: "start_date")
            column(name: "end_date")
        }
        createIndex(indexName: "IDX-reserve-club_id_start_date_end_date", tableName: "reserve", unique: false) {
            column(name: "club_id")
            column(name: "start_date")
            column(name: "end_date")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserve-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserve' AND CONSTRAINT_NAME='FK-reserve-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserve-user_id", baseTableName: "reserve", baseColumnNames:
                "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserve-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserve' AND CONSTRAINT_NAME='FK-reserve-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserve-club_id", baseTableName: "reserve", baseColumnNames:
                "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-reserved_game") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_game'")
        }
        createTable(tableName: "reserved_game") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-reserved_game-id")
            }
            column(name: "reserve_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "game_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-reserved_game") {
        createIndex(indexName: "IDX-reserved_game-reserve_id", tableName: "reserved_game", unique: false) {
            column(name: "reserve_id")
        }
        createIndex(indexName: "IDX-reserved_game-game_id", tableName: "reserved_game", unique: false) {
            column(name: "game_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserved_game-reserve_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_game' AND " +
                    "CONSTRAINT_NAME='FK-reserved_game-reserve_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserved_game-reserve_id", baseTableName: "reserved_game",
                baseColumnNames: "reserve_id", referencedTableName: "reserve", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserved_game-game_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_game' AND " +
                    "CONSTRAINT_NAME='FK-reserved_game-game_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserved_game-game_id", baseTableName: "reserved_game",
                baseColumnNames: "game_id", referencedTableName: "game", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-reserved_table") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_table'")
        }
        createTable(tableName: "reserved_table") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-reserved_table-id")
            }
            column(name: "reserve_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "table_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-reserved_table") {
        createIndex(indexName: "IDX-reserved_table-reserve_id", tableName: "reserved_table", unique: false) {
            column(name: "reserve_id")
        }
        createIndex(indexName: "IDX-reserved_table-table_id", tableName: "reserved_table", unique: false) {
            column(name: "table_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserved_table-reserve_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_table' AND " +
                    "CONSTRAINT_NAME='FK-reserved_table-reserve_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserved_table-reserve_id", baseTableName: "reserved_table",
                baseColumnNames: "reserve_id", referencedTableName: "reserve", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-reserved_table-table_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='reserved_table' AND " +
                    "CONSTRAINT_NAME='FK-reserved_table-table_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-reserved_table-table_id", baseTableName: "reserved_table",
                baseColumnNames: "table_id", referencedTableName: "table_room", referencedColumnNames: "id")
    }
}
databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-table_room") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='table_room'")
        }
        createTable(tableName: "table_room") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-table_room-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT") {
                constraints(nullable: true)
            }
            column(name: "max_capacity", type: "INT") {
                constraints(nullable: true)
            }
            column(name: "latitude", type: "DECIMAL(7, 5)") {
                constraints(nullable: true)
            }
            column(name: "longitude", type: "DECIMAL(8, 5)") {
                constraints(nullable: true)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-table_room-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='table_room' AND " +
                    "CONSTRAINT_NAME='FK-table_room-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-table_room-club_id", baseTableName: "table_room", baseColumnNames:
                "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createIndexes-table_room") {
        createIndex(indexName: "IDX-table_room-club_id", tableName: "table_room", unique: false) {
            column(name: "club_id")
        }
    }
}
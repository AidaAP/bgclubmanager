databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-game") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='game'")
        }
        createTable(tableName: "game") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-game-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT") {
                constraints(nullable: true)
            }
            column(name: "url_image", type: "VARCHAR(255)") {
                constraints(nullable: true)
            }
            column(name: "copies", type: "INT") {
                constraints(nullable: false)
            }
            column(name: "url_bgg", type: "VARCHAR(255)") {
                constraints(nullable: true)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-game-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='game' AND CONSTRAINT_NAME='FK-game-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-game-club_id", baseTableName: "game", baseColumnNames: "club_id",
                referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createIndexes-game") {
        createIndex(indexName: "IDX-game-club_id", tableName: "game", unique: false) {
            column(name: "club_id")
        }
    }
}
databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-club") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='club'")
        }
        createTable(tableName: "club") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-club-id")
            }
            column(name: "owner_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT")
            column(name: "email", type: "VARCHAR(255)")
            column(name: "phone", type: "VARCHAR(50)")
            column(name: "latitude", type: "DECIMAL(7, 5)")
            column(name: "longitude", type: "DECIMAL(8, 5)")
            column(name: "url_image", type: "VARCHAR(255)")
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-club") {
        createIndex(indexName: "IDX-club-owner_id", tableName: "club", unique: false) {
            column(name: "owner_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-club-owner_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='club' AND CONSTRAINT_NAME='FK-club-owner_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-club-owner_id", baseTableName: "club", baseColumnNames: "owner_id",
                referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-member") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='member'")
        }
        createTable(tableName: "member") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-member-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "type", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "start_date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-member") {
        createIndex(indexName: "IDX-member-club_id", tableName: "member", unique: false) {
            column(name: "club_id")
        }
        createIndex(indexName: "IDX-member-user_id", tableName: "member", unique: false) {
            column(name: "user_id")
        }
        createIndex(indexName: "IDX-member-club_id_type", tableName: "member", unique: false) {
            column(name: "club_id")
            column(name: "type")
        }
        createIndex(indexName: "IDX-member-user_id_type", tableName: "member", unique: false) {
            column(name: "user_id")
            column(name: "type")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-member-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='member' AND CONSTRAINT_NAME='FK-member-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-member-club_id", baseTableName: "member", baseColumnNames:
                "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-member-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='member' AND CONSTRAINT_NAME='FK-member-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-member-user_id", baseTableName: "member", baseColumnNames:
                "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-member-club_id_user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='member' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-member-club_id_user_id", tableName: "member", columnNames: "club_id, " +
                "user_id")
    }
}
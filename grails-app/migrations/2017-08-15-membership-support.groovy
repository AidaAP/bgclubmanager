databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-membership_request") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='membership_request'")
        }
        createTable(tableName: "membership_request") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-membership_request-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-membership_request") {
        createIndex(indexName: "IDX-membership_request-club_id", tableName: "membership_request", unique: false) {
            column(name: "club_id")
        }
        createIndex(indexName: "IDX-membership_request-user_id", tableName: "membership_request", unique: false) {
            column(name: "user_id")
        }
        createIndex(indexName: "IDX-membership_request-club_id_user_id", tableName: "membership_request", unique:
                true) {
            column(name: "club_id")
            column(name: "user_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-membership_request-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='membership_request' AND " +
                    "CONSTRAINT_NAME='FK-membership_request-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-membership_request-club_id", baseTableName: "membership_request",
                baseColumnNames: "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-membership_request-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='membership_request' AND " +
                    "CONSTRAINT_NAME='FK-membership_request-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-membership_request-user_id", baseTableName: "membership_request",
                baseColumnNames: "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-membership_request-club_id_user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='membership_request' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-membership_request-club_id_user_id", tableName: "membership_request",
                columnNames: "club_id, user_id")
    }
}
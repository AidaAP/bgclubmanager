databaseChangeLog = {
    include file: '2017-04-23-initial-database.groovy'
    include file: '2017-04-26-user-language-support.groovy'
    include file: '2017-06-12-user-token-support.groovy'
    include file: '2017-06-14-club-support.groovy'
    include file: '2017-08-15-membership-support.groovy'
    include file: '2017-08-28-announcement-support.groovy'
    include file: '2017-09-11-poll-support.groovy'
    include file: '2017-10-14-event-support.groovy'
    include file: '2017-11-05-game-support.groovy'
    include file: '2017-11-08-table-support.groovy'
    include file: '2017-11-21-reserve-support.groovy'
}
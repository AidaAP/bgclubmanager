databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-event") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='event'")
        }
        createTable(tableName: "event") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-event-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "name", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT") {
                constraints(nullable: true)
            }
            column(name: "start_date", type: "DATETIME") {
                constraints(nullable: false)
            }
            column(name: "end_date", type: "DATETIME") {
                constraints(nullable: false)
            }
            column(name: "latitude", type: "DECIMAL(7, 5)") {
                constraints(nullable: true)
            }
            column(name: "longitude", type: "DECIMAL(8, 5)") {
                constraints(nullable: true)
            }
            column(name: "is_public", type: "BIT(1)") {
                constraints(nullable: false)
            }
            column(name: "max_attendees", type: "INT") {
                constraints(nullable: true)
            }
            column(name: "start_date_enrollment", type: "DATETIME") {
                constraints(nullable: false)
            }
            column(name: "end_date_enrollment", type: "DATETIME") {
                constraints(nullable: false)
            }
            column(name: "self_enrollment", type: "BIT(1)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-event-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='event' AND CONSTRAINT_NAME='FK-event-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-event-club_id", baseTableName: "event", baseColumnNames: "club_id",
                referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-attendance_request") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance_request'")
        }
        createTable(tableName: "attendance_request") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-attendance_request-id")
            }
            column(name: "event_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-attendance_request") {
        createIndex(indexName: "IDX-attendance_request-event_id", tableName: "attendance_request", unique: false) {
            column(name: "event_id")
        }
        createIndex(indexName: "IDX-attendance_request-user_id", tableName: "attendance_request", unique: false) {
            column(name: "user_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-attendance_request-event_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance_request' AND " +
                    "CONSTRAINT_NAME='FK-attendance_request-event_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-attendance_request-event_id", baseTableName: "attendance_request",
                baseColumnNames: "event_id", referencedTableName: "event", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-attendance_request-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance_request' AND " +
                    "CONSTRAINT_NAME='FK-attendance_request-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-attendance_request-user_id", baseTableName: "attendance_request",
                baseColumnNames: "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-attendance_request-event_id_user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance_request' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-attendance_request-event_id_user_id", tableName: "attendance_request",
                columnNames: "event_id, user_id")
    }

    changeSet(author: "aida.aljan", id: "createTable-attendance") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance'")
        }
        createTable(tableName: "attendance") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-attendance-id")
            }
            column(name: "event_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-attendance") {
        createIndex(indexName: "IDX-attendance-event_id", tableName: "attendance", unique: false) {
            column(name: "event_id")
        }
        createIndex(indexName: "IDX-attendance-user_id", tableName: "attendance", unique: false) {
            column(name: "user_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-attendance-event_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance' AND " +
                    "CONSTRAINT_NAME='FK-attendance-event_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-attendance-event_id", baseTableName: "attendance",
                baseColumnNames: "event_id", referencedTableName: "event", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-attendance-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance' AND " +
                    "CONSTRAINT_NAME='FK-attendance-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-attendance-user_id", baseTableName: "attendance",
                baseColumnNames: "user_id", referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-attendance-event_id_user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='attendance' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-attendance-event_id_user_id", tableName: "attendance", columnNames:
                "event_id, user_id")
    }

    changeSet(author: "aida.aljan", id: "createTable-oragnization_request") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization_request'")
        }
        createTable(tableName: "organization_request") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-organization_request-id")
            }
            column(name: "event_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-organization_request") {
        createIndex(indexName: "IDX-organization_request-event_id", tableName: "organization_request", unique: false) {
            column(name: "event_id")
        }
        createIndex(indexName: "IDX-organization_request-club_id", tableName: "organization_request", unique: false) {
            column(name: "club_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-organization_request-event_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization_request' AND " +
                    "CONSTRAINT_NAME='FK-organization_request-event_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-organization_request-event_id", baseTableName:
                "organization_request", baseColumnNames: "event_id", referencedTableName: "event",
                referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-organization_request-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization_request' AND " +
                    "CONSTRAINT_NAME='FK-organization_request-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-organization_request-club_id", baseTableName:
                "organization_request", baseColumnNames: "club_id", referencedTableName: "club", referencedColumnNames:
                "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-organization_request-event_id_club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization_request' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-organization_request-event_id_club_id", tableName:
                "organization_request", columnNames: "event_id, club_id")
    }

    changeSet(author: "aida.aljan", id: "createTable-organization") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization'")
        }
        createTable(tableName: "organization") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-organization-id")
            }
            column(name: "event_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-organization") {
        createIndex(indexName: "IDX-organization-event_id", tableName: "organization", unique: false) {
            column(name: "event_id")
        }
        createIndex(indexName: "IDX-organization-club_id", tableName: "organization", unique: false) {
            column(name: "club_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-organization-event_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization' AND " +
                    "CONSTRAINT_NAME='FK-organization-event_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-organization-event_id", baseTableName: "organization",
                baseColumnNames: "event_id", referencedTableName: "event", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-organization-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization' AND " +
                    "CONSTRAINT_NAME='FK-organization-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-organization-club_id", baseTableName: "organization",
                baseColumnNames: "club_id", referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addUniqueConstraint-organization-event_id_club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='organization' AND CONSTRAINT_TYPE='UNIQUE'")
        }
        addUniqueConstraint(constraintName: "UQ-organization-event_id_club_id", tableName: "organization", columnNames:
                "event_id, club_id")
    }
}
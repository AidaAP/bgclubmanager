databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-poll") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='poll'")
        }
        createTable(tableName: "poll") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-poll-id")
            }
            column(name: "club_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "title", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "description", type: "TEXT") {
                constraints(nullable: true)
            }
            column(name: "start_date_voting", type: "DATETIME") {
                constraints(nullable: true)
            }
            column(name: "end_date_voting", type: "DATETIME") {
                constraints(nullable: true)
            }
            column(name: "published", type: "BIT(1)") {
                constraints(nullable: false)
            }
            column(name: "start_date_publish", type: "DATETIME") {
                constraints(nullable: true)
            }
            column(name: "end_date_publish", type: "DATETIME") {
                constraints(nullable: true)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-poll") {
        createIndex(indexName: "IDX-poll-start_date_voting_end_date_voting", tableName: "poll", unique: false) {
            column(name: "start_date_voting")
            column(name: "end_date_voting")
        }
        createIndex(indexName: "IDX-poll-published_start_date_publish_end_date_publish", tableName: "poll", unique:
                false) {
            column(name: "published")
            column(name: "start_date_publish")
            column(name: "end_date_publish")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-poll-club_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='poll' AND CONSTRAINT_NAME='FK-poll-club_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-poll-club_id", baseTableName: "poll", baseColumnNames: "club_id",
                referencedTableName: "club", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-poll_option") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='poll_option'")
        }
        createTable(tableName: "poll_option") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-poll_option-id")
            }
            column(name: "poll_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "text", type: "TEXT") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-poll_option") {
        createIndex(indexName: "IDX-poll_option-poll_id", tableName: "poll_option", unique: false) {
            column(name: "poll_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-poll_option-poll_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='poll_option' AND " +
                    "CONSTRAINT_NAME='FK-poll_option-poll_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-poll_option-poll_id", baseTableName: "poll_option",
                baseColumnNames: "poll_id", referencedTableName: "poll", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "createTable-vote") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='vote'")
        }
        createTable(tableName: "vote") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-vote-id")
            }
            column(name: "user_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "poll_option_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
            column(name: "date", type: "DATETIME") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-vote") {
        createIndex(indexName: "IDX-vote-user_id", tableName: "vote", unique: false) {
            column(name: "user_id")
        }
        createIndex(indexName: "IDX-vote-poll_option_id", tableName: "vote", unique: false) {
            column(name: "poll_option_id")
        }
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-vote-user_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='vote' AND CONSTRAINT_NAME='FK-vote-user_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-vote-user_id", baseTableName: "vote", baseColumnNames: "user_id",
                referencedTableName: "user", referencedColumnNames: "id")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-vote-poll_option_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='vote' AND CONSTRAINT_NAME='FK-vote-poll_option_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-vote-poll_option_id", baseTableName: "vote", baseColumnNames:
                "poll_option_id", referencedTableName: "poll_option", referencedColumnNames: "id")
    }
}
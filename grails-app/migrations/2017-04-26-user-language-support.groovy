databaseChangeLog = {

    changeSet(author: "aida.aljan", id: "createTable-language") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='language'")
        }
        createTable(tableName: "language") {
            column(name: "id", type: "VARCHAR(255)") {
                constraints(nullable: false, primaryKey: true, primaryKeyName: "PK-language-id")
            }
            column(name: "code", type: "VARCHAR(5)") {
                constraints(nullable: false, unique: true, uniqueConstraintName: "UQ-language-code")
            }
        }
    }

    changeSet(author: "aida.aljan", id: "createIndexes-language") {
        createIndex(indexName: "IDX-language-code", tableName: "language", unique: true) {
            column(name: "code")
        }
    }

    changeSet(author: "aida.aljan", id: "create-initial-language-es") {
        preConditions(onFail: 'MARK_RAN') {
            dbms(type: "mysql")
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM language WHERE code='es'")
        }
        sql("INSERT INTO language VALUES (REPLACE(UUID(),'-',''), 'es')")
    }

    changeSet(author: "aida.aljan", id: "create-initial-language-en") {
        preConditions(onFail: 'MARK_RAN') {
            dbms(type: "mysql")
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM language WHERE code='en'")
        }
        sql("INSERT INTO language VALUES (REPLACE(UUID(),'-',''), 'en')")
    }

    changeSet(author: "aida.aljan", id: "addColumn-user-language_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user' AND COLUMN_NAME='language_id'")
        }
        addColumn(tableName: "user") {
            column(name: "language_id", type: "VARCHAR(255)") {
                constraints(nullable: false)
            }
        }
    }

    changeSet(author: "aida.aljan", id: "set-all-users-language") {
        sql("UPDATE user SET language_id=(SELECT id FROM language WHERE code='es')")
    }

    changeSet(author: "aida.aljan", id: "addForeignKeyConstraint-user-language_id") {
        preConditions(onFail: 'MARK_RAN') {
            sqlCheck(expectedResult: "0", "SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE " +
                    "TABLE_SCHEMA='bgclubmanager' AND TABLE_NAME='user' AND CONSTRAINT_NAME='FK-user-language_id'")
        }
        addForeignKeyConstraint(constraintName: "FK-user-language_id", baseTableName: "user", baseColumnNames:
                "language_id", referencedTableName: "language", referencedColumnNames: "id")
    }
}
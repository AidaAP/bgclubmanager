<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="description" content="Aplicación web de gestión de clubes de juegos de mesa">
    <meta name="keywords" content="board,mesa,game,juego,club">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>
    <title>
        <g:layoutTitle default="BGClubManager"/>
    </title>

    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="AdminLTE.css"/>

    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic"
          rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <asset:javascript src="application.js"/>
    <asset:javascript src="AdminLTE.js"/>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico"/>

    <g:layoutHead/>
</head>

<body class="hold-transition skin-bgcm sidebar-mini">
<div class="wrapper">

    <%-- ALERTS --%>
    <div id="alertContainer"></div>

    <%-- LOADER --%>
    <div id="loaderContainer" class="hidden">
        <div class="loader"></div>
    </div>

    <%-- Header --%>
    <g:mainHeader/>

    <%-- Navigation --%>
    <g:sideMenu/>

    <%-- Content Wrapper (page content) --%>
    <div class="content-wrapper">
        <g:layoutBody/>
    </div>

    <%-- Footer --%>
    <g:footer/>
</div>

<%-- COMMON MODAL --%>
<div id="modalCommon" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="labelCommon"></div>

</body>
</html>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="announcement.default.plural.label"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="announcement.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-newspaper-o"></i> <g:message code="sideMenu.club.announcements.label"/></li>
    </ol>
</section>

<section class="content">
    <%-- Only club admins can create announcements --%>
    <g:if test="${authUser.isAdminOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreateAnnouncement" type="button" class="btn bg-purple"><i
                    class="fa fa-plus"></i> <g:message code="default.create.label" args="${[message(code:
                    'announcement.default.label').toLowerCase()]}"/></button>
        </div>
    </g:if>
    <g:if test="${announcements}">
        <div class="row">
            <g:each var="element" in="${announcements}">
                <div class="col-md-12">
                    <div class="box box-announcement">
                        <div class="box-header with-border">
                            <g:link controller="announcement" action="show" params="${[clubId: club.id, id:
                                    element.announcement.id]}">
                                <div class="announcement-block">
                                    <span class="title ${element.read ? 'read' : ''}"/>${element.announcement.title}</span>
                                    <g:if test="${!element.announcement.published}">
                                        <span class="text-red"><g:message code="announcement.draft.label"/></span>
                                    </g:if>
                                    <g:else>
                                        <g:set var="startDatePublishStr" value="${formatDate(date:
                                                element.announcement.startDatePublish, type: 'datetime',
                                                style: 'SHORT')}"/>
                                        <g:set var="endDatePublishStr" value="${(element.announcement.endDatePublish &&
                                                authUser.isAdminOf(club)) ? ' - ' + formatDate(date:
                                                element.announcement.endDatePublish, type: 'datetime',
                                                style: 'SHORT') : ''}"/>
                                        <span class="datePublish">${startDatePublishStr}${endDatePublishStr}</span>
                                    </g:else>
                                </div>
                            </g:link>
                            <%-- Only club admins can edit/delete announcements --%>
                            <g:if test="${authUser.isAdminOf(club)}">
                                <div class="box-tools">
                                    <button id="btnShowEditAnnouncement_${element.announcement.id}" type="button"
                                            class="btn btn-box-tool"
                                            title="${message(code: 'default.button.edit.label')}"
                                            data-announcementid="${element.announcement.id}"><i
                                            class="fa fa-pencil"></i></button>
                                    <button id="btnShowConfirmDeleteAnnouncement_${element.announcement.id}"
                                            type="button" class="btn btn-box-tool"
                                            title="${message(code: 'default.button.delete.label')}"
                                            data-announcementid="${element.announcement.id}"><i
                                            class="fa fa-remove"></i></button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${announcementCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), announcementCount),
                                    announcementCount,
                                    message(code: 'announcement.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'announcement.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowCreateAnnouncement').on('click', function() {

            var url = '${createLink(controller: "announcement", action: "create")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowEditAnnouncement_"]').on('click', function() {

            var url = '${createLink(controller: "announcement", action: "edit")}';
            var data = { id: $(this).data('announcementid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowConfirmDeleteAnnouncement_"]').on('click', function() {

            var url = '${createLink(controller: "announcement", action: "confirmDelete")}';
            var data = { id: $(this).data('announcementid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
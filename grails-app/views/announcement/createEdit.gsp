<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'announcement.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditAnnouncement" controller="announcement" action="save" method="POST"
                class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${announcement.id}"/>
                <g:hiddenField name="club.id" value="${announcement.club.id}"/>
                <div id="formGroup_title" class="form-group">
                    <label for="title" class="col-md-2 control-label"><g:message
                            code="announcement.title.label"/> *</label>
                    <div class="col-md-10">
                        <g:textArea name="title" value="${announcement.title}" escapeHtml="${false}" rows="3"
                                    class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_content" class="form-group">
                    <label for="content" class="col-md-2 control-label"><g:message
                            code="announcement.content.label"/> *</label>
                    <div class="col-md-10">
                        <g:textArea name="content" value="${announcement.content}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_publication" class="form-group">
                    <div id="formGroup_published" class="col-md-offset-2 col-md-2 checkbox">
                        <label>
                            <g:checkBox name="published" value="${announcement.published}"/> <g:message
                                    code="announcement.published.label"/>
                            <span class="help-block"> </span>
                        </label>
                    </div>
                    <div id="formGroup_startDatePublish" class="col-md-4 ${announcement.published ? '' : 'disabled'}">
                        <label for="startDatePublish" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDatePublish" value="${announcement.startDatePublish}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDatePublish" class="col-md-4 ${announcement.published ? '' : 'disabled'}">
                        <label for="endDatePublish" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDatePublish" value="${announcement.endDatePublish}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditAnnouncement" name="btnCreateEditAnnouncement" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        CKEDITOR.replace('content', {
            toolbar: [
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike']},
                { name: 'color', items: ['TextColor', 'BGColor']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
                { name: 'insert', items: ['Link', 'Table', 'SpecialChar', 'Smiley']}
            ]
        });

        CKEDITOR.instances.content.on('change', function() {
            $('#content').val(CKEDITOR.instances.content.getData());
        });

        var datetimepickerOptions = {
            locale: '${authUser.locale}',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash-o',
                close: 'fa fa-remove'
            }
        };

        $('#startDatePublish').datetimepicker(datetimepickerOptions);
        <g:if test="${announcement.startDatePublish}">
            $('#startDatePublish').data("DateTimePicker").date(new Date('${announcement.startDatePublish}'));
        </g:if>
        $('#endDatePublish').datetimepicker(datetimepickerOptions);
        <g:if test="${announcement.endDatePublish}">
            $('#endDatePublish').data("DateTimePicker").date(new Date('${announcement.endDatePublish}'));
        </g:if>

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditAnnouncement #title').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditAnnouncement').trigger('reset');
            formValidation.clearErrors();
        });

        $('#published').on('change', function() {
            if (this.checked) {
                // Enable dates
                $('#formGroup_startDatePublish').removeClass('disabled');
                $('#startDatePublish').data("DateTimePicker").date(new Date());
                $('#formGroup_endDatePublish').removeClass('disabled');
            } else {
                // Disable dates
                $('#formGroup_startDatePublish').addClass('disabled');
                $('#formGroup_endDatePublish').addClass('disabled');
            }
        });

        $('#formCreateEditAnnouncement').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "announcement", action: "save")}';
            var data = $('#formCreateEditAnnouncement').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
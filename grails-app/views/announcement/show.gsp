<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'announcement.default.label')]}"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${announcement.club.id}">${announcement.club.name}</g:link></li>
        <li><g:link controller="announcement" action="index" params="[clubId: announcement.club.id]"><i
                class="fa fa-newspaper-o"></i> <g:message code="sideMenu.club.announcements.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'announcement.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
    <%-- Only club admins can edit/delete announcements --%>
        <g:if test="${authUser.isAdminOf(announcement.club)}">
            <div class="row col-md-12 margin-bottom">
                <button id="btnShowEditAnnouncement" type="button" class="btn bg-purple margin-r-5"><i
                        class="fa fa-pencil"></i> <g:message code="default.button.edit.label"/></button>
                <button id="btnShowConfirmDeleteAnnouncement" type="button" class="btn bg-purple"><i
                        class="fa fa-remove"></i> <g:message code="default.button.delete.label"/></button>
            </div>
        </g:if>
        <div class="row col-md-12 margin-bottom">
            <h1>${announcement.title}</h1>
            <g:if test="${!announcement.published}">
                <span class="text-red"><g:message code="announcement.draft.label"/></span>
            </g:if>
            <g:else>
                <span><g:formatDate date="${announcement.startDatePublish}" type="datetime" style="SHORT"/><g:if
                        test="${announcement.endDatePublish && authUser.isAdminOf(announcement.club)}"> - <g:formatDate
                            date="${announcement.endDatePublish}" type="datetime" style="SHORT"/></g:if></span>
            </g:else>
        </div>
        <div class="row col-md-12">
            ${raw(announcement.content)}
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowEditAnnouncement').on('click', function() {

            var url = '${createLink(controller: "announcement", action: "edit")}';
            var data = { id: '${announcement.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeleteAnnouncement').on('click', function() {

            var url = '${createLink(controller: "announcement", action: "confirmDelete")}';
            var data = { id: '${announcement.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
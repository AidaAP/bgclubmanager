<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="auth"/>
    <title><g:message code='springSecurity.login.title'/></title>
</head>

<body>
<div class="auth-box">
    <div class="auth-title"><a href="/"><b>BGClubManager</b></a></div>

    <div class="auth-form">

        <p class="auth-form-title"><g:message code='springSecurity.login.header'/></p>

        <g:if test='${flash.message}'>
            <div class="auth-form-message">${flash.message}</div>
        </g:if>

        <form action="${postUrl ?: '/login/authenticate'}" method="POST" id="loginForm" class="cssform" autocomplete="off">
            <div class="form-group has-feedback">
                <g:textField id="username" name="${usernameParameter ?: 'username'}" class="text_ form-control"
                             placeholder="${message(code: 'springSecurity.login.username.label')}"/>
                <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <g:passwordField id="password" name="${passwordParameter ?: 'password'}" class="text_ form-control"
                                 placeholder="${message(code: 'springSecurity.login.password.label')}"/>
                <span class="fa fa-lock form-control-feedback"></span>
            </div>
            <div id="remember_me_holder" class="row">
                <div class="col-xs-12">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"name="${rememberMeParameter ?: 'remember-me'}" id="remember_me"
                                       <g:if test='${hasCookie}'>checked="checked"</g:if>/> <g:message
                                    code='springSecurity.login.remember.me.label'/>
                            </label>
                        </div>
                    </div>
                    <div class="col-xs-4 pull-right">
                        <g:submitButton name="submit" value="${message(code: 'springSecurity.login.button')}"
                                        class="btn btn-bgcm"/>
                    </div>
                </div>
            </div>
            <div id="register_holder" class="row">
                <div class="col-xs-12">
                    <a href="${createLink(controller: 'user', action: 'register')}" class="pull-right"><g:message
                            code="user.register.button"/></a>
                </div>
            </div>
        </form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {
        $('#loginForm input').first().focus();
    });
</g:javascript>

</body>
</html>
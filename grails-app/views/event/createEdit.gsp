<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'event.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditEvent" controller="event" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${event.id}"/>
                <g:hiddenField name="club.id" value="${event.club.id}"/>
                <g:hiddenField name="latitude" value="${event.latitude}"/>
                <g:hiddenField name="longitude" value="${event.longitude}"/>
                <div id="formGroup_name" class="form-group">
                    <label for="name" class="col-md-2 control-label"><g:message code="event.name.label"/> *</label>
                    <div class="col-md-10">
                        <g:textField name="name" value="${event.name}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-2 control-label"><g:message
                            code="event.description.label"/></label>
                    <div class="col-md-10">
                        <g:textArea name="description" value="${event.description}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_eventDates" class="form-group">
                    <label class="col-md-2 control-label"><g:message code="event.dates.label"/></label>
                    <div id="formGroup_startDate" class="col-md-4">
                        <label for="startDate" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDate" value="${event.startDate}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDate" class="col-md-4">
                        <label for="endDate" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDate" value="${event.endDate}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </div>
                <div id="formGroup_enrollment" class="form-group">
                    <label class="col-md-2 control-label"><g:message code="event.enrollment.label"/></label>
                    <div id="formGroup_startDateEnrollment" class="col-md-4">
                        <label for="startDateEnrollment" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDateEnrollment" value="${event.startDateEnrollment}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDateEnrollment" class="col-md-4">
                        <label for="endDateEnrollment" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDateEnrollment" value="${event.endDateEnrollment}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_selfEntollment" class="col-md-2 checkbox">
                        <label>
                            <g:checkBox name="selfEnrollment" value="${event.selfEnrollment}"/> <g:message
                                    code="event.selfEnrollment.label"/>
                            <span class="help-block"> </span>
                        </label>
                    </div>
                </div>
                <div id="formGroup_isPublic" class="form-group">
                    <label for="isPublic" class="col-md-2 control-label"><g:message code="event.isPublic.label"/>
                    *</label>
                    <div class="col-md-10">
                        <div class="checkbox">
                            <label>
                                <g:checkBox name="isPublic" value="${event.isPublic}"/>
                            </label>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </div>
                <div id="formGroup_maxAttendees" class="form-group">
                    <label for="maxAttendees" class="col-md-2 control-label"><g:message
                            code="event.maxAttendees.label"/></label>
                    <div class="col-sm-6 col-md-3">
                        <g:textField name="maxAttendees" value="${event.maxAttendees}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_location" class="form-group">
                    <div class="col-md-12">
                        <label class="col-md-2 control-label"><a data-toggle="collapse"
                                                                 href="#collapseMapEdit${event.id}"><g:message
                                    code="default.location.label"/></a></label>
                        <div class="col-md-10 control-label">
                            <button id="btnClearLocation" type="button" class="btn btn-xs btn-default"><g:message
                                    code="default.clear.label"
                                    args="${[message(code: 'default.location.label').toLowerCase()]}"/></button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-offset-2 col-md-10">
                            <div id="collapseMapEdit${event.id}" class="panel-collapse collapse">
                                <div id="map_edit_${event.id}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditEvent" name="btnCreateEditEvent" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        CKEDITOR.replace('description', {
            toolbar: [
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike']},
                { name: 'color', items: ['TextColor', 'BGColor']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
                { name: 'insert', items: ['Link', 'Table', 'SpecialChar', 'Smiley']}
            ]
        });

        CKEDITOR.instances.description.on('change', function() {
            $('#description').val(CKEDITOR.instances.description.getData());
        });

        var datetimepickerOptions = {
            locale: '${authUser.locale}',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash-o',
                close: 'fa fa-remove'
            }
        };

        $('#startDate').datetimepicker(datetimepickerOptions);
        <g:if test="${event.startDate}">
            $('#startDate').data("DateTimePicker").date(new Date('${event.startDate}'));
        </g:if>
        $('#endDate').datetimepicker(datetimepickerOptions);
        <g:if test="${event.endDate}">
            $('#endDate').data("DateTimePicker").date(new Date('${event.endDate}'));
        </g:if>
        $('#startDateEnrollment').datetimepicker(datetimepickerOptions);
        <g:if test="${event.startDateEnrollment}">
            $('#startDateEnrollment').data("DateTimePicker").date(new Date('${event.startDateEnrollment}'));
        </g:if>
        $('#endDateEnrollment').datetimepicker(datetimepickerOptions);
        <g:if test="${event.endDateEnrollment}">
            $('#endDateEnrollment').data("DateTimePicker").date(new Date('${event.endDateEnrollment}'));
        </g:if>

        // Create map
        var map = L.map('map_edit_${event.id}', {
            // Default location: A Coruña
            center: L.latLng(43.362343, -8.411540),
            zoom: 13
        });

        // Add layer (Stamen.Terrain)
        L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
            {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 0,
                maxZoom: 18,
                ext: 'png'
            }
        ).addTo(map);

        var marker;

        <g:if test="${event.latitude && event.longitude}">

            var latitude = parseFloat('${event.latitude}');
            var longitude = parseFloat('${event.longitude}');

            // Locate map
            map.flyTo(new L.LatLng(latitude, longitude));

            // Add club marker
            marker = L.marker(
                { lat: latitude, lng: longitude },
                { draggable: true }
            ).addTo(map);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        </g:if>
        <g:else>
            $('#btnClearLocation').hide();
        </g:else>

        // Fix map visualization on expand
        $('#collapseMapEdit${event.id}').on('shown.bs.collapse', function () {
            map.invalidateSize(true);
        });

        // Detect location changes
        map.on('click', function(event) {
            if (marker) {
                map.removeLayer(marker);
            }
            marker = L.marker(
                event.latlng,
                { draggable: true }
            ).addTo(map);
            $('#btnClearLocation').show();
            updateLatitudeLongitude(event.latlng.lat, event.latlng.lng);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        });

        function updateLatitudeLongitude(latitude, longitude) {
            $('input#latitude:hidden').val(latitude);
            $('input#longitude:hidden').val(longitude);
        };

        $('#btnClearLocation').on('click', function() {
            updateLatitudeLongitude(null, null);
            marker.removeFrom(map);
            $('#btnClearLocation').hide();
        });

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditEvent #name').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditEvent').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCreateEditEvent').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "event", action: "save")}';
            var data = $('#formCreateEditEvent').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
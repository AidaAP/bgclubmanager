<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'event.default.label')]}"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${event.club.id}">${event.club.name}</g:link></li>
        <li><g:link controller="event" action="index" params="[clubId: event.club.id]"><i class="fa fa-ticket"></i>
            <g:message code="sideMenu.club.events.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'event.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row col-md-12 margin-bottom">
            <g:render template="showActions"/>
        </div>
        <div class="row col-md-12 margin-bottom">
            <h1><g:if test="${isAttendee}"><span class="attendee text-navy" title="${message(code:
                    'event.attendee.title')}"><i class="fa fa-calendar-check-o"></i></span> </g:if>${event.name}</h1>
            <span><g:formatDate date="${event.startDate}" type="datetime" style="SHORT"/> - <g:formatDate
                date="${event.endDate}" type="datetime" style="SHORT"/></span>
            <g:if test="${event.isEnrollable()}">
                <g:set var="startDateEnrollmentStr" value="${message(code: event.selfEnrollment ?
                        'event.selfEnrollmentDates.label' : 'event.enrollmentDates.label') + ' ' + message(code:
                        'default.date.from.label').toLowerCase() + ' ' + formatDate(date: event.startDateEnrollment,
                        type: 'datetime', style: 'SHORT')}"/>
                <g:set var="endDateEnrollmentStr" value="${' ' + message(code: 'default.date.to.label').toLowerCase() +
                        ' ' + formatDate(date: event.endDateEnrollment, type: 'datetime', style: 'SHORT')}"/>
                <br/><span class="text-info">${startDateEnrollmentStr}${endDateEnrollmentStr}</span>
            </g:if>
        </div>
        <div class="row col-md-12 ${!(event.latitude && event.longitude) ? 'margin-bottom' : ''}">
            <div>
                <label><a data-toggle="collapse" href="#collapseOrganizersShow${event.id}"><g:message
                        code="organizer.default.plural.label"/></a></label>
                <div id="collapseOrganizersShow${event.id}" class="panel panel-collapse collapse">
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-${authUser.isAdminOf(event.club) ? '9' : '12'}">
                            <g:each var="organizer" in="${organizers}" status="i">
                                <g:link controller="club" action="show"
                                        id="${organizer.id}">${organizer.name}</g:link>${i < organizers.size() - 1 ?
                                    ', ' : ''}
                            </g:each>
                        </div>
                        <%-- Only owner club admins can manage event organizers --%>
                        <g:if test="${authUser.isAdminOf(event.club)}">
                            <div class="col-xs-12 col-md-3">
                                <button id="btnShowManageEventOrganizers" type="button"
                                        class="btn bg-navy margin-r-5"><i class="fa fa-cogs"></i> <g:message
                                        code="default.manage.label" args="${[message(code:
                                                'organizer.default.plural.label').toLowerCase()]}"/></button>
                            </div>
                        </g:if>
                    </div>
                </div>
            </div>
            <div>
                <label><g:message code="event.isPublic.label"/>: </label>
                <span>${message(code: "default.${event.isPublic ? 'yes' : 'no'}.label")}</span>
            </div>
            <div>
                <label><g:message code="event.selfEnrollment.label"/>: </label>
                <span>${message(code: "default.${event.selfEnrollment ? 'yes' : 'no'}.label")}</span>
            </div>
            <div>
                <label><g:message code="event.maxAttendees.label"/>: </label>
                <span>${event.maxAttendees != null ? event.maxAttendees : '-'}</span>
            </div>
            <div>
                <label><g:if test="${isOrganizerAdmin || isAttendee}"><a data-toggle="collapse"
                href="#collapseAttendeesShow${event.id}"></g:if><g:message code="attendance.number.label"/><g:if
                        test="${isOrganizerAdmin || isAttendee}"></a></g:if>: </label>
                <span>${numAttendees}</span>
                <g:if test="${isOrganizerAdmin || isAttendee}">
                    <div id="collapseAttendeesShow${event.id}" class="panel panel-collapse collapse">
                        <div class="panel-body">
                            <div class="col-xs-12 col-md-${(event.isEnrollable() && authUser.isAdminOf(event.club)) ?
                                    '9' : '12'}">
                                <g:each var="attendee" in="${attendees}" status="i">
                                    <g:link controller="user" action="show"
                                            params="${[username: attendee.username]}">${attendee.username}</g:link>${i <
                                        attendees.size() - 1 ? ', ' : ''}
                                </g:each>
                            </div>
                            <%-- Only owner club admins can manage event attendees --%>
                            <g:if test="${event.isEnrollable() && authUser.isAdminOf(event.club)}">
                                <div class="col-xs-12 col-md-3">
                                    <button id="btnShowManageEventAttendees" type="button"
                                            class="btn bg-navy margin-r-5"><i class="fa fa-cogs"></i> <g:message
                                            code="default.manage.label" args="${[message(code:
                                                    'attendee.default.plural.label').toLowerCase()]}"/></button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </g:if>
            </div>
        </div>
        <g:if test="${event.latitude && event.longitude}">
            <div class="row col-md-12 margin-bottom">
                <b><a data-toggle="collapse" href="#collapseMapShow${event.id}"><g:message
                        code="default.location.label"/></a></b>
                <div id="collapseMapShow${event.id}" class="panel-collapse collapse">
                    <div id="map_show_${event.id}"></div>
                </div>
            </div>
        </g:if>
        <g:if test="${event.description}">
            <div class="row col-md-12">
                ${raw(event.description)}
            </div>
        </g:if>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        <g:if test="${event.latitude && event.longitude}">

            var latitude = parseFloat('${event.latitude}');
            var longitude = parseFloat('${event.longitude}');

            // Create map
            var map = L.map('map_show_${event.id}', {
                // Event location
                center: L.latLng(latitude, longitude),
                zoom: 13
            });

            // Add layer (Stamen.Terrain)
            L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
                {
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    subdomains: 'abcd',
                    minZoom: 0,
                    maxZoom: 18,
                    ext: 'png'
                }
            ).addTo(map);

            // Add event marker
            L.marker(
                { lat: latitude, lng: longitude }
            ).addTo(map);

            // Fix map visualization on expand
            $('#collapseMapShow${event.id}').on('shown.bs.collapse', function () {
                map.invalidateSize(true);
            });
        </g:if>

        $('#btnShowManageEventOrganizers').on('click', function() {

            var url = '${createLink(controller: "organization", action: "index")}';
            var data = { eventId: '${event.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowManageEventAttendees').on('click', function() {

            var url = '${createLink(controller: "attendance", action: "index")}';
            var data = { eventId: '${event.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
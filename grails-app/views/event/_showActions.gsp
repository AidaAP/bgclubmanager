<%-- Only owner club admins can edit events --%>
<g:if test="${authUser.isAdminOf(event.club)}">
    <button id="btnShowEditEvent" type="button" class="btn bg-navy margin-r-5"><i class="fa fa-pencil"></i> <g:message
            code="default.button.edit.label"/></button>
</g:if>
<%-- Only owner club admins can manage delete events --%>
<g:if test="${authUser.isAdminOf(event.club)}">
    <button id="btnShowConfirmDeleteEvent" type="button" class="btn bg-navy"><i class="fa fa-remove"></i> <g:message
            code="default.button.delete.label"/></button>
</g:if>
<%-- Only organizer admins can leave organization --%>
<g:elseif test="${isOrganizerAdmin}">
    <button id="btnShowConfirmLeaveEvent" type="button" class="btn btn-danger"><i class="fa fa-sign-out"></i> <g:message
            code="organization.leave.label"/></button>
</g:elseif>
<%-- A user can only request attendance if he is not an attendee already --%>
<g:if test="${event.isEnrollable() && !(isAttendee || hasAttendanceRequest)}">
    <button id="btnRequestAttendance" type="button" class="btn bg-navy"><i
            class="fa fa-calendar-plus-o"></i> <g:message code="attendance.request.label"/></button>
</g:if>
<%-- A user can only cancel attendance if he is an attendee already and event is enrollable --%>
<g:if test="${event.isEnrollable() && isAttendee}">
    <button id="btnCancelAttendance" type="button" class="btn bg-navy"><i class="fa fa-calendar-minus-o"></i> <g:message
            code="default.cancel.label" args="${[message(code: 'attendance.default.label').toLowerCase()]}"/></button>
</g:if>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowEditEvent').on('click', function() {

            var url = '${createLink(controller: "event", action: "edit")}';
            var data = { id: '${event.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeleteEvent').on('click', function() {

            var url = '${createLink(controller: "event", action: "confirmDelete")}';
            var data = { id: '${event.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmLeaveEvent').on('click', function() {

            var url = '${createLink(controller: "organization", action: "confirmDelete")}';
            var data = { eventId: '${event.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnRequestAttendance').on('click', function() {

            var url = '${createLink(controller: "attendance", action: "save")}';
            var data = { 'event.id': '${event.id}' };

            $.get(url, data, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                }

                // Reload page
                window.location.reload();
            });
        });

        $('#btnCancelAttendance').on('click', function() {

            var url = '${createLink(controller: "attendance", action: "cancel")}';
            var data = { 'event.id': '${event.id}', attendees: '${authUser.id}' };

            $.get(url, data, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                }

                // Reload page
                window.location.reload();
            });
        });
    });
</g:javascript>
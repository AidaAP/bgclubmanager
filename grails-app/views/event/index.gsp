<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="event.default.plural.label"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="event.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-ticket"></i> <g:message code="sideMenu.club.events.label"/></li>
    </ol>
</section>

<section class="content">
    <%-- Only club admins can create events --%>
    <g:if test="${authUser.isAdminOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreateEvent" type="button" class="btn bg-navy"><i class="fa fa-plus"></i> <g:message
                    code="default.create.label" args="${[message(code: 'event.default.label').toLowerCase()]}"/></button>
        </div>
    </g:if>
    <g:if test="${events}">
        <div class="row">
            <g:each var="event" in="${events}">
                <div class="col-md-12">
                    <div class="box box-event">
                        <div class="box-header with-border">
                            <g:link controller="event" action="show" params="${[clubId: club.id, id: event.id]}">
                                <div class="event-block">
                                    <g:if test="${authUser.isAttendeeOf(event)}">
                                        <span class="attendee text-navy" title="${message(code:
                                                'event.attendee.title')}"><i class="fa fa-calendar-check-o"></i></span>
                                    </g:if>
                                    <span class="title"/>${event.name}</span>
                                    <g:set var="startDateStr" value="${formatDate(date: event.startDate, type:
                                            'datetime', style: 'SHORT')}"/>
                                    <g:set var="endDateStr" value="${' - ' + formatDate(date: event.endDate, type:
                                            'datetime', style: 'SHORT')}"/>
                                    <g:set var="enrollmentStr" value="${event.isEnrollable() ? ' (' + message(code:
                                            event.selfEnrollment ? 'event.selfEnrollmentDates.label':
                                                    'event.enrollmentDates.label') + ' ' + message(code:
                                            'default.date.to.label').toLowerCase() + ' ' + formatDate(date:
                                            event.endDateEnrollment, type: 'datetime', style: 'SHORT') + ')' : ''}"/>
                                <span class="datePublish">${startDateStr}${endDateStr}${enrollmentStr}</span>
                                </div>
                            </g:link>
                        <%-- Only club admins can edit/delete events --%>
                            <g:if test="${authUser.isAdminOf(club)}">
                                <div class="box-tools">
                                    <button id="btnShowEditEvent_${event.id}" type="button" class="btn btn-box-tool"
                                            title="${message(code: 'default.button.edit.label')}"
                                            data-eventid="${event.id}"><i class="fa fa-pencil"></i></button>
                                    <button id="btnShowConfirmDeleteEvent_${event.id}" type="button"
                                            class="btn btn-box-tool" title="${message(code:
                                            'default.button.delete.label')}" data-eventid="${event.id}"><i
                                            class="fa fa-remove"></i></button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${eventCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), eventCount), eventCount,
                                    message(code: 'event.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'event.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowCreateEvent').on('click', function() {

            var url = '${createLink(controller: "event", action: "create")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowEditEvent_"]').on('click', function() {

            var url = '${createLink(controller: "event", action: "edit")}';
            var data = { id: $(this).data('eventid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowConfirmDeleteEvent_"]').on('click', function() {

            var url = '${createLink(controller: "event", action: "confirmDelete")}';
            var data = { id: $(this).data('eventid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
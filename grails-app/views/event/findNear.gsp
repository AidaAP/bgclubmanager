<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.search.label" args="${[message(code: 'event.default.plural.label')]}"/></title>

    <asset:stylesheet src="fullcalendar/fullcalendar.min.css"/>

    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="fullcalendar/fullcalendar.min.js"/>
    <asset:javascript src="fullcalendar/locale-all.js"/>

    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="event.default.plural.label"/></h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                <i class="fa fa-info-circle margin-r-5"></i><g:message code="event.find.note"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="box box-event">
                <div class="box-body no-padding">
                    <div id="map"></div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="box box-event">
                <div class="box-body no-padding">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        // Create map
        var map = L.map('map', {
            // Default location: A Coruña
            center: L.latLng(43.362343, -8.411540),
            zoom: 13
        });

        // Locate map
        map.locate({ setView: true, maxZoom: 13 });
        map.on('locationerror', function() { map.flyTo(new L.LatLng(43.362343, -8.411540)); });

        // Add layer (Stamen.Terrain)
        L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
            {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 0,
                maxZoom: 18,
                ext: 'png'
            }
        ).addTo(map);

        var markers = [];

        // Listen to map changes
        map.on('locationfound', function(event) { findNearEvents() });
        map.on('zoomend', function(event) { findNearEvents() });
        map.on('moveend', function(event) { findNearEvents() });

        // Fix map visualization on expand
        $('#collapseMapFindEvents').on('shown.bs.collapse', function () {
            map.invalidateSize(true);
        });

        $('#calendar').fullCalendar({
            locale: '${authUser.locale}',
            timezone: 'local',
            header: {
                left: 'prev,next',
                center: 'title',
                right: ''
            },
            events: function(start, end, timezone, callback) {

                var startStr = start.format('YYYY-MM-DD');
                var endStr = end.format('YYYY-MM-DD');

                var bounds = map.getBounds();
                var neLat = bounds._northEast.lat;
                var neLong = bounds._northEast.lng;
                var swLat = bounds._southWest.lat;
                var swLong = bounds._southWest.lng;

                var url = '${createLink(controller: "event", action: "findNearEvents")}';
                var data = { start: startStr, end: endStr, neLat: neLat, neLong: neLong, swLat: swLat, swLong: swLong };

                $.getJSON(url, data, function(response) {
                    if (response.error) {
                        // Show error
                        alerts.show(error, 'danger', 4000);
                    } else {

                        // Update calendar events
                        callback(response);

                        // Remove old markers
                        $.each(markers, function(index, marker) {
                            marker.removeFrom(map);
                        });

                        // Add new markers
                        $.each(response, function(index, event) {

                            var marker = L.marker(
                                { lat: parseFloat(event.latitude), lng: parseFloat(event.longitude) },
                                { title: event.title, alt: event.title, riseOnHover: true }
                            );

                            var popupContent = '<b><a href="' + event.url + '">' + event.title + '</a></b>';
                            if (event.description != null) {
                                popupContent += '<br>' + event.description;
                            };

                            marker.bindPopup(popupContent);
                            marker.addTo(map);
                            markers.push(marker);
                        });
                    }
                });
            },
            eventRender: function(event, element) {
                element.attr('title', event.title);
            },
            aspectRatio: 1.4
        });

        function findNearEvents() {
            $('#calendar').fullCalendar('refetchEvents');
        };
    });
</g:javascript>

</body>
</html>
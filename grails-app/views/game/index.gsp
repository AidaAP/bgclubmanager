<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="game.default.plural.label"/></title>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="game.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-puzzle-piece"></i> <g:message code="sideMenu.club.games.label"/></li>
    </ol>
</section>

<section class="content">
<%-- Only club admins can create games --%>
    <g:if test="${authUser.isAdminOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <div class="btn-group">
                <button type="button" class="btn bg-yellow dropdown-toggle" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="true"><i class="fa fa-plus"></i> <g:message
                        code="default.add.label" args="${[message(code: 'game.default.label').toLowerCase()]}"/> <span
                        class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li id="btnShowCreateGameFromBGG"><a href="javascript:void(0);"><g:message
                            code="game.create.fromBGG.label"/></a></li>
                    <li id="btnShowCreateGameManually"><a href="javascript:void(0);"><g:message
                            code="game.create.manually.label"/></a></li>
                </ul>
            </div>
        </div>
    </g:if>
    <g:if test="${games}">
        <div class="row">
            <g:each var="game" in="${games}">
                <g:link controller="game" action="show" params="${[clubId: club.id, id: game.id]}"
                        class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10">
                    <div class="game-card col-md-12">
                        <div class="col-md-4 hidden-xs">
                            <g:gameImage gameId="${game.id}"/>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <span class="name">${game.name}</span>
                        </div>
                    </div>
                </g:link>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${gameCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), gameCount), gameCount,
                                    message(code: 'game.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'game.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowCreateGameFromBGG').on('click', function() {

            var url = '${createLink(controller: "game", action: "showBGGSearch")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowCreateGameManually').on('click', function() {

            var url = '${createLink(controller: "game", action: "create")}';
            var data = { clubId: '${club.id}', origin: 'MANUAL' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
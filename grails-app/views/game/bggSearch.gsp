<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.search.label" args="${[message(code:
                    'game.default.plural.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formBGGSearch" controller="game" action="bggSearch" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <div id="formGroup_name" class="form-group">
                    <div class="col-md-12">
                        <div class="input-group col-md-12">
                            <g:textField id="name" name="name" class="form-control" placeholder="${message(code:
                                    'default.button.search.label')}..."/>
                            <span class="input-group-btn">
                                <button id="btnBGGSearch" type="submit" class="btn btn-default"><span
                                        class="fa fa-search"></span></button>
                            </span>
                        </div>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_games" class="form-group hidden">
                    <div class="col-md-12">
                        <g:select name="games" from="${[]}" class="form-control selectpicker"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button id="btnShowCreateEditGame" type="button" class="btn btn-bgcm" disabled="disabled"><g:message
                        code="default.paginate.next"/></button>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        var $select = $('select[name="games"]');
        $select.selectpicker();

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formBGGSearch #name').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formBGGSearch').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formBGGSearch').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            // Disable 'next' button
            $('#btnShowCreateEditGame').attr('disabled', true);

            var url = '${createLink(controller: "game", action: "bggSearch")}';
            var data = $('#formBGGSearch').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                    loader.hide();
                } else if (response.games) {

                    $('#formGroup_games').removeClass('hidden');

                    var $select = $('select[name="games"]');

                    $select.empty();

                    $.each(response.games, function(idx, game) {
                        var option = '<option id="' + game.urlBGG + '" data-name="' + game.name +
                            '" data-description="' + game.description + '" data-urlimage="' + game.urlImage +
                            '" data-urlbgg="' + game.urlBGG + '">' + game.urlBGG + '</option>';
                        var $option = $(option);
                        var content = '<span><img src="' + game.urlImage + '" class="img-sm margin-r-5"/> ' + game.name + '</span>';
                        $option.attr('data-content', content);
                        $select.append($option);
                    });

                    $select.selectpicker('refresh');

                    if ($select.selectpicker('val')) {
                        // Enable 'next' button
                        $('#btnShowCreateEditGame').attr('disabled', false);
                    }

                    loader.hide();
                }
            });
        });

        $('#btnShowCreateEditGame').on('click', function() {

            loader.show();

            var selectedOption = $select.find('option[id="' + $select.selectpicker('val') + '"]');

            var url = '${createLink(controller: "file", action: "uploadGameExternalImage")}';
            var data = { externalImageUrl: selectedOption.data('urlimage') };
            $.post(url, data, function(savedFilePath) {

                url = '${createLink(controller: "game", action: "create")}';
                var data = {
                    clubId: '${club.id}',
                    name: selectedOption.data('name'),
                    description: selectedOption.data('description'),
                    urlImage: savedFilePath,
                    urlBGG: selectedOption.data('urlbgg')
                };

                $.get(url, data, function(response) {
                    var error = response.error;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else {
                        $('#modalCommon').html(response);
                    }
                    loader.hide();
                });
            });
        });
    });
</g:javascript>
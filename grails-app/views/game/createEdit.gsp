<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'game.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditGame" controller="game" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${game.id}"/>
                <g:hiddenField name="club.id" value="${game.club.id}"/>
                <g:hiddenField name="urlImage" value="${game.urlImage}"/>
                <div id="formGroup_urlImageDz" class="form-group">
                    <div id="urlImageDz" class="dropzone"></div>
                </div>
                <div id="formGroup_name" class="form-group">
                    <label for="name" class="col-md-2 control-label"><g:message code="game.name.label"/> *</label>
                    <div class="col-md-10">
                        <g:textField name="name" value="${game.name}" escapeHtml="${false}" rows="3"
                                     class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-2 control-label"><g:message
                            code="game.description.label"/></label>
                    <div class="col-md-10">
                        <g:textArea name="description" value="${game.description}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_copies" class="form-group">
                    <label for="copies" class="col-md-2 control-label"><g:message code="game.copies.label"/> *</label>
                    <div class="col-md-3">
                        <g:textField name="copies" value="${game.copies}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_urlBGG" class="form-group">
                    <label for="urlBGG" class="col-md-2 control-label"><g:message code="game.urlBGG.label"/></label>
                    <div class="col-md-10">
                        <g:textField name="urlBGG" value="${game.urlBGG}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditAnnouncement" name="btnCreateEditAnnouncement" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        CKEDITOR.replace('description', {
            toolbar: [
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike']},
                { name: 'color', items: ['TextColor', 'BGColor']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
                { name: 'insert', items: ['Link', 'Table', 'SpecialChar', 'Smiley']}
            ]
        });

        CKEDITOR.instances.description.on('change', function() {
            $('#description').val(CKEDITOR.instances.description.getData());
        });

        var urlImageDz = new Dropzone("div#urlImageDz", {
            url: '${createLink(controller: "file", action: "uploadGameImage")}',
            autoProcessQueue: false,
            acceptedFiles: '.jpeg,.jpg,.png',
            maxFiles: 1,
            maxFilesize: 2,
            parallelUploads: 1,
            addRemoveLinks: true,
            uploadMultiple: false,
            dictDefaultMessage: "${message(code: 'dropzone.dictDefaultMessage')}",
            dictFallbackMessage: "${message(code: 'dropzone.dictFallbackMessage')}",
            dictFallbackText: "${message(code: 'dropzone.dictFallbackText')}",
            dictFileTooBig: "${message(code: 'dropzone.dictFileTooBig')}",
            dictInvalidFileType: "${message(code: 'dropzone.dictInvalidFileType')}",
            dictResponseError: "${message(code: 'dropzone.dictResponseError')}",
            dictCancelUpload: "${message(code: 'dropzone.dictCancelUpload')}",
            dictCancelUploadConfirmation: "${message(code: 'dropzone.dictCancelUploadConfirmation')}",
            dictRemoveFile: "${message(code: 'dropzone.dictRemoveFile')}",
            dictMaxFilesExceeded: "${message(code: 'dropzone.dictMaxFilesExceeded')}",
            init: function() {
                if ('${game.urlImage}') {
                    // Preload game image
                    var url = '${createLink(controller: "file", action: "getMockFile")}';
                    var data = { path: '${game.urlImage}' };
                    $.getJSON(url, data, function(response) {
                        var mockFile = { path: response.path, name: response.name, size: response.size };
                        urlImageDz.emit('addedfile', mockFile);
                        urlImageDz.emit('thumbnail', mockFile, response.loadFileUrl);
                        urlImageDz.emit('complete', mockFile);
                    });
                }
                this.on('removedfile', function(file) {
                    var currentFileName = $('input#urlImage:hidden').val();
                    if (file.path == currentFileName) {
                        $('input#urlImage:hidden').val(null);
                    }
                });
                this.on('error', function(file, errorMessage, x) {
                    // Remove file
                    this.removeFile(file);
                    alerts.show(errorMessage, 'danger', 4000);
                });
                this.on('success', function(file, path) {
                    // Update urlImage
                    $('input#urlImage:hidden').val(path);
                    createEditGame();
                });
            }
        });

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditGame #name').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditGame').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCreateEditGame').on('submit', function(e) {

            e.preventDefault();

            if (urlImageDz.getQueuedFiles().length > 0) {
                // Upload image
                return urlImageDz.processQueue();
            } else {
                return createEditGame();
            }
        });

        function createEditGame() {

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "game", action: "save")}';
            var data = $('#formCreateEditGame').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        };
    });
</g:javascript>
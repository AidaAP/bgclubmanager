<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'game.default.label')]}"/></title>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${game.club.id}">${game.club.name}</g:link></li>
        <li><g:link controller="game" action="index" params="[clubId: game.club.id]"><i class="fa fa-puzzle-piece"></i>
            <g:message code="sideMenu.club.games.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'game.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <%-- Only club admins can edit/delete games --%>
        <g:if test="${authUser.isAdminOf(game.club)}">
            <div class="row col-md-12 margin-bottom">
                <button id="btnShowEditGame" type="button" class="btn bg-yellow margin-r-5"><i
                        class="fa fa-pencil"></i> <g:message code="default.button.edit.label"/></button>
                <button id="btnShowConfirmDeleteGame" type="button" class="btn bg-yellow"><i
                        class="fa fa-remove"></i> <g:message code="default.button.delete.label"/></button>
            </div>
        </g:if>
        <div class="row col-md-12">
            <h1><g:if test="${game.urlImage}"><g:gameImage gameId="${game.id}" class="game-image"/></g:if>${game.name}</h1>
        </div>
        <div class="row col-md-12 margin-bottom">
            <div>
                <label><g:message code="game.copies.label"/>: </label>
                <span>${game.copies}</span>
            </div>
            <g:if test="${game.urlBGG}">
                <div>
                    <label><g:message code="game.urlBGG.label"/>: </label>
                    <span><a href="${game.urlBGG}" target="_blank">${game.urlBGG}</a></span>
                </div>
            </g:if>
        </div>
        <g:if test="${game.description}">
            <div class="row col-md-12">
                ${raw(game.description)}
            </div>
        </g:if>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowEditGame').on('click', function() {

            var url = '${createLink(controller: "game", action: "edit")}';
            var data = { id: '${game.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeleteGame').on('click', function() {

            var url = '${createLink(controller: "game", action: "confirmDelete")}';
            var data = { id: '${game.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'club.default.label')]}"/></title>

    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="default.detail.label" args="${[message(code: 'club.default.label')]}"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li class="active">${club.name}</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-club">
                <div class="box-body">
                    <g:clubImage clubId="${club.id}" class="profile-img img-responsive img-circle"/>
                    <h3 class="profile-username text-center">${club.name}</h3>
                    <ul class="list-group list-group-unbordered">
                        <g:if test="${SpringSecurityUtils.ifAllGranted('ROLE_ADMIN') || isMember || !hasMembershipRequest}">
                            <g:render template="showActions"/>
                        </g:if>
                        <g:if test="${club.description}">
                            <li class="list-group-item">
                                ${club.description}
                            </li>
                        </g:if>
                        <li class="list-group-item">
                            <b><g:message code="club.numMembers.label"/></b> <span
                                class="pull-right">${numMembers}</span>
                        </li>
                        <li class="list-group-item">
                            <b><g:message code="club.owner.label"/></b> <span class="pull-right"><g:link
                                controller="user" action="show"
                                params="${[username: club.owner.username]}">${club.owner.username}</g:link></span>
                        </li>
                        <li class="list-group-item">
                            <b><g:message code="club.email.label"/></b> <span class="pull-right">${club.email}</span>
                        </li>
                        <li class="list-group-item">
                            <b><g:message code="club.phone.label"/></b> <span class="pull-right">${club.phone}</span>
                        </li>
                        <li class="list-group-item">
                            <g:if test="${club.latitude && club.longitude}">
                                <b><a data-toggle="collapse" href="#collapseMapShow${club.id}"><g:message
                                        code="default.location.label"/></a></b>
                                <div id="collapseMapShow${club.id}" class="panel-collapse collapse">
                                    <div id="map_show_${club.id}"></div>
                                </div>
                            </g:if>
                            <g:else>
                                <g:message code="club.location.notSpecified"/>
                            </g:else>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<g:if test="${club.latitude && club.longitude}">
    <g:javascript>
        $(document).ready(function() {

            var latitude = parseFloat('${club.latitude}');
            var longitude = parseFloat('${club.longitude}');

            // Create map
            var map = L.map('map_show_${club.id}', {
                // Club location
                center: L.latLng(latitude, longitude),
                zoom: 13
            });

            // Add layer (Stamen.Terrain)
            L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
                {
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    subdomains: 'abcd',
                    minZoom: 0,
                    maxZoom: 18,
                    ext: 'png'
                }
            ).addTo(map);

            // Add club marker
            L.marker(
                { lat: latitude, lng: longitude },
                { title: '${club.name}', alt: '${club.name}', riseOnHover: true }
            ).addTo(map);

            // Fix map visualization on expand
            $('#collapseMapShow${club.id}').on('shown.bs.collapse', function () {
                map.invalidateSize(true);
            });
        });
    </g:javascript>
</g:if>

</body>
</html>
<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils" %>
<li class="list-group-item text-center">
<%-- Clubs can only be updated by admin users or by its admins --%>
    <g:if test="${SpringSecurityUtils.ifAllGranted('ROLE_ADMIN') || isAdmin}">
        <button id="btnShowEditClub" type="button" class="btn btn-default margin-r-5"><i
                class="fa fa-pencil"></i> <g:message code="default.button.edit.label"/></button>
    </g:if>
<%-- Clubs can only be deleted by admins or its owners --%>
    <g:if test="${SpringSecurityUtils.ifAllGranted('ROLE_ADMIN') || isOwner}">
        <button id="btnShowConfirmDeleteClub" type="button" class="btn btn-danger margin-r-5"><i
                class="fa fa-trash-o"></i> <g:message code="default.delete.label"
                                                      args="${[message(code: 'club.default.label').toLowerCase()]}"/></button>
    </g:if>
<%-- A user can only enrol a club if he is not member already --%>
    <g:if test="${!(isMember || hasMembershipRequest)}">
        <button id="btnRequestMembership" type="button" class="btn btn-default"><i class="fa fa-send-o"></i> <g:message
                code="membership.request.label"/></button>
    </g:if>
<%-- A user can only leave a club if he is member (not owner) --%>
    <g:if test="${isMember && !isOwner}">
        <button id="btnShowConfirmLeaveClub" type="button" class="btn btn-default"><i class="fa fa-remove"></i> <g:message
                code="membership.leave.label"/></button>
    </g:if>
</li>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowEditClub').on('click', function() {
            $.get('${createLink(controller: "club", action: "edit", id: club.id)}', function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeleteClub').on('click', function() {
            $.get('${createLink(controller: "club", action: "confirmDelete", id: club.id)}', function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnRequestMembership').on('click', function() {

            var url = '${createLink(controller: "membership", action: "requestMembership")}';
            var data = { 'club.id': '${club.id}' };

            $.post(url, data, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                }

                window.location.href = '${createLink(controller: "club", action: "show", id: club.id)}';
            });
        });

        $('#btnShowConfirmLeaveClub').on('click', function() {

            var url = '${createLink(controller: "membership", action: "confirmAction")}';
            var data = {
                selectedUsers: ['${sec.loggedInUserInfo(field: "id")}'],
                title: '${message(code: "membership.leave.label")}',
                message: '${message(code: "membership.leave.message")}',
                actionLink: '${createLink(controller: "membership", action: "leaveClub", id: club.id)}',
                postLink: '${createLink(controller: "club", action: "show", id: club.id)}'
            };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>
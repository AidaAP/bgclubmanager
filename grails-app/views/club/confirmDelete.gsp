<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <g:form name="formDeleteClub" controller="club" action="delete" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <p>${message}</p>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnDeleteClub" name="btnDeleteClub" value="${message(code:
                        'default.button.delete.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formDeleteClub').on('submit', function(e) {

            e.preventDefault();

            var url = '${createLink(controller: "club", action: "delete", id: id)}';

            loader.show();

            $.post(url, {}, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                    window.location.href = '${createLink(controller: "club", action: "index")}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>
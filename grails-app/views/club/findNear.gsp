<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.search.label" args="${[message(code: 'club.default.plural.label')]}"/></title>

    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="default.search.label" args="${[message(code: 'club.default.plural.label')]}"/></h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
                <i class="fa fa-info-circle margin-r-5"></i><g:message code="club.findBetweenCoordinates.note"/>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-bgcm">
                <div class="box-body">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        // Create map
        var map = L.map('map', {
            // Default location: A Coruña
            center: L.latLng(43.362343, -8.411540),
            zoom: 13
        });

        // Locate map
        map.locate({ setView: true, maxZoom: 13 });
        map.on('locationerror', function() { map.flyTo(new L.LatLng(43.362343, -8.411540)); });

        // Add layer (Stamen.Terrain)
        L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
            {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 0,
                maxZoom: 18,
                ext: 'png'
            }
        ).addTo(map);

        var markers = [];

        // Listen to map changes
        map.on('locationfound', function(event) { findNearClubs(event) });
        map.on('zoomend', function(event) { findNearClubs(event) });
        map.on('moveend', function(event) { findNearClubs(event) });

        function findNearClubs(event) {

            var bounds = event.target.getBounds();

            var neLat = bounds._northEast.lat;
            var neLong = bounds._northEast.lng;
            var swLat = bounds._southWest.lat;
            var swLong = bounds._southWest.lng;

            var url = '${createLink(controller: "club", action: "findNearClubs")}';
            var data = { neLat: neLat, neLong: neLong, swLat: swLat, swLong: swLong };

            $.getJSON(url, data, function(response) {

                if (response.error) {
                    // Show error
                    alerts.show(error, 'danger', 4000);
                } else {

                    // Remove old markers
                    $.each(markers, function(index, marker) {
                        marker.removeFrom(map);
                    });

                    // Add new markers
                    $.each(response.clubs, function(index, club) {
                        var marker = L.marker(
                            { lat: parseFloat(club.latitude), lng: parseFloat(club.longitude) },
                            { title: club.name, alt: club.name, riseOnHover: true }
                        );
                        var popupContent = '<b><a href="/club/show/' + club.id + '">' + club.name + '</a></b>';
                        if (club.description != null) {
                            popupContent += '<br>' + club.description;
                        };
                        marker.bindPopup(popupContent);
                        marker.addTo(map);
                        markers.push(marker);
                    });
                }
            });
        };
    });
</g:javascript>

</body>
</html>
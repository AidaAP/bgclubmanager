<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'club.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditClub" controller="club" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${club.id}"/>
                <g:hiddenField name="urlImage" value="${club.urlImage}"/>
                <g:hiddenField name="latitude" value="${club.latitude}"/>
                <g:hiddenField name="longitude" value="${club.longitude}"/>
                <div id="formGroup_urlImageDz" class="form-group">
                    <div id="urlImageDz" class="dropzone"></div>
                </div>
                <div id="formGroup_name" class="form-group">
                    <label for="name" class="col-md-3 control-label"><g:message code="club.name.label"/> *</label>
                    <div class="col-md-8">
                        <g:textField name="name" value="${club.name}" class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-3 control-label"><g:message
                            code="club.description.label"/></label>
                    <div class="col-md-8">
                        <g:textArea name="description" value="${club.description}" escapeHtml="${false}" rows="5"
                                    class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="formGroup_email" class="form-group">
                    <label for="email" class="col-md-3 control-label"><g:message code="club.email.label"/></label>
                    <div class="col-md-8">
                        <g:textField name="email" value="${club.email}" class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="formGroup_phone" class="form-group">
                    <label for="phone" class="col-md-3 control-label"><g:message code="club.phone.label"/></label>
                    <div class="col-md-8">
                        <g:textField name="phone" value="${club.phone}" class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="formGroup_location" class="form-group">
                    <div class="col-md-12">
                        <label class="col-md-3 control-label"><a data-toggle="collapse"
                                                                 href="#collapseMapEdit${club.id}"><g:message
                                    code="default.location.label"/></a></label>
                        <div class="col-md-8 control-label">
                            <button id="btnClearLocation" type="button" class="btn btn-xs btn-default"><g:message
                                    code="default.clear.label"
                                    args="${[message(code: 'default.location.label').toLowerCase()]}"/></button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-offset-3 col-md-8">
                            <div id="collapseMapEdit${club.id}" class="panel-collapse collapse">
                                <div id="map_edit_${club.id}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <g:submitButton id="btnCreateEditClub" name="btnCreateEditClub" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        var urlImageDz = new Dropzone("div#urlImageDz", {
            url: '${createLink(controller: "file", action: "uploadClubImage")}',
            autoProcessQueue: false,
            acceptedFiles: '.jpeg,.jpg,.png',
            maxFiles: 1,
            maxFilesize: 2,
            parallelUploads: 1,
            addRemoveLinks: true,
            uploadMultiple: false,
            dictDefaultMessage: "${message(code: 'dropzone.dictDefaultMessage')}",
            dictFallbackMessage: "${message(code: 'dropzone.dictFallbackMessage')}",
            dictFallbackText: "${message(code: 'dropzone.dictFallbackText')}",
            dictFileTooBig: "${message(code: 'dropzone.dictFileTooBig')}",
            dictInvalidFileType: "${message(code: 'dropzone.dictInvalidFileType')}",
            dictResponseError: "${message(code: 'dropzone.dictResponseError')}",
            dictCancelUpload: "${message(code: 'dropzone.dictCancelUpload')}",
            dictCancelUploadConfirmation: "${message(code: 'dropzone.dictCancelUploadConfirmation')}",
            dictRemoveFile: "${message(code: 'dropzone.dictRemoveFile')}",
            dictMaxFilesExceeded: "${message(code: 'dropzone.dictMaxFilesExceeded')}",
            init: function() {
                if ('${club.urlImage}') {
                    // Preload club image
                    var url = '${createLink(controller: "file", action: "getMockFile")}';
                    var data = { path: '${club.urlImage}' };
                    $.getJSON(url, data, function(response) {
                        var mockFile = { path: response.path, name: response.name, size: response.size };
                        urlImageDz.emit('addedfile', mockFile);
                        urlImageDz.emit('thumbnail', mockFile, response.loadFileUrl);
                        urlImageDz.emit('complete', mockFile);
                    });
                }
                this.on('removedfile', function(file) {
                    var currentFileName = $('input#urlImage:hidden').val();
                    if (file.path == currentFileName) {
                        $('input#urlImage:hidden').val(null);
                    }
                });
                this.on('error', function(file, errorMessage, x) {
                    // Remove file
                    this.removeFile(file);
                    alerts.show(errorMessage, 'danger', 4000);
                });
                this.on('success', function(file, path) {
                    // Update urlImage
                    $('input#urlImage:hidden').val(path);
                    createEditClub();
                });
            }
        });

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditClub input:visible:enabled:first').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditClub').trigger('reset');
            formValidation.clearErrors();
        });

        // Create map
        var map = L.map('map_edit_${club.id}', {
            // Default location: A Coruña
            center: L.latLng(43.362343, -8.411540),
            zoom: 13
        });

        // Add layer (Stamen.Terrain)
        L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
            {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 0,
                maxZoom: 18,
                ext: 'png'
            }
        ).addTo(map);

        var marker;

        <g:if test="${club.latitude && club.longitude}">

            var latitude = parseFloat('${club.latitude}');
            var longitude = parseFloat('${club.longitude}');

            // Locate map
            map.flyTo(new L.LatLng(latitude, longitude));

            // Add club marker
            marker = L.marker(
                { lat: latitude, lng: longitude },
                { draggable: true }
            ).addTo(map);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        </g:if>
        <g:else>
            $('#btnClearLocation').hide();
        </g:else>

        // Fix map visualization on expand
        $('#collapseMapEdit${club.id}').on('shown.bs.collapse', function () {
            map.invalidateSize(true);
        });

        // Detect location changes
        map.on('click', function(event) {
            if (marker) {
                map.removeLayer(marker);
            }
            marker = L.marker(
                event.latlng,
                { draggable: true }
            ).addTo(map);
            $('#btnClearLocation').show();
            updateLatitudeLongitude(event.latlng.lat, event.latlng.lng);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        });

        function updateLatitudeLongitude(latitude, longitude) {
            $('input#latitude:hidden').val(latitude);
            $('input#longitude:hidden').val(longitude);
        };

        $('#btnClearLocation').on('click', function() {
            updateLatitudeLongitude(null, null);
            marker.removeFrom(map);
            $('#btnClearLocation').hide();
        });

        $('#formCreateEditClub').on('submit', function(e) {

            e.preventDefault();

            if (urlImageDz.getQueuedFiles().length > 0) {
                // Upload image
                return urlImageDz.processQueue();
            } else {
                return createEditClub();
            }
        });

        function createEditClub() {

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "club", action: "save")}';
            var data = $('#formCreateEditClub').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        };
    });
</g:javascript>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.list.label" args="${[message(code: 'club.default.plural.label')]}"/></title>

    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="default.list.label" args="${[message(code: 'club.default.plural.label')]}"/></h1>
</section>

<section class="content">
    <div class="row col-md-12 margin-bottom">
        <button id="btnShowCreateClub" type="button" class="btn bg-olive"><i class="fa fa-plus"></i> <g:message
                code="default.create.label" args="${[message(code: 'club.default.label').toLowerCase()]}"/></button>
    </div>
    <g:if test="${clubs}">
        <div class="row">
            <g:each var="club" in="${clubs}">
                <g:link controller="club" action="show" params="${[id: club.id]}"
                        class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10">
                    <div class="club-card col-md-12">
                        <div class="col-md-4 hidden-xs">
                            <g:clubImage clubId="${club.id}" class="img-circle"/>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <span class="name">${club.name}</span>
                        </div>
                    </div>
                </g:link>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="club" action="${actionName}" total="${clubCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), clubCount), clubCount,
                                    message(code: 'club.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'club.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {
        $('#btnShowCreateClub').on('click', function() {
            $.get('${createLink(controller: "club", action: "create")}', function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
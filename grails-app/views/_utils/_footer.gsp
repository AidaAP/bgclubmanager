<sec:ifNotLoggedIn>
    <footer>
        <a id="to-top" href="#start" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
    </footer>
</sec:ifNotLoggedIn>
<sec:ifLoggedIn>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">Built with Grails <b><g:meta name="info.app.grailsVersion"/></b></div>
        Board Game Club Manager <b><g:meta name="info.app.version"/></b>
    </footer>
</sec:ifLoggedIn>
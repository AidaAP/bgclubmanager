<sec:ifNotLoggedIn>
    <a id="menu-toggle" href="javascript:void(0);" class="btn btn-dark btn-lg toggle" onclick="expandSidebar();"><i
            class="fa fa-bars"></i></a>
    <nav id="sidebar-wrapper">
        <ul class="sidebar-nav">
            <a id="menu-close" href="javascript:void(0);" class="btn btn-light btn-lg pull-right toggle"
               onclick="collapseSidebar();"><i class="fa fa-times"></i></a>
            <li class="sidebar-brand">
                <a href="#start" onclick="collapseSidebar();">BGClubManager</a>
            </li>
            <g:each var="menuItem" in="${menuItems}">
                <li><a href="${menuItem.link}" onclick="collapseSidebar();">${menuItem.label}</a></li>
            </g:each>
            <li>
                <a href="javascript:void(0);" onclick="showContactModal();"><g:message
                        code="sideMenu.contact.label"/></a>
            </li>
            <li>
                <a href="${createLink(controller: 'login', action: 'auth')}"><g:message
                        code="springSecurity.login.button"/></a>
            </li>
        </ul>
    </nav>

    <div id="contact" class="modal fade active" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">
                        &times;</span></button>

                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1 text-center">
                            <h4><strong>Board Game Club Manager</strong></h4>
                            <hr class="small">
                            <ul class="list-unstyled">
                                <li><i class="fa fa-envelope-o fa-fw"></i> <a href="mailto:bgclubmanager@gmail.com">
                                    bgclubmanager@gmail.com</a></li>
                                <li><i class="fa fa-twitter fa-fw"></i> <a href="http://www.twitter.com/bgclubmanager"
                                                                           target="_blank">@bgclubmanager</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <g:javascript>
        // Expands the sidebar menu
        function expandSidebar() {
            $("#sidebar-wrapper").addClass("active");
            return false;
        };

        // Collapses the sidebar menu
        function collapseSidebar() {
            $("#sidebar-wrapper").removeClass("active");
            return false;
        };

        // Shows contact modal
        function showContactModal() {
            $("#contact").modal("show");
            collapseSidebar();
        };

        $(document).ready(function () {

            // Scrolls to the selected menu item on the page
            $(function () {
                $('a[href*="#"]:not([href="#"],[data-toggle],[data-target],[data-slide])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
            //#to-top button appears after scrolling
            var fixed = false;
            $(document).scroll(function () {
                if ($(this).scrollTop() > 250) {
                    if (!fixed) {
                        fixed = true;
                        $('#to-top').show("slow", function () {
                            $('#to-top').css({
                                position: 'fixed',
                                display: 'block'
                            });
                        });
                    }
                } else {
                    if (fixed) {
                        fixed = false;
                        $('#to-top').hide("slow", function () {
                            $('#to-top').css({
                                display: 'none'
                            });
                        });
                    }
                }
            });
        });
    </g:javascript>
</sec:ifNotLoggedIn>

<sec:ifLoggedIn>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <g:each var="menuItem" in="${menuItems}">
                    <li class="${menuItem.children ? 'treeview' : ''} ${menuItem.active ? 'active' : ''}">
                        <g:link url="${menuItem.link}">
                            <i class="${menuItem.iconClass}"></i>
                            <span>${menuItem.label}</span>
                            <g:if test="${menuItem.bubble || menuItem.children}">
                                <span class="pull-right-container">
                                    <g:if test="${menuItem.children}">
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </g:if>
                                    <g:if test="${menuItem.bubble}">
                                        <small class="label pull-right ${menuItem.bubble.class}">${menuItem.bubble.text}</small>
                                    </g:if>
                                </span>
                            </g:if>
                        </g:link>
                        <g:if test="${menuItem.children}">
                            <ul class="treeview-menu">
                                <g:each var="child" in="${menuItem.children}">
                                    <li class="${child.active ? 'active' : ''}">
                                        <g:link url="${child.link}">
                                            <i class="${child.iconClass ?: 'fa fa-circle-o'}"></i>
                                            <span>${child.label}</span>
                                            <g:if test="${child.bubble}">
                                                <span class="pull-right-container">
                                                    <small class="label pull-right ${child.bubble.class}">${child.bubble.text}</small>
                                                </span>
                                            </g:if>
                                        </g:link>
                                    </li>
                                </g:each>
                            </ul>
                        </g:if>
                    </li>
                </g:each>
            </ul>
        </section>
    </aside>
</sec:ifLoggedIn>
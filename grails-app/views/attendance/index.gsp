<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.cancel.label" args="${[message(code:
                    'attendance.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCancelEventAttendances" controller="attendance" action="cancel" method="POST"
                class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="event.id" value="${event.id}"/>
                <div id="formGroup_attendees" class="form-group">
                    <div class="col-md-12">
                        <g:select name="attendees" from="${attendees}" optionKey="id" optionValue="id"
                                  multiple="multiple" data-live-search="${true}" data-live-search-normalize="${true}"
                                  data-icon-base="fa" data-tick-icon="fa-check" data-selected-text-format="count"
                                  class="form-control selectpicker"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCancelEventAttendances" name="btnCancelEventAttendances" value="${message(code:
                        'default.button.delete.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#modalCommon').on('show.bs.modal', function() {

            var $select = $('select[name="attendees"]');

            // Add user image
            <g:each var="attendee" in="${attendees}">
                var content = '<span>${userImage(username: attendee.username, class: "img-circle img-sm margin-r-5")}'
                    + ' ${attendee.username}</span>';
                $select.find('option[value="${attendee.id}"]').attr('data-content', content);
            </g:each>

            // Disable current user
            $select.find('option[value="${authUser.id}"]').attr('disabled', 'disabled');

            $select.selectpicker();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCancelEventAttendances').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCancelEventAttendances').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "attendance", action: "cancel")}';
            var data = $('#formCancelEventAttendances').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
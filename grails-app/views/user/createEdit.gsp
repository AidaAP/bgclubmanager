<%@ page import="com.bgclubmanager.core.Language" %>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${(actionName == 'edit') ? 'edit' : 'create'}.label"
                                               args="${[message(code: 'user.' + ((actionName == 'edit') ? 'default' :
                                                       'admin') + '.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditUser" controller="user" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${user.id}"/>
                <g:hiddenField name="urlImage" value="${user.urlImage}"/>
                <g:if test="${actionName == 'edit'}">
                    <div id="formGroup_urlImageDz" class="form-group">
                        <div id="urlImageDz" class="dropzone"></div>
                    </div>
                </g:if>
                <div id="formGroup_username" class="form-group">
                    <label for="username" class="col-md-4 control-label"><g:message code="user.username.label"/>
                    *</label>
                    <div class="col-md-7">
                        <g:textField name="username" value="${user.username}" class="form-control"
                                     disabled="${actionName == 'edit'}"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <g:if test="${actionName == 'edit'}">
                    <div id="formGroup_name" class="form-group">
                        <label for="name" class="col-md-4 control-label"><g:message code="user.name.label"/></label>
                        <div class="col-md-7">
                            <g:textField name="name" value="${user.name}" class="form-control"/>
                            <span class="help-block"></span>
                        </div>
                    </div>
                    <div id="formGroup_surname" class="form-group">
                        <label for="surname" class="col-md-4 control-label"><g:message code="user.surname.label"/></label>
                        <div class="col-md-7">
                            <g:textField name="surname" value="${user.surname}" class="form-control"/>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </g:if>
                <div id="formGroup_email" class="form-group">
                    <label for="email" class="col-md-4 control-label"><g:message code="user.email.label"/> *</label>
                    <div class="col-md-7">
                        <g:textField name="email" value="${user.email}" class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
                <div id="formGroup_language" class="form-group">
                    <label for="language" class="col-md-4 control-label"><g:message code="user.language.label"/>
                    *</label>
                    <div class="col-md-7">
                        <g:select name="language" from="${Language.list()}" optionKey="id" optionValue="${{
                            message(code: 'language.' + it.code) }}" value="${user.language?.id}"
                                  class="form-control"/>
                        <span class="help-block"></span>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <g:submitButton id="btnCreateEditUser" name="btnCreateEditUser" value="${message(code:
                        'default.button.' + ((actionName == 'edit') ? 'edit' : 'create') + '.label')}"
                                class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        <g:if test="${actionName == 'edit'}">
            var urlImageDz = new Dropzone("div#urlImageDz", {
                url: '${createLink(controller: "file", action: "uploadUserImage")}',
                autoProcessQueue: false,
                acceptedFiles: '.jpeg,.jpg,.png',
                maxFiles: 1,
                maxFilesize: 2,
                parallelUploads: 1,
                addRemoveLinks: true,
                uploadMultiple: false,
                dictDefaultMessage: "${message(code: 'dropzone.dictDefaultMessage')}",
                dictFallbackMessage: "${message(code: 'dropzone.dictFallbackMessage')}",
                dictFallbackText: "${message(code: 'dropzone.dictFallbackText')}",
                dictFileTooBig: "${message(code: 'dropzone.dictFileTooBig')}",
                dictInvalidFileType: "${message(code: 'dropzone.dictInvalidFileType')}",
                dictResponseError: "${message(code: 'dropzone.dictResponseError')}",
                dictCancelUpload: "${message(code: 'dropzone.dictCancelUpload')}",
                dictCancelUploadConfirmation: "${message(code: 'dropzone.dictCancelUploadConfirmation')}",
                dictRemoveFile: "${message(code: 'dropzone.dictRemoveFile')}",
                dictMaxFilesExceeded: "${message(code: 'dropzone.dictMaxFilesExceeded')}",
                init: function() {
                    if ('${user.urlImage}') {
                        // Preload user image
                        var url = '${createLink(controller: "file", action: "getMockFile")}';
                        var data = { path: '${user.urlImage}' };
                        $.getJSON(url, data, function(response) {
                            var mockFile = { path: response.path, name: response.name, size: response.size };
                            urlImageDz.emit('addedfile', mockFile);
                            urlImageDz.emit('thumbnail', mockFile, response.loadFileUrl);
                            urlImageDz.emit('complete', mockFile);
                        });
                    }
                    this.on('removedfile', function(file) {
                        var currentFileName = $('input#urlImage:hidden').val();
                        if (file.path == currentFileName) {
                            $('input#urlImage:hidden').val(null);
                        }
                    });
                    this.on('error', function(file, errorMessage, x) {
                        // Remove file
                        this.removeFile(file);
                        alerts.show(errorMessage, 'danger', 4000);
                    });
                    this.on('success', function(file, path) {
                        // Update urlImage
                        $('input#urlImage:hidden').val(path);
                        createEditUser();
                    });
                }
            });
        </g:if>

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditUser input:visible:enabled:first').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditUser').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCreateEditUser').on('submit', function(e) {

            e.preventDefault();

            <g:if test="${actionName == 'edit'}">
                if (urlImageDz.files.length > 0) {
                    // Upload image
                    return urlImageDz.processQueue();
                } else {
            </g:if>
                    return createEditUser();
            <g:if test="${actionName == 'edit'}">
                }
            </g:if>
        });

        function createEditUser() {

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "user", action: "save")}';
            var data = $('#formCreateEditUser').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        };
    });
</g:javascript>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <div class="modal-body">
            <p>${message}</p>
        </div>
        <div class="modal-footer">
            <button id="btnShowSecureDelete" type="button" class="btn btn-bgcm"><g:message
                    code="default.button.delete.label"/></button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                    code="default.button.cancel.label" default="Cancel"/></button>
        </div>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {
        $('#btnShowSecureDelete').on('click', function() {
            $.get('${createLink(controller: "user", action: "secureDelete", id: id)}', function(response) {
                $('#modalCommon').html(response);
            });
        });
    });
</g:javascript>
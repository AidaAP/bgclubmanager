<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.change.label" args="${[message(code:
                    'user.password.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formChangePassword" controller="user" action="doChangePassword" method="POST"
                class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <div id="formGroup_oldPassword" class="form-group">
                    <label for="oldPassword" class="col-md-4 control-label"><g:message
                            code="changePasswordCommand.oldPassword.label"/> *</label>
                    <div class="col-md-7">
                        <g:passwordField name="oldPassword" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_newPassword" class="form-group">
                    <label for="newPassword" class="col-md-4 control-label"><g:message
                            code="changePasswordCommand.newPassword.label"/> *</label>
                    <div class="col-md-7">
                        <g:passwordField name="newPassword" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_confirmNewPassword" class="form-group">
                    <label for="confirmNewPassword" class="col-md-4 control-label"><g:message
                            code="changePasswordCommand.confirmNewPassword.label"/> *</label>
                    <div class="col-md-7">
                        <g:passwordField name="confirmNewPassword" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnChangePassowrd" name="btnChangePassword" value="${message(code:
                        'default.button.save.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formChangePassword input:first').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formChangePassword').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formChangePassword').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "user", action: "doChangePassword")}';
            var data = $('#formChangePassword').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Show success/error
                    var error = response.error
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <g:form name="formDeleteUser" controller="user" action="${(sec.loggedInUserInfo(field: 'id') == id) ?
                'deregister' : 'delete'}" method="POST" class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-12">
                        ${message}
                    </div>
                </div>
                <g:if test="${sec.loggedInUserInfo(field: 'id') == id}">
                    <div id="formGroup_password" class="form-group">
                        <div class="col-md-offset-3 col-md-6">
                            <g:passwordField name="password" class="form-control"/>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </g:if>
                <g:else>
                    <div id="formGroup_username" class="form-group">
                        <div class="col-md-offset-3 col-md-6">
                            <g:textField name="username" class="form-control"/>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </g:else>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnDeleteUser" name="btnDeleteUser" value="${message(code:
                        'default.button.delete.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formDeleteUser input:visible:enabled:first').focus();

        $('#formDeleteUser').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "user", action: "${(sec.loggedInUserInfo(field: 'id') == id) ? 'deregister' : 'delete'}", id: id)}';
            var data = $('#formDeleteUser').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                var error = response.error;
                var success = response.success;

                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                    window.location.href = '${createLink(controller: "user", action: "index")}';
                } else {
                    window.location.href = '${grailsApplication.config.grails.serverURL}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>
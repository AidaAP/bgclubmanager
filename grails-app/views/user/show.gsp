<%@ page import="com.bgclubmanager.user.UserRole; grails.plugin.springsecurity.SpringSecurityUtils" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="${actionName == 'account' ? 'user.profile.label' : 'default.detail.label'}"
                      args="${[message(code: 'user.default.label')]}"/></title>
</head>

<body>

<section class="content-header">
    <h1><g:message code="${actionName == 'account' ? 'user.profile.label' : 'default.detail.label'}"
                   args="${[message(code: 'user.default.label')]}"/></h1>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-offset-1 col-md-10">
            <div class="box box-user${user.hasAuthority('ROLE_ADMIN') ? '-admin' : ''}">
                <div class="box-body">
                    <g:userImage username="${user.username}" class="profile-img img-responsive img-circle"/>
                    <h3 class="profile-username text-center">${user.name} ${user.surname}</h3>
                    <ul class="list-group list-group-unbordered">
                        <g:if test="${SpringSecurityUtils.ifAllGranted('ROLE_ADMIN') ||
                                (sec.loggedInUserInfo(field: 'id') == user.id)}">
                            <g:render template="showActions"/>
                        </g:if>
                        <li class="list-group-item">
                            <b><g:message code="user.username.label"/></b> <span
                                class="pull-right">${user.username}</span>
                        </li>
                        <li class="list-group-item">
                            <b><g:message code="user.name.label"/></b> <span class="pull-right">${user.name}</span>
                        </li>
                        <li class="list-group-item">
                            <b><g:message code="user.surname.label"/></b> <span
                                class="pull-right">${user.surname}</span>
                        </li>
                        <g:if test="${SpringSecurityUtils.ifAllGranted('ROLE_ADMIN') ||
                                (sec.loggedInUserInfo(field: 'id') == user.id)}">
                            <li class="list-group-item">
                                <b><g:message code="user.email.label"/></b> <span class="pull-right">${user.email}</span>
                            </li>
                        </g:if>
                        <li class="list-group-item">
                            <b><g:message code="user.language.label"/></b> <span
                                class="pull-right"><g:message code="language.${user.language.code}"/></span>
                        </li>
                        <g:if test="${sec.loggedInUserInfo(field: 'id') == user.id}">
                            <li class="list-group-item">
                                <b><g:message code="userToken.default.label"/></b> <span id="apiToken"
                                                                                         class="pull-right">${user.token}</span>
                            </li>
                        </g:if>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

</body>
</html>
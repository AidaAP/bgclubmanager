<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.list.label" args="${[message(code: 'user.default.plural.label')]}"/></title>
</head>

<body>

<section class="content-header">
    <h1><g:message code="default.list.label" args="${[message(code: 'user.default.plural.label')]}"/></h1>
</section>

<section class="content">
    <sec:ifAllGranted roles="ROLE_ADMIN">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreateAdmin" type="button" class="btn bg-light-blue"><i class="fa fa-plus"></i> <g:message
                    code="default.create.label" args="${[message(code: 'user.admin.label').toLowerCase()]}"/></button>
        </div>
    </sec:ifAllGranted>
    <g:if test="${users}">
        <div class="row">
            <g:each var="user" in="${users}">
                <g:link controller="user" action="show" params="${[username: user.username]}"
                        class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10">
                    <div class="user-card col-md-12 ${user.authorities.any { it.authority == 'ROLE_ADMIN' } ?
                            'user-card-admin' : ''}">
                        <div class="col-md-4 hidden-xs">
                            <g:userImage username="${user.username}" class="img-circle"/>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <span class="name">${user.username}</span>
                            <br>
                            <span>${user.name} ${user.surname}</span>
                        </div>
                    </div>
                </g:link>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="user" action="index" total="${userCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), userCount), userCount,
                                    message(code: 'user.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'user.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {
        $('#btnShowCreateAdmin').on('click', function() {
            $.get('${createLink(controller: "user", action: "createAdmin")}', function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
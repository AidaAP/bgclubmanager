<%@ page import="com.bgclubmanager.core.Language" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="auth"/>
    <title><g:message code='user.register.title'/></title>
</head>

<body>
<div class="auth-box">
    <div class="auth-title"><a href="/"><b>BGClubManager</b></a></div>

    <div class="auth-form">

        <p class="auth-form-title"><g:message code='user.register.header'/></p>

        <g:if test='${flash.message}'>
            <div class="auth-form-message">${flash.message}</div>
        </g:if>

        <g:form name="formRegister" controller="user" action="doRegister">
            <div id="formGroup_username" class="form-group has-feedback">
                <g:textField name="username" placeholder="${message(code: 'user.username.label')}"
                             class="form-control"/>
                <span class="fa fa-user form-control-feedback"></span>
                <span class="help-block"> </span>
            </div>
            <div id="formGroup_password" class="form-group has-feedback">
                <g:passwordField name="password" placeholder="${message(code: 'user.password.label')}"
                                 class="form-control"/>
                <span class="fa fa-lock form-control-feedback"></span>
                <span class="help-block"> </span>
            </div>
            <div id="formGroup_email" class="form-group has-feedback">
                <g:textField name="email" placeholder="${message(code: 'user.email.label')}"
                             class="form-control"/>
                <span class="fa fa-envelope form-control-feedback"></span>
                <span class="help-block"> </span>
            </div>
            <div class="form-group">
                <g:select name="language" from="${Language.list()}" optionKey="id"
                          optionValue="${{message(code: 'language.' + it.code)}}"
                          value="${Language.findByCode(grailsApplication.config.default.languages.find { it.default }?.code)?.id}"
                          class="form-control"/>
            </div>
            <div id="remember_me_holder" class="row">
                <div class="col-xs-12">
                    <div class="pull-right">
                        <g:submitButton name="submit" value="${message(code: 'user.register.button')}"
                                        class="btn btn-bgcm"/>
                    </div>
                </div>
            </div>
            <div id="auth_holder" class="row">
                <div class="col-xs-12">
                    <a href="${createLink(controller: 'login', action: 'auth')}" class="pull-right"><g:message
                            code="springSecurity.login.button"/></a>
                </div>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formRegister input').first().focus();

        $('#formRegister').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "user", action: "doRegister")}';
            var data = $('#formRegister').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                var error = response.error;

                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else if (error) {
                    // Show error
                    alerts.show(error, 'danger', 4000);
                } else {
                    // Redirect to dashboard
                    window.location.href = '${createLink(controller: "dashboard", action: "index")}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>

</body>
</html>
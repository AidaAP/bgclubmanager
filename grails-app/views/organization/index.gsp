<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.manage.label" args="${[message(code:
                    'organizer.default.plural.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formManageEventOrganizers" controller="event" action="save" method="POST"
                class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="event.id" value="${event.id}"/>
                <g:hiddenField name="organizers" value="${clubOwner.id}"/>
                <div id="formGroup_organizers" class="form-group">
                    <div class="col-md-12">
                        <g:select name="organizers" from="${clubs}" optionKey="id" optionValue="id"
                                  value="${organizers.collect{it.id}}" multiple="multiple"
                                  data-live-search="${true}" data-live-search-normalize="${true}" data-icon-base="fa"
                                  data-tick-icon="fa-check" data-selected-text-format="count"
                                  class="form-control selectpicker"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnSaveEventOrganizers" name="btnSaveEventOrganizers" value="${message(code:
                        'default.button.save.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#modalCommon').on('show.bs.modal', function() {

            var $select = $('select[name="organizers"]');

            // Add club image
            <g:each var="club" in="${clubs}">
                var content = '<span>${clubImage(clubId: club.id, class: "img-circle img-sm margin-r-5")} ${club.name}</span>';
                $select.find('option[value="${club.id}"]').attr('data-content', content);
            </g:each>

            // Disable owner
            $select.find('option[value="${clubOwner.id}"]').attr('disabled', 'disabled');

            // Disable requested
            <g:each var="req" in="${requested}">
                $select.find('option[value="${req.id}"]').attr('disabled', 'disabled');
            </g:each>

            $select.selectpicker();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formManageEventOrganizers').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formManageEventOrganizers').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "organization", action: "save")}';
            var data = $('#formManageEventOrganizers').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
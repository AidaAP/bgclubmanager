<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <g:form name="formDeleteOrganization" controller="organization" action="delete" method="POST"
                class="form-horizontal" autocomplete="off">
            <g:hiddenField name="event.id" value="${event.id}"/>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-md-12">
                        ${message}
                    </div>
                </div>
                <div class="form-group">
                    <g:each var="club" in="${clubs}">
                        <div class="col-md-12 checkbox">
                            <label>
                                <g:checkBox name="deleteClubs" value="${club.id}" checked="${false}"
                                            disabled="${club.id == event.club.id}"/> ${club.name}
                            </label>
                        </div>
                    </g:each>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnDeleteOrganization" name="btnDeleteOrganization" value="${message(code:
                        'organization.leave.label')}" class="btn btn-bgcm" disabled="disabled"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('input:checkbox[name="deleteClubs"]').on('change', function() {
            $('#btnDeleteOrganization').attr('disabled', $('input:checkbox[name="deleteClubs"]:checked').length == 0);
        });

        $('#formDeleteOrganization').on('submit', function(e) {

            e.preventDefault();

            var url = '${createLink(controller: "organization", action: "delete")}';
            var data = $('#formDeleteOrganization').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                    window.location.href = '${grailsApplication.config.grails.serverURL}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>
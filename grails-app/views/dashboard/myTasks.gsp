<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="dashboard.title"/></title>
</head>

<body>

<section class="content-header">
    <h1><i class="fa fa-check-square-o"></i> <g:message code="dashboard.myTasks.card.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="dashboard" action="index"><i class="fa fa-dashboard"></i> <g:message
                code="sideMenu.dashboard.label"/></g:link></li>
        <li class="active"><g:message code="dashboard.myTasks.card.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${tasks}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnAcceptTasks" type="button" class="btn bg-maroon" disabled="disabled"><i
                    class="fa fa-check"></i> <g:message code="default.button.accept.label"/></button>
            <button id="btnRejectTasks" type="button" class="btn bg-maroon" disabled="disabled"><i
                    class="fa fa-times"></i> <g:message code="default.button.reject.label"/></button>
        </div>
        <div class="row col-md-12">
            <ul class="todo-list">
                <g:each var="task" in="${tasks}">
                    <li>
                        <input type="checkbox" value="${task.id}" data-type="${task.type}">
                        <span><g:formatDate date="${task.date}" type="datetime" style="SHORT"/></span>
                        <span class="text">${task.text}</span>
                    </li>
                </g:each>
            </ul>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="dashboard" action="${actionName}" total="${taskCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), taskCount), taskCount,
                                    message(code: 'task.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'task.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('input:checkbox').on('change', function() {
            if ($('input:checkbox:checked').length > 0) {
                $('#btnAcceptTasks').prop('disabled', false);
                $('#btnRejectTasks').prop('disabled', false);
            } else {
                $('#btnAcceptTasks').prop('disabled', true);
                $('#btnRejectTasks').prop('disabled', true);
            }
        });

        $('#btnAcceptTasks').on('click', function() {
            return acceptRejectTasks(true);
        });

        $('#btnRejectTasks').on('click', function() {
            return acceptRejectTasks(false);
        });

        function acceptRejectTasks(accept) {

            var deferred = [];

            var membershipIds = $('input:checkbox:checked[data-type="membership"]').map(function(){return this.value;}).get();
            if (membershipIds.length > 0) {
                deferred.push(acceptRejectMembershipRequests(accept));
            }

            var organizationIds = $('input:checkbox:checked[data-type="organization"]').map(function(){return this.value;}).get();
            if (organizationIds.length > 0) {
                deferred.push(acceptRejectOrganizationRequests(accept));
            }

            var attendanceIds = $('input:checkbox:checked[data-type="attendance"]').map(function(){return this.value;}).get();
            if (attendanceIds.length > 0) {
                deferred.push(acceptRejectAttendanceRequests(accept));
            }

            if (deferred) {

                loader.show();

                $.when.apply(null, deferred).done(function() {
                    // Reload first page
                    window.location.href = '${createLink(controller: "dashboard", action: "myTasks")}';
                    loader.hide();
                });
            }
        };

        function acceptRejectMembershipRequests(accept) {

            var ids = $('input:checkbox:checked[data-type="membership"]').map(function(){return this.value;}).get();
            if (ids.length > 0) {

                var url
                if (accept) {
                    url = '${createLink(controller: "membership", action: "acceptMembershipRequests")}';
                } else {
                    url = '${createLink(controller: "membership", action: "rejectMembershipRequests")}';
                }

                var data = { ids: ids };

                return $.getJSON(url, data, function(response) {
                    if (response.error) {
                        alerts.show(response.error, 'error', 4000);
                    } else if (response.success) {
                        alerts.show(response.success, 'success', 4000);
                    }
                });
            }
        };

        function acceptRejectOrganizationRequests(accept) {

            var ids = $('input:checkbox:checked[data-type="organization"]').map(function(){return this.value;}).get();
            if (ids.length > 0) {

                var url
                if (accept) {
                    url = '${createLink(controller: "organization", action: "acceptOrganizationRequests")}';
                } else {
                    url = '${createLink(controller: "organization", action: "rejectOrganizationRequests")}';
                }

                var data = { ids: ids };

                return $.getJSON(url, data, function(response) {
                    if (response.error) {
                        alerts.show(response.error, 'error', 4000);
                    } else if (response.success) {
                        alerts.show(response.success, 'success', 4000);
                    }
                });
            }
        };

        function acceptRejectAttendanceRequests(accept) {

            var ids = $('input:checkbox:checked[data-type="attendance"]').map(function(){return this.value;}).get();
            if (ids.length > 0) {

                var url
                if (accept) {
                    url = '${createLink(controller: "attendance", action: "acceptAttendanceRequests")}';
                } else {
                    url = '${createLink(controller: "attendance", action: "rejectAttendanceRequests")}';
                }

                var data = { ids: ids };

                return $.getJSON(url, data, function(response) {
                    if (response.error) {
                        alerts.show(response.error, 'error', 4000);
                    } else if (response.success) {
                        alerts.show(response.success, 'success', 4000);
                    }
                });
            }
        };
    });
</g:javascript>

</body>
</html>
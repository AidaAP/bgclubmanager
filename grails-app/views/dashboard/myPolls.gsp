<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="dashboard.title"/></title>
</head>

<body>

<section class="content-header">
    <h1><i class="fa fa-bar-chart"></i> <g:message code="dashboard.myPolls.card.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="dashboard" action="index"><i class="fa fa-dashboard"></i> <g:message
                code="sideMenu.dashboard.label"/></g:link></li>
        <li class="active"><g:message code="dashboard.myPolls.card.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${polls}">
        <div class="row">
            <g:each var="poll" in="${polls}">
                <div class="col-md-12">
                    <div class="box box-poll">
                        <div class="box-header with-border">
                            <g:link controller="poll" action="show" params="${[clubId: poll.club.id, id: poll.id]}">
                                <div class="poll-block">
                                    <span class="title">${poll.title}</span>
                                    <span class="datePublish"><g:formatDate date="${poll.startDatePublish}"
                                                                            type="datetime" style="SHORT"/></span>
                                </div>
                            </g:link>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" total="${pollCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), pollCount),
                                    pollCount, message(code: 'poll.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'poll.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

</body>
</html>
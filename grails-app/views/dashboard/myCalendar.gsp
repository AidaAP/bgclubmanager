<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="dashboard.title"/></title>

    <asset:stylesheet src="fullcalendar/fullcalendar.min.css"/>

    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="fullcalendar/fullcalendar.min.js"/>
    <asset:javascript src="fullcalendar/locale-all.js"/>
</head>

<body>

<section class="content-header">
    <h1><i class="fa fa-calendat"></i> <g:message code="dashboard.myCalendar.card.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="dashboard" action="index"><i class="fa fa-dashboard"></i> <g:message
                code="sideMenu.dashboard.label"/></g:link></li>
        <li class="active"><g:message code="dashboard.myCalendar.card.label"/></li>
    </ol>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-calendar">
                <div class="box-body no-padding">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {
        $('#calendar').fullCalendar({
            locale: '${authUser.locale}',
            timezone: 'local',
            header: {
                left: 'prev,next',
                center: 'title',
                right: ''
            },
            events: JSON.parse('${items}'),
            eventRender: function(event, element) {
                element.attr('title', event.title);
            },
            aspectRatio: 2.8
        });
    });
</g:javascript>

</body>
</html>
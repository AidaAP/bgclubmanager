<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="dashboard.title"/></title>
</head>

<body>

<section class="content-header">
    <h1><i class="fa fa-send-o"></i> <g:message code="dashboard.myRequests.card.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="dashboard" action="index"><i class="fa fa-dashboard"></i> <g:message
                code="sideMenu.dashboard.label"/></g:link></li>
        <li class="active"><g:message code="dashboard.myRequests.card.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${requests}">
        <div class="row col-md-12">
            <ul class="todo-list">
                <g:each var="request" in="${requests}">
                    <li>
                        <span><g:formatDate date="${request.date}" type="datetime" style="SHORT"/></span>
                        <span class="text">${request.text}</span>
                    </li>
                </g:each>
            </ul>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="dashboard" action="${actionName}" total="${requestCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), requestCount), requestCount,
                                    message(code: 'request.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'request.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="dashboard.title"/></title>
</head>

<body>

<section class="content-header">
    <h1><g:message code="dashboard.title"/></h1>
</section>

<section class="content">
    <g:if test="${cards}">
        <div class="row">
            <g:each var="card" in="${cards}">
                <div class="col-lg-3 col-xs-12">
                    <g:link url="${card.link}" class="small-box ${card.cssClass}">
                        <div class="inner">
                            <h3>${card.number}</h3>

                            <p>${card.text}</p>
                        </div>
                        <div class="icon">
                            <i class="${card.cssIcon}"></i>
                        </div>
                    </g:link>
                </div>
            </g:each>
        </div>
    </g:if>
</section>

</body>
</html>
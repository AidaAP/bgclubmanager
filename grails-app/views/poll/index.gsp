<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="poll.default.plural.label"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="poll.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-bar-chart"></i> <g:message code="sideMenu.club.polls.label"/></li>
    </ol>
</section>

<section class="content">
    <%-- Only club admins can create polls --%>
    <g:if test="${authUser.isAdminOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreatePoll" type="button" class="btn bg-red"><i class="fa fa-plus"></i> <g:message
                    code="default.create.label" args="${[message(code: 'poll.default.label').toLowerCase()]}"/></button>
        </div>
    </g:if>
    <g:if test="${polls}">
        <div class="row">
            <g:each var="element" in="${polls}">
                <div class="col-md-12">
                    <div class="box box-poll">
                        <div class="box-header with-border">
                            <g:link controller="poll" action="show" params="${[clubId: club.id, id: element.poll.id]}">
                                <div class="poll-block">
                                    <span class="title ${element.votable ? '' : 'voted'}"/>${element.poll.title}</span>
                                <g:if test="${!element.poll.published}">
                                    <span class="text-red"><g:message code="poll.draft.label"/></span>
                                </g:if>
                                <g:else>
                                    <g:set var="startDatePublishStr" value="${formatDate(date:
                                            element.poll.startDatePublish, type: 'datetime', style: 'SHORT')}"/>
                                    <g:set var="endDatePublishStr" value="${(element.poll.endDatePublish &&
                                            authUser.isAdminOf(club)) ? ' - ' + formatDate(date:
                                            element.poll.endDatePublish, type: 'datetime', style: 'SHORT') : ''}"/>
                                    <g:set var="votingStr" value="${element.poll.isVotable() ? '(' + message(code:
                                            'poll.votingDates.label') + (element.poll.endDateVoting ? ' ' +
                                            message(code: 'default.date.to.label').toLowerCase() + ' ' +
                                            formatDate(date: element.poll.endDateVoting, type: 'datetime', style:
                                                    'SHORT') : '') + ')' : ''}"/>
                                    <span class="datePublish">${startDatePublishStr}${endDatePublishStr}
                                        ${votingStr}</span>
                                </g:else>
                                </div>
                            </g:link>
                        <%-- Only club admins can edit/delete polls --%>
                            <g:if test="${authUser.isAdminOf(club)}">
                                <div class="box-tools">
                                    <button id="btnShowEditPoll_${element.poll.id}" type="button"
                                            class="btn btn-box-tool" ${element.poll.hasVotes() ? 'disabled=disabled'
                                            : ''} title="${message(code: element.poll.hasVotes() ?
                                            'poll.update.error.hasVotes' : 'default.button.edit.label')}"
                                            data-pollid="${element.poll.id}"><i class="fa fa-pencil"></i></button>
                                    <button id="btnShowConfirmDeletePoll_${element.poll.id}" type="button"
                                            class="btn btn-box-tool"
                                            title="${message(code: 'default.button.delete.label')}"
                                            data-pollid="${element.poll.id}"><i class="fa fa-remove"></i></button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${pollCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), pollCount), pollCount,
                                    message(code: 'poll.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'poll.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowCreatePoll').on('click', function() {

            var url = '${createLink(controller: "poll", action: "create")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowEditPoll_"]').on('click', function() {

            var url = '${createLink(controller: "poll", action: "edit")}';
            var data = { id: $(this).data('pollid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowConfirmDeletePoll_"]').on('click', function() {

            var url = '${createLink(controller: "poll", action: "confirmDelete")}';
            var data = { id: $(this).data('pollid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
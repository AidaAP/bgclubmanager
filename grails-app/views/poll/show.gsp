<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'poll.default.label')]}"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>

    <asset:javascript src="Chart.min.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${poll.club.id}">${poll.club.name}</g:link></li>
        <li><g:link controller="poll" action="index" params="[clubId: poll.club.id]"><i
                class="fa fa-bar-chart"></i> <g:message code="sideMenu.club.polls.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'poll.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
    <%-- Only club admins can edit/delete polls --%>
        <g:if test="${authUser.isAdminOf(poll.club)}">
            <div class="row col-md-12 margin-bottom">
                <button id="btnShowEditPoll" type="button" class="btn bg-red margin-r-5" ${poll.hasVotes() ?
                        'disabled=disabled' : ''} title="${poll.hasVotes() ? message(code:
                        "poll.update.error.hasVotes") : ''}"><i class="fa fa-pencil"></i> <g:message
                        code="default.button.edit.label"/></button>
                <button id="btnShowConfirmDeletePoll" type="button" class="btn bg-red"><i
                        class="fa fa-remove"></i> <g:message code="default.button.delete.label"/></button>
            </div>
        </g:if>
        <div class="row col-md-12 margin-bottom">
            <h1>${poll.title}</h1>
            <g:if test="${!poll.published}">
                <span class="text-red"><g:message code="poll.draft.label"/></span>
            </g:if>
            <g:else>
                <g:set var="startDatePublishStr" value="${formatDate(date: poll.startDatePublish, type: 'datetime',
                        style: 'SHORT')}"/>
                <g:set var="endDatePublishStr" value="${(poll.endDatePublish && authUser.isAdminOf(poll.club)) ?
                        ' - ' + formatDate(date: poll.endDatePublish, type: 'datetime', style: 'SHORT') : ''}"/>
                <span>${startDatePublishStr}${endDatePublishStr}</span>
                <g:set var="dateVotingStr" value="${(poll.startDateVoting || poll.endDateVoting) ? message(code:
                        'poll.votingDates.label') : ''}"/>
                <g:set var="startDateVotingStr" value="${poll.startDateVoting ? ' ' + message(code:
                        'default.date.from.label').toLowerCase() + ' ' + formatDate(date: poll.startDateVoting, type:
                        'datetime', style: 'SHORT') : ''}"/>
                <g:set var="endDateVotingStr" value="${poll.endDateVoting ? ' ' +
                        message(code: 'default.date.to.label').toLowerCase() + ' ' + formatDate(date:
                        poll.endDateVoting, type: 'datetime', style: 'SHORT') : ''}"/>
                <br/><span class="text-info">${dateVotingStr}${startDateVotingStr}${endDateVotingStr}</span>
            </g:else>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-12">
                ${raw(poll.description)}
                <g:form name="formVotePoll" controller="poll" action="vote" method="POST" class="form"
                        autocomplete="off">
                    <div class="form-group">
                        <label><g:message code="poll.options.label"/></label>
                        <g:each var="option" in="${poll.options}">
                            <div class="radio">
                                <label>
                                    <g:if test="${poll.isVotable() && !poll.isVotedBy(authUser)}">
                                        <g:radio name="response" value="${option.id}" data-waschecked="false" />
                                        ${option.text}
                                    </g:if>
                                    <g:else>
                                        <span class="${option.isVotedBy(authUser) ? 'text-bold' : ''}"><i
                                                class="fa fa-circle"
                                                style="color: ${pollResults.options.find{ it.key == option.id}.value};"></i>
                                            ${option.text}</span>
                                    </g:else>
                                </label>
                            </div>
                        </g:each>
                    </div>
                    <g:if test="${poll.isVotable()}">
                        <div>
                            <g:submitButton id="btnVotePoll" name="btnVotePoll" value="${message(code:
                                    'poll.sendVote.label')}" class="btn bg-red" disabled="${poll.isVotedBy(authUser)}"/>
                        </div>
                    </g:if>
                </g:form>
            </div>
            <div class="col-md-6 col-sm-12">
                <canvas id="pollResults"></canvas>
            </div>
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        <g:if test="${showResults}">

            // Draw chart
            var chart = new Chart($('#pollResults'), {
                type: 'bar',
                data: {
                    labels: JSON.parse('${pollResults.labels}'),
                    datasets: [{
                        label: '${message(code: "vote.default.plural.label")}',
                        data: JSON.parse('${pollResults.votes}'),
                        backgroundColor: JSON.parse('${pollResults.options.collect { it.value } as grails.converters.JSON}')
                    }]
                },
                options: {
                    legend: false,
                    scales: {
                        xAxes: [{
                            display: false
                        }],
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: 1
                            }
                        }]
                    }
                }
            });
        </g:if>

        $('input[name="response"]').on('click', function() {
            if ($(this).data('waschecked')) {
                $(this).prop('checked', false);
                $(this).data('waschecked', false);
            } else {
                $(this).data('waschecked', true);
            }
            $('input[name="response"][value!="' + $(this).val() + '"]').data('waschecked', false);
        });

        $('#formVotePoll').on('submit', function(e) {

            e.preventDefault();

            var url = '${createLink(controller: "poll", action: "vote")}';
            var data = { id: $('input[name="response"]:checked').val() };

            loader.show();

            $.post(url, data, function(response) {

                // Show success/error
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                }

                loader.hide();

                // Reload page
                window.location.reload();
            });
        });

        $('#btnShowEditPoll').on('click', function() {

            var url = '${createLink(controller: "poll", action: "edit")}';
            var data = { id: '${poll.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeletePoll').on('click', function() {

            var url = '${createLink(controller: "poll", action: "confirmDelete")}';
            var data = { id: '${poll.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
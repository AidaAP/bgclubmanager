<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'poll.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditPoll" controller="poll" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${poll.id}"/>
                <g:hiddenField name="club.id" value="${poll.club.id}"/>
                <div id="formGroup_title" class="form-group">
                    <label for="title" class="col-md-2 control-label"><g:message code="poll.title.label"/> *</label>
                    <div class="col-md-10">
                        <g:textArea name="title" value="${poll.title}" escapeHtml="${false}" rows="3"
                                    class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-2 control-label"><g:message
                            code="poll.description.label"/></label>
                    <div class="col-md-10">
                        <g:textArea name="description" value="${poll.description}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_options" class="form-group">
                    <label for="options" class="col-md-2 control-label"><g:message code="poll.options.label"/> *</label>
                    <div id="options" class="col-md-10">
                        <g:each var="option" in="${poll.options}" status="index">
                            <div id="formGroup_options[${index}].text">
                                <div class="input-group margin-bottom">
                                    <g:hiddenField name="option_${index}" value="${option.id}"/>
                                    <g:textField name="option_${index}" value="${option.text}"
                                                 data-optionid="${option.id}" class="form-control"/>
                                    <span class="input-group-btn">
                                        <button id="btnRemovePollOption_${index}" type="button"
                                                class="btn btn-danger btn-flat" data-numoption="${index}"><i
                                                class="fa fa-remove"></i></button>
                                    </span>
                                </div>
                            </div>
                        </g:each>
                    </div>
                    <div class="col-md-offset-2 col-md-10">
                        <button id="btnAddPollOption" type="button" class="btn btn-block btn-success"><g:message
                                code="poll.createEdit.addOption.label"/></button>
                    </div>
                    <span class="col-md-offset-2 col-md-10 help-block"> </span>
                </div>
                <div id="formGroup_publication" class="form-group">
                    <div id="formGroup_published" class="col-md-offset-2 col-md-2 checkbox">
                        <label>
                            <g:checkBox name="published" value="${poll.published}"/> <g:message
                                    code="poll.published.label"/>
                            <span class="help-block"> </span>
                        </label>
                    </div>
                    <div id="formGroup_startDatePublish" class="col-md-4 ${poll.published ? '' : 'disabled'}">
                        <label for="startDatePublish" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDatePublish" value="${poll.startDatePublish}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDatePublish" class="col-md-4 ${poll.published ? '' : 'disabled'}">
                        <label for="endDatePublish" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDatePublish" value="${poll.endDatePublish}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_textVoting" class="col-md-offset-2 col-md-2 ${poll.published ? '' : 'disabled'}">
                        <span><g:message code="poll.votingDates.label"/></span>
                    </div>
                    <div id="formGroup_startDateVoting" class="col-md-4 ${poll.published ? '' : 'disabled'}">
                        <label for="startDateVoting" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDateVoting" value="${poll.startDateVoting}"
                                             class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDateVoting" class="col-md-4 ${poll.published ? '' : 'disabled'}">
                        <label for="endDateVoting" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/></label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDateVoting" value="${poll.endDateVoting}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditPoll" name="btnCreateEditPoll" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        CKEDITOR.replace('description', {
            toolbar: [
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike']},
                { name: 'color', items: ['TextColor', 'BGColor']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
                { name: 'insert', items: ['Link', 'Table', 'SpecialChar', 'Smiley']}
            ]
        });

        CKEDITOR.instances.description.on('change', function() {
            $('#description').val(CKEDITOR.instances.description.getData());
        });

        var datetimepickerOptions = {
            locale: '${authUser.locale}',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash-o',
                close: 'fa fa-remove'
            }
        };

        $('#btnAddPollOption').on('click', function() {

            var numOption = 0;
            $('button[id^="btnRemovePollOption_"]').each(function() {
                var val = $(this).data('numoption');
                if (numOption <= val) {
                    numOption = val + 1;
                }
            });

            var newHidden = document.createElement('input');
            newHidden.id = 'option_' + numOption.toString();
            newHidden.type = 'hidden';
            newHidden.name = newHidden.id;

            var newInput = document.createElement('input');
            newInput.id = 'option_' + numOption.toString();
            newInput.type = 'text';
            newInput.name = newInput.id;
            newInput.className = 'form-control';

            var newIcon = document.createElement('i');
            newIcon.className = 'fa fa-remove';

            var newButton = document.createElement('button');
            newButton.id = 'btnRemovePollOption_' + numOption.toString();
            newButton.type = 'button';
            newButton.className = 'btn btn-danger btn-flat';
            newButton.setAttribute('data-numoption', numOption);
            newButton.appendChild(newIcon);
            newButton.onclick = function() {
                return removeOption($(this));
            };

            var newSpan = document.createElement('span');
            newSpan.className = 'input-group-btn';
            newSpan.appendChild(newButton);

            var newDiv = document.createElement('div');
            newDiv.className = 'input-group margin-bottom';
            newDiv.appendChild(newHidden);
            newDiv.appendChild(newInput);
            newDiv.appendChild(newSpan);

            var newOption = document.createElement('div');
            newOption.id = 'formGroup_options[' + numOption.toString() + '].text';
            newOption.appendChild(newDiv);

            $('#options').append(newOption);

            $('#' + newInput.id + '[type="text"]').focus();
        });

        $('button[id^="btnRemovePollOption_"]').on('click', function() {
            return removeOption($(this));
        });

        function removeOption(option) {

            var val = option.data('numoption');

            $('button[id^="btnRemovePollOption_"]').each(function() {

                var num = $(this).data('numoption');
                if (num > val) {

                    var newNum = num - 1;
                    var newNumStr = newNum.toString();

                    $('input#option_' + num).attr('name', 'option_' + newNumStr);
                    $('input#option_' + num).attr('id', 'option_' + newNumStr);

                    $('button#btnRemovePollOption_' + num).data('numoption', newNum);
                    $('button#btnRemovePollOption_' + num).attr('id', 'btnRemovePollOption_' + newNumStr);

                    $('div[id="formGroup_options[' + num + '].text"]').attr('id', 'formGroup_options[' + newNumStr + '].text');
                }
            });

            option.parent().parent().parent().remove();
        };

        $('#startDatePublish').datetimepicker(datetimepickerOptions);
        <g:if test="${poll.startDatePublish}">
            $('#startDatePublish').data("DateTimePicker").date(new Date('${poll.startDatePublish}'));
        </g:if>
        $('#endDatePublish').datetimepicker(datetimepickerOptions);
        <g:if test="${poll.endDatePublish}">
            $('#endDatePublish').data("DateTimePicker").date(new Date('${poll.endDatePublish}'));
        </g:if>
        $('#startDateVoting').datetimepicker(datetimepickerOptions);
        <g:if test="${poll.startDateVoting}">
            $('#startDateVoting').data("DateTimePicker").date(new Date('${poll.startDateVoting}'));
        </g:if>
        $('#endDateVoting').datetimepicker(datetimepickerOptions);
        <g:if test="${poll.endDateVoting}">
            $('#endDateVoting').data("DateTimePicker").date(new Date('${poll.endDateVoting}'));
        </g:if>

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditPoll #title').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditPoll').trigger('reset');
            formValidation.clearErrors();
        });

        $('#published').on('change', function() {
            if (this.checked) {
                // Enable dates
                var now = new Date();
                $('#formGroup_startDatePublish').removeClass('disabled');
                $('#startDatePublish').data("DateTimePicker").date(now);
                $('#formGroup_endDatePublish').removeClass('disabled');
                $('#formGroup_textVoting').removeClass('disabled');
                $('#formGroup_startDateVoting').removeClass('disabled');
                $('#startDateVoting').data("DateTimePicker").date(now);
                $('#formGroup_endDateVoting').removeClass('disabled');
            } else {
                // Disable dates
                $('#formGroup_startDatePublish').addClass('disabled');
                $('#formGroup_endDatePublish').addClass('disabled');
                $('#formGroup_textVoting').addClass('disabled');
                $('#formGroup_startDateVoting').addClass('disabled');
                $('#formGroup_endDateVoting').addClass('disabled');
            }
        });

        $('#formCreateEditPoll').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "poll", action: "save")}';
            var data = $('#formCreateEditPoll').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
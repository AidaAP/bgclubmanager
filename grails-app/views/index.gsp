<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="external"/>
</head>

<body>

<!-- Header -->
<header id="start" class="header">
    <div class="text-vertical-center">
        <h1>Board Game Club Manager</h1>
        <h3><g:message code="index.header.slogan"/></h3>
        <br>
        <a href="#about" class="btn btn-dark btn-lg"><g:message code="index.header.more.button"/><br><i
                class="fa fa-angle-double-down"></i></a>
    </div>
</header>

<!-- About -->
<section id="about" class="about">
    <div class="text-center text-vertical-center">
        <h2><g:message code="index.about.title"/></h2>
        <p class="lead"><g:message code="index.about.moreClubs"/></p>
        <p class="lead"><g:message code="index.about.moreFellows"/></p>
        <p class="lead"><g:message code="index.about.moreGames"/></p>
        <br>
        <a href="#services" class="btn btn-dark btn-lg"><g:message code="index.about.evenMore.button"/><br><i
                class="fa fa-angle-double-down"></i></a>
    </div>
</section>

<!-- Services -->
<section id="services" class="services">
    <div class="text-center text-vertical-center">
        <div class="col-lg-10 col-lg-offset-1">
            <h2><g:message code="index.services.title"/></h2>
            <hr class="small">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="service-item">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-map-marker fa-stack-1x service-icon"></i>
                        </span>
                        <h4><strong><g:message code="index.services.find.title"/></strong></h4>
                        <p><g:message code="index.services.find.message"/></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-item">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-cogs fa-stack-1x service-icon"></i>
                        </span>
                        <h4><strong><g:message code="index.services.manage.title"/></strong></h4>
                        <p><g:message code="index.services.manage.message"/></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-item">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-calendar fa-stack-1x service-icon"></i>
                        </span>
                        <h4><strong><g:message code="index.services.upToDate.title"/></strong></h4>
                        <p><g:message code="index.services.upToDate.message"/></p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="service-item">
                        <span class="fa-stack fa-4x">
                            <i class="fa fa-circle fa-stack-2x"></i>
                            <i class="fa fa-code fa-stack-1x service-icon"></i>
                        </span>
                        <h4><strong><g:message code="index.services.api.title"/></strong></h4>
                        <p><g:message code="index.services.api.message"/></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-10 col-lg-offset-1">
            <br>
            <a href="${createLink(controller: 'user', action: 'register')}" class="btn btn-dark btn-lg"><g:message
                    code="user.register.button"/></a>
        </div>
    </div>
</section>

</body>

</html>
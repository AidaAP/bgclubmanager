<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'reserve.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditReserve" controller="reserve" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${reserveCommand.id}"/>
                <g:hiddenField name="club.id" value="${reserveCommand.club.id}"/>
                <div id="formGroup_title" class="form-group">
                    <label for="title" class="col-md-2 control-label"><g:message code="reserve.title.label"/> *</label>
                    <div class="col-md-10">
                        <g:textArea name="title" value="${reserveCommand.title}" escapeHtml="${false}" rows="3"
                                    class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-2 control-label"><g:message
                            code="reserve.description.label"/></label>
                    <div class="col-md-10">
                        <g:textArea name="description" value="${reserveCommand.description}" escapeHtml="${false}"
                                    rows="3" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_dates" class="form-group">
                    <div id="formGroup_startDate" class="col-md-offset-2 col-md-5">
                        <label for="startDate" class="col-md-4 control-label"><g:message
                                code="default.date.from.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="startDate" value="${reserveCommand.startDate}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                    <div id="formGroup_endDate" class="col-md-5">
                        <label for="endDate" class="col-md-4 control-label"><g:message
                                code="default.date.to.label"/> *</label>
                        <div class="col-md-8">
                            <div class="form-group input-group date">
                                <div class="input-group-addon"><span class="fa fa-calendar"></span></div>
                                <g:textField name="endDate" value="${reserveCommand.endDate}" class="form-control"/>
                            </div>
                            <span class="help-block"> </span>
                        </div>
                    </div>
                </div>
                <div id="formGroup_games" class="form-group">
                    <label for="games" class="col-md-2 control-label"><g:message
                            code="game.default.plural.label"/></label>
                    <div class="col-md-10">
                        <g:select name="games" from="${clubGames}" optionKey="id" optionValue="name"
                                  value="${reserveCommand.games.collect{it.id}}" multiple="multiple"
                                  data-live-search="${true}" data-live-search-normalize="${true}" data-icon-base="fa"
                                  data-tick-icon="fa-check" data-selected-text-format="count"
                                  class="form-control selectpicker"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_tables" class="form-group">
                    <label for="tables" class="col-md-2 control-label"><g:message
                            code="table.default.plural.label"/></label>
                    <div class="col-md-10">
                        <g:select name="tables" from="${clubTables}" optionKey="id" optionValue="name"
                                  value="${reserveCommand.tables.collect{it.id}}" multiple="multiple"
                                  data-live-search="${true}" data-live-search-normalize="${true}" data-icon-base="fa"
                                  data-tick-icon="fa-check" data-selected-text-format="count"
                                  class="form-control selectpicker"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditReserve" name="btnCreateEditReserve" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        var datetimepickerOptions = {
            locale: '${authUser.locale}',
            icons: {
                time: 'fa fa-clock-o',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-crosshairs',
                clear: 'fa fa-trash-o',
                close: 'fa fa-remove'
            }
        };

        $('#startDate').datetimepicker(datetimepickerOptions);
        <g:if test="${reserveCommand.startDate}">
            $('#startDate').data("DateTimePicker").date(new Date('${reserveCommand.startDate}'));
        </g:if>
        $('#endDate').datetimepicker(datetimepickerOptions);
        <g:if test="${reserveCommand.endDate}">
            $('#endDate').data("DateTimePicker").date(new Date('${reserveCommand.endDate}'));
        </g:if>

        $('#modalCommon').on('shown.bs.modal', function() {

            $('#formCreateEditReserve #title').focus();

            // Add game images
            <g:each var="game" in="${clubGames}">
                var content = '<span>${gameImage(gameId: game.id, class: "img-sm margin-r-5")}' + '${game.name}</span>';
                $('#games').find('option[value="${game.id}"]').attr('data-content', content);
            </g:each>

            $('#games').selectpicker();
            $('#tables').selectpicker();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditReserve').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCreateEditReserve').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "reserve", action: "save")}';
            var data = $('#formCreateEditReserve').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'reserve.default.label')]}"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>

    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${reserve.club.id}">${reserve.club.name}</g:link></li>
        <li><g:link controller="reserve" action="index" params="[clubId: reserve.club.id]"><i
                class="fa fa-calendar"></i> <g:message code="sideMenu.club.reserves.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'reserve.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <div class="row col-md-12 margin-bottom">
            <g:render template="showActions"/>
        </div>
        <div class="row col-md-12 margin-bottom">
            <h1>${reserve.title}</h1>
            <span><g:formatDate date="${reserve.startDate}" type="datetime" style="SHORT"/> - <g:formatDate
                date="${reserve.endDate}" type="datetime" style="SHORT"/></span>
        </div>
        <g:if test="${reserve.description}">
            <div class="row col-md-12 margin-bottom">${reserve.description}</div>
        </g:if>
        <g:if test="${reserve.reservedGames}">
            <div class="row col-md-12">
                <label><g:message code="game.default.plural.label"/>:</label>
                <span><g:each var="reservedGame" in="${reserve.reservedGames}" status="i">
                    <g:link controller="game" action="show"
                            params="${[clubId: reserve.clubId,
                                           id: reservedGame.game.id]}">${reservedGame.game.name}</g:link>${i <
                            reserve.reservedGames.size() - 1 ? ', ' : ''}</g:each></span>
            </div>
        </g:if>
        <g:if test="${reserve.reservedTables}">
            <div class="row col-md-12">
                <label><g:message code="table.default.plural.label"/>:</label>
                <span><g:each var="reservedTable" in="${reserve.reservedTables}" status="i">
                    <g:link controller="table" action="show"
                            params="${[clubId: reserve.clubId,
                                           id: reservedTable.table.id]}">${reservedTable.table.name}</g:link>${i <
                            reserve.reservedTables.size() - 1 ? ', ' : ''}</g:each></span>
            </div>
        </g:if>
    </div>
</section>

</body>
</html>
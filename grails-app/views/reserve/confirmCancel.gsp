<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <g:form name="formCancelReserve" controller="reserve" action="cancel" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <p>${message}</p>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCancelReserve" name="btnCancelReserve" value="${message(code:
                        'default.yes.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message code="default.no.label"
                                                                                              default="No"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formCancelReserve').on('submit', function(e) {

            e.preventDefault();

            var url = '${createLink(controller: "reserve", action: "cancel", id: reserve.id)}';

            loader.show();

            $.post(url, {}, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                    window.location.href = '${createLink(controller: "reserve", action: "index", params: [clubId: reserve.club.id])}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>
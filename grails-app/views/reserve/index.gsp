<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="reserve.default.plural.label"/></title>

    <asset:stylesheet src="datetimepicker/bootstrap-datetimepicker.min.css"/>
    <asset:stylesheet src="fullcalendar/fullcalendar.min.css"/>

    <asset:javascript src="moment/moment-with-locales.min.js"/>
    <asset:javascript src="datetimepicker/bootstrap-datetimepicker.min.js"/>
    <asset:javascript src="fullcalendar/fullcalendar.min.js"/>
    <asset:javascript src="fullcalendar/locale-all.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="reserve.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-calendar"></i> <g:message code="sideMenu.club.events.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${authUser.isMemberOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreateReserve" type="button" class="btn bg-gray"><i class="fa fa-plus"></i> <g:message
                    code="default.create.label" args="${[message(code: 'reserve.default.label').toLowerCase()]}"/></button>
        </div>
    </g:if>
    <div class="row col-md-12 margin-bottom">
        <div class="col-md-6">
            <label for="gamesFilter" class="col-md-2 control-label"><g:message code="game.default.plural.label"/></label>
            <div class="col-md-10">
                <g:select name="gamesFilter" from="${clubGames}" optionKey="id" optionValue="name" value="${[]}"
                          multiple="multiple" data-live-search="${true}" data-live-search-normalize="${true}"
                          data-icon-base="fa" data-tick-icon="fa-check" data-selected-text-format="count"
                          class="form-control selectpicker"/>
                <span class="help-block"> </span>
            </div>
        </div>
        <div class="col-md-6">
            <label for="tablesFilter" class="col-md-2 control-label"><g:message code="table.default.plural.label"/></label>
            <div class="col-md-10">
                <g:select name="tablesFilter" from="${clubTables}" optionKey="id" optionValue="name" value="${[]}"
                          multiple="multiple" data-live-search="${true}" data-live-search-normalize="${true}"
                          data-icon-base="fa" data-tick-icon="fa-check" data-selected-text-format="count"
                          class="form-control selectpicker"/>
                <span class="help-block"> </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-reserve">
                <div class="box-body no-padding">
                    <div id="calendar"></div>
                </div>
            </div>
        </div>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        // Add game images
        <g:each var="game" in="${clubGames}">
            var content = '<span>${gameImage(gameId: game.id, class: "img-sm margin-r-5")}' + '${game.name}</span>';
            $('#gamesFilter').find('option[value="${game.id}"]').attr('data-content', content);
        </g:each>

        $('#gamesFilter').selectpicker();
        $('#tablesFilter').selectpicker();

        $('#gamesFilter').on('changed.bs.select', function() {
            $('#calendar').fullCalendar('refetchEvents');
        });

        $('#tablesFilter').on('changed.bs.select', function() {
            $('#calendar').fullCalendar('refetchEvents');
        });

        $('#calendar').fullCalendar({
            locale: '${authUser.locale}',
            timezone: 'local',
            header: {
                left: 'prev,next',
                center: 'title',
                right: ''
            },
            events: function(start, end, timezone, callback) {

                var games = $('#gamesFilter').selectpicker('val');
                var tables = $('#tablesFilter').selectpicker('val');
                var startStr = start.format('YYYY-MM-DD');
                var endStr = end.format('YYYY-MM-DD');

                var url = '${createLink(controller: "reserve", action: "findReserves")}';
                var data = { clubId: '${club.id}', games: games, tables: tables, start: startStr, end: endStr };

                $.getJSON(url, data, function(response) {
                    if (response.error) {
                        // Show error
                        alerts.show(error, 'danger', 4000);
                    } else {
                        // Update calendar events
                        callback(response);
                    }
                });
            },
            eventRender: function(event, element) {
                element.attr('title', event.title);
            },
            aspectRatio: 2.8
        });

        $('#btnShowCreateReserve').on('click', function() {

            var url = '${createLink(controller: "reserve", action: "create")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
<%@ page import="grails.plugin.springsecurity.SpringSecurityUtils" %>

<%-- Reserves can only be updated by the users that made the reserves and if the reserve is not started --%>
<g:if test="${!reserve.isStarted() && (reserve.user == authUser)}">
    <button id="btnShowEditReserve" type="button" class="btn bg-gray margin-r-5"><i class="fa fa-pencil"></i> <g:message
            code="default.button.edit.label"/></button>
</g:if>
<%-- Reserves can only be cancelled by club admins or the users that made the reserves and if the reserve is not finished --%>
<g:if test="${!reserve.isFinished() && ((reserve.user == authUser) || (authUser.isAdminOf(reserve.club)))}">
    <button id="btnShowConfirmCancelReserve" type="button" class="btn bg-gray"><i class="fa fa-ban"></i> <g:message
            code="default.cancel.label" args="${[message(code: 'reserve.default.label').toLowerCase()]}"/></button>
</g:if>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowEditReserve').on('click', function() {

            var url = '${createLink(controller: "reserve", action: "edit")}';
            var data = { id: '${reserve.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmCancelReserve').on('click', function() {

            var url = '${createLink(controller: "reserve", action: "confirmCancel")}';
            var data = { id: '${reserve.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>
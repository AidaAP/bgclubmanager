<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.button.delete.confirm.message"/></h4>
        </div>
        <g:form name="formDeleteTable" controller="table" action="delete" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <p>${message}</p>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnDeleteTable" name="btnDeleteTable" value="${message(code:
                        'default.button.delete.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formDeleteTable').on('submit', function(e) {

            e.preventDefault();

            var url = '${createLink(controller: "table", action: "delete", id: table.id)}';

            loader.show();

            $.post(url, {}, function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                    window.location.href = '${createLink(controller: "table", action: "index", params: [clubId: table.club.id])}';
                }

                loader.hide();
            });
        });
    });
</g:javascript>
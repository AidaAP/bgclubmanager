<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="table.default.plural.label"/></title>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <h1><g:message code="table.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-cubes"></i> <g:message code="sideMenu.club.tables.label"/></li>
    </ol>
</section>

<section class="content">
<%-- Only club admins can create tables --%>
    <g:if test="${authUser.isAdminOf(club)}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnShowCreateTable" type="button" class="btn bg-blue"><i class="fa fa-plus"></i> <g:message
                    code="default.create.label" args="${[message(code: 'table.default.label').toLowerCase()]}"/></button>
        </div>
    </g:if>
    <g:if test="${tables}">
        <div class="row">
            <g:each var="table" in="${tables}">
                <div class="col-md-12">
                    <div class="box box-table">
                        <div class="box-header with-border">
                            <g:link controller="table" action="show" params="${[clubId: club.id, id: table.id]}">
                                <div class="table-block">
                                    <span class="title"/>${table.name}</span>
                                    <g:if test="${table.maxCapacity}">
                                        <span class="maxCapacity">(<g:message code="table.maxCapacity.label"/>:
                                        ${table.maxCapacity})</span>
                                    </g:if>
                                </div>
                            </g:link>
                            <%-- Only club admins can edit/delete tables --%>
                            <g:if test="${authUser.isAdminOf(club)}">
                                <div class="box-tools">
                                    <button id="btnShowEditTable_${table.id}" type="button" class="btn btn-box-tool"
                                            title="${message(code: 'default.button.edit.label')}"
                                            data-tableid="${table.id}"><i class="fa fa-pencil"></i></button>
                                    <button id="btnShowConfirmDeleteTable_${table.id}" type="button"
                                            class="btn btn-box-tool" title="${message(code:
                                            'default.button.delete.label')}" data-tableid="${table.id}"><i
                                            class="fa fa-remove"></i></button>
                                </div>
                            </g:if>
                        </div>
                    </div>
                </div>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${tableCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), tableCount),
                                    tableCount,
                                    message(code: 'table.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'table.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnShowCreateTable').on('click', function() {

            var url = '${createLink(controller: "table", action: "create")}';
            var data = { clubId: '${club.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowEditTable_"]').on('click', function() {

            var url = '${createLink(controller: "table", action: "edit")}';
            var data = { id: $(this).data('tableid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('[id^="btnShowConfirmDeleteTable_"]').on('click', function() {

            var url = '${createLink(controller: "table", action: "confirmDelete")}';
            var data = { id: $(this).data('tableid') };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
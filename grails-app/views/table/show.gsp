<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="default.detail.label" args="${[message(code: 'table.default.label')]}"/></title>

    <script>
        var CKEDITOR_BASEPATH = '/assets/ckeditor/';
    </script>
    <asset:javascript src="ckeditor/ckeditor.js"/>
    <asset:javascript src="ckeditor/bootstrap-ckeditor-fix.js"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.css"/>
    <g:external uri="https://unpkg.com/leaflet@1.1.0/dist/leaflet.js"/>
</head>

<body>

<section class="content-header">
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${table.club.id}">${table.club.name}</g:link></li>
        <li><g:link controller="table" action="index" params="[clubId: table.club.id]"><i class="fa fa-cubes"></i>
            <g:message code="sideMenu.club.tables.label"/></g:link></li>
        <li class="active"><g:message code="default.detail.label" args="[message(code: 'table.default.label')]"/></li>
    </ol>
</section>

<section class="content">
    <div class="container-fluid">
        <%-- Only club admins can edit/delete tables --%>
        <g:if test="${authUser.isAdminOf(table.club)}">
            <div class="row col-md-12 margin-bottom">
                <button id="btnShowEditTable" type="button" class="btn bg-blue margin-r-5"><i
                        class="fa fa-pencil"></i> <g:message code="default.button.edit.label"/></button>
                <button id="btnShowConfirmDeleteTable" type="button" class="btn bg-blue"><i class="fa fa-remove"></i>
                    <g:message code="default.button.delete.label"/></button>
            </div>
        </g:if>
        <div class="row col-md-12 margin-bottom">
            <h1>${table.name}</h1>
            <g:if test="${table.maxCapacity}">
                <span>(<g:message code="table.maxCapacity.label"/>: ${table.maxCapacity})</span>
            </g:if>
        </div>
        <g:if test="${table.latitude && table.longitude}">
            <div class="row col-md-12 margin-bottom">
                <b><a data-toggle="collapse" href="#collapseMapShow${table.id}"><g:message
                        code="default.location.label"/></a></b>
                <div id="collapseMapShow${table.id}" class="panel-collapse collapse">
                    <div id="map_show_${table.id}"></div>
                </div>
            </div>
        </g:if>
        <g:if test="${table.description}">
            <div class="row col-md-12">
                ${raw(table.description)}
            </div>
        </g:if>
    </div>
</section>

<g:javascript>
    $(document).ready(function() {

        <g:if test="${table.latitude && table.longitude}">

            var latitude = parseFloat('${table.latitude}');
            var longitude = parseFloat('${table.longitude}');

            // Create map
            var map = L.map('map_show_${table.id}', {
                // Event location
                center: L.latLng(latitude, longitude),
                zoom: 13
            });

            // Add layer (Stamen.Terrain)
            L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
                {
                    attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                    subdomains: 'abcd',
                    minZoom: 0,
                    maxZoom: 18,
                    ext: 'png'
                }
            ).addTo(map);

            // Add event marker
            L.marker(
                { lat: latitude, lng: longitude }
            ).addTo(map);

            // Fix map visualization on expand
            $('#collapseMapShow${table.id}').on('shown.bs.collapse', function () {
                map.invalidateSize(true);
            });
        </g:if>

        $('#btnShowEditTable').on('click', function() {

            var url = '${createLink(controller: "table", action: "edit")}';
            var data = { id: '${table.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });

        $('#btnShowConfirmDeleteTable').on('click', function() {

            var url = '${createLink(controller: "table", action: "confirmDelete")}';
            var data = { id: '${table.id}' };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        });
    });
</g:javascript>

</body>
</html>
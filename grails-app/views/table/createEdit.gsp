<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title"><g:message code="default.${actionName}.label" args="${[message(code:
                    'table.default.label').toLowerCase()]}"/></h4>
        </div>
        <g:form name="formCreateEditTable" controller="table" action="save" method="POST" class="form-horizontal"
                autocomplete="off">
            <div class="modal-body">
                <g:hiddenField name="id" value="${table.id}"/>
                <g:hiddenField name="club.id" value="${table.club.id}"/>
                <g:hiddenField name="latitude" value="${table.latitude}"/>
                <g:hiddenField name="longitude" value="${table.longitude}"/>
                <div id="formGroup_name" class="form-group">
                    <label for="name" class="col-md-2 control-label"><g:message code="table.name.label"/> *</label>
                    <div class="col-md-10">
                        <g:textField name="name" value="${table.name}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_description" class="form-group">
                    <label for="description" class="col-md-2 control-label"><g:message
                            code="table.description.label"/></label>
                    <div class="col-md-10">
                        <g:textArea name="description" value="${table.description}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_maxCapacity" class="form-group">
                    <label for="maxCapacity" class="col-md-2 control-label"><g:message
                            code="table.maxCapacity.label"/></label>
                    <div class="col-sm-6 col-md-3">
                        <g:textField name="maxCapacity" value="${table.maxCapacity}" class="form-control"/>
                        <span class="help-block"> </span>
                    </div>
                </div>
                <div id="formGroup_location" class="form-group">
                    <div class="col-md-12">
                        <label class="col-md-2 control-label"><a data-toggle="collapse"
                                                                 href="#collapseMapEdit${table.id}"><g:message
                                    code="default.location.label"/></a></label>
                        <div class="col-md-10 control-label">
                            <button id="btnClearLocation" type="button" class="btn btn-xs btn-default"><g:message
                                    code="default.clear.label"
                                    args="${[message(code: 'default.location.label').toLowerCase()]}"/></button>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="col-md-offset-2 col-md-10">
                            <div id="collapseMapEdit${table.id}" class="panel-collapse collapse">
                                <div id="map_edit_${table.id}"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnCreateEditTable" name="btnCreateEditTable" value="${message(code:
                        'default.button.' + actionName + '.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        CKEDITOR.replace('description', {
            toolbar: [
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike']},
                { name: 'color', items: ['TextColor', 'BGColor']},
                { name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Blockquote']},
                { name: 'insert', items: ['Link', 'Table', 'SpecialChar', 'Smiley']}
            ]
        });

        CKEDITOR.instances.description.on('change', function() {
            $('#description').val(CKEDITOR.instances.description.getData());
        });

        // Create map
        var map = L.map('map_edit_${table.id}', {
            // Default location: A Coruña
            center: L.latLng(43.362343, -8.411540),
            zoom: 13
        });

        // Add layer (Stamen.Terrain)
        L.tileLayer('https://stamen-tiles-{s}.a.ssl.fastly.net/terrain/{z}/{x}/{y}.{ext}',
            {
                attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
                subdomains: 'abcd',
                minZoom: 0,
                maxZoom: 18,
                ext: 'png'
            }
        ).addTo(map);

        var marker;

        <g:if test="${table.latitude && table.longitude}">

            var latitude = parseFloat('${table.latitude}');
            var longitude = parseFloat('${table.longitude}');

            // Locate map
            map.flyTo(new L.LatLng(latitude, longitude));

            // Add club marker
            marker = L.marker(
                { lat: latitude, lng: longitude },
                { draggable: true }
            ).addTo(map);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        </g:if>
        <g:else>
            $('#btnClearLocation').hide();
        </g:else>

        // Fix map visualization on expand
        $('#collapseMapEdit${table.id}').on('shown.bs.collapse', function () {
            map.invalidateSize(true);
        });

        // Detect location changes
        map.on('click', function(event) {
            if (marker) {
                map.removeLayer(marker);
            }
            marker = L.marker(
                event.latlng,
                { draggable: true }
            ).addTo(map);
            $('#btnClearLocation').show();
            updateLatitudeLongitude(event.latlng.lat, event.latlng.lng);
            marker.on('moveend', function(e) { updateLatitudeLongitude(e.target._latlng.lat, e.target._latlng.lng); });
        });

        function updateLatitudeLongitude(latitude, longitude) {
            $('input#latitude:hidden').val(latitude);
            $('input#longitude:hidden').val(longitude);
        };

        $('#btnClearLocation').on('click', function() {
            updateLatitudeLongitude(null, null);
            marker.removeFrom(map);
            $('#btnClearLocation').hide();
        });

        $('#modalCommon').on('shown.bs.modal', function() {
            $('#formCreateEditTable #name').focus();
        });

        $('#modalCommon').on('hide.bs.modal', function() {
            $('#formCreateEditTable').trigger('reset');
            formValidation.clearErrors();
        });

        $('#formCreateEditTable').on('submit', function(e) {

            e.preventDefault();

            // Clear previous validations
            formValidation.clearErrors();

            var url = '${createLink(controller: "table", action: "save")}';
            var data = $('#formCreateEditTable').serialize();

            loader.show();

            $.post(url, data, function(response) {

                var fieldErrors = response.fieldErrors;
                if (fieldErrors) {
                    // Show field errors
                    formValidation.showErrors(fieldErrors);
                } else {

                    // Close modal
                    $('#modalCommon').modal('hide');

                    // Reload page
                    window.location.reload();

                    // Show success/error
                    var error = response.error;
                    var success = response.success;
                    if (error) {
                        alerts.show(error, 'danger', 4000);
                    } else if (success) {
                        alerts.show(success, 'success', 4000);
                    }
                }

                loader.hide();
            });
        });
    });
</g:javascript>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="error.${response.status}.title"/></title>
</head>

<body>

<section class="content">
    <div class="error-page">
        <h2 class="headline text-${(response.status > 499) ? 'red' : 'yellow'}">${response.status}</h2>

        <div class="error-content">
            <h3><i class="fa fa-warning text-${(response.status > 499) ? 'red' : 'yellow'}"></i> <g:message
                    code="error.${response.status}.message"/></h3>
            <p><g:message code="error.${response.status}.explanation"/></p>
        </div>
    </div>
</section>

</body>
</html>
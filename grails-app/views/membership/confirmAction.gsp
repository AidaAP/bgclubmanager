<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;
            </span></button>
            <h4 class="modal-title">${title}</h4>
        </div>
        <g:form name="formConfirmAction" url="${actionLink}" method="POST" class="form-horizontal" autocomplete="off">
            <div class="modal-body">
                <p>${message}</p>
            </div>
            <div class="modal-footer">
                <g:submitButton id="btnConfirmAction" name="btnConfirmAction" value="${message(code:
                        'default.button.confirm.label')}" class="btn btn-bgcm"/>
                <button type="button" class="btn btn-default" data-dismiss="modal"><g:message
                        code="default.button.cancel.label" default="Cancel"/></button>
            </div>
        </g:form>
    </div>
</div>

<g:javascript>
    $(document).ready(function() {

        $('#formConfirmAction').on('submit', function(e) {

            e.preventDefault();

            loader.show();

            $.post('${actionLink}', JSON.parse('${data as grails.converters.JSON}'), function(response) {

                var error = response.error;
                var success = response.success;

                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else if (success) {
                    alerts.show(success, 'success', 4000);
                }

                window.location.href = '${postLink}';

                loader.hide();
            });

        });
    });
</g:javascript>
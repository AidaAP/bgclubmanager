<%@ page import="com.bgclubmanager.club.Member" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="club.userList.label"/></title>
</head>

<body>

<section class="content-header">
    <h1><g:message code="club.userList.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-user"></i> <g:message code="sideMenu.club.members.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${users}">
        <%-- Only club admins can perform actions on members --%>
        <g:if test="${authUser.isAdminOf(club)}">
            <div class="row col-md-12 margin-bottom">
                <button id="btnEditMemberships" type="button" class="btn btn-primary"><i
                        class="fa fa-pencil"></i> <g:message code="default.button.edit.label"/></button>
            </div>
            <div id="actionButtons" class="row col-md-12 margin-bottom hidden">
                <button id="btnSetClubAdmins" type="button" class="btn btn-bgcm margin-r-5" disabled="disabled"><i
                        class="fa fa-cogs"></i> <g:message code="membership.changeType.toAdmin.label"/></button>
                <button id="btnSetClubBasicMembers" type="button" class="btn btn-bgcm" disabled="disabled"><i
                        class="fa fa-user-o"></i> <g:message code="membership.changeType.toBasic.label"/></button>
                <button id="btnRemoveUsersFromClub" type="button" class="btn btn-bgcm" disabled="disabled"><i
                        class="fa fa-remove"></i> <g:message code="membership.revoke.label"/></button>
                <button id="btnCancelAction" type="button" class="btn btn-default"><g:message
                        code="default.button.cancel.label"/></button>
            </div>
        </g:if>
        <div class="row">
            <g:each var="user" in="${users}">
                <g:link controller="user" action="show" params="${[username: user.username]}"
                        class="col-md-offset-0 col-md-4 col-xs-offset-1 col-xs-10">
                    <div class="user-card col-md-12 ${user.isAdmin ? 'user-card-admin' : ''}"
                         data-userid="${user.id}" data-membertype="${user.isAdminOfClub ? 'ADMIN' : 'BASIC'}">
                        <div class="col-md-4 hidden-xs">
                            <g:userImage username="${user.username}" class="img-circle"/>
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <span class="name">${user.username}</span><g:if test="${user.isAdminOfClub}"><span
                                class="pull-right text-muted">(ADMIN)</span></g:if>
                            <br>
                            <span>${user.name} ${user.surname}</span>
                        </div>
                    </div>
                </g:link>
            </g:each>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${userCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), userCount), userCount,
                                    message(code: 'user.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'user.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('#btnEditMemberships').on('click', function() {

            // Disable button
            $(this).prop('disabled', true);

            // Show action buttons
            $('#actionButtons').removeClass('hidden');

            // Disable user links
            $('.user-card').bind('click', userSelectorHandler);
        });

        $('#btnCancelAction').on('click', function() {

            // Hide action buttons
            $('#actionButtons').addClass('hidden');

            // Deselect all users
            $('.user-card').removeClass('selected');

            // Enable edit button
            $('#btnEditMemberships').prop('disabled', false);

            // Enable user links
            $('.user-card').unbind('click', userSelectorHandler);

            // Disable action buttons
            $('#actionButtons > button:not(#btnCancelAction)').prop('disabled', true);
        });

        $('#btnSetClubAdmins').on('click', function() {

            var title = '${message(code: "membership.changeType.toAdmin.label")}';
            var message = '${message(code: "membership.changeType.toAdmin.message")}';
            var actionLink = '${createLink(controller: "membership", action: "changeMembershipType",
        params: [id: club.id, type: Member.Type.ADMIN])}';
            var postLink = '${createLink(controller: "membership", action: "getMembers", params: [clubId: club.id])}';

            return showConfirmAction(title, message, actionLink, postLink);
        });

        $('#btnSetClubBasicMembers').on('click', function() {

            var title = '${message(code: "membership.changeType.toBasic.label")}';
            var message = '${message(code: "membership.changeType.toBasic.message")}';
            var actionLink = '${createLink(controller: "membership", action: "changeMembershipType",
        params: [id: club.id, type: Member.Type.BASIC])}';
            var postLink = '${createLink(controller: "membership", action: "getMembers", params: [clubId: club.id])}';

            return showConfirmAction(title, message, actionLink, postLink);
        });

        $('#btnRemoveUsersFromClub').on('click', function() {

            var title = '${message(code: "membership.revoke.label")}';
            var message = '${message(code: "membership.revoke.message")}';
            var actionLink = '${createLink(controller: "membership", action: "expelUsers", id: club.id)}';
            var postLink = '${createLink(controller: "membership", action: "getMembers", params: [clubId: club.id])}';

            return showConfirmAction(title, message, actionLink, postLink);
        });

        function showConfirmAction(title, message, actionLink, postLink) {

            var url = '${createLink(controller: "membership", action: "confirmAction")}';
            var data = {
                selectedUsers: $('.user-card.selected').map(function() { return $(this).data('userid'); }).get(),
                title: title, message: message, actionLink: actionLink, postLink: postLink
            };

            $.get(url, data, function(response) {
                var error = response.error;
                if (error) {
                    alerts.show(error, 'danger', 4000);
                } else {
                    $('#modalCommon').html(response);
                    $('#modalCommon').modal('show');
                }
            });
        };
    });

    function enableDisableActionButtons() {

        var noBasicMembersSelected = $('.user-card.selected:not([data-membertype="ADMIN"])').length == 0;
        var noAdminsSelected = $('.user-card.selected:not([data-membertype="BASIC"])').length == 0;
        var noUsersSelected = $('.user-card.selected').length == 0;

        $('#btnSetClubAdmins').prop('disabled', noBasicMembersSelected);
        $('#btnSetClubBasicMembers').prop('disabled', noAdminsSelected);
        $('#btnRemoveUsersFromClub').prop('disabled', noUsersSelected);
    };

    var userSelectorHandler = function() {

        // User cannot select itself or owner
        var forbiddenUsers = ['${club.owner.id}', '${authUser.id}'];

        if ($.inArray($(this).data('userid'), forbiddenUsers) == -1) {
            $(this).toggleClass('selected');
            enableDisableActionButtons();
        }

        return false;
    };
</g:javascript>

</body>
</html>
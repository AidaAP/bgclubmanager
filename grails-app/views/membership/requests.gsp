<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title><g:message code="request.default.plural.label"/></title>
</head>

<body>

<section class="content-header">
    <h1><g:message code="request.default.plural.label"/></h1>
    <ol class="breadcrumb">
        <li><g:link controller="club" action="myIndex"><i class="fa fa-users"></i> <g:message
                code="sideMenu.clubs.label"/></g:link></li>
        <li><g:link controller="club" action="show" id="${club.id}">${club.name}</g:link></li>
        <li class="active"><i class="fa fa-user-plus"></i> <g:message code="sideMenu.club.requests.label"/></li>
    </ol>
</section>

<section class="content">
    <g:if test="${requests}">
        <div class="row col-md-12 margin-bottom">
            <button id="btnAcceptRequests" type="button" class="btn bg-teal" disabled="disabled"><i
                    class="fa fa-check"></i> <g:message code="default.button.accept.label"/></button>
            <button id="btnRejectRequests" type="button" class="btn bg-teal" disabled="disabled"><i
                    class="fa fa-times"></i> <g:message code="default.button.reject.label"/></button>
        </div>
        <div class="row col-md-12">
            <ul class="todo-list">
                <g:each var="request" in="${requests}">
                    <li>
                        <input type="checkbox" value="${request.id}">
                        <span><g:formatDate date="${request.date}" type="datetime" style="SHORT"/></span>
                        <span class="text">${request.text}</span>
                    </li>
                </g:each>
            </ul>
        </div>
        <div class="row text-center">
            <div class="col-md-12 pagination">
                <g:paginate controller="${controllerName}" action="${actionName}" params="${[clubId: club.id]}"
                            total="${requestCount}" />
            </div>
            <div class="col-md-12">
                <g:message code="default.paginate.show.message"
                           args="${[params.int('offset') + 1,
                                    Math.min(params.int('offset') + params.int('max'), requestCount), requestCount,
                                    message(code: 'request.default.plural.label').toLowerCase()]}"/>
            </div>
        </div>
    </g:if>
    <g:else>
        <h1 class="text-center">
            <p><i class="fa fa-comment-o fa-3x"></i></p>
            <p><g:message code="default.list.empty.message"
                          args="${[message(code: 'request.default.plural.label').toLowerCase()]}"/></p>
        </h1>
    </g:else>
</section>

<g:javascript>
    $(document).ready(function() {

        $('input:checkbox').on('change', function() {
            if ($('input:checkbox:checked').length > 0) {
                $('#btnAcceptRequests').prop('disabled', false);
                $('#btnRejectRequests').prop('disabled', false);
            } else {
                $('#btnAcceptRequests').prop('disabled', true);
                $('#btnRejectRequests').prop('disabled', true);
            }
        });

        $('#btnAcceptRequests').on('click', function() {

            var url = '${createLink(controller: "membership", action: "acceptMembershipRequests")}';

            var ids = [];
            $.each($('input:checkbox:checked'), function(idx, checkbox) { ids.push(checkbox.value); });
            var data = { ids: ids };

            loader.show();

            $.getJSON(url, data, function(response) {
                if (response.error) {
                    alerts.show(response.error, 'error', 4000);
                } else if (response.success) {
                    alerts.show(response.success, 'success', 4000);
                }

                // Reload first page
                window.location.href = '${createLink(controller: "membership", action: "getMembershipRequests", params: [clubId: club.id])}';

                loader.hide();
            });
        });

        $('#btnRejectRequests').on('click', function() {

            var url = '${createLink(controller: "membership", action: "rejectMembershipRequests")}';

            var ids = [];
            $.each($('input:checkbox:checked'), function(idx, checkbox) { ids.push(checkbox.value); });
            var data = { ids: ids };

            loader.show();

            $.getJSON(url, data, function(response) {
                if (response.error) {
                    alerts.show(response.error, 'error', 4000);
                } else if (response.success) {
                    alerts.show(response.success, 'success', 4000);
                }

                // Reload first page
                window.location.href = '${createLink(controller: "membership", action: "getMembershipRequests", params: [clubId: club.id])}';

                loader.hide();
            });
        });
    });
</g:javascript>

</body>
</html>
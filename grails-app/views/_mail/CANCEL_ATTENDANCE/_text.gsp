<g:message code="mail.text.CANCEL_ATTENDANCE"
           args="${[user.username, event.name, createLink(controller: 'event', action: 'show',
                   params: [id: event.id, clubId: event.club.id], absolute: true)]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
<g:message code="mail.text.CHANGE_MEMBERSHIP_TYPE"
           args="${[user.username, club.name,
                    createLink(controller: 'club', action: 'show', id: club.id, absolute: true),
                    message(code: type1, locale: user.locale).toLowerCase(),
                    message(code: type2, locale: user.locale).toLowerCase()]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
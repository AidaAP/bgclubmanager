<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

<body>
<link href="//fonts.googleapis.com/css?family=Lato:100,400,700" rel="stylesheet" type="text/css"/>
<style>@import url(http://fonts.googleapis.com/css?family=Open+Sans);</style>
<table style="width: 768px;
box-shadow: 0 1px 2px 0 rgba(0, 0, 0, 0.16), 0 2px 10px 0 rgba(0, 0, 0, 0.12);
border-collapse: collapse;
border: 1px solid #d7d7d7;">

    <tr style="background-color: #d5772f;
    background-image: linear-gradient(to right, #6F2A3F 0%, #821616 33%, #D5772F 66%, #CB9D48 100%);
    background-image: -moz-linear-gradient(left, #6F2A3F 0%, #821616 33%, #D5772F 66%, #CB9D48 100%);
    background-image: -ms-linear-gradient(left, #6F2A3F 0%, #821616 33%, #D5772F 66%, #CB9D48 100%);
    background-image: -o-linear-gradient(left, #6F2A3F 0%, #821616 33%, #D5772F 66%, #CB9D48 100%);
    background-image: -webkit-gradient(linear, left top, right top, color-stop(0, #6F2A3F), color-stop(33, #821616), color-stop(66, #D5772F), color-stop(100, #CB9D48));
    background-image: -webkit-linear-gradient(left, #6F2A3F 0%, #821616 33%, #D5772F 66%, #CB9D48 100%);">

        <td style="height: 64px;
        color: #fff;
        padding: 0 16px;
        font-size: 18px;
        font-weight: bold;
        font-family: Lato, Arial;"><span><g:message code="mail.title.default" args="${[user.username]}"
                                                    locale="${user.locale}"/></span></td>
    </tr>
    <tr>
        <td style="padding: 24px 32px 32px 32px;">
            <table style="border-collapse: collapse;
            font-family: Lato, Arial;
            font-size: 16px;
            font-weight: normal;
            color: #666;">
                <tr>
                    <td><g:message code="mail.body.CHANGE_MEMBERSHIP_TYPE"
                                   args="${[createLink(controller: 'club', action: 'show', id: club.id, absolute: true),
                                            club.name,
                                            message(code: type1, locale: user.locale).toLowerCase(),
                                            message(code: type2, locale: user.locale).toLowerCase()]}"
                                   locale="${user.locale}"/>
                </tr>
                <tr>
                    <td><g:message code="mail.footer.default" locale="${user.locale}"/></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
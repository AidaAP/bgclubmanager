<g:message code="mail.text.REJECT_MEMBERSHIP_REQUEST"
           args="${[user.username, club.name, createLink(controller: 'club', action: 'show', id: club.id, absolute: true)]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
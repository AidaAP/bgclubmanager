<g:message code="mail.text.CANCEL_RESERVE"
           args="${[user.username, reserve.title, formatDate(date: reserve.startDate, type: 'datetime', style: 'SHORT')]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
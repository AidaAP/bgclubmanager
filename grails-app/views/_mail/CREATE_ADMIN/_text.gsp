<g:message code="mail.text.CREATE_ADMIN"
           args="${[user.username, user.password, createLink(absolute: true, controller: 'user', action: 'account')]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
<g:message code="mail.text.REVOKE_MEMBERSHIP"
           args="${[user.username, club.name, createLink(controller: 'club', action: 'show', id: club.id, absolute: true)]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
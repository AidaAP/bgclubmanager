<g:message code="mail.text.REGISTER_USER"
           args="${[user.username, createLink(controller: 'user', action: 'account', absolute: true)]}"
           locale="${user.locale}"/>


<g:message code="mail.footer.default" locale="${user.locale}"/>
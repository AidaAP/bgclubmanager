package bgclubmanager

import grails.boot.GrailsApp
import grails.boot.config.GrailsAutoConfiguration
import org.springframework.beans.factory.config.YamlPropertiesFactoryBean
import org.springframework.context.EnvironmentAware
import org.springframework.core.env.Environment
import org.springframework.core.env.PropertiesPropertySource
import org.springframework.core.io.FileSystemResource
import org.springframework.core.io.Resource

class Application extends GrailsAutoConfiguration implements EnvironmentAware {

    static final String CONFIG_DIR_KEY = 'BGCM_CONFIG_DIR'
    static final String DEFAULT_CONFIG_DIR = '/opt/config/bgclubmanager'
    static final String CONFIG_FILE_NAME = 'application.yml'

    static void main(String[] args) {
        GrailsApp.run(Application, args)
    }

    @Override
    void setEnvironment(Environment environment) {

        println "--------------------------------------------------------------------------------"
        println "--------------------------------------------------------------------------------"

        def configDir = System.getenv(CONFIG_DIR_KEY) ?: System.getProperty(CONFIG_DIR_KEY) ?: DEFAULT_CONFIG_DIR
        println "--- Loading configuration from ${configDir}"

        def configFile = new File(configDir, CONFIG_FILE_NAME)
        println "--- Loading configuration file ${configFile}"

        def hasExternalConfig = configFile.exists()
        println "--- Config file ${hasExternalConfig ? '' : 'not '}found"

        if (hasExternalConfig) {

            Resource resourceConfig = new FileSystemResource(configFile.absolutePath)
            YamlPropertiesFactoryBean propertyFactoryBean = new YamlPropertiesFactoryBean()
            propertyFactoryBean.setResources(resourceConfig)
            propertyFactoryBean.afterPropertiesSet()

            Properties properties = propertyFactoryBean.getObject()
            environment.propertySources.addFirst(new PropertiesPropertySource('BGCMExternalConfig', properties))
        }

        println "--------------------------------------------------------------------------------"
        println "--------------------------------------------------------------------------------"
    }
}
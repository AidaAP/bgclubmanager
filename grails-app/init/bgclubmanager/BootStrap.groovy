package bgclubmanager

import com.bgclubmanager.core.Language
import com.bgclubmanager.user.Role
import com.bgclubmanager.user.User
import com.bgclubmanager.user.UserRole

class BootStrap {

    def grailsApplication
    def localeResolver

    def init = { servletContext ->

        // Create basic roles
        def roleConfig = grailsApplication.config.default.role
        if (roleConfig) {
            roleConfig.each {
                if (!Role.findByAuthority(it.value)) {
                    def role = new Role(authority: it.value)
                    role.save(flush: true)
                    log.debug "Created basic role '${role.authority}'"
                }
            }
        } else {
            log.error "Missing mandatory configuration 'default.role'"
        }

        // Create basic languages
        def langConfig = grailsApplication.config.default.languages
        if (langConfig) {
            langConfig.each {

                // Create language
                if (!Language.findByCode(it.code)) {
                    def lang = new Language(code: it.code)
                    lang.save(flush: true)
                    log.debug "Created basic language '${lang.code}'"
                }

                // Set default locale
                if (it.default) {
                    localeResolver.setDefaultLocale(new Locale(it.code))
                }
            }
        }

        // Create basic admin
        def userConfig = grailsApplication.config.default.user
        if (userConfig) {

            def adminUser = User.findByUsername(userConfig.username)
            if (!adminUser) {
                adminUser = new User(userConfig)
                adminUser.language = Language.findByCode(langConfig.find { it.default }?.code)
                adminUser.save(flush: true)
                log.debug "Created basic admin '${adminUser.username}'"
            }

            def adminRole = Role.findByAuthority(roleConfig.admin)
            if (adminRole) {

                def userRole = UserRole.findByUserAndRole(adminUser, adminRole)
                if (!userRole) {
                    userRole = new UserRole(user: adminUser, role: adminRole)
                    userRole.save(flush: true)
                    log.debug "Assigned role '${adminRole.authority}' to user '${adminUser.username}'"
                }
            } else {
                log.error "Not found basic role '${roleConfig.admin}'"
            }
        } else {
            log.error "Missing mandatory configuration 'default.user'"
        }
    }

    def destroy = {
    }
}
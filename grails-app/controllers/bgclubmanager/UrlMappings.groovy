package bgclubmanager

class UrlMappings {

    static mappings = {

        // Regular mappings
        "/$controller/$action?/$id?(.$format)?"{
            format = 'html'
            constraints {
                // apply constraints here
            }
        }

        // Show user by name
        "/user/show/$username?(.$format)?"(controller: 'user', action: 'show')

        // Club sections
        group "/club", {
            "/$id/details" (controller: 'club', action: 'show')
            "/$clubId/requests" (controller: 'membership', action: 'getMembershipRequests')
            "/$clubId/members" (controller: 'membership', action: 'getMembers')
            "/$clubId/news" (controller: 'announcement', action: 'index')
            "/$clubId/news/$id" (controller: 'announcement', action: 'show')
            "/$clubId/polls" (controller: 'poll', action: 'index')
            "/$clubId/polls/$id" (controller: 'poll', action: 'show')
            "/$clubId/events" (controller: 'event', action: 'index')
            "/$clubId/events/$id" (controller: 'event', action: 'show')
            "/$clubId/games" (controller: 'game', action: 'index')
            "/$clubId/games/$id" (controller: 'game', action: 'show')
            "/$clubId/tables" (controller: 'table', action: 'index')
            "/$clubId/tables/$id" (controller: 'table', action: 'show')
            "/$clubId/reserves" (controller: 'reserve', action: 'index')
            "/$clubId/reserves/$id" (controller: 'reserve', action: 'show')
        }

        // Root
        "/"(controller: 'home', action: 'index')

        // Error mappings
        "500"(view:'/error')
        "404"(view:'/error')
        "403"(view:'/error')

        // REST mappings
        group "/api/rest", {
            "/clubs"(resources: 'RESTClub') {
                "/image"(controller: 'RESTClub', method: 'PUT', action: 'updateImage')
                "/image"(controller: 'RESTClub', method: 'DELETE', action: 'deleteImage')
                "/requests"(resources: 'RESTMembershipRequest')
                "/requests/accept"(controller: 'RESTMembershipRequest', method: 'PUT', action: 'accept')
                "/requests/reject"(controller: 'RESTMembershipRequest', method: 'PUT', action: 'reject')
                "/members"(resources: 'RESTMembership')
                "/members/changeType"(controller: 'RESTMembership', method: 'PUT', action: 'changeType')
                "/members/expel"(controller: 'RESTMembership', method: 'PUT', action: 'expel')
                "/news"(resources: 'RESTAnnouncement')
                "/polls"(resources: 'RESTPoll')
                "/events"(resources: 'RESTEvent') {
                    "/organizations"(resources: 'RESTOrganization')
                    "/organizations/leave"(controller: 'RESTOrganization', method: 'PUT', action: 'leave')
                    "/organizationrequests"(resources: 'RESTOrganizationRequest')
                    "/attendances"(resources: 'RESTAttendance')
                    "/attendances/cancel"(controller: 'RESTAttendance', method: 'PUT', action: 'cancel')
                    "/attendancerequests"(resources: 'RESTAttendanceRequest')
                    "/attendancerequests/accept"(controller: 'RESTAttendanceRequest', method: 'PUT', action: 'accept')
                    "/attendancerequests/reject"(controller: 'RESTAttendanceRequest', method: 'PUT', action: 'reject')
                }
                "/games"(resources: 'RESTGame') {
                    "/image"(controller: 'RESTGame', method: 'PUT', action: 'updateImage')
                    "/image"(controller: 'RESTGame', method: 'DELETE', action: 'deleteImage')
                }
                "/tables"(resources: 'RESTTable')
                "/reserves"(resources: 'RESTReserve')
            }
            "/organizationrequests"(resources: 'RESTOrganizationRequest')
            "/organizationrequests/accept"(controller: 'RESTOrganizationRequest', method: 'PUT', action: 'accept')
            "/organizationrequests/reject"(controller: 'RESTOrganizationRequest', method: 'PUT', action: 'reject')
            "/attendancerequests"(resources: 'RESTAttendanceRequest')
            "/attendancerequests/accept"(controller: 'RESTAttendanceRequest', method: 'PUT', action: 'accept')
            "/attendancerequests/reject"(controller: 'RESTAttendanceRequest', method: 'PUT', action: 'reject')
        }
    }
}
package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class OrganizationController {

    def organizationService

    @Secured('isAuthenticated()')
    def index(final String eventId) {

        def model = [:]

        def event = Event.read(eventId)
        if (!event) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'event.default.label'), eventId])
            render model as JSON
            return null
        }

        if (!authenticatedUser.isAdminOf(event.club)) {
            model.error = message(code: 'organization.manage.error.notAllowed')
            render model as JSON
            return null
        }

        def clubs = Club.list(sort: 'name', order: 'asc')
        def organizers = event.getOrganizers()
        def requested = OrganizationRequest.findAllByEvent(event).collect { it.club }

        render(view: 'index', model: [event: event, clubs: clubs, clubOwner: event.club, organizers: organizers,
                                      requested: requested])
        return null
    }

    @Secured('isAuthenticated()')
    def save(final Event event) {

        def result = [:]

        if (!authenticatedUser.isAdminOf(event.club)) {
            render(status: 403)
            return null
        }

        def organizers = params.list('organizers').collect { Club.read(it) } as Club[]

        def eventResult = organizationService.setEventOrganizers(event, organizers)
        if (eventResult.hasErrors()) {
            result = [error: message(error: eventResult.errors.globalError)]
        } else {
            result = [success: message(code: 'organization.setEventOrganizers.success')]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def acceptOrganizationRequests() {

        def result = [:]

        OrganizationRequest[] organizationRequests = OrganizationRequest.getAll(params.list('ids[]'))
        def processedRequests = organizationService.acceptOrganizationRequests(organizationRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'organization.acceptRequests.error')
        } else {
            result.success = message(code: 'organization.acceptRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def rejectOrganizationRequests() {

        def result = [:]

        OrganizationRequest[] organizationRequests = OrganizationRequest.getAll(params.list('ids[]'))
        def processedRequests = organizationService.rejectOrganizationRequests(organizationRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'organization.rejectRequests.error')
        } else {
            result.success = message(code: 'organization.rejectRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String eventId) {
        render(view: 'confirmDelete', model: [eventId: eventId, message: message(code: 'organization.leave.message')])
    }

    @Secured('isAuthenticated()')
    def secureDelete(final String eventId) {

        def event = Event.read(eventId)
        if (!event) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                    '.label'), eventId])]
            render model as JSON
        } else {

            def clubs = event.getOrganizers().findAll { authenticatedUser.isAdminOf(it) }

            render(view: 'secureDelete', model: [event: event, clubs: clubs, message: message(code: 'organization' +
                    '.leaveSecure.message')])
        }

        return null
    }


    @Secured('isAuthenticated()')
    def delete(final Event event) {

        def result = [:]

        Club[] clubs = Club.findAllByIdInList(params.list('deleteClubs'))

        def eventResult = organizationService.leaveOrganization(event, clubs)
        if (eventResult.hasErrors()) {
            result = [ error: message(error: eventResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'organization.leave.success') ]
        }

        render result as JSON
    }
}

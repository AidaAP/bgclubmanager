package com.bgclubmanager.event

import com.bgclubmanager.user.User
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class AttendanceController {

    def attendanceService

    @Secured('isAuthenticated()')
    def index(final String eventId) {

        def model = [:]

        def event = Event.read(eventId)
        if (!event) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'event.default.label'), eventId])
            render model as JSON
            return null
        }

        if (!authenticatedUser.isAdminOf(event.club)) {
            model.error = message(code: 'organization.manage.error.notAllowed')
            render model as JSON
            return null
        }

        render(view: 'index', model: [event: event, attendees: event.getAttendees(), authUser: authenticatedUser])
        return null
    }

    @Secured('isAuthenticated()')
    def save(final Event event) {

        def result = [:]

        def attendanceRequestResult = attendanceService.requestAttendance(event)
        if (attendanceRequestResult.hasErrors()) {
            result = [error: message(error: attendanceRequestResult.errors.globalError)]
        } else {
            result = [success: message(code: 'attendance.request.success')]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def acceptAttendanceRequests() {

        def result = [:]

        AttendanceRequest[] attendanceRequests = AttendanceRequest.getAll(params.list('ids[]'))
        def processedRequests = attendanceService.acceptAttendanceRequests(attendanceRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'attendance.acceptRequests.error')
        } else {
            result.success = message(code: 'attendance.acceptRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def rejectAttendanceRequests() {

        def result = [:]

        AttendanceRequest[] attendanceRequests = AttendanceRequest.getAll(params.list('ids[]'))
        def processedRequests = attendanceService.rejectAttendanceRequests(attendanceRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'attendance.rejectRequests.error')
        } else {
            result.success = message(code: 'attendance.rejectRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def cancel(final Event event) {

        def result = [:]

        def attendances = []

        def userIds = params.list('attendees')
        def users = User.getAll(userIds)
        users.each {
            attendances << Attendance.findByEventAndUser(event, it)
        }

        def attendancesResult = attendanceService.cancelAttendances(attendances as Attendance[])
        if (attendancesResult.any { it.hasErrors() }) {
            result = [error: message(code: 'attendance.cancelMultiple.error')]
        } else {
            result = [success: message(code: 'attendance.cancelMultiple.success')]
        }

        render result as JSON
    }
}

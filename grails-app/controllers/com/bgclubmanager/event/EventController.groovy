package com.bgclubmanager.event

import com.bgclubmanager.club.Club
import com.bgclubmanager.util.CommonUtils
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class EventController {

    def eventService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        def searchResult = eventService.getClubEvents(club, params.offset, params.max)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, events: searchResult.list, eventCount:
                searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def event = Event.read(id)
        if (!event) {
            render(status: 404)
            return null
        }

        def organizers = event.getOrganizers()
        def attendees = event.getAttendees()

        def eventIsPublic = event.isPublic
        def userIsOrganizerMember = organizers.any { authenticatedUser.isMemberOf(it) }
        def userIsRequestedOrganizerAdmin = OrganizationRequest.findAllByEvent(event).collect { it.club }.any {
            authenticatedUser.isAdminOf(it) }
        def isAttendee = authenticatedUser.isAttendeeOf(event)
        def userHasRequestedAttendance = AttendanceRequest.findByEventAndUser(event, authenticatedUser)

        def allowed = eventIsPublic || userIsOrganizerMember || userIsRequestedOrganizerAdmin || isAttendee ||
                userHasRequestedAttendance
        if (!allowed) {
            render(status: 403)
            return null
        }

        def numAttendees = Attendance.countByEvent(event)
        def isOrganizerAdmin = organizers.any { authenticatedUser.isAdminOf(it) }
        def hasAttendanceRequest = AttendanceRequest.countByEventAndUser(event, authenticatedUser) == 1

        render(view: 'show', model: [authUser: authenticatedUser, event: event, organizers: organizers, attendees:
                attendees, isOrganizerAdmin: isOrganizerAdmin, numAttendees: numAttendees, isAttendee: isAttendee,
                                     hasAttendanceRequest: hasAttendanceRequest])
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.event = new Event(club: club)
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def event = Event.read(id)
        if (!event) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'event.default.label'), id])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.event = event
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save(final Event event) {

        def result = [:]

        def edit = event.id != null

        // Fix dates & coordinates
        def dateFormat = message(code: 'default.datetimenosec.format')
        event.startDate = params.startDate ? Date.parse(dateFormat, params.startDate) : null
        event.endDate = params.endDate ? Date.parse(dateFormat, params.endDate) : null
        event.startDateEnrollment = params.startDateEnrollment ? Date.parse(dateFormat, params.startDateEnrollment) : null
        event.endDateEnrollment = params.endDateEnrollment ? Date.parse(dateFormat, params.endDateEnrollment) : null
        event.latitude = params.float('latitude')
        event.longitude = params.float('longitude')
        event.clearErrors()

        def eventResult = edit ? eventService.update(event) : eventService.create(event)
        if (eventResult.hasErrors()) {
            if (eventResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                eventResult.errors.fieldErrors.each {
                    fieldErrors << [field: it.field, message: message(error: it)]
                }
                result = [fieldErrors: fieldErrors]
            } else {
                result = [error: message(error: eventResult.errors.globalError)]
            }
        } else {
            result = [success: message(code: "event.${edit ? 'update' : 'create'}.success")]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def event = Event.read(id)
        if (!event) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'event.default.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmDelete', model: [event: event, message: message(code: 'event.delete.message')])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def eventResult = eventService.delete(id)
        if (eventResult.hasErrors()) {
            result = [ error: message(error: eventResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'event.delete.success') ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def findNear() {
        render(view: 'findNear', model: [authUser: authenticatedUser])
    }

    @Secured('isAuthenticated()')
    def findNearEvents(final String start, final String end, final Float neLat, final Float neLong, final Float swLat,
                       final Float swLong) {

        def fromDate = Date.parse('yyyy-MM-dd', start)
        def toDate = Date.parse('yyyy-MM-dd', end) + 1

        def result = eventService.findEvents(fromDate, toDate, neLat, neLong, swLat, swLong)

        def events = result.events.collect { event ->

            def eventColor = CommonUtils.getItemColor(event.id)
            def eventUrl = createLink(controller: 'event', action: 'show', params: [id: event.id, clubId: event.club.id])

            return [id: event.id, title: event.name, description: event.description, start: event.startDate, end:
                    event.endDate, backgroundColor: eventColor, borderColor: eventColor, url: eventUrl, latitude:
                    event.latitude, longitude: event.longitude]
        }

        render events as JSON
    }
}
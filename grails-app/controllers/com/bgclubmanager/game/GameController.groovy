package com.bgclubmanager.game

import com.bgclubmanager.club.Club
import com.bgclubmanager.event.Event
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class GameController {

    def BGGService
    def gameService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 12

        def searchResult = gameService.getGames(club, params.offset, params.max)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, games: searchResult.list, gameCount:
                searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def game = Game.read(id)
        if (!game) {
            render(status: 404)
            return null
        }

        render(view: 'show', model: [authUser: authenticatedUser, game: game])
    }

    @Secured('isAuthenticated()')
    def showBGGSearch(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {

            model.authUser = authenticatedUser
            model.club = club

            render(view: 'bggSearch', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def bggSearch(final String name) {

        def result = [:]

        if (name?.length() > 1) {
            result.games = BGGService.search(name)
        } else {
            result.fieldErrors = [[field: 'name', message: message(code: 'game.bggSearch.error.minSize')]]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {

            model.authUser = authenticatedUser
            model.game = new Game(club: club, copies: 1)

            if (params.name) {
                model.game.name = params.name
            }

            if (params.description) {
                model.game.description = params.description
            }

            if (params.urlImage) {
                model.game.urlImage = params.urlImage
            }

            if (params.urlBGG) {
                model.game.urlBGG = params.urlBGG
            }

            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def game = Game.read(id)
        if (!game) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'game.default.label'), id])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.game = game
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save(final Game game) {

        def result = [:]

        def edit = game.id != null

        def gameResult = edit ? gameService.update(game) : gameService.create(game)
        if (gameResult.hasErrors()) {
            if (gameResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                gameResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: gameResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "game.${edit ? 'update' : 'create'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def game = Game.read(id)
        if (!game) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'game.default.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmDelete', model: [game: game, message: message(code: 'game.delete.message')])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def gameResult = gameService.delete(id)
        if (gameResult.hasErrors()) {
            result = [ error: message(error: gameResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'game.delete.success') ]
        }

        render result as JSON
    }
}

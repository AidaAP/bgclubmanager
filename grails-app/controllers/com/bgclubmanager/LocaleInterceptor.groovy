package com.bgclubmanager

import com.bgclubmanager.core.Language

class LocaleInterceptor {

    def localeResolver
    def springSecurityService

    LocaleInterceptor() {
        matchAll()
    }

    boolean before() {

        def locale

        // 1.- By parameter
        if (params.lang) {
            locale = new Locale(params.lang)
            localeResolver.setLocale(request, response, locale)
            return true
        }

        // 2.- By user language
        if (springSecurityService.isLoggedIn()) {
            locale = new Locale(springSecurityService.currentUser.language.code)
            localeResolver.setLocale(request, response, locale)
            return true
        }

        // 3.- By browser preference (first language we have)
        if (request.locales) {
            locale = request.locales.find { Language.findByCode(it.language) }
            localeResolver.setLocale(request, response, locale)
            return true
        }

        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.event.Event
import com.bgclubmanager.event.OrganizationRequest
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTOrganizationRequestController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def organizationService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def clubs = params.RESTClubId ? [Club.read(params.RESTClubId)] : Member.adminClubs(authenticatedUser).list()
            if (params.RESTClubId && !clubs) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def events = params.RESTEventId ? Event.findAllByClubInListAndId(clubs, params.RESTEventId) : Event
                        .findAllByClubInList(clubs)
                if (params.RESTEventId && !events) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    result.errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                            '.label'), params.RESTEventId])]
                } else {

                    def isOrganizerAdmin = events.every { event ->
                        event.getOrganizers().any { authenticatedUser.isAdminOf(it) }
                    }
                    if (SpringSecurityUtils.ifAllGranted(ROLE_ADMIN) || isOrganizerAdmin) {

                        def totalCount = OrganizationRequest.countByEventInList(events)

                        int startIndex = params.offset ? Math.min(params.int('offset'), totalCount) : 0
                        int endIndex = params.max ? Math.min((startIndex + params.int('max')), totalCount) : totalCount
                        def list = OrganizationRequest.findAllByEventInList(events).subList(startIndex, endIndex)

                        result = [list: list, totalCount: totalCount]
                    } else {
                        response.status = HttpServletResponse.SC_FORBIDDEN
                    }
                }
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def accept() {

        def result = [:]
        def errors = []

        if (!params.ids) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def clubs = params.RESTClubId ? [Club.read(params.RESTClubId)] : Member.adminClubs(authenticatedUser).list()
            if (params.RESTClubId && !clubs) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def events = params.RESTEventId ? Event.findAllByClubInListAndId(clubs, params.RESTEventId) : Event
                        .findAllByClubInList(clubs)
                if (params.RESTEventId && !events) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    result.errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                            '.label'), params.RESTEventId])]
                } else if (!clubs.every { authenticatedUser.isAdminOf(it) }) {
                    response.status = HttpServletResponse.SC_FORBIDDEN
                } else {

                    def ids = params.list('ids')

                    OrganizationRequest[] organizationRequests = []
                    ids.each { id ->
                        OrganizationRequest organizationRequest = OrganizationRequest.findByEventInListAndId(events, id)
                        if (!organizationRequest) {
                            result.put(id, message(code: 'default.not.found.message', args: [message(code:
                                    'organizationRequest.default.label'), id]))
                        } else {
                            organizationRequests += organizationRequest
                        }
                    }

                    if (organizationRequests) {
                        def processedRequests = organizationService.acceptOrganizationRequests(organizationRequests)
                        processedRequests.each { processedRequest ->
                            def error = processedRequest.errors ? processedRequest.errors.allErrors.collect {
                                message(error: it)
                            }.join(', ') : null
                            result.put(processedRequest.id, error ?: 'OK')
                        }
                    }
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def reject() {

        def result = [:]
        def errors = []

        if (!params.ids) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def clubs = params.RESTClubId ? [Club.read(params.RESTClubId)] : Member.adminClubs(authenticatedUser).list()
            if (params.RESTClubId && !clubs) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def events = params.RESTEventId ? Event.findAllByClubInListAndId(clubs, params.RESTEventId) : Event
                        .findAllByClubInList(clubs)
                if (params.RESTEventId && !events) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    result.errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                            '.label'), params.RESTEventId])]
                } else if (!clubs.every { authenticatedUser.isAdminOf(it) }) {
                    response.status = HttpServletResponse.SC_FORBIDDEN
                } else {

                    def ids = params.list('ids')

                    OrganizationRequest[] organizationRequests = []
                    ids.each { id ->
                        OrganizationRequest organizationRequest = OrganizationRequest.findByEventInListAndId(events, id)
                        if (!organizationRequest) {
                            result.put(id, message(code: 'default.not.found.message', args: [message(code:
                                    'organizationRequest.default.label'), id]))
                        } else {
                            organizationRequests += organizationRequest
                        }
                    }

                    if (organizationRequests) {
                        def processedRequests = organizationService.rejectOrganizationRequests(organizationRequests)
                        processedRequests.each { processedRequest ->
                            def error = processedRequest.errors ? processedRequest.errors.allErrors.collect {
                                message(error: it)
                            }.join(', ') : null
                            result.put(processedRequest.id, error ?: 'OK')
                        }
                    }
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}
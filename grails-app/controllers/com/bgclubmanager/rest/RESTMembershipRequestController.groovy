package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.MembershipRequest
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

class RESTMembershipRequestController {

    def membershipService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def requests = membershipService.getMembershipRequests(club, params.int('offset'), params.int('max'))

                result = [ list: requests, totalCount: requests.totalCount ]
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def accept() {

        def result = [:]
        def errors = []

        def club = Club.read(params.RESTClubId)
        if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def ids = params.list('ids')

            if (!ids) {
                response.status = HttpServletResponse.SC_BAD_REQUEST
                errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
            } else {

                MembershipRequest[] membershipRequests = []
                ids.each { id ->
                    MembershipRequest membershipRequest = MembershipRequest.findByClubAndId(club, id)
                    if (!membershipRequest) {
                        result.put(id, message(code: 'default.not.found.message', args: [message(code: 'request' +
                                '.default.label'), id]))
                    } else {
                        membershipRequests += membershipRequest
                    }
                }

                if (membershipRequests) {
                    def processedRequests = membershipService.acceptMembershipRequests(membershipRequests)
                    processedRequests.each { processedRequest ->
                        def error = processedRequest.errors ? processedRequest.errors.allErrors.collect {
                            message(error: it) }.join(', ') : null
                        result.put(processedRequest.id, error ?: 'OK')
                    }
                }
            }

            if (errors) {
                result.errors = errors
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def reject() {

        def result = [:]
        def errors = []

        def club = Club.read(params.RESTClubId)
        if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def ids = params.list('ids')

            if (!ids) {
                response.status = HttpServletResponse.SC_BAD_REQUEST
                errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
            } else {

                MembershipRequest[] membershipRequests = []
                ids.each { id ->
                    MembershipRequest membershipRequest = MembershipRequest.findByClubAndId(club, id)
                    if (!membershipRequest) {
                        result.put(id, message(code: 'default.not.found.message', args: [message(code: 'request' +
                                '.default.label'), id]))
                    } else {
                        membershipRequests += membershipRequest
                    }
                }

                if (membershipRequests) {
                    def processedRequests = membershipService.rejectMembershipRequests(membershipRequests)
                    processedRequests.each { processedRequest ->
                        def error = processedRequest.errors ? processedRequest.errors.allErrors.collect {
                            message(error: it) }.join(', ') : null
                        result.put(processedRequest.id, error ?: 'OK')
                    }
                }
            }

            if (errors) {
                result.errors = errors
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}

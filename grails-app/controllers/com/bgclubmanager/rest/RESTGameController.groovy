package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.game.Game
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

class RESTGameController {

    def fileService
    def gameService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def games = gameService.getGames(club, params.int('offset'), params.int('max'))

                result = [ list: [], totalCount: games.totalCount ]

                // Fix urlImage
                games.list.each { game ->
                    def gameResult = new Game(game.properties)
                    if (game.urlImage) {
                        gameResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                                [path: game.urlImage])
                    }
                    result.list << gameResult
                }
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def game = Game.findByClubAndId(club, params.id)
            if (params.id && !game) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'game.default.label'),
                                                                            params.id])]
            } else {

                def gameResult = new Game(game.properties)
                // Fix urlImage
                if (game.urlImage) {
                    gameResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                            [path: game.urlImage])
                }

                result = [ game: gameResult ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final Game game) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        } else {
            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                game.club = club
                game.urlImage = null

                def createdGame = gameService.create(game)
                if (createdGame.hasErrors()) {
                    if (createdGame.errors.hasFieldErrors()) {
                        createdGame.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: createdGame.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {

                    def gameResult = new Game(createdGame.properties)
                    // Fix urlImage
                    if (createdGame.urlImage) {
                        gameResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                                [path: createdGame.urlImage])
                    }
                    // Fix id
                    gameResult.id = createdGame.id

                    result = [ game: gameResult ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update(final Game game) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def gameDB = Game.findByClubAndId(club, params.id)
            if (params.id && !gameDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'game.default.label'),
                                                                            params.id])]
            } else {

                // Prevent modifications on urlImage
                if (game) {
                    Game.withNewSession {
                        game.urlImage = Game.read(game.id)?.urlImage
                    }
                }

                def updatedGame = gameService.update(game)
                if (updatedGame.hasErrors()) {
                    if (updatedGame.errors.hasFieldErrors()) {
                        updatedGame.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: updatedGame.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {

                    def gameResult = new Game(updatedGame.properties)
                    // Fix urlImage
                    if (updatedGame.urlImage) {
                        gameResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                                [path: updatedGame.urlImage])
                    }

                    result = [ game: gameResult ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def updateImage() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.RESTGameId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTGameId'])
        }

        if (!params.image) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['image'])
        } else if (!params.image.startsWith('data:image/')) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.wrongParameter', args: ['image'])
        }

        if (!errors) {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def game = Game.findByClubAndId(club, params.RESTGameId)
                if (!game) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    errors = [message(code: 'default.not.found.message', args: [message(code: 'game.default.label'),
                                                                                params.id])]
                } else {

                    game.urlImage = fileService.saveGameImage(params.image)

                    gameService.update(game)
                    if (game.hasErrors()) {
                        if (game.errors.hasFieldErrors()) {
                            game.errors.fieldErrors.each {
                                errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                            }
                        } else {
                            errors << message(error: game.errors.globalError)
                        }
                    } else {

                        def gameResult = new Game(game.properties)
                        // Fix urlImage
                        if (game.urlImage) {
                            gameResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile',
                                    params: [path: game.urlImage])
                        }

                        result = [ game: gameResult ]
                    }
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def deleteImage() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.RESTGameId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTGameId'])
        }

        if (!errors) {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def game = Game.findByClubAndId(club, params.RESTGameId)
                if (!game) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    errors = [message(code: 'default.not.found.message', args: [message(code: 'game.default.label'),
                                                                                params.id])]
                } else {

                    game.urlImage = null

                    gameService.update(game)
                    if (game.hasErrors()) {
                        if (game.errors.hasFieldErrors()) {
                            game.errors.fieldErrors.each {
                                errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                            }
                        } else {
                            errors << message(error: game.errors.globalError)
                        }
                    } else {
                        response.status = HttpServletResponse.SC_NO_CONTENT
                    }
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def gameDB = Game.findByClubAndId(club, id)
            if (id && !gameDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'game.default.label'), id])]
            } else {

                def gameResult = gameService.delete(id)
                if (gameResult.hasErrors()) {
                    if (gameResult.errors.hasFieldErrors()) {
                        gameResult.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: gameResult.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    response.status = HttpServletResponse.SC_NO_CONTENT
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}
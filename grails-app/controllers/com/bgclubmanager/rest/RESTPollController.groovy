package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.poll.Poll
import com.bgclubmanager.poll.PollCommand
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTPollController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def pollService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {
                result = pollService.getPolls(club, params.int('offset'), params.int('max'))
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def poll = Poll.findByClubAndId(club, params.id)
            if (params.id && !poll) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'poll.default.label'),
                                                                            params.id])]
            } else {
                result = [ poll: poll ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final PollCommand pollCommand) {

        def result = [:]
        def errors = []

        def club

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        } else {
            club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
                errors << message(code: 'poll.create.error.notAllowed')
            }
        }

        if (!errors) {

            pollCommand.club = club

            def pollResult = pollService.create(pollCommand)
            if (pollResult.hasErrors()) {
                if (pollResult.errors.hasFieldErrors()) {
                    pollResult.errors.fieldErrors.each {
                        errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                    }
                } else {
                    errors << message(error: pollResult.errors.globalError)
                }
                response.status = HttpServletResponse.SC_BAD_REQUEST
            } else {
                result = [ poll: pollResult ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update() {

        def result = [:]
        def errors = []

        def commandParams = [:] + params + request.JSON
        commandParams.dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        commandParams.clubId = params.RESTClubId
        def pollCommand = request.JSON ? PollCommand.fromParams(commandParams) : null

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
            errors << message(code: 'poll.create.error.notAllowed')
        } else {

            def pollDB = Poll.findByClubAndId(club, params.id)
            if (params.id && !pollDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'poll.default.label'),
                                                                            params.id])]
            } else {

                def updatedPoll = pollService.update(pollCommand)
                if (updatedPoll.hasErrors()) {
                    if (updatedPoll.errors.hasFieldErrors()) {
                        updatedPoll.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: updatedPoll.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    result = [ poll: updatedPoll ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def pollDB = Poll.findByClubAndId(club, id)
            if (id && !pollDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'poll.default.label'), id])]
            } else {

                def pollResult = pollService.delete(id)
                if (pollResult.hasErrors()) {
                    if (pollResult.errors.hasFieldErrors()) {
                        pollResult.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: pollResult.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    response.status = HttpServletResponse.SC_NO_CONTENT
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}

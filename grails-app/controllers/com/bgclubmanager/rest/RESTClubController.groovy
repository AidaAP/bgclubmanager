package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured

import javax.servlet.http.HttpServletResponse

class RESTClubController {

    def clubService
    def fileService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {
            result = clubService.listAll(params.int('offset'), params.int('max'), params.sort, params.order)

            // Fix urlImage
            result.list.each { club ->
                if (club.urlImage) {
                    club.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                            [path: club.urlImage])
                }
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.id) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.id)
        if (params.id && !club) {
            errors << message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), params.id])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            result.errors = errors
        } else {

            def clubResult = new Club(club.properties)
            // Fix urlImage
            if (club.urlImage) {
                clubResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                        [path: club.urlImage])
            }

            result = [ club: clubResult ]
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final Club club) {

        def result = [:]
        def errors = []

        // Delete non-modifiable fields
        club?.owner = null
        club?.urlImage = null

        def clubResult = clubService.create(club)
        if (clubResult.hasErrors()) {
            if (clubResult.errors.hasFieldErrors()) {
                clubResult.errors.fieldErrors.each {
                    errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                }
            } else {
                errors << message(error: clubResult.errors.globalError)
            }
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {
            result = [ club: clubResult ]
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update(final Club club) {

        def result = [:]
        def errors = []

        // Prevent modifications on urlImage
        if (club) {
            Club.withNewSession {
                club.urlImage = Club.read(club.id)?.urlImage
            }
        }

        def updatedClub = clubService.update(club)
        if (updatedClub.hasErrors()) {
            if (updatedClub.errors.hasFieldErrors()) {
                updatedClub.errors.fieldErrors.each {
                    errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                }
            } else {
                errors << message(error: updatedClub.errors.globalError)
            }
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def clubResult = new Club(club.properties)
            // Fix urlImage
            if (club.urlImage) {
                clubResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                        [path: club.urlImage])
            }

            result = [ club: clubResult ]
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def updateImage() {

        def result = [:]
        def errors = []

        def club = Club.get(params.RESTClubId)

        if (!club) {
            errors << message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), params.RESTClubId])
        } else if (!params.image) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['image'])
        } else if (!params.image.startsWith('data:image/')) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['image'])
        } else {

            club.urlImage = fileService.saveClubImage(params.image)

            clubService.update(club)
            if (club.hasErrors()) {
                if (club.errors.hasFieldErrors()) {
                    club.errors.fieldErrors.each {
                        errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                    }
                } else {
                    errors << message(error: club.errors.globalError)
                }
            } else {

                def clubResult = new Club(club.properties)
                // Fix urlImage
                if (club.urlImage) {
                    clubResult.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                            [path: club.urlImage])
                }

                result = [ club: clubResult ]
            }
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def deleteImage() {

        def result = [:]
        def errors = []

        def club = Club.get(params.RESTClubId)
        if (!club) {
            errors << message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), params.RESTClubId])
        } else {

            club.urlImage = null

            clubService.update(club)
            if (club.hasErrors()) {
                if (club.errors.hasFieldErrors()) {
                    club.errors.fieldErrors.each {
                        errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                    }
                } else {
                    errors << message(error: club.errors.globalError)
                }
            } else {
                response.status = HttpServletResponse.SC_NO_CONTENT
            }
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final Club club) {

        def result = [:]
        def errors = []

        def clubResult = clubService.delete(club?.id)
        if (clubResult.hasErrors()) {
            if (clubResult.errors.hasFieldErrors()) {
                clubResult.errors.fieldErrors.each {
                    errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                }
            } else {
                errors << message(error: clubResult.errors.globalError)
            }
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {
            response.status = HttpServletResponse.SC_NO_CONTENT
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}
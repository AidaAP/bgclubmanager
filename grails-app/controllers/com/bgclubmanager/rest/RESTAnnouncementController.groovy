package com.bgclubmanager.rest

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.club.Club
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTAnnouncementController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def announcementService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {
                result = announcementService.getAnnouncements(club, params.int('offset'), params.int('max'))
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def announcement = Announcement.findByClubAndId(club, params.id)
            if (params.id && !announcement) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'announcement.default.label'),
                                                                            params.id])]
            } else {
                result = [ announcement: announcementService.read(announcement) ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final Announcement announcement) {

        def result = [:]
        def errors = []

        def club

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        } else {
            club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            }
        }

        if (!errors) {

            announcement.club = club

            def announcementResult = announcementService.create(announcement)
            if (announcementResult.hasErrors()) {
                if (announcementResult.errors.hasFieldErrors()) {
                    announcementResult.errors.fieldErrors.each {
                        errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                    }
                } else {
                    errors << message(error: announcementResult.errors.globalError)
                }
                response.status = HttpServletResponse.SC_BAD_REQUEST
            } else {
                result = [ announcement: announcementResult ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update(final Announcement announcement) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def announcementDB = Announcement.findByClubAndId(club, params.id)
            if (params.id && !announcementDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'announcement.default.label'),
                                                                            params.id])]
            } else {

                def updatedAnnouncement = announcementService.update(announcement)
                if (updatedAnnouncement.hasErrors()) {
                    if (updatedAnnouncement.errors.hasFieldErrors()) {
                        updatedAnnouncement.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: updatedAnnouncement.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    result = [ announcement: updatedAnnouncement ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def announcementDB = Announcement.findByClubAndId(club, id)
            if (id && !announcementDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'announcement.default' +
                        '.label'), id])]
            } else {

                def announcementResult = announcementService.delete(id)
                if (announcementResult.hasErrors()) {
                    if (announcementResult.errors.hasFieldErrors()) {
                        announcementResult.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: announcementResult.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    response.status = HttpServletResponse.SC_NO_CONTENT
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}

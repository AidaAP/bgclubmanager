package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.Event
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTAttendanceController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def attendanceService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.RESTEventId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTEventId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def event = Event.findByClubAndId(club, params.RESTEventId)
                if (!event) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    result.errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                            '.label'), params.RESTEventId])]
                } else {

                    def isOrganizerAdmin = event.getOrganizers().any { authenticatedUser.isAdminOf(it) }
                    def isAttendee = authenticatedUser.isAttendeeOf(event)
                    if (SpringSecurityUtils.ifAllGranted(ROLE_ADMIN) || isOrganizerAdmin || isAttendee) {

                        def totalCount = Attendance.countByEvent(event)

                        int startIndex = params.offset ? Math.min(params.int('offset'), totalCount) : 0
                        int endIndex = params.max ? Math.min((startIndex + params.int('max')), totalCount) : totalCount
                        def list = Attendance.findAllByEvent(event).subList(startIndex, endIndex)

                        result = [ list: list, totalCount: totalCount ]
                    } else {
                        response.status = HttpServletResponse.SC_FORBIDDEN
                    }
                }
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def cancel() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.RESTEventId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTEventId'])
        }

        if (!params.ids) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else {

                def event = Event.findByClubAndId(club, params.RESTEventId)
                if (!event) {
                    response.status = HttpServletResponse.SC_NOT_FOUND
                    result.errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default' +
                            '.label'), params.RESTEventId])]
                } else if (!authenticatedUser.isAdminOf(club)) {
                    response.status = HttpServletResponse.SC_FORBIDDEN
                } else {

                    def ids = params.list('ids')

                    Attendance[] attendances = []
                    ids.each { id ->
                        Attendance attendance = Attendance.findByEventAndId(event, id)
                        if (!attendance) {
                            result.put(id, message(code: 'default.not.found.message', args: [message(code:
                                    'attendance.default.label'), id]))
                        } else {
                            attendances += attendance
                        }
                    }

                    if (attendances) {
                        def processedAttendances = attendanceService.cancelAttendances(attendances)
                        processedAttendances.each { processedAttendance ->
                            def error = processedAttendance.errors ? processedAttendance.errors.allErrors.collect {
                                message(error: it)
                            }.join(', ') : null
                            result.put(processedAttendance.id, error ?: 'OK')
                        }
                    }
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}
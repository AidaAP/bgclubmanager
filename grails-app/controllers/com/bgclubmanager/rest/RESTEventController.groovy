package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.event.Event
import com.bgclubmanager.event.OrganizationRequest
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTEventController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def eventService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (!isLoggedIn()) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {
                result = eventService.getClubEvents(club, params.int('offset'), params.int('max'))
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def event = Event.findByClubAndId(club, params.id)
            if (params.id && !event) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default.label'),
                                                                            params.id])]
            } else {

                def organizers = event.getOrganizers()

                def eventIsPublic = event.isPublic
                def userIsOrganizerMember = organizers.any { authenticatedUser.isMemberOf(it) }
                def userIsRequestedOrganizerAdmin = OrganizationRequest.findAllByEvent(event).collect { it.club }.any {
                    authenticatedUser.isAdminOf(it) }
                def isAttendee = authenticatedUser.isAttendeeOf(event)
                def userHasRequestedAttendance = AttendanceRequest.findByEventAndUser(event, authenticatedUser)

                def allowed = eventIsPublic || userIsOrganizerMember || userIsRequestedOrganizerAdmin || isAttendee ||
                        userHasRequestedAttendance
                if (!allowed) {
                    response.status = HttpServletResponse.SC_FORBIDDEN
                } else {
                    result = [ event: event ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final Event event) {

        def result = [:]
        def errors = []

        def club

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        } else {
            club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            }
        }

        if (!errors) {

            event.club = club

            def eventResult = eventService.create(event)
            if (eventResult.hasErrors()) {
                if (eventResult.errors.hasFieldErrors()) {
                    eventResult.errors.fieldErrors.each {
                        errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                    }
                } else {
                    errors << message(error: eventResult.errors.globalError)
                }
                response.status = HttpServletResponse.SC_BAD_REQUEST
            } else {
                result = [ event: eventResult ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update(final Event event) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def eventDB = Event.findByClubAndId(club, params.id)
            if (params.id && !eventDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default.label'),
                                                                            params.id])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def updatedEvent = eventService.update(event)
                if (updatedEvent.hasErrors()) {
                    if (updatedEvent.errors.hasFieldErrors()) {
                        updatedEvent.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: updatedEvent.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    result = [ event: updatedEvent ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def eventDB = Event.findByClubAndId(club, id)
            if (id && !eventDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'event.default.label'), id])]
            } else if (!authenticatedUser.isAdminOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def eventResult = eventService.delete(id)
                if (eventResult.hasErrors()) {
                    if (eventResult.errors.hasFieldErrors()) {
                        eventResult.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: eventResult.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    response.status = HttpServletResponse.SC_NO_CONTENT
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}
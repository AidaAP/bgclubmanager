package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.user.User
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTMembershipController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def membershipService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (params.offset && !(params.int('offset') >= 0)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['offset'])
        }

        if (params.max && !(params.int('max') >= 1)) {
            errors << message(code: 'api.rest.error.wrongParameter', args: ['max'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def members = membershipService.getMembers(club, params.int('offset'), params.int('max'))

                // Fix urlImage
                members.each { user ->
                    if (user.urlImage) {
                        user.urlImage = createLink(absolute: true, controller: 'file', action: 'loadFile', params:
                                [path: user.urlImage])
                    }
                }

                result = [ list: members, totalCount: members.totalCount ]
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def changeType() {

        def result = [:]
        def errors = []

        def club = Club.read(params.RESTClubId)
        if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def ids = params.list('ids')
            if (!ids) {
                response.status = HttpServletResponse.SC_BAD_REQUEST
                errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
            }

            def memberType
            def type = params.type
            if (!type) {
                response.status = HttpServletResponse.SC_BAD_REQUEST
                errors << message(code: 'api.rest.error.missingParameter', args: ['type'])
            } else {
                memberType = Member.Type.values().find { it.name() == type }
                if (!memberType) {
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                    errors << message(code: 'api.rest.error.wrongParameter', args: ['type'])
                }
            }

            if (errors) {
                result.errors = errors
            } else {

                String[] memberships = []
                ids.each { id ->

                    def user = User.read(id) ?: User.findByUsername(id)

                    Member membership = Member.findByClubAndUser(club, user)
                    if (!membership) {
                        result.put(id, message(code: 'membership.revoke.error.userNotInClub', args: [id]))
                    } else {
                        memberships += membership.id
                    }
                }

                if (memberships) {
                    def processedMemberships = membershipService.changeMultipleMembershipTypes(memberships, memberType)
                    processedMemberships.each { processedMembership ->
                        def error = processedMembership.errors ? processedMembership.errors.allErrors.collect {
                            message(error: it) }.join(', ') : null
                        result.put(processedMembership.user.username, error ?: 'OK')
                    }
                }
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def expel() {

        def result = [:]
        def errors = []

        def club = Club.read(params.RESTClubId)
        if (!authenticatedUser.isAdminOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def ids = params.list('ids')

            if (!ids) {
                response.status = HttpServletResponse.SC_BAD_REQUEST
                errors << message(code: 'api.rest.error.missingParameter', args: ['ids'])
            } else {

                String[] memberships = []
                ids.each { id ->

                    def user = User.read(id) ?: User.findByUsername(id)

                    Member membership = Member.findByClubAndUser(club, user)
                    if (!membership) {
                        result.put(id, message(code: 'membership.revoke.error.userNotInClub', args: [id]))
                    } else {
                        memberships += membership.id
                    }
                }

                if (memberships) {
                    def processedMemberships = membershipService.deleteMultiple(memberships)
                    processedMemberships.each { processedMembership ->
                        def error = processedMembership.errors ? processedMembership.errors.allErrors.collect {
                            message(error: it) }.join(', ') : null
                        result.put(processedMembership.user.username, error ?: 'OK')
                    }
                }
            }

            if (errors) {
                result.errors = errors
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}

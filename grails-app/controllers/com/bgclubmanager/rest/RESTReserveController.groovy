package com.bgclubmanager.rest

import com.bgclubmanager.club.Club
import com.bgclubmanager.reserve.Reserve
import com.bgclubmanager.reserve.ReserveCommand
import grails.converters.JSON
import grails.converters.XML
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

import javax.servlet.http.HttpServletResponse

class RESTReserveController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def reserveService

    @Secured('isAuthenticated()')
    def index() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (errors) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            result.errors = errors
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                result.errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default' +
                        '.label'), params.RESTClubId])]
            } else if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                String[] games = params.list('games')
                String[] tables = params.list('tables')

                def fromDate = params.date('fromDate', "yyyy-MM-dd'T'HH:mm:ss'Z'")
                def toDate = params.date('toDate', "yyyy-MM-dd'T'HH:mm:ss'Z'")

                result = reserveService.findReserves(club, games, tables, fromDate, toDate)
            }
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def show() {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
            response.status = HttpServletResponse.SC_FORBIDDEN
        } else {

            def reserve = Reserve.findByClubAndId(club, params.id)
            if (params.id && !reserve) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'reserve.default.label'),
                                                                            params.id])]
            } else {
                result = [ reserve: reserve ]
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def save(final ReserveCommand reserveCommand) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        } else {

            def club = Club.read(params.RESTClubId)
            if (!club) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                            params.RESTClubId])]
            } else if (!authenticatedUser.isMemberOf(club)) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                reserveCommand.club = club

                def createdReserve = reserveService.create(reserveCommand)
                if (createdReserve.hasErrors()) {
                    if (createdReserve.errors.hasFieldErrors()) {
                        createdReserve.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: createdReserve.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    result = [ reserve: createdReserve ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def update(final ReserveCommand reserveCommand) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!params.id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def reserveDB = Reserve.findByClubAndId(club, params.id)
            if (params.id && !reserveDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'reserve.default.label'),
                                                                            params.id])]
            } else if (reserveDB.user != authenticatedUser) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                reserveCommand.id = params.id
                reserveCommand.club = club

                def updatedReserve = reserveService.update(reserveCommand)
                if (updatedReserve.hasErrors()) {
                    if (updatedReserve.errors.hasFieldErrors()) {
                        updatedReserve.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: updatedReserve.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    result = [ reserve: updatedReserve ]
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]
        def errors = []

        if (!params.RESTClubId) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['RESTClubId'])
        }

        if (!id) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            errors << message(code: 'api.rest.error.missingParameter', args: ['id'])
        }

        def club = Club.read(params.RESTClubId)
        if (params.RESTClubId && !club) {
            response.status = HttpServletResponse.SC_NOT_FOUND
            errors = [message(code: 'default.not.found.message', args: [message(code: 'club.default.label'),
                                                                        params.RESTClubId])]
        } else {

            def reserveDB = Reserve.findByClubAndId(club, id)
            if (id && !reserveDB) {
                response.status = HttpServletResponse.SC_NOT_FOUND
                errors = [message(code: 'default.not.found.message', args: [message(code: 'reserve.default.label'), id])]
            } else if ((reserveDB.user != authenticatedUser) && (!authenticatedUser.isAdminOf(club))) {
                response.status = HttpServletResponse.SC_FORBIDDEN
            } else {

                def reserveResult = reserveService.cancel(id)
                if (reserveResult.hasErrors()) {
                    if (reserveResult.errors.hasFieldErrors()) {
                        reserveResult.errors.fieldErrors.each {
                            errors << "${message(code: 'api.rest.error.wrongParameter', args: [it.field])} (${message(error: it)})"
                        }
                    } else {
                        errors << message(error: reserveResult.errors.globalError)
                    }
                    response.status = HttpServletResponse.SC_BAD_REQUEST
                } else {
                    response.status = HttpServletResponse.SC_NO_CONTENT
                }
            }
        }

        if (errors) {
            result.errors = errors
        }

        withFormat {
            json { render result as JSON }
            xml { render result as XML }
            '*' { render result as JSON }
        }
    }
}

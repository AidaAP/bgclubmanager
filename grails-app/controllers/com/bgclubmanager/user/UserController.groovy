package com.bgclubmanager.user

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.security.core.context.SecurityContextHolder

class UserController {

    def springSecurityService
    def userService

    @Secured('isAuthenticated()')
    def index() {

        params.offset = params.offset ?: 0
        params.max = params.max ?: 12

        def listParams = [:]
        listParams.offset = params.offset
        listParams.max = params.max
        listParams.sort = 'username'
        listParams.order = 'asc'

        def searchResult = User.list(listParams)

        render(view: 'index', model: [users: searchResult, userCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String username) {

        def user = User.findByUsername("${username}.${params.format}") ?: User.findByUsername(username)
        if (!user) {
            render(status: 404)
            return null
        }

        render(view: 'show', model: [user: user])
    }

    @Secured('permitAll')
    def register() {
        if (!loggedIn) {
            render(view: 'register')
        } else {
            redirect(controller: 'dashboard', action: 'index')
        }
    }

    @Secured('permitAll')
    def doRegister(final User user) {

        def result = [:]

        def userResult = userService.register(user)
        if (userResult.hasErrors()) {
            if (userResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                userResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: userResult.errors.globalError) ]
            }
        } else {
            springSecurityService.reauthenticate(user.username)
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def account() {
        render(view: 'show', model: [user: authenticatedUser])
    }

    @Secured('isAuthenticated()')
    def changePassword(final String id) {

        def user = User.read(id)
        if (!user) {
            def result = [error: message(code: 'default.not.found.message',
                    args: [message(code: 'user.default.label'), id]) ]
            render result as JSON
            return
        }

        if (authenticatedUser.id != id) {
            def result = [error: message(code: 'user.changePassword.error.notAllowed') ]
            render result as JSON
            return
        }

        render(view: 'changePassword', model: [user: user])
    }

    @Secured('isAuthenticated()')
    def doChangePassword(final ChangePasswordCommand command) {

        def result = [:]

        def commandResult = userService.changePassword(command)
        if (commandResult.hasErrors()) {
            if (commandResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                commandResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: commandResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: 'user.changePassword.success') ]
        }

        render result as JSON
    }

    @Secured('ROLE_ADMIN')
    def createAdmin() {
        render(view: 'createEdit', model: [user: new User()])
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def user = User.read(id)
        if (!user) {
            def result = [error: message(code: 'default.not.found.message',
                    args: [message(code: 'user.default.label'), id]) ]
            render result as JSON
            return
        }

        render(view: 'createEdit', model: [user: user])
    }

    @Secured('isAuthenticated()')
    def save(final User user) {

        def result = [:]

        def edit = user.id != null

        def userResult = edit ? userService.update(user) : userService.createAdmin(user)
        if (userResult.hasErrors()) {
            if (userResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                userResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: userResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "user.${edit ? 'update' : 'createAdmin'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def message = message(code: "user.delete.${(springSecurityService.currentUserId == id) ? 'self' : 'other'}.message")

        render(view: 'confirmDelete', model: [id: id, message: message])
    }

    @Secured('isAuthenticated()')
    def secureDelete(final String id) {

        def message = message(code: "user.deleteSecure.${(springSecurityService.currentUserId == id) ? 'self' : 'other'}.message")

        render(view: 'secureDelete', model: [id: id, message: message])
    }

    @Secured('isAuthenticated()')
    def deregister(final String password) {

        def result = [:]

        if (!password) {
            result = [ fieldErrors: [ [ field: 'password', message: message(code: 'default.null.message') ] ] ]
        } else if (!springSecurityService.passwordEncoder.isPasswordValid(springSecurityService.currentUser.password,
                password, null)) {
            result = [ fieldErrors: [ [ field: 'password', message: message(code: 'user.deleteSecure.self.error.password') ] ] ]
        } else {

            def userResult = userService.delete(springSecurityService.currentUserId)
            if (userResult.hasErrors()) {
                result = [ error: message(error: userResult.errors.globalError) ]
            } else {
                SecurityContextHolder.clearContext()
            }
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def delete(final String id, final String username) {

        def result = [:]

        if (!username) {
            result = [ fieldErrors: [ [ field: 'username', message: message(code: 'default.null.message') ] ] ]
        } else if (username != User.read(id)?.username) {
            result = [ fieldErrors: [ [ field: 'username', message: message(code: 'user.deleteSecure.other.error.username') ]
            ] ]
        } else {

            def userResult = userService.delete(id)
            if (userResult.hasErrors()) {
                result = [ error: message(error: userResult.errors.globalError) ]
            } else {
                result = [ success: message(code: 'user.delete.success') ]
            }
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def generateAPIToken() {

        def result = userService.generateToken()
        if (result?.hasErrors()) {
            result = [ error: message(error: result.errors.globalError) ]
        }

        render result as JSON
    }
}
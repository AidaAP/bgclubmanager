package com.bgclubmanager.reserve

import com.bgclubmanager.club.Club
import com.bgclubmanager.event.Event
import com.bgclubmanager.game.Game
import com.bgclubmanager.table.Table
import com.bgclubmanager.util.CommonUtils
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

class ReserveController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def reserveService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
            render(status: 403)
            return null
        }

        def clubGames = Game.findAllByClub(club)
        def clubTables = Table.findAllByClub(club)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, clubGames: clubGames, clubTables:
                clubTables])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def reserve = Reserve.read(id)
        if (!reserve) {
            render(status: 404)
            return null
        }

        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(reserve.club)) {
            render(status: 403)
            return null
        }

        render(view: 'show', model: [authUser: authenticatedUser, reserve: reserve])
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else if (!authenticatedUser.isMemberOf(club)) {
            render(status: 403)
            return null
        } else {

            def reserveCommand = new ReserveCommand(club: club, games: [], tables: [])
            def clubGames = Game.findAllByClub(club)
            def clubTables = Table.findAllByClub(club)

            render(view: 'createEdit', model: [authUser: authenticatedUser, reserveCommand: reserveCommand,
                                               clubGames: clubGames, clubTables: clubTables])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def reserve = Reserve.read(id)
        if (!reserve) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'reserve.default.label'), id])
            render model as JSON
        } else {

            def reserveCommand = ReserveCommand.fromReserve(reserve)
            def clubGames = Game.findAllByClub(reserve.club)
            def clubTables = Table.findAllByClub(reserve.club)

            render(view: 'createEdit', model: [authUser: authenticatedUser, reserveCommand: reserveCommand,
                                               clubGames: clubGames, clubTables: clubTables])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save(final ReserveCommand reserveCommand) {

        def result = [:]

        if (!authenticatedUser.isMemberOf(reserveCommand?.club)) {
            render(status: 403)
            return null
        }

        // Fix dates
        def dateFormat = message(code: 'default.datetimenosec.format')
        reserveCommand.startDate = params.startDate ? Date.parse(dateFormat, params.startDate) : null
        reserveCommand.endDate = params.endDate ? Date.parse(dateFormat, params.endDate) : null

        reserveCommand.clearErrors()

        def edit = reserveCommand.id != null

        def reserveResult = edit ? reserveService.update(reserveCommand) : reserveService.create(reserveCommand)
        if (reserveResult.hasErrors()) {
            if (reserveResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                reserveResult.errors.fieldErrors.each {
                    def field = it.field.replace('reserved', '').toLowerCase()
                    fieldErrors << [ field: field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: reserveResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "reserve.${edit ? 'update' : 'create'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def findReserves(final String clubId, final String start, final String end) {

        def club = clubId ? Club.read(clubId) : null
        if (clubId && !club) {
            render(status: 404)
            return null
        }

        String[] games = params.list('games[]')
        String[] tables = params.list('tables[]')

        def fromDate = Date.parse('yyyy-MM-dd', start)
        def toDate = Date.parse('yyyy-MM-dd', end) + 1

        def result = reserveService.findReserves(club, games, tables, fromDate, toDate)

        def reserves = result.reserves.collect { reserve ->

            def reserveColor = CommonUtils.getItemColor(reserve.id)
            def reserveUrl = createLink(controller: 'reserve', action: 'show', params: [id: reserve.id, clubId: reserve.club.id])

            return [id: reserve.id, title: reserve.title, description: reserve.description, start: reserve.startDate,
                    end: reserve.endDate, backgroundColor: reserveColor, borderColor: reserveColor, url: reserveUrl]
        }

        render reserves as JSON
    }

    @Secured('isAuthenticated()')
    def confirmCancel(final String id) {

        def reserve = Reserve.read(id)
        if (!reserve) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'reserve.default.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmCancel', model: [reserve: reserve, message: message(code: 'reserve.cancel.message')])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def cancel(final String id) {

        def result = [:]

        def reserveResult = reserveService.cancel(id)
        if (reserveResult.hasErrors()) {
            result = [ error: message(error: reserveResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'reserve.cancel.success') ]
        }

        render result as JSON
    }
}

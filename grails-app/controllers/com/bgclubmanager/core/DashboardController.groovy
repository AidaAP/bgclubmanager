package com.bgclubmanager.core

import com.bgclubmanager.club.Member
import com.bgclubmanager.club.MembershipRequest
import com.bgclubmanager.event.AttendanceRequest
import com.bgclubmanager.event.OrganizationRequest
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class DashboardController {

    def announcementService
    def eventService
    def pollService
    def reserveService

    @Secured('isAuthenticated()')
    def index() {

        def cards = []

        // My tasks
        def adminClubs = Member.adminClubs(authenticatedUser).list()
        if (adminClubs) {
            def numMembershipRequests = MembershipRequest.countByClubInList(adminClubs)
            def numOrganizationRequests = OrganizationRequest.countByClubInList(adminClubs)
            def numAttendanceRequests = AttendanceRequest.pendingForClubs(adminClubs).list().size()
            def numTasks = numMembershipRequests + numOrganizationRequests + numAttendanceRequests
            cards << [
                    cssClass: 'bg-maroon',
                    number: numTasks,
                    text: message(code: 'dashboard.myTasks.card.label'),
                    cssIcon: 'fa fa-check-square-o',
                    link: createLink(controller: 'dashboard', action: 'myTasks')
            ]
        }

        // My announcements
        cards << [
                cssClass: 'bg-purple',
                number: authenticatedUser.numUnreadAnnouncements(),
                text: message(code: 'dashboard.myAnnouncements.card.label'),
                cssIcon: 'fa fa-newspaper-o',
                link: createLink(controller: 'dashboard', action: 'myAnnouncements')
        ]

        // My calendar
        cards << [
                cssClass: 'bg-orange',
                number: authenticatedUser.numUnfinishedEvents() + authenticatedUser.numUnfinishedReserves(),
                text: message(code: 'dashboard.myCalendar.card.label'),
                cssIcon: 'fa fa-calendar',
                link: createLink(controller: 'dashboard', action: 'myCalendar')
        ]

        // My polls
        cards << [
                cssClass: 'bg-red',
                number: authenticatedUser.numUnvotedPolls(),
                text: message(code: 'dashboard.myPolls.card.label'),
                cssIcon: 'fa fa-bar-chart',
                link: createLink(controller: 'dashboard', action: 'myPolls')
        ]

        // My clubs
        def numClubs = Member.countByUser(authenticatedUser)
        cards << [
                cssClass: 'bg-olive',
                number: numClubs,
                text: message(code: 'sideMenu.clubs.myClubs.label'),
                cssIcon: 'fa fa-users',
                link: createLink(controller: 'club', action: 'myIndex')
        ]

        // My requests
        def numRequests = MembershipRequest.countByUser(authenticatedUser) + AttendanceRequest.countByUser(authenticatedUser)
        cards << [
                cssClass: 'bg-teal',
                number: numRequests,
                text: message(code: 'dashboard.myRequests.card.label'),
                cssIcon: 'fa fa-send-o',
                link: createLink(controller: 'dashboard', action: 'myRequests')
        ]

        render(view: 'index', model: [cards: cards])
    }

    @Secured('isAuthenticated()')
    def myRequests() {

        def result = []

        // Membership requests
        def membershipRequests = MembershipRequest.findAllByUser(authenticatedUser).collect {
            def clubLink = createLink(controller: 'club', action: 'show', id: it.club.id)
            def clubName = it.club.name
            [ date: it.date, text: message(code: 'dashboard.myRequests.list.membership.text', args: [clubLink, clubName]) ]
        }
        result = result + membershipRequests

        // Attendance requests
        def attendanceRequests = AttendanceRequest.findAllByUser(authenticatedUser).collect {
            def eventLink = createLink(controller: 'event', action: 'show', params: [id: it.event.id, clubId: it.event.club.id])
            def eventName = it.event.name
            [ date: it.date, text: message(code: 'dashboard.myRequests.list.attendance.text', args: [eventLink, eventName]) ]
        }
        result = result + attendanceRequests

        result.sort{ a, b -> b.date <=> a.date }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 8
        def lastIndex = Math.min(params.offset + params.max, result.size())

        def requests = result.subList(params.offset, lastIndex)

        render(view: 'myRequests', model: [requests: requests, requestCount: result.size()])
    }

    @Secured('isAuthenticated()')
    def myTasks() {

        def result = []

        def adminClubs = Member.adminClubs(authenticatedUser).list()

        // Membership requests
        def membershipRequests = adminClubs ? MembershipRequest.findAllByClubInList(adminClubs).collect {
            def userLink = createLink(controller: 'user', action: 'show', params: [username: it.user.username])
            def username = it.user.username
            def clubLink = createLink(controller: 'club', action: 'show', id: it.club.id)
            def clubName = it.club.name
            [ type: 'membership', id: it.id, date: it.date, text: message(code: 'membership.request.list.text',
                    args: [userLink, username, clubLink, clubName]) ]
        } : []
        result = result + membershipRequests

        // Organization requests
        def organizationRequests = adminClubs ? OrganizationRequest.findAllByClubInList(adminClubs).collect {
            def clubLink = createLink(controller: 'club', action: 'show', id: it.club.id)
            def clubName = it.club.name
            def eventLink = createLink(controller: 'event', action: 'show', params: [id: it.event.id, clubId: it
                    .event.club.id])
            def eventName = it.event.name
            [ type: 'organization', id: it.id, date: it.date, text: message(code: 'organization.request.list.text',
                    args: [clubLink, clubName, eventLink, eventName]) ]
        } : []
        result = result + organizationRequests

        // Attendance requests
        def attendanceRequests = adminClubs ? AttendanceRequest.pendingForClubs(adminClubs).list().collect {
            def userLink = createLink(controller: 'user', action: 'show', params: [username: it.user.username])
            def username = it.user.username
            def eventLink = createLink(controller: 'event', action: 'show', params: [id: it.event.id, clubId: it
                    .event.club.id])
            def eventName = it.event.name
            [ type: 'attendance', id: it.id, date: it.date, text: message(code: 'attendance.request.list.text',
                    args: [userLink, username, eventLink, eventName]) ]
        } : []
        result = result + attendanceRequests

        result.sort{ a, b -> b.date <=> a.date }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 8
        def lastIndex = Math.min(params.offset + params.max, result.size())

        def tasks = result.subList(params.offset, lastIndex)

        render(view: 'myTasks', model: [tasks: tasks, taskCount: result.size()])
    }

    @Secured('isAuthenticated()')
    def myAnnouncements() {

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        // Announcements
        def searchResult = announcementService.getUnreadAnnouncements(params.offset, params.max)

        render(view: 'myAnnouncements', model: [announcements: searchResult.list,
                                                announcementCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def myCalendar() {

        // Events
        def events = eventService.getUnfinishedEvents().collect { event ->

            def eventUrl = createLink(controller: 'event', action: 'show', params: [id: event.id, clubId: event.club.id])

            return [title: event.name, start: event.startDate, end: event.endDate, className: 'bg-navy', url: eventUrl]
        }

        // Reserves
        def reserves = reserveService.findReserves(null, null, null, new Date(), null).reserves.collect { reserve ->

            def reserveUrl = createLink(controller: 'reserve', action: 'show', params: [id: reserve.id, clubId: reserve.club.id])

            return [title: reserve.title, start: reserve.startDate, end: reserve.endDate, className: 'bg-gray',
                    textColor: 'black', url: reserveUrl]
        }

        def items = [] + events + reserves

        render(view: 'myCalendar', model: [authUser: authenticatedUser, items: items as JSON])
    }

    @Secured('isAuthenticated()')
    def myPolls() {

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        // Polls
        def searchResult = pollService.getUnvotedPolls(params.offset, params.max)

        render(view: 'myPolls', model: [polls: searchResult.list, pollCount: searchResult.totalCount])
    }
}

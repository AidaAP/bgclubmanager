package com.bgclubmanager.core

import grails.plugin.springsecurity.annotation.Secured

class HomeController {

    @Secured('permitAll')
    def index() {
        if (!loggedIn) {
            render(view: '/index')
        } else {
            redirect(controller: 'dashboard', action: 'index')
        }
    }
}
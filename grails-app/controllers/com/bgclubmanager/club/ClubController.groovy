package com.bgclubmanager.club

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class ClubController {

    def clubService

    @Secured('isAuthenticated()')
    def index() {

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 12

        def searchResult = clubService.listAll(params.offset, params.max, 'name', 'asc')

        render(view: 'index', model: [clubs: searchResult.list, clubCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def myIndex() {

        params.offset = params.offset ?: 0
        params.max = params.max ?: 12

        def listParams = [:]
        listParams.offset = params.offset
        listParams.max = params.max
        listParams.sort = 'name'
        listParams.order = 'asc'

        def searchResult = Member.memberships(authenticatedUser).list(params)

        render(view: 'index', model: [clubs: searchResult, clubCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def club = Club.read(id)
        if (!club) {
            render(status: 404)
            return null
        }

        def numMembers = Member.countByClub(club)
        def isOwner = authenticatedUser == club.owner
        def isAdmin = authenticatedUser.isAdminOf(club)
        def isMember = authenticatedUser.isMemberOf(club)
        def hasMembershipRequest = authenticatedUser.hasMembershipRequestFor(club)

        render(view: 'show', model: [club: club, numMembers: numMembers, isOwner: isOwner, isAdmin: isAdmin, isMember:
                isMember, hasMembershipRequest: hasMembershipRequest])
    }

    @Secured('isAuthenticated()')
    def findNear() {
        render(view: 'findNear')
    }

    @Secured('isAuthenticated()')
    def findNearClubs(final Float neLat, final Float neLong, final Float swLat, final Float swLong) {
        def clubs = clubService.findBetweenCoordinates(neLat, neLong, swLat, swLong)
        render clubs as JSON
        return null
    }

    @Secured('isAuthenticated()')
    def create() {
        render(view: 'createEdit', model: [club: new Club()])
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def club = Club.read(id)
        if (!club) {
            def result = [error: message(code: 'default.not.found.message',
                    args: [message(code: 'club.default.label'), id]) ]
            render result as JSON
            return
        }

        render(view: 'createEdit', model: [club: club])
    }

    @Secured('isAuthenticated()')
    def save(final Club club) {

        def result = [:]

        def edit = club.id != null

        // Fix coordinates
        club.latitude = params.float('latitude')
        club.longitude = params.float('longitude')

        def clubResult = edit ? clubService.update(club) : clubService.create(club)
        if (clubResult.hasErrors()) {
            if (clubResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                clubResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: clubResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "club.${edit ? 'update' : 'create'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {
        render(view: 'confirmDelete', model: [id: id, message: message(code: "club.delete.message")])
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def clubResult = clubService.delete(id)
        if (clubResult.hasErrors()) {
            result = [ error: message(error: clubResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'club.delete.success') ]
        }

        render result as JSON
    }
}
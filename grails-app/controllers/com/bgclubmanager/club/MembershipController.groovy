package com.bgclubmanager.club

import com.bgclubmanager.user.User
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

class MembershipController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def membershipService

    @Secured('isAuthenticated()')
    def requestMembership(final Club club) {

        def result = [:]

        def membershipResult = membershipService.requestMembership(club)
        if (membershipResult.hasErrors()) {
            if (membershipResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                membershipResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: membershipResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: 'membership.request.success') ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def getMembershipRequests(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        if (!authenticatedUser.isAdminOf(club)) {
            render(status: 403)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 10

        def result = membershipService.getMembershipRequests(club, params.offset, params.max)

        def requests = result.collect {
            def userName = it.user.username
            def userLink = createLink(controller: 'user', action: 'show', params: [username: userName])
            def text = message(code: 'club.membership.request.list.text', args: [userLink, userName])
            [ id: it.id, date: it.date, text: text ]
        }

        render(view: 'requests', model: [club: club, requests: requests, requestCount: result.totalCount])
    }

    @Secured('isAuthenticated()')
    def acceptMembershipRequests() {

        def result = [:]

        MembershipRequest[] membershipRequests = MembershipRequest.getAll(params.list('ids[]'))
        def processedRequests = membershipService.acceptMembershipRequests(membershipRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'membership.acceptRequests.error')
        } else {
            result.success = message(code: 'membership.acceptRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def rejectMembershipRequests() {

        def result = [:]

        MembershipRequest[] membershipRequests = MembershipRequest.getAll(params.list('ids[]'))
        def processedRequests = membershipService.rejectMembershipRequests(membershipRequests)

        if (processedRequests.any { it.hasErrors() }) {
            result.error = message(code: 'membership.rejectRequests.error')
        } else {
            result.success = message(code: 'membership.rejectRequests.success')
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def getMembers(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
            render(status: 403)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 12

        def searchResult = membershipService.getMembers(club, params.offset, params.max)

        def users = [] + searchResult
        users.each { user ->
            def userDB = User.read(user.id)
            user.isAdmin = userDB.authorities.any { it.authority == 'ROLE_ADMIN' }
            user.isAdminOfClub = userDB.isAdminOf(club)
        }

        render(view: 'members', model: [authUser: authenticatedUser, club: club, users: users, userCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def confirmAction() {

        def selectedUsers = params.list('selectedUsers[]')
        def title = params.title
        def message = params.message
        def actionLink = params.actionLink
        def postLink = params.postLink

        render(view: 'confirmAction', model: [data: [selectedUsers: selectedUsers], title: title, message: message,
                                              actionLink: actionLink, postLink: postLink])
    }

    @Secured('isAuthenticated()')
    def leaveClub(final String id) {

        def result = [:]

        def club = Club.read(id)
        if (!club) {
            result = [ error: message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), id]) ]
        } else {

            def member = Member.findByClubAndUser(club, authenticatedUser)
            if (!member) {
                result = [ error: message(code: 'membership.leave.error.notAMember') ]
            } else {

                def memberResult = membershipService.delete(member.id)
                if (memberResult.hasErrors()) {
                    result = [ error: message(error: memberResult.errors.globalError) ]
                } else {
                    result = [ success: message(code: 'membership.leave.success') ]
                }
            }
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def expelUsers(final String id) {

        def result = [:]

        def club = Club.read(id)
        if (!club) {
            result = [ error: message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), id]) ]
        } else {

            def selectedUsers = params.list('selectedUsers[]')
            String[] members = []
            selectedUsers.each { userId ->
                def user = User.read(userId)
                def member = Member.findByClubAndUser(club, user)
                if (member) {
                    members += member.id
                }
            }

            if (members) {
                def membersResult = membershipService.deleteMultiple(members)
                if (!membersResult.any { it.hasErrors() }) {
                    result = [ success: message(code: 'membership.revoke.success') ]
                }
            }
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def changeMembershipType(final String id) {

        def result = [:]

        def type = Member.Type.valueOf(params.type)

        def club = Club.read(id)
        if (!club) {
            result = [ error: message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), id]) ]
        } else {

            def selectedUsers = params.list('selectedUsers[]')
            String[] members = []
            selectedUsers.each { userId ->
                def user = User.read(userId)
                def member = Member.findByClubAndUser(club, user)
                if (member) {
                    members += member.id
                }
            }

            if (members) {
                def membersResult = membershipService.changeMultipleMembershipTypes(members, type)
                if (!membersResult.any { it.hasErrors() }) {
                    result = [ success: message(code: 'membership.changeType.success') ]
                }
            }
        }

        render result as JSON
    }
}
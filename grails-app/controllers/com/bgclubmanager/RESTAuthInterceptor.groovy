package com.bgclubmanager

import com.bgclubmanager.user.UserToken
import grails.converters.JSON
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.i18n.LocaleContextHolder
import org.springframework.security.core.context.SecurityContextHolder

import javax.servlet.http.HttpServletResponse

class RESTAuthInterceptor {

    @Value('${default.api.authHeader}')
    final String HEADER_AUTH

    final String[] ALLOWED_FORMATS = ['all', 'json', 'xml']

    def messageSource
    def springSecurityService

    RESTAuthInterceptor() {
        match(uri: '/api/rest/**')
    }

    boolean before() {

        def authToken = request.getHeader(HEADER_AUTH)
        if (!authToken) {
            // Missing auth header
            response.status = HttpServletResponse.SC_FORBIDDEN
            def result = [message: messageSource.getMessage('api.rest.error.missingHeader', [HEADER_AUTH] as
                    Object[], LocaleContextHolder.locale)]
            render result as JSON
            return false
        }

        def user = UserToken.read(authToken)?.user
        if (!user) {
            // Incorrect auth header
            response.status = HttpServletResponse.SC_FORBIDDEN
            def result = [message: messageSource.getMessage('api.rest.error.wrongHeader', [HEADER_AUTH] as Object[],
                    LocaleContextHolder.locale)]
            render result as JSON
            return false
        }

        if (response.format && !(response.format in ALLOWED_FORMATS)) {
            response.status = HttpServletResponse.SC_BAD_REQUEST
            def result = [message: messageSource.getMessage('api.rest.error.wrongFormat',
                    [ALLOWED_FORMATS.toString()] as Object[], LocaleContextHolder.locale)]
            render result as JSON
            return false
        }

        // Authenticate user
        springSecurityService.reauthenticate user.username

        log.debug "Authenticated user: ${user.username}"

        return true
    }

    boolean after() {

        // Remove authenticated user
        if (springSecurityService.isLoggedIn()) {
            SecurityContextHolder.clearContext()
        }

        return true
    }

    void afterView() {
        // no-op
    }
}
package com.bgclubmanager.util

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class FileController {

    def fileService

    @Secured('permitAll')
    def loadFile(final String path) {
        if (path) {
            def file = new File(path)
            if (file?.exists()) {
                response.outputStream << file.bytes
            }
        }
    }

    @Secured('isAuthenticated()')
    def uploadUserImage() {

        response.status = 200
        def result

        def f = request.getFile('file')
        if (!f || f.empty) {
            response.status = 400
            result = message(code: 'file.error.noFile')
        } else {

            result = fileService.saveUserImage(f)
            if (!result) {
                response.status = 500
                result = message(code: 'file.error.upload')
            }
        }

        render result
    }

    @Secured('isAuthenticated()')
    def uploadClubImage() {

        response.status = 200
        def result

        def f = request.getFile('file')
        if (!f || f.empty) {
            response.status = 400
            result = message(code: 'file.error.noFile')
        } else {

            result = fileService.saveClubImage(f)
            if (!result) {
                response.status = 500
                result = message(code: 'file.error.upload')
            }
        }

        render result
    }

    @Secured('isAuthenticated()')
    def uploadGameImage() {

        response.status = 200
        def result

        def f = request.getFile('file')
        if (!f || f.empty) {
            response.status = 400
            result = message(code: 'file.error.noFile')
        } else {

            result = fileService.saveGameImage(f)
            if (!result) {
                response.status = 500
                result = message(code: 'file.error.upload')
            }
        }

        render result
    }

    @Secured('isAuthenticated()')
    def uploadGameExternalImage(final String externalImageUrl) {

        response.status = 200
        def result

        if (!externalImageUrl) {
            response.status = 400
            result = message(code: 'file.error.noFile')
        } else {

            result = fileService.saveExternalGameImage(externalImageUrl)
            if (!result) {
                response.status = 500
                result = message(code: 'file.error.upload')
            }
        }

        render result
    }

    @Secured('isAuthenticated()')
    def getMockFile(final String path) {

        def result = [:]

        if (path) {

            def file = new File(path)
            if (file?.exists()) {

                def name = file.name
                def size = file.size()
                def loadFileUrl = createLink(controller: 'file', action: 'loadFile', params: [path: path])

                result = [ path: path, name: name, size: size, loadFileUrl: loadFileUrl ]
            }
        }

        render result as JSON
    }
}
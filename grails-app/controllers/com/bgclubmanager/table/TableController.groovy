package com.bgclubmanager.table

import com.bgclubmanager.club.Club
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class TableController {

    def tableService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        def searchResult = tableService.getTables(club, params.offset, params.max)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, tables: searchResult.list, tableCount:
                searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def table = Table.read(id)
        if (!table) {
            render(status: 404)
            return null
        }

        render(view: 'show', model: [authUser: authenticatedUser, table: table])
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {

            model.authUser = authenticatedUser
            model.table = new Table(club: club)

            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def table = Table.read(id)
        if (!table) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'table.default.label'), id])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.table = table
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save(final Table table) {

        def result = [:]

        def edit = table.id != null

        // Fix coordinates
        table.latitude = params.float('latitude')
        table.longitude = params.float('longitude')
        table.clearErrors()

        def tableResult = edit ? tableService.update(table) : tableService.create(table)
        if (tableResult.hasErrors()) {
            if (tableResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                tableResult.errors.fieldErrors.each {
                    fieldErrors << [field: it.field, message: message(error: it)]
                }
                result = [fieldErrors: fieldErrors]
            } else {
                result = [error: message(error: tableResult.errors.globalError)]
            }
        } else {
            result = [success: message(code: "table.${edit ? 'update' : 'create'}.success")]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def table = Table.read(id)
        if (!table) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'table.default.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmDelete', model: [table: table, message: message(code: 'table.delete.message')])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def tableResult = tableService.delete(id)
        if (tableResult.hasErrors()) {
            result = [ error: message(error: tableResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'table.delete.success') ]
        }

        render result as JSON
    }
}

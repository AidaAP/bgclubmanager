package com.bgclubmanager.announcement

import com.bgclubmanager.club.Club
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

class AnnouncementController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def announcementService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
            render(status: 403)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        def searchResult = announcementService.getAnnouncements(club, params.offset, params.max)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, announcements: searchResult.list,
                                      announcementCount: searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def announcement = Announcement.read(id)
        if (!announcement) {
            def result = [error: message(code: 'default.not.found.message', args: [message(code: 'announcement' +
                    '.default.label'), id]) ]
            render result as JSON
            return
        }

        def result = announcementService.read(announcement)

        render(view: 'show', model: [authUser: authenticatedUser, announcement: result])
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.announcement = new Announcement(club: club, published: false)
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def announcement = Announcement.read(id)
        if (!announcement) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'announcement.default' +
                    '.label'), id])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.announcement = announcement
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save(final Announcement announcement) {

        def result = [:]

        def edit = announcement.id != null

        // Fix dates
        def dateFormat = message(code: 'default.datetimenosec.format')
        announcement.startDatePublish = params.startDatePublish ? Date.parse(dateFormat, params.startDatePublish) : null
        announcement.endDatePublish = params.endDatePublish ? Date.parse(dateFormat, params.endDatePublish) : null
        announcement.clearErrors()

        def announcementResult = edit ? announcementService.update(announcement) : announcementService.create(announcement)
        if (announcementResult.hasErrors()) {
            if (announcementResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                announcementResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else {
                result = [ error: message(error: announcementResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "announcement.${edit ? 'update' : 'create'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def announcement = Announcement.read(id)
        if (!announcement) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'announcement.default' +
                    '.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmDelete', model: [announcement: announcement, message: message(code: "announcement" +
                    ".delete.message")])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def announcementResult = announcementService.delete(id)
        if (announcementResult.hasErrors()) {
            result = [ error: message(error: announcementResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'announcement.delete.success') ]
        }

        render result as JSON
    }
}

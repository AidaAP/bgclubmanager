package com.bgclubmanager.poll

import com.bgclubmanager.club.Club
import com.bgclubmanager.util.CommonUtils
import grails.converters.JSON
import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.annotation.Secured
import org.springframework.beans.factory.annotation.Value

class PollController {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    def pollService

    @Secured('isAuthenticated()')
    def index(final String clubId) {

        def club = Club.read(clubId)
        if (!club) {
            render(status: 404)
            return null
        }

        if (SpringSecurityUtils.ifNotGranted(ROLE_ADMIN) && !authenticatedUser.isMemberOf(club)) {
            render(status: 403)
            return null
        }

        params.offset = params.int('offset') ?: 0
        params.max = params.int('max') ?: 5

        def searchResult = pollService.getPolls(club, params.offset, params.max)

        render(view: 'index', model: [authUser: authenticatedUser, club: club, polls: searchResult.list, pollCount:
                searchResult.totalCount])
    }

    @Secured('isAuthenticated()')
    def show(final String id) {

        def poll = Poll.read(id)
        if (!poll) {
            render(status: 404)
            return null
        }

        def isAdmin = SpringSecurityUtils.ifAllGranted(ROLE_ADMIN)
        def isClubMember = authenticatedUser.isMemberOf(poll.club)
        def isClubAdmin = authenticatedUser.isAdminOf(poll.club)
        if (!(isAdmin || isClubAdmin || (isClubMember && poll.isVisible()))) {
            render(status: 403)
            return null
        }

        def showResults = isAdmin || (poll.isVisible() && (poll.isVotedBy(authenticatedUser) || poll.isAlreadyClosed()))
        def pollResults = [:]
        if (showResults) {

            // Labels
            pollResults.labels = poll.options.collect { it.text } as JSON

            // Number of votes
            pollResults.votes = poll.options.collect { Vote.countByPollOption(it) } as JSON
        }

        // Colors
        pollResults.options = poll.options.collectEntries { [(it.id): CommonUtils.getItemColor(it.id)] }

        render(view: 'show', model: [authUser: authenticatedUser, poll: poll, showResults: showResults, pollResults:
                pollResults])
    }

    @Secured('isAuthenticated()')
    def create(final String clubId) {

        def model = [:]

        def club = Club.read(clubId)
        if (!club) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'club.default.label'), clubId])
            render model as JSON
        } else {

            def poll = new Poll(club: club, published: false)
            poll.addToOptions(new PollOption())
            poll.addToOptions(new PollOption())

            render(view: 'createEdit', model: [authUser: authenticatedUser, poll: poll])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def edit(final String id) {

        def model = [:]

        def poll = Poll.read(id)
        if (!poll) {
            model.error = message(code: 'default.not.found.message', args: [message(code: 'poll.default.label'), id])
            render model as JSON
        } else {
            model.authUser = authenticatedUser
            model.poll = poll
            render(view: 'createEdit', model: model)
        }

        return null
    }

    @Secured('isAuthenticated()')
    def save() {

        def result = [:]

        def commandParams = [:] + params
        commandParams.dateFormat = message(code: 'default.datetimenosec.format')
        commandParams.clubId = params.'club.id'
        commandParams.published = params.boolean('published').asBoolean()
        commandParams.options = params.findAll { it.key.startsWith('option_') }.collect {
            [id: it.value[0] ?: null, text: it.value[1]]
        }
        def pollCommand = PollCommand.fromParams(commandParams)

        def edit = pollCommand.id != null

        def pollResult = edit ? pollService.update(pollCommand) : pollService.create(pollCommand)
        if (pollResult.hasErrors()) {
            if (pollResult.errors.hasFieldErrors()) {
                def fieldErrors = []
                pollResult.errors.fieldErrors.each {
                    fieldErrors << [ field: it.field, message: message(error: it) ]
                }
                result = [ fieldErrors: fieldErrors ]
            } else if (pollResult.errors.globalError.code == 'poll.error.options.sameText') {
                result = [ fieldErrors: [[ field: 'options', message: message(error: pollResult.errors.globalError) ]] ]
            } else {
                result = [ error: message(error: pollResult.errors.globalError) ]
            }
        } else {
            result = [ success: message(code: "poll.${edit ? 'update' : 'create'}.success") ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def vote(final String id) {

        def result = [:]

        def pollOption = PollOption.read(id)

        def voteResult = pollService.vote(pollOption)
        if (voteResult.hasErrors()) {
            result = [ error: message(error: voteResult.errors.globalError) ]
        }

        render result as JSON
    }

    @Secured('isAuthenticated()')
    def confirmDelete(final String id) {

        def poll = Poll.read(id)
        if (!poll) {
            def model = [error: message(code: 'default.not.found.message', args: [message(code: 'poll.default.label'), id])]
            render model as JSON
        } else {
            render(view: 'confirmDelete', model: [poll: poll, message: message(code: 'poll.delete.message')])
        }

        return null
    }

    @Secured('isAuthenticated()')
    def delete(final String id) {

        def result = [:]

        def pollResult = pollService.delete(id)
        if (pollResult.hasErrors()) {
            result = [ error: message(error: pollResult.errors.globalError) ]
        } else {
            result = [ success: message(code: 'poll.delete.success') ]
        }

        render result as JSON
    }
}
package bgclubmanager

import com.bgclubmanager.club.Club
import com.bgclubmanager.club.MembershipRequest
import com.bgclubmanager.game.Game
import com.bgclubmanager.user.User
import grails.plugin.springsecurity.SpringSecurityUtils
import org.grails.plugins.codecs.MD5CodecExtensionMethods
import org.springframework.beans.factory.annotation.Value

class UtilsTagLib {

    @Value('${default.role.admin}')
    final String ROLE_ADMIN

    static defaultEncodeAs = [taglib: 'none']

    def springSecurityService

    def mainHeader = { attrs, body ->
        out << render(template: '/_utils/mainHeader')
    }

    def sideMenu = { attrs, body ->

        def menuItems = []

        if (springSecurityService.isLoggedIn()) {

            // Club sideMenu
            if (((controllerName == 'club') && (actionName == 'show')) ||
                    ((controllerName == 'membership') && (actionName in ['getMembershipRequests', 'getMembers'])) ||
                    (controllerName == 'announcement') ||
                    (controllerName == 'poll') ||
                    ((controllerName == 'event') && (!['findNear', 'findNearEvents'].contains(actionName)) ||
                    (controllerName == 'game') ||
                    (controllerName == 'table') ||
                    (controllerName == 'reserve')
            )) {
                def clubId = params.clubId ?: params.id
                menuItems = getClubSideMenu(clubId)
            } else {
                // Logged-in users
                menuItems = getMainSideMenu()
            }
        } else {
            // Not logged-in users
            menuItems = getExternalSideMenu()
        }

        out << render(template: '/_utils/sideMenu', model: [menuItems: menuItems])
    }

    private getMainSideMenu() {

        def menuItems = [
                [
                        label    : message(code: 'sideMenu.dashboard.label'),
                        link     : createLink(controller: 'dashboard', action: 'index'),
                        iconClass: 'fa fa-dashboard',
                        active   : (controllerName == 'dashboard')
                ],
                [
                        label    : message(code: 'sideMenu.users.label'),
                        link     : '#',
                        iconClass: 'fa fa-user',
                        active   : (controllerName == 'user'),
                        children : [
                                [
                                        label : message(code: 'sideMenu.users.profile.label'),
                                        link  : createLink(controller: 'user', action: 'account'),
                                        active: ((controllerName == 'user') && (actionName == 'account'))
                                ],
                                [
                                        label : message(code: 'sideMenu.users.list.label'),
                                        link  : createLink(controller: 'user', action: 'index'),
                                        active: ((controllerName == 'user') && (actionName == 'index'))
                                ]
                        ]
                ],
                [
                        label    : message(code: 'sideMenu.clubs.label'),
                        link     : '#',
                        iconClass: 'fa fa-users',
                        active   : (controllerName == 'club'),
                        children : [
                                [
                                        label : message(code: 'sideMenu.clubs.myClubs.label'),
                                        link  : createLink(controller: 'club', action: 'myIndex'),
                                        active: ((controllerName == 'club') && (actionName == 'myIndex'))
                                ],
                                [
                                        label : message(code: 'sideMenu.clubs.explore.label'),
                                        link  : createLink(controller: 'club', action: 'findNear'),
                                        active: ((controllerName == 'club') && (actionName == 'findNear'))
                                ],
                                [
                                        label : message(code: 'sideMenu.clubs.list.label'),
                                        link  : createLink(controller: 'club', action: 'index'),
                                        active: ((controllerName == 'club') && (actionName == 'index'))
                                ]
                        ]
                ],
                [
                        label    : message(code: 'sideMenu.club.events.label'),
                        link     : createLink(controller: 'event', action: 'findNear'),
                        iconClass: 'fa fa-ticket',
                        active   : ((controllerName == 'event') && (actionName in ['findNear', 'findNearEvents']))
                ]
        ]

        return menuItems
    }

    private getClubSideMenu(final String clubId) {

        def currentUser = springSecurityService.currentUser
        def club = Club.read(clubId)

        def menuItems = []

        // Details
        menuItems << [
                label    : message(code: 'sideMenu.club.details.label'),
                link     : createLink(controller: 'club', action: 'show', id: clubId),
                iconClass: 'fa fa-info-circle',
                active   : (controllerName == 'club') && (actionName == 'show')
        ]

        // Membership requests (only visible for club admins)
        if (currentUser.isAdminOf(club)) {
            def menuItem = [
                    label    : message(code: 'sideMenu.club.requests.label'),
                    link     : createLink(controller: 'membership', action: 'getMembershipRequests', params: [clubId: clubId]),
                    iconClass: 'fa fa-user-plus',
                    active   : (controllerName == 'membership') && (actionName == 'getMembershipRequests')
            ]
            def numRequests = MembershipRequest.countByClub(club)
            if (numRequests > 0) {
                menuItem.bubble = [ class: 'bg-teal', text: numRequests ]
            }
            menuItems << menuItem
        }

        if (SpringSecurityUtils.ifAllGranted(ROLE_ADMIN) || currentUser.isMemberOf(club)) {

            // Members (only visible for admins or club members)
            def menuItemMembers = [
                    label    : message(code: 'sideMenu.club.members.label'),
                    link     : createLink(controller: 'membership', action: 'getMembers', params: [clubId: clubId]),
                    iconClass: 'fa fa-user',
                    active   : (controllerName == 'membership') && (actionName == 'getMembers')
            ]
            menuItems << menuItemMembers

            // Announcements (only visible for admins or club members)
            def menuItemAnnouncements = [
                    label    : message(code: 'sideMenu.club.announcements.label'),
                    link     : createLink(controller: 'announcement', action: 'index', params: [clubId: clubId]),
                    iconClass: 'fa fa-newspaper-o',
                    active   : (controllerName == 'announcement')
            ]
            def numUnreadAnnouncements = currentUser.numUnreadAnnouncements(club)
            if (numUnreadAnnouncements > 0) {
                menuItemAnnouncements.bubble = [ class: 'bg-purple', text: numUnreadAnnouncements ]
            }
            menuItems << menuItemAnnouncements

            // Polls (only visible for admins or club members)
            def menuItemPolls = [
                    label    : message(code: 'sideMenu.club.polls.label'),
                    link     : createLink(controller: 'poll', action: 'index', params: [clubId: clubId]),
                    iconClass: 'fa fa-bar-chart',
                    active   : (controllerName == 'poll')
            ]
            def numUnvotedPolls = currentUser.numUnvotedPolls(club)
            if (numUnvotedPolls > 0) {
                menuItemPolls.bubble = [ class: 'bg-red', text: numUnvotedPolls ]
            }
            menuItems << menuItemPolls

            // Reserves (only visible for admins or club members)
            def menuItemReserves = [
                    label    : message(code: 'sideMenu.club.reserves.label'),
                    link     : createLink(controller: 'reserve', action: 'index', params: [clubId: clubId]),
                    iconClass: 'fa fa-calendar',
                    active   : (controllerName == 'reserve')
            ]
            menuItems << menuItemReserves
        }

        // Events
        def menuItemEvents = [
                label    : message(code: 'sideMenu.club.events.label'),
                link     : createLink(controller: 'event', action: 'index', params: [clubId: clubId]),
                iconClass: 'fa fa-ticket',
                active   : (controllerName == 'event')
        ]
        menuItems << menuItemEvents

        // Games
        def menuItemGames = [
                label    : message(code: 'sideMenu.club.games.label'),
                link     : createLink(controller: 'game', action: 'index', params: [clubId: clubId]),
                iconClass: 'fa fa-puzzle-piece',
                active   : (controllerName == 'game')
        ]
        menuItems << menuItemGames

        // Tables
        def menuItemTables = [
                label    : message(code: 'sideMenu.club.tables.label'),
                link     : createLink(controller: 'table', action: 'index', params: [clubId: clubId]),
                iconClass: 'fa fa-cubes',
                active   : (controllerName == 'table')
        ]
        menuItems << menuItemTables

        return menuItems
    }

    private getExternalSideMenu() {

        def menuItems = [
                [label: message(code: 'sideMenu.about.label'), link: '#about'],
                [label: message(code: 'sideMenu.services.label'), link: '#services']
        ]

        return menuItems
    }

    def footer = { attrs, body ->
        out << render(template: '/_utils/footer')
    }

    def userImage = { attrs, body ->

        def username = attrs.username
        if (!username) {
            log.error "Tag [userImage] is missing required attribute [username]"
            return
        }

        def user = User.findByUsername(username)
        if (!user) {
            log.error "Tag [userImage]: user '${username}' not found"
            return
        }

        out << bgcmImage(urlImage: user.urlImage, key: username, class: attrs.class)
    }

    def clubImage = { attrs, body ->

        def clubId = attrs.clubId
        if (!clubId) {
            log.error "Tag [clubImage] is missing required attribute [clubId]"
            return
        }

        def club = Club.read(clubId)
        if (!club) {
            log.error "Tag [clubImage]: club '${clubId}' not found"
            return
        }

        out << bgcmImage(urlImage: club.urlImage, key: clubId, class: attrs.class)
    }

    def gameImage = { attrs, body ->

        def gameId = attrs.gameId
        if (!gameId) {
            log.error "Tag [gameImage] is missing required attribute [gameId]"
            return
        }

        def game = Game.read(gameId)
        if (!game) {
            log.error "Tag [gameImage]: game '${gameId}' not found"
            return
        }

        out << bgcmImage(urlImage: game.urlImage, key: gameId, class: attrs.class)
    }

    def bgcmImage = { attrs, body ->

        def key = attrs.key
        if (!key) {
            log.error "Tag [bgcmImage] is missing required attribute [key]"
            return
        }

        def source
        def urlImage = attrs.urlImage
        if (urlImage) {
            source = createLink(controller: 'file', action: 'loadFile', params: [path: urlImage])
        } else {
            // Gravatar image
            def emailHash = MD5CodecExtensionMethods.encodeAsMD5("${attrs.key}@bgclubmanager.es".trim().toLowerCase())
            source = "https://www.gravatar.com/avatar/${emailHash}?d=identicon"
        }

        def imgStr = "<img id='${attrs.id ?: UUID.randomUUID().toString()}' src='${source}'"

        if (attrs.class) {
            imgStr += " class='${attrs.class}'"
        }

        imgStr += "/>"

        out << imgStr
    }
}
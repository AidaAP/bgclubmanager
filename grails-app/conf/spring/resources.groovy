beans = {
    // MailUtils
    mailUtils(com.bgclubmanager.mail.MailUtils) {
        mailService = ref('mailService')
        messageSource = ref('messageSource')
    }
}

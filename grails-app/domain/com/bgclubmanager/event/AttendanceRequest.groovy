package com.bgclubmanager.event

import com.bgclubmanager.user.User

class AttendanceRequest {

    String id
    Event event
    User user
    Date date

    static constraints = {
        event nullable: false, unique: ['user']
        user nullable: false, unique: ['event']
        date nullable: false, unique: false
    }

    static mapping = {
        table 'attendance_request'
        version false
        id generator: 'uuid'
        event column: 'event_id'
        user column: 'user_id'
        date column: 'date'
    }

    static namedQueries = {
        pendingForClubs { clubs ->
            event {
                inList('club', clubs)
                eq('selfEnrollment', false)
            }
        }
    }
}

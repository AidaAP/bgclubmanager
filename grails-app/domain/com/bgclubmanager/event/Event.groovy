package com.bgclubmanager.event

import com.bgclubmanager.club.Club

class Event {

    String id
    Club club
    String name
    String description
    Date startDate
    Date endDate
    Float latitude
    Float longitude
    boolean isPublic
    Integer maxAttendees
    Date startDateEnrollment
    Date endDateEnrollment
    boolean selfEnrollment

    static constraints = {
        club nullable: false, unique: false
        name nullable: false, blank: false, unique: false
        description nullable: true, unique: false
        startDate nullable: false, unique: false
        endDate nullable: false, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDate?.after(val)) {
                errors.rejectValue('endDate', 'event.error.datesFromTo')
            }
        }
        latitude nullable: true, precission: 5, unique: false
        longitude nullable: true, precission: 5, unique: false
        isPublic nullable: false, unique: false
        maxAttendees nullable: true, unique: false, min: 1
        startDateEnrollment nullable: false, unique: false
        endDateEnrollment nullable: false, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDateEnrollment?.after(val)) {
                errors.rejectValue('endDateEnrollment', 'event.error.enrollmentDatesFromTo')
            }
        }
        selfEnrollment nullable: false, unique: false
    }

    static mapping = {
        table 'event'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        name column: 'name'
        description column: 'description'
        startDate column: 'start_date'
        endDate column: 'end_date'
        latitude column: 'latitude'
        longitude column: 'longitude'
        isPublic column: 'is_public'
        maxAttendees column: 'max_attendees'
        startDateEnrollment column: 'start_date_enrollment'
        endDateEnrollment column: 'end_date_enrollment'
        selfEnrollment column: 'self_enrollment'
    }

    def isEnrollable() {
        def now = new Date()
        return (this.startDateEnrollment <= now) && (this.endDateEnrollment > now)
    }

    def getOrganizers() {
        return Organization.findAllByEvent(this).collect { it.club }
    }

    def getAttendees() {
        return Attendance.findAllByEvent(this).collect { it.user }
    }

    def isMaxAttendeesReached() {

        def result = false

        if (this.maxAttendees) {
            result = !(Attendance.countByEvent(this) < this.maxAttendees)
        }

        return result
    }
}

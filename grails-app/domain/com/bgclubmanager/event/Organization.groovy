package com.bgclubmanager.event

import com.bgclubmanager.club.Club

class Organization {

    String id
    Event event
    Club club

    static constraints = {
        event nullable: false, unique: ['club']
        club nullable: false, unique: ['event']
    }

    static mapping = {
        table 'organization'
        version false
        id generator: 'uuid'
        event column: 'event_id'
        club column: 'club_id'
    }
}

package com.bgclubmanager.event

import com.bgclubmanager.club.Club

class OrganizationRequest {

    String id
    Event event
    Club club
    Date date

    static constraints = {
        event nullable: false, unique: ['club']
        club nullable: false, unique: ['event']
        date nullable: false, unique: false
    }

    static mapping = {
        table 'organization_request'
        version false
        id generator: 'uuid'
        event column: 'event_id'
        club column: 'club_id'
        date column: 'date'
    }
}

package com.bgclubmanager.event

import com.bgclubmanager.user.User

class Attendance {

    String id
    Event event
    User user

    static constraints = {
        event nullable: false, unique: ['user']
        user nullable: false, unique: ['event']
    }

    static mapping = {
        table 'attendance'
        version false
        id generator: 'uuid'
        event column: 'event_id'
        user column: 'user_id'
    }
}
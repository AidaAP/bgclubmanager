package com.bgclubmanager.table

import com.bgclubmanager.club.Club
import com.bgclubmanager.reserve.ReservedTable

class Table {

    String id
    Club club
    String name
    String description
    Integer maxCapacity
    Float latitude
    Float longitude

    static constraints = {
        club nullable: false, unique: false
        name nullable: false, blank: false, unique: false
        description nullable: true, unique: false
        maxCapacity nullable: true, unique: false, min: 1
        latitude nullable: true, precission: 5, unique: false
        longitude nullable: true, precission: 5, unique: false
    }

    static mapping = {
        table 'table_room'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        name column: 'name'
        description column: 'description'
        maxCapacity column: 'max_capacity'
        latitude column: 'latitude'
        longitude column: 'longitude'
    }

    def isAvailable(final Date startDate, final Date endDate, final String excludedReserveId) {

        def list = ReservedTable.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('table', this)
            reserve {
                lt('startDate', endDate)
                gt('endDate', startDate)
                if (excludedReserveId) {
                    ne('id', excludedReserveId)
                }
            }
            readOnly(true)
        }

        return list.size() == 0
    }
}

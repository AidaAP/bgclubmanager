package com.bgclubmanager.club

import com.bgclubmanager.user.User

class Member {

    enum Type {
        ADMIN, BASIC
    }

    String id
    Club club
    User user
    Type type
    Date startDate

    static constraints = {
        club nullable: false, unique: ['user']
        user nullable: false, unique: ['club']
        type nullable: false, unique: false
        startDate nullable: false, unique: false
    }

    static mapping = {
        table 'member'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        user column: 'user_id'
        type column: 'type'
        startDate column: 'start_date'
    }

    static namedQueries = {
        members { club ->
            projections { property('user') }
            eq('club', club)
        }
        admins { club ->
            projections { property('user') }
            eq('club', club)
            eq('type', Type.ADMIN)
        }
        memberships { user ->
            projections { property('club') }
            eq('user', user)
        }
        adminClubs { user ->
            projections { property('club') }
            eq('user', user)
            eq('type', Type.ADMIN)
        }
    }
}
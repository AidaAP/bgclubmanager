package com.bgclubmanager.club

import com.bgclubmanager.user.User

class MembershipRequest {

    String id
    Club club
    User user
    Date date

    static constraints = {
        club nullable: false, unique: ['user']
        user nullable: false, unique: ['club']
        date nullable: false, unique: false
    }

    static mapping = {
        table 'membership_request'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        user column: 'user_id'
        date column: 'date'
    }
}

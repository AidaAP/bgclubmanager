package com.bgclubmanager.club

import com.bgclubmanager.user.User

class Club {

    String id
    User owner
    String name
    String description
    String email
    String phone
    Float latitude
    Float longitude
    String urlImage

    static constraints = {
        owner nullable: false, unique: false
        name nullable: false, blank: false, unique: false
        description nullable: true, blank: true, unique: false
        email nullable: true, blank: true, unique: false, email: true
        phone nullable: true, blank: true, unique: false
        latitude nullable: true, precission: 5, unique: false
        longitude nullable: true, precission: 5, unique: false
        urlImage nullable: true, blank: true, unique: false
    }

    static mapping = {
        table 'club'
        version false
        id generator: 'uuid'
        owner column: 'owner_id'
        name column: 'name'
        description column: 'description'
        email column: 'email'
        phone column: 'phone'
        latitude column: 'latitude'
        longitude column: 'longitude'
        urlImage column: 'url_image'
    }
}
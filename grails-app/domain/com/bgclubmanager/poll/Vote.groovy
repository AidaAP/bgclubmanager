package com.bgclubmanager.poll

import com.bgclubmanager.user.User

class Vote {

    String id
    User user
    PollOption pollOption
    Date date

    static constraints = {
        user nullable: false, unique: false
        pollOption nullable: false, unique: false
        date nullable: false, unique: false
    }

    static mapping = {
        table 'vote'
        version false
        id generator: 'uuid'
        user column: 'user_id'
        pollOption column: 'poll_option_id'
        date column: 'date'
    }

    static namedQueries = {
        fromPoll { poll ->
            pollOption {
                eq('poll', poll)
            }
        }
    }
}

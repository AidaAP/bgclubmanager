package com.bgclubmanager.poll

import com.bgclubmanager.club.Club
import com.bgclubmanager.user.User

class Poll {

    String id
    Club club
    String title
    String description
    Date startDateVoting
    Date endDateVoting
    boolean published
    Date startDatePublish
    Date endDatePublish

    static hasMany = [options: PollOption]

    static constraints = {
        club nullable: false, unique: false
        title nullable: false, blank: false, unique: false
        description nullable: true, unique: false
        startDateVoting nullable: true, unique: false, validator: { val, obj, errors ->
            if (obj.published && !val) {
                errors.rejectValue('startDateVoting', 'default.null.message')
            }
        }
        endDateVoting nullable: true, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDateVoting?.after(val)) {
                errors.rejectValue('endDateVoting', 'poll.error.votingDatesFromTo')
            }
        }
        published nullable: false, unique: false
        startDatePublish nullable: true, unique: false, validator: { val, obj, errors ->
            if (obj.published && !val) {
                errors.rejectValue('startDatePublish', 'default.null.message')
            }
        }
        endDatePublish nullable: true, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDatePublish?.after(val)) {
                errors.rejectValue('endDatePublish', 'poll.error.publishDatesFromTo')
            }
        }
        options nullable: false, minSize: 2
    }

    static mapping = {
        table 'poll'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        title column: 'title'
        description column: 'description'
        startDateVoting column: 'start_date_voting'
        endDateVoting column: 'end_date_voting'
        published column: 'published'
        startDatePublish column: 'start_date_publish'
        endDatePublish column: 'end_date_publish'
        options cascade: 'all-delete-orphan', sort: 'id', order: 'asc'
    }

    static namedQueries = {
        visible {
            def now = new Date()
            eq('published', true)
            le('startDatePublish', now)
            or {
                isNull('endDatePublish')
                gt('endDatePublish', now)
            }
        }
        votable {
            def now = new Date()
            le('startDateVoting', now)
            or {
                isNull('endDateVoting')
                gt('endDateVoting', now)
            }
        }
        visibleAndVotable {
            visible()
            votable()
        }
    }

    def isVisible() {
        def now = new Date()
        return this.published && (this.startDatePublish <= now) && (!this.endDatePublish || (this.endDatePublish > now))
    }

    def isVotable() {
        def now = new Date()
        return this.published && (this.startDateVoting <= now) && (!this.endDateVoting || (this.endDateVoting > now))
    }

    def isAlreadyClosed() {
        def now = new Date()
        return this.published && this.endDateVoting && (this.endDateVoting < now)
    }

    def isVotedBy(final User user) {
        return this.options.any { it.isVotedBy(user) }
    }

    def hasVotes() {

        def list = Vote.createCriteria().list(max: 1) {
            projections { property('id') }
            pollOption {
                eq('poll', this)
            }
            readOnly(true)
        }

        return list.size() != 0
    }
}

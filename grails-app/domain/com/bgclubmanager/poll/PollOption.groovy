package com.bgclubmanager.poll

import com.bgclubmanager.user.User

class PollOption {

    String id
    String text

    static belongsTo = [poll: Poll]

    static constraints = {
        poll nullable: false, unique: false
        text nullable: false, blank: false, validator: { val, obj, errors ->

            def objPoll = obj?.poll
            def pollErrors = objPoll?.errors

            if ((objPoll?.options?.count { it.text == val } > 1) &&
                    (!pollErrors?.allErrors?.any { it.code == 'poll.error.options.sameText' })) {
                pollErrors.reject('poll.error.options.sameText')
            }
        }
    }

    static mapping = {
        table 'poll_option'
        version false
        id generator: 'uuid'
        poll column: 'poll_id'
        text column: 'text'
    }

    def isVotedBy(final User user) {

        def list = Vote.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('user', user)
            eq('pollOption', this)
            readOnly(true)
        }

        return list.size() != 0
    }
}

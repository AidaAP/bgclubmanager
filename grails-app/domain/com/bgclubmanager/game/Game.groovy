package com.bgclubmanager.game

import com.bgclubmanager.club.Club
import com.bgclubmanager.reserve.ReservedGame

class Game {

    String id
    Club club
    String name
    String description
    String urlImage
    Integer copies
    String urlBGG

    static constraints = {
        club nullable: false, unique: false
        name nullable: false, blank: false, unique: false
        description nullable: true, unique: false
        urlImage nullable: true, unique: false
        copies nullable: false, unique: false, min: 1
        urlBGG nullable: true, unique: false, matches: /(?i)^https?:\/\/(www\.)?boardgamegeek\.com\/(boardgame|boardgameexpansion|boardgameaccessory|videogame|videogameexpansion|rpgitem|rpgissue)\/\d+(\/[\w \.-]+)?\/?$/
    }

    static mapping = {
        table 'game'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        name column: 'name'
        description column: 'description'
        urlImage column: 'url_image'
        copies column: 'copies'
        urlBGG column: 'url_bgg'
    }

    def isAvailable(final Date startDate, final Date endDate, final String excludedReserveId) {

        def list = ReservedGame.createCriteria().list() {
            projections { property('id') }
            eq('game', this)
            reserve {
                lt('startDate', endDate)
                gt('endDate', startDate)
                if (excludedReserveId) {
                    ne('id', excludedReserveId)
                }
            }
            readOnly(true)
        }

        def reservedCopies = list.size()

        return reservedCopies < this.copies
    }
}

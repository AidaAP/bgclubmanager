package com.bgclubmanager.user

import com.bgclubmanager.announcement.Announcement
import com.bgclubmanager.announcement.Reading
import com.bgclubmanager.club.Club
import com.bgclubmanager.club.Member
import com.bgclubmanager.club.MembershipRequest
import com.bgclubmanager.core.Language
import com.bgclubmanager.event.Attendance
import com.bgclubmanager.event.Event
import com.bgclubmanager.poll.Poll
import com.bgclubmanager.poll.Vote
import com.bgclubmanager.reserve.Reserve
import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

@EqualsAndHashCode(includes='username')
@ToString(includes='username', includeNames=true, includePackage=false)
class User implements Serializable {

    private static final long serialVersionUID = 1

    transient springSecurityService

    String id
    String username
    String password
    String name
    String surname
    String email
    String urlImage
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    Language language

    Set<Role> getAuthorities() {
        UserRole.findAllByUser(this)*.role
    }

    def beforeInsert() {
        encodePassword()
    }

    def beforeUpdate() {
        if (isDirty('password')) {
            encodePassword()
        }
    }

    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
    }

    static transients = ['springSecurityService']

    static constraints = {
        username nullable: false, blank: false, unique: true
        password nullable: false, blank: false, unique: false, password: true
        name nullable: true, blank: true, unique: false
        surname nullable: true, blank: true, unique: false
        email nullable: false, blank: false, unique: false, email: true
        urlImage nullable: true, blank: true, unique: false
        language nullable: false, unique: false
    }

    static mapping = {
        table 'user'
        version false
        id generator: 'uuid'
        username column: 'username'
        password column: 'password'
        name column: 'name'
        surname column: 'surname'
        email column: 'email'
        urlImage column: 'url_image'
        language column: 'language_id'
    }

    def getLocale() {
        return new Locale(this.language.code)
    }

    def getToken() {
        return UserToken.findByUser(this)?.id
    }

    def hasAuthority(final String authority) {

        def result = false

        def role = Role.findByAuthority(authority)
        if (role) {
            result = UserRole.exists(this.id, role.id)
        }

        return result
    }

    def isMemberOf(final Club club) {

        def list = Member.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('club', club)
            eq('user', this)
            readOnly(true)
        }

        return list.size() != 0
    }

    def isAdminOf(final Club club) {

        def list = Member.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('club', club)
            eq('user', this)
            eq('type', Member.Type.ADMIN)
            readOnly(true)
        }

        return list.size() != 0
    }

    def hasMembershipRequestFor(final Club club) {

        def list = MembershipRequest.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('club', club)
            eq('user', this)
            readOnly(true)
        }

        return list.size() != 0
    }

    def numUnreadAnnouncements(final Club club) {

        def result = 0

        def clubFilter = club ? [club] : Member.memberships(this).list()

        if (clubFilter) {

            def readAnnouncementIds = Reading.createCriteria().list() {
                createAlias('announcement', 'a')
                projections { property('a.id') }
                inList('a.club', clubFilter)
                eq('user', this)
                readOnly(true)
            }

            result = Announcement.visible.count {
                inList('club', clubFilter)
                if (readAnnouncementIds) {
                    not { inList('id', readAnnouncementIds) }
                }
            }
        }

        return result
    }

    def numUnvotedPolls(final Club club) {

        def result = 0

        def clubFilter = club ? [club] : Member.memberships(this).list()

        if (clubFilter) {

            def votedPollIds = Vote.createCriteria().list() {
                createAlias('pollOption', 'po')
                createAlias('po.poll', 'p')
                projections { property('p.id') }
                inList('p.club', clubFilter)
                eq('user', this)
                readOnly(true)
            }

            result = Poll.visibleAndVotable.count {
                inList('club', clubFilter)
                if (votedPollIds) {
                    not { inList('id', votedPollIds) }
                }
            }
        }

        return result
    }

    def isAttendeeOf(final Event event) {

        def list = Attendance.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('event', event)
            eq('user', this)
            readOnly(true)
        }

        return list.size() != 0
    }

    def numUnfinishedEvents() {
        return Attendance.createCriteria().get {
            projections { rowCount() }
            eq('user', this)
            event {
                gt('endDate', new Date())
            }
            readOnly(true)
        }
    }

    def numUnfinishedReserves() {
        return Reserve.createCriteria().get {
            projections { rowCount() }
            eq('user', this)
            gt('endDate', new Date())
            readOnly(true)
        }
    }
}
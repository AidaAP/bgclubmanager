package com.bgclubmanager.user

class UserToken {

    String id
    User user

    static constraints = {
        user nullable: false, blank: false, unique: true
    }

    static mapping = {
        table 'user_token'
        version false
        id generator: 'uuid'
        user column: 'user_id'
    }
}
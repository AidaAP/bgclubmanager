package com.bgclubmanager.core

class Language {

    String id
    String code

    static constraints = {
        code nullable: false, blank: false, unique: true
    }

    static mapping = {
        table 'language'
        version false
        id generator: 'uuid'
        code column: 'code'
    }
}

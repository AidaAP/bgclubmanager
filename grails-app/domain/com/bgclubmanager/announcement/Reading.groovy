package com.bgclubmanager.announcement

import com.bgclubmanager.user.User

class Reading {

    String id
    User user
    Announcement announcement
    Date date

    static constraints = {
        user nullable: false, unique: ['announcement']
        announcement nullable: false, unique: false
        date nullable: false, unique: false
    }

    static mapping = {
        table 'reading'
        version false
        id generator: 'uuid'
        user column: 'user_id'
        announcement column: 'announcement_id'
        date column: 'date'
    }
}
package com.bgclubmanager.announcement

import com.bgclubmanager.club.Club
import com.bgclubmanager.user.User

class Announcement {

    String id
    Club club
    String title
    String content
    boolean published
    Date startDatePublish
    Date endDatePublish

    static constraints = {
        club nullable: false, unique: false
        title nullable: false, blank: false, unique: false
        content nullable: false, blank: false, unique: false
        published nullable: false, unique: false
        startDatePublish nullable: true, unique: false, validator: { val, obj, errors ->
            if (obj.published && !val) {
                errors.rejectValue('startDatePublish', 'default.null.message')
            }
        }
        endDatePublish nullable: true, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDatePublish?.after(val)) {
                errors.rejectValue('endDatePublish', 'announcement.error.datesFromTo')
            }
        }
    }

    static mapping = {
        table 'announcement'
        version false
        id generator: 'uuid'
        club column: 'club_id'
        title column: 'title'
        content column: 'content'
        published column: 'published'
        startDatePublish column: 'start_date_publish'
        endDatePublish column: 'end_date_publish'
    }

    static namedQueries = {
        visible {
            def now = new Date()
            eq('published', true)
            le('startDatePublish', now)
            or {
                isNull('endDatePublish')
                gt('endDatePublish', now)
            }
        }
    }

    def readBy(final User user) {

        def list = Reading.createCriteria().list(max: 1) {
            projections { property('id') }
            eq('user', user)
            eq('announcement', this)
            readOnly(true)
        }

        return list.size() != 0
    }
}
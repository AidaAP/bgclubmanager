package com.bgclubmanager.reserve

import com.bgclubmanager.game.Game

class ReservedGame {

    String id
    Game game

    static belongsTo = [reserve: Reserve]

    static constraints = {
        reserve nullable: false, unique: false
        game nullable: false, unique: false
    }

    static mapping = {
        table 'reserved_game'
        version false
        id generator: 'uuid'
        reserve column: 'reserve_id'
        game column: 'game_id'
    }
}

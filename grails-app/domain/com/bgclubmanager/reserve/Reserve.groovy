package com.bgclubmanager.reserve

import com.bgclubmanager.club.Club
import com.bgclubmanager.user.User

class Reserve {

    String id
    User user
    Club club
    String title
    String description
    Date startDate
    Date endDate

    static hasMany = [reservedGames: ReservedGame, reservedTables: ReservedTable]

    static constraints = {
        user nullable: false, unique: false
        club nullable: false, unique: false
        title nullable: false, blank: false, unique: false
        description nullable: true, unique: false
        startDate nullable: false, unique: false, validator: { val, obj, errors ->
            def now = new Date()
            if (val && now.after(val)) {
                errors.rejectValue('startDate', 'reserve.error.pastStartDate')
            }
        }
        endDate nullable: false, unique: false, validator: { val, obj, errors ->
            if (val && obj.startDate?.after(val)) {
                errors.rejectValue('endDate', 'reserve.error.datesFromTo')
            }
        }
        reservedGames nullable: false, validator: { val, obj, errors ->
            if (val?.any { it.game.club.id != obj.club.id }) {
                errors.rejectValue('reservedGames', 'reserve.error.games.wrongClub')
            }
        }
        reservedTables nullable: false, validator: { val, obj, errors ->
            if (val?.any { it.table.club.id != obj.club.id }) {
                errors.rejectValue('reservedTables', 'reserve.error.tables.wrongClub')
            }
        }
    }

    static mapping = {
        table 'reserve'
        version false
        id generator: 'uuid'
        user column: 'user_id'
        club column: 'club_id'
        title column: 'title'
        description column: 'description'
        startDate column: 'start_date'
        endDate column: 'end_date'
        reservedGames cascade: 'all-delete-orphan', sort: 'id', order: 'asc'
        reservedTables cascade: 'all-delete-orphan', sort: 'id', order: 'asc'
    }

    static namedQueries = {
        withGame { game ->
            reservedGames {
                eq('game', game)
            }
        }
        withTable { table ->
            reservedTables {
                eq('table', table)
            }
        }
    }

    def isStarted() {
        return this.startDate <= new Date()
    }

    def isFinished() {
        return this.endDate <= new Date()
    }
}

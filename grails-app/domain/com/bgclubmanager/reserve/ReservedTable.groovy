package com.bgclubmanager.reserve

import com.bgclubmanager.table.Table

class ReservedTable {

    String id
    Table table

    static belongsTo = [reserve: Reserve]

    static constraints = {
        reserve nullable: false, unique: false
        table nullable: false, unique: false
    }

    static mapping = {
        table 'reserved_table'
        version false
        id generator: 'uuid'
        reserve column: 'reserve_id'
        table column: 'table_id'
    }
}
